//
//  Map+empty.swift
//  panda
//
//  Created by Edouard Roussillon on 6/19/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

extension Map {
    class func emptyMap() -> Map {
        let emptyDictionary = [String: Any]()
        
        return Map(mappingType: .fromJSON, JSON: emptyDictionary)
    }
}
