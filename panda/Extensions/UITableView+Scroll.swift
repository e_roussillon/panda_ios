//
//  UITableView+Scroll.swift
//  panda
//
//  Created by Edouard Roussillon on 5/8/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit

extension UITableView {
    
    func scrollToBottom(_ list: [AnyObject]?, withAnim: Bool) {
        guard let l = list, l.count > 0 else { return }
        let indexPath = IndexPath(row: l.count - 1, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: withAnim)
//        self.layoutIfNeeded()
    }
    
    func scrollToBottom(_ withAnim: Bool, atIndex: IndexPath) {
        self.scrollToRow(at: atIndex, at: .top, animated: withAnim)
    }
    
    func scrollToBottom(_ withAnim: Bool = true) {
        let numberOfSections = self.numberOfSections - 1
        let numberOfRows = self.numberOfRows(inSection: numberOfSections) - 1
        if numberOfSections >= 0 && numberOfRows >= 0 {
            let indexPath = IndexPath(row: numberOfRows, section: numberOfSections)
            self.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: withAnim)
        }
    }
}
