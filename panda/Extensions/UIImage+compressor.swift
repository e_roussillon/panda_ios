//
//  UIImage+compressor.swift
//  panda
//
//  Created by Edouard Roussillon on 10/1/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension UIImage {
    
    func compressImage() -> Data {
        // Reducing file size to a 10th
        var actualHeight : CGFloat = self.size.height
        var actualWidth : CGFloat = self.size.width
        let maxHeight : CGFloat = 1136.0
        let maxWidth : CGFloat = 640.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else{
                actualHeight = maxHeight;
                actualWidth = maxWidth;
                compressionQuality = 1;
            }
        }
        
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        self.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext();
        let imageData = UIImageJPEGRepresentation(img!, compressionQuality);
        UIGraphicsEndImageContext();

        return imageData!
    }
    
    public convenience init?(localName fileName: String) {
        let pathFile = UIImage.getPath(fileName)!.path
        self.init(contentsOfFile: pathFile)
    }
    
    static func getPath(_ localName: String) -> URL? {
        let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/Images/")
        return path.appendingPathComponent(localName)
    }
    
    func saveImage(_ fileName: String) {
        let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/Images/")
        let pathFile = path.appendingPathComponent(fileName).path
        
        do {
            try FileManager.default.createDirectory(atPath: pathFile, withIntermediateDirectories: true, attributes: nil)
            try FileManager.default.removeItem(atPath: pathFile)
            
            try self.compressImage().write(to: URL(fileURLWithPath: pathFile), options: NSData.WritingOptions.atomic)
        } catch {
            print("Error when creation file!!")
        }
    }
    
}
