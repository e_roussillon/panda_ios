//
//  NSUserDefaultsUtil.swift
//  carrefour
//
//  Created by Edouard Roussillon on 1/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension UserDefaults {
    static func saveString(_ key: String, value: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    static func stringWithKey(_ key: String) -> String {
        let defaults = UserDefaults.standard
        if let string = defaults.object(forKey: key) as? String {
            return string
        } else {
            return ""
        }
    }
    
    static func saveBool(_ key: String, value: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    static func boolWithKey(_ key: String) -> Bool {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: key)
    }
    
}
