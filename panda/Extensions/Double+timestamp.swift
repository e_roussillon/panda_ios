//
//  Double+timestamp.swift
//  panda
//
//  Created by Edouard Roussillon on 8/8/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension Double {
    
    func toDate() -> Date? {
        return UnixFormatterTransform.init().transformFromJSON(self)
    }
    
}


