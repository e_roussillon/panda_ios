//
//  UIFontUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit


extension UIFont {
    
    static func initHelvetica(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: font)!
    }
    
    static func initHelveticaBold(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: font)!
    }
    
    static func initHelveticaBoldItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-BoldItalic", size: font)!
    }
    
    static func initHelveticaCondensedBlack(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-CondensedBlack", size: font)!
    }
    
    static func initHelveticaCondensedBold(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-CondensedBold", size: font)!
    }
    
    static func initHelveticaItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Italic", size: font)!
    }
    
    static func initHelveticaLight(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: font)!
    }
    
    static func initHelveticaLightItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-LightItalic", size: font)!
    }
    
    static func initHelveticaMedium(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Medium", size: font)!
    }
    
    static func initHelveticaMediumItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-MediumItalic", size: font)!
    }
    
    static func initHelveticaUltraLight(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-UltraLight", size: font)!
    }
    
    static func initHelveticaUltraLightItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-UltraLightItalic", size: font)!
    }
    
    static func initHelveticaThin(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Thin", size: font)!
    }
    
    static func initHelveticaThinItalic(_ font:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-ThinItalic", size: font)!
    }    
}
