//
//  Int+time.swift
//  panda
//
//  Created by Edouard Roussillon on 7/16/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension Int
{
    func secondsToString() -> String {
        let (_, m, s) = secondsToHoursMinutesSeconds(self)
        
        if m != "0" {
            return "\(m):\(s) \(NSLocalizedString("chat_connection_retry_timer_unit", comment: ""))"
        } else {
            return "\(s) \(NSLocalizedString("chat_connection_retry_timer_unit", comment: ""))"
        }
    }
    
    fileprivate func secondsToHoursMinutesSeconds(_ seconds : Int) -> (String, String, String) {
        return ("\(seconds / 3600)", "\((seconds % 3600) / 60)", self.secondeToString((seconds % 3600) % 60))
    }
    
    fileprivate func secondeToString(_ val: Int) -> String {
        if val >= 10 {
            return "\(val)"
        } else {
            return "0\(val)"
        }
    }
}
