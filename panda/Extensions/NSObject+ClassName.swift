//
//  NSObjectUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension NSObject {
    
    class var className: String! {
        let classString: String = NSStringFromClass(self.classForCoder())
        return classString.components(separatedBy: ".").last
    }

}
