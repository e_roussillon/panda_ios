//
//  Array+Remove.swift
//  panda
//
//  Created by Edouard Roussillon on 4/29/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    mutating func removeObject(_ object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    mutating func removeObjectsInArray(_ array: [Element]) {
        for object in array {
            self.removeObject(object)
        }
    }
}
