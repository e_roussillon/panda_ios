//
//  NSDateFormatter.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

open class UnixFormatterTransform: TransformType {

    fileprivate static let dateFormatter: DateFormatter = DateFormatter()
    public typealias Object = Date
    public typealias JSON = Double
    
    public init() {
    }
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let unixTime = value as? Double {
            return Date(timeIntervalSince1970: TimeInterval(unixTime))
        }
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> Double? {
        if let date = value {
            return Double(date.timeIntervalSince1970)
        }
        return nil
    }
}

extension DateFormatter {
    
    static func initFullDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    static func initISO8601Date() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    static func initISO8601ZDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(identifier: "UTC")
        return DateFormatterTransform(dateFormatter: formatter)
        
        //        let formatter = NSDateFormatter()
        ////        formatter.timeZone = NSTimeZone(abbreviation: "GMT")
        ////        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        //        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    static func initISO8601ZSmallDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = TimeZone(identifier: "UTC")
        return DateFormatterTransform(dateFormatter: formatter)
        
        //        let formatter = NSDateFormatter()
        ////        formatter.timeZone = NSTimeZone(abbreviation: "GMT")
        ////        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        //        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    static func initSmallDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        //        formatter.timeZone = NSTimeZone(name: "UTC")
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    static func initMeduimDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM"
        //        formatter.timeZone = NSTimeZone(name: "UTC")
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    
    static func initDate() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "MM/dd/yyyy"
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
    
    static func initTime() -> DateFormatterTransform {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "HH:mm"
        return DateFormatterTransform(dateFormatter: formatter)
    }
    
}
