//
//  UITableViewCellUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

protocol BaseTableViewCellDelegate {
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat
}

extension UITableViewCell {
    
    //
    //MARK: Need to be implemented
    //
    
    func configureCell(_ objects: [Any]?) {
        
    }
    
    //
    //MARK: GENERIC
    //
    
    static func cellIdentifier() -> String {
        return self.className
    }

    static func registerNibIntoTableView(_ tableView: UITableView) {
        let name  = self.cellIdentifier()
        let cellNib = UINib(nibName: name, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: name)
    }
    
    static func initCell(_ tableView: UITableView, indexPath: IndexPath, objects: [Any]?) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(), for: indexPath)
        cell.configureCell(objects)
        return cell
    }
    
    static func initCell(_ tableView: UITableView, objects: [Any]?) -> UITableViewCell? {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier())
        cell?.configureCell(objects)
        return cell
    }
}
