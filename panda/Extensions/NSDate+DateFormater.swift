//
//  NSDate+Extension.swift
//  Tasty
//
//  Created by Vitaliy Kuzmenko on 17/10/14.
//  http://github.com/vitkuzmenko
//  Copyright (c) 2014 Vitaliy Kuz'menko. All rights reserved.
//

import Foundation

func NSDateTimeAgoLocalizedStrings(_ key: String) -> String {
    let resourcePath: String?

    if let frameworkBundle = Bundle(identifier: "com.kevinlawler.NSDateTimeAgo") {
        // Load from Framework
        resourcePath = frameworkBundle.resourcePath
    } else {
        // Load from Main Bundle
        resourcePath = Bundle.main.resourcePath
    }

    if resourcePath == nil {
        return ""
    }

    let path = URL(fileURLWithPath: resourcePath!).appendingPathComponent("NSDateTimeAgo.bundle")
    guard let bundle = Bundle(url: path) else {
        return ""
    }

    return NSLocalizedString(key, tableName: "NSDateTimeAgo", bundle: bundle, comment: "")
}

extension Date
{
    
    func getShortStyleDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: self)
    }
    
    func getMediumStyleDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter.string(from: self)
    }
    
    func getLongStyleDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        return dateFormatter.string(from: self)
    }
    
    func getFullStyleDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .full
        return dateFormatter.string(from: self)
    }
    
    func getHourOfDay()->Int {
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(NSCalendar.Unit.hour, from: self)
        return myComponents.hour!
    }
    
    func getDayOfWeek()->Int {
        let myCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let myComponents = (myCalendar as NSCalendar).components(NSCalendar.Unit.weekday, from: self)
        return myComponents.weekday!
    }
    
    func getDateToString() -> String {
        return DateFormatter.initSmallDate().transformToJSON(self) ?? ""
    }
    
    func getDateToFullString() -> String {
        return DateFormatter.initFullDate().transformToJSON(self) ?? ""
    }
    
    func getDateToISO8601ZString() -> String {
        return DateFormatter.initISO8601ZDate().transformToJSON(self) ?? ""
    }
    
    var timestamp: TimeInterval {
        return Date().timeIntervalSince1970
    }
    
    func getShortTimeString() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm" //format style. Browse online to get a format that fits your needs.
        return dateFormatter.string(from: self)
    }
    
    func getDateMessage() -> String {
        let dateFormatter = DateFormatter()
        
        if self.isSameDays(Date()) {
            dateFormatter.dateFormat = "HH:mm"
        } else {
            dateFormatter.dateFormat = "dd/MMM HH:mm"
        }
        
        return dateFormatter.string(from: self)
    }
    
    func getDateMessageSection() -> String {
        let dateFormatter = DateFormatter()
        
        if self.isSameDays(Date()) {
            dateFormatter.dateFormat = "HH:mm"
        } else {
            dateFormatter.dateFormat = "dd/MMM"
        }
        
        return dateFormatter.string(from: self)
    }
    
    func getDateUserConnection() -> String {
        let dateFormatter = DateFormatter()
        
        if self.isSameDays(Date()) {
            dateFormatter.dateFormat = NSLocalizedString("friend_time_last_seen_today", comment: "")
        } else {
            dateFormatter.dateFormat = NSLocalizedString("friend_time_last_seen", comment: "")
        }
        
        return dateFormatter.string(from: self)
    }
    
    func isSameDays(_ date:Date) -> Bool {
        let calendar = Calendar.current
        let comps1 = (calendar as NSCalendar).components([NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.day], from:self)
        let comps2 = (calendar as NSCalendar).components([NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.day], from:date)
        
        return (comps1.day == comps2.day) && (comps1.month == comps2.month) && (comps1.year == comps2.year)
    }
    
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    
    func addDays(_ daysToAdd : Int) -> Date
    {
        let secondsInDays : TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded : Date = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    
    func addHours(_ hoursToAdd : Int) -> Date
    {
        let secondsInHours : TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded : Date = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func getAge() -> Int {
        let calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let now: Date = Date()
        let calcAge = calendar.dateComponents([.year], from: self, to: now)
        return calcAge.year!
    }
}

extension Date {
    
    // shows 1 or two letter abbreviation for units.
    // does not include 'ago' text ... just {value}{unit-abbreviation}
    // does not include interim summary options such as 'Just now'
    public var getTimeAgoSimple: String {
        let components = self.dateComponents()
        
        if components.year! > 0 {
            return stringFromFormat("%%d%@yr", withValue: components.year!)
        }
        
        if components.month! > 0 {
            return stringFromFormat("%%d%@mo", withValue: components.month!)
        }
        
        // TODO: localize for other calanders
        if components.day! >= 7 {
            let value = components.day!/7
            return stringFromFormat("%%d%@w", withValue: value)
        }
        
        if components.day! > 0 {
            return stringFromFormat("%%d%@d", withValue: components.day!)
        }
        
        if components.hour! > 0 {
            return stringFromFormat("%%d%@h", withValue: components.hour!)
        }
        
        if components.minute! > 0 {
            return stringFromFormat("%%d%@m", withValue: components.minute!)
        }
        
        if components.second! > 0 {
            return stringFromFormat("%%d%@s", withValue: components.second! )
        }
        
        return NSLocalizedString("generic_label_now", comment: "")
    }
    
    public var getTimeAgo: String {
        let components = self.dateComponents()
        
        if components.year! > 0 {
            if components.year! < 2 {
                return NSDateTimeAgoLocalizedStrings("Last year")
            } else {
                return stringFromFormat("%%d %@years ago", withValue: components.year!)
            }
        }
        
        if components.month! > 0 {
            if components.month! < 2 {
                return NSDateTimeAgoLocalizedStrings("Last month")
            } else {
                return stringFromFormat("%%d %@months ago", withValue: components.month!)
            }
        }
        
        // TODO: localize for other calanders
        if components.day! >= 7 {
            let week = components.day!/7
            if week < 2 {
                return NSDateTimeAgoLocalizedStrings("Last week")
            } else {
                return stringFromFormat("%%d %@weeks ago", withValue: week)
            }
        }
        
        if components.day! > 0 {
            if components.day! < 2 {
                return NSDateTimeAgoLocalizedStrings("Yesterday")
            } else  {
                return stringFromFormat("%%d %@days ago", withValue: components.day!)
            }
        }
        
        if components.hour! > 0 {
            if components.hour! < 2 {
                return NSDateTimeAgoLocalizedStrings("An hour ago")
            } else  {
                return stringFromFormat("%%d %@hours ago", withValue: components.hour!)
            }
        }
        
        if components.minute! > 0 {
            if components.minute! < 2 {
                return NSDateTimeAgoLocalizedStrings("A minute ago")
            } else {
                return stringFromFormat("%%d %@minutes ago", withValue: components.minute!)
            }
        }
        
        if components.second! > 0 {
            if components.second! < 5 {
                return NSDateTimeAgoLocalizedStrings("Just now")
            } else {
                return stringFromFormat("%%d %@seconds ago", withValue: components.second!)
            }
        }
        
        return NSLocalizedString("generic_label_now", comment: "")
    }
    
    fileprivate func dateComponents() -> DateComponents {
        let calander = Calendar.current
        return (calander as NSCalendar).components([.second, .minute, .hour, .day, .month, .year], from: self, to: Date(), options: [])
    }
    
    fileprivate func stringFromFormat(_ format: String, withValue value: Int) -> String {
        let localeFormat = String(format: format, getLocaleFormatUnderscoresWithValue(Double(value)))
        return String(format: NSDateTimeAgoLocalizedStrings(localeFormat), value)
    }
    
    fileprivate func getLocaleFormatUnderscoresWithValue(_ value: Double) -> String {
        guard let localeCode = Locale.preferredLanguages.first else {
            return ""
        }
        
        // Russian (ru) and Ukrainian (uk)
        if localeCode == "ru" || localeCode == "uk" {
            let XY = Int(floor(value)) % 100
            let Y = Int(floor(value)) % 10
            
            if Y == 0 || Y > 4 || (XY > 10 && XY < 15) {
                return ""
            }
            
            if Y > 1 && Y < 5 && (XY < 10 || XY > 20) {
                return "_"
            }
            
            if Y == 1 && XY != 11 {
                return "__"
            }
        }
        
        return ""
    }
    
}
