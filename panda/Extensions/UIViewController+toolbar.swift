//
//  UIViewController+toolbar.swift
//  panda
//
//  Created by Edouard Roussillon on 9/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension UIViewController {
 
    //
    //MARK: ToolBar
    //
    
    internal func initToolBar(_ hiddeButton:Bool?=true, withPres:Bool?=true, withNext:Bool?=true) -> UIToolbar {
        let toolbar:UIToolbar = UIToolbar()
        toolbar.accessibilityIdentifier = "TOOLBAR"
        toolbar.sizeToFit()
        
        let flex2Button = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(UIViewController.resignKeyboard(_:)))
        doneButton.accessibilityIdentifier = "TOOLBAR_DONE"
        var itemsArray:[UIBarButtonItem]?
        
        if hiddeButton == false {
            let presButton = UIBarButtonItem(image: UIImage(named:"left_on"), style: UIBarButtonItemStyle.done, target: self, action: #selector(UIViewController.clickPres(_:)))
            presButton.accessibilityIdentifier = "TOOLBAR_PRES"
            let flexButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: self, action: nil)
            flexButton.width = 20.0
            let nextButton = UIBarButtonItem(image: UIImage(named:"right_on"), style: UIBarButtonItemStyle.done, target: self, action: #selector(UIViewController.clickNext(_:)))
            nextButton.accessibilityIdentifier = "TOOLBAR_NEXT"
            
            presButton.isEnabled = withPres!
            nextButton.isEnabled = withNext!
            
            itemsArray = [presButton, flexButton, nextButton, flex2Button, doneButton]
        } else {
            itemsArray = [flex2Button, doneButton]
        }
        
        toolbar.setItems(itemsArray, animated: true)
        
        return toolbar;
    }
    
    @objc internal func clickPres(_ sender: UIBarButtonItem) {
        
    }
    
    @objc internal func clickNext(_ sender: UIBarButtonItem) {
        
    }
    
    @objc internal func resignKeyboard(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
}
