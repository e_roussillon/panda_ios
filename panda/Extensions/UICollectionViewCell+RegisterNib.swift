//
//  UICollectionViewCellUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

protocol BaseCollectionViewCellDelegate {
    static func sizeWithObjects(_ objects: [AnyObject]?) -> CGSize
}


private var heightAtIndexPathKey: UInt8 = 0

extension UICollectionViewCell {
    
    //
    //MARK: Need to be implemented
    //
    
    func configureCell(_ objects: [Any]?) {
        
    }
    
    //
    //MARK: GENERIC
    //
    
    static func cellIdentifier() -> String {
        return self.className
    }
    
    static func registerNibIntoCollection(_ collection: UICollectionView) {
        let name  = self.cellIdentifier()
        let cellNib = UINib(nibName: name, bundle: nil)
        collection.register(cellNib, forCellWithReuseIdentifier: name)
    }
    
    static func initCell(_ collectionView: UICollectionView, indexPath: IndexPath, objects: [Any]?) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier(), for: indexPath)
        cell.configureCell(objects)
        return cell
    }
}
