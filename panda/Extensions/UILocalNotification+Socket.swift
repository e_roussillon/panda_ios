//
//  UILocalNotification+Socket.swift
//  panda
//
//  Created by Edouard Roussillon on 9/14/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import BRYXBanner
import AudioToolbox.AudioServices

extension UILocalNotification {
    
    static func showPrivateMessage(_ privateMessage: PrivateMessageModel) {
        if !SocketManager.sharedInstance.isSocketConnected() {
            let notification = UILocalNotification()
            notification.alertBody = privateMessage.msg ?? ""
            notification.userInfo = ["type": NotifType.message.rawValue,
                                 "data": privateMessage.toJSON()]
            notification.soundName = "default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        } else {
            self.showNotif(NSLocalizedString("private_chat_notification_title", comment: "").replacingOccurrences(of: "%1", with: privateMessage.fromUserName ?? ""), message: privateMessage.msg ?? "", tap: {
                if let userId = privateMessage.fromUserId {
                    let user = UserModel.getUser(userId)
                    UIViewController.pushNotificationMessage(user)
                }
            })
        }
    }
    
    static func showFriendship(_ pseudo: String, userId: Int) {
        if !SocketManager.sharedInstance.isSocketConnected() {
            let notification = UILocalNotification()
            notification.alertBody = NSLocalizedString("friendship_notification", comment: "").replacingOccurrences(of: "%1", with: pseudo)
            notification.userInfo = ["type": NotifType.friendship.rawValue,
                                     "data": ["pseudo": pseudo, "id" : userId]]
            notification.soundName = "default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        } else {
            self.showNotif(NSLocalizedString("friendship_screen_title", comment: ""), message: NSLocalizedString("friendship_notification", comment: "").replacingOccurrences(of: "%1", with: pseudo), tap: {
                let user = UserModel.getUser(userId)
                UIViewController.pushNotificationFriendship(user)
            })
        }
    }
    
    fileprivate static func showNotif(_ title: String, message: String, tap: (() -> ())? = nil) {
        let banner = Banner(title: title, subtitle: message, backgroundColor: UIColor.white, didTapBlock: tap)
        banner.textColor = Constants.COLOR.kAppTint
        banner.show(duration: 3.0)
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
    }
    
}
