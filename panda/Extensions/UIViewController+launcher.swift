//
//  UIViewController+launcher.swift
//  panda
//
//  Created by Edouard Roussillon on 7/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension UIViewController {
    
    //
    //MARK: Logout
    //
    
    static func logoutPanda(_ withAnim:Bool) {
        SessionManager.sharedInstance.logout()
        self.launchSplash(withAnim)
    }
    
    //
    //MARK: Launcher
    //
    
    static func launchSearch(_ removedSearchAfterSelecteChat: Bool = false) {
        let searchViewController = StoryboardUtil.searchViewController()
        searchViewController.removedSearchAfterSelecteChat = removedSearchAfterSelecteChat
        self.launcher(searchViewController, popBack: false, presentVC: false, withAnim: true)
    }
    
    static func lauchLogoutReported(_ time: Int, logoutUser: Bool = true) {
        SessionManager.sharedInstance.logout()
        let msg = NSLocalizedString("dialog_message_reported", comment: "").replacingOccurrences(of: "%1", with: time.secondsToString())

        AlertUtil.showTitleWithMessage(message: msg) { (alert) in
            if logoutUser {
                logoutPanda(true)
            }
        }
    }
    
    static func lauchLogoutForced() {
        SessionManager.sharedInstance.logout()
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_logout_froced", comment: "")) { (alert) in
            logoutPanda(true)
        }
    }
    
    static func showAlertGeneric() {
        AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_title_error_server", comment: ""), message: NSLocalizedString("dialog_message_error_server", comment: ""))
    }
    
    static func launchProfileUser(_ userId: Int, userName: String) {
        launchProfileUser(UserModel.getUser(userId))
    }
    
    static func launchProfileUser(_ user: UserModel) {
        guard let userId = user.userId else {
            return
        }
        
        if userId == SessionManager.sharedInstance.user.userId {
            let vc = StoryboardUtil.profileViewController()
            self.launcher(BaseNavigationController(rootViewController: vc), popBack: false, presentVC: true, withAnim: true)
        } else {
            let vc = StoryboardUtil.flyingProfileViewController()
            vc.initUser(user)
            
            self.launcher(BaseNavigationController(rootViewController: vc), popBack: false, presentVC: true, withAnim: true)
        }
    }
    
    static func launchChatting(_ user: UserModel, friendshipStatus: FriendshipType = FriendshipType.notFriend, friendshipLastAction: Int = -1, removedSearchAfterSelecteChat: Bool = false) {
        let chattingViewController = StoryboardUtil.chatViewController(user, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction, removedSearchAfterSelecteChat: removedSearchAfterSelecteChat)
        self.launcher(chattingViewController, popBack: false, presentVC: false, withAnim: true)
    }
    
    static func launchSplash(_ withAnim:Bool) {
        self.launcher(StoryboardUtil.splashNavViewController(), popBack : true, presentVC: true, withAnim: withAnim)
    }
    
    static func launchWelcome() {
        let welcome = StoryboardUtil.welcomeViewController()
        welcome.view.backgroundColor = UIColor.clear
        
        let nav = BaseNavigationController(rootViewController: welcome)
        nav.providesPresentationContextTransitionStyle = true;
        nav.definesPresentationContext = true;
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.view.backgroundColor = UIColor.clear
        nav.setNavigationBarHidden(true, animated: false)
        
        self.launcher(nav, popBack : true, presentVC: true, withAnim: true)
    }
    
    fileprivate static func launcher(_ newVC: UIViewController, popBack: Bool, presentVC: Bool, withAnim:Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let window = appDelegate.window {
            
            if let view = window.rootViewController {
                var lookFor = true
                var vc: UIViewController = view
                
                while(lookFor) {
                    if let present = vc.presentedViewController {
                        vc = present
                    } else if let nav = vc as? UINavigationController {
                        OperationQueue.main.addOperation {
                            
                            if popBack {
                                nav.popToRootViewController(animated: true)
                            }
                            
                            if presentVC {
                                nav.present(newVC, animated: withAnim, completion: {
                                    if let baseVC: BaseNavigationController = newVC as? BaseNavigationController,
                                        let _ = baseVC.viewControllers.first as? SplashViewController {
                                        UIViewController.resetScreenToHome()
                                    }
                                })
                            } else {
                                nav.pushViewController(newVC, animated: withAnim)
                            }
                            
                        }
                        lookFor = false
                    }
                }
            }
        }
    }
    
    static func resetScreenToHome() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let window = appDelegate.window {
            
            if let view = window.rootViewController {
                let vc: UIViewController = view
                
                if let nav = vc as? BaseNavigationController,
                    let home = nav.viewControllers.first as? HomeViewController {
                    home.moveToPosition(1, withAnimation: false)
                    if let tabNav = home.viewControllers?.first as? BaseNavigationController,
                        let tab = tabNav.viewControllers.first as? TabBarChatViewController {
                        tab.selectedIndex = 0
                    }
                }
            }
        }
    }
    
    static func pushNotificationMessage(_ user: UserModel, friendshipStatus: FriendshipType = FriendshipType.notFriend, friendshipLastAction: Int = -1) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let window = appDelegate.window {
            
            if let view = window.rootViewController {
                let vc: UIViewController = view
                
                if let nav = vc as? BaseNavigationController,
                    let home = nav.viewControllers.first as? HomeViewController {
                    home.moveToPosition(1, withAnimation: false)
                    if let tabNav = home.viewControllers?.first as? BaseNavigationController,
                        let tab = tabNav.viewControllers.first as? TabBarChatViewController {
                        tab.selectedIndex = 2
                    }
                    
                    if let chat = nav.viewControllers.last as? ChatViewController, chat.user.userId == user.userId {
                        //DO NOTHING WE ARE ON THE RIGHT VIEW
                    } else {
                        nav.popViewController(animated: true)
                        let chattingViewController = StoryboardUtil.chatViewController(user, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
                        nav.pushViewController(chattingViewController, animated: true)
                    }
                }
                
                if let present = vc.presentedViewController {
                    present.dismiss(animated: true, completion: {})
                }
            }
        }
    }
    
    static func pushNotificationFriendship(_ user: UserModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let window = appDelegate.window {
            
            if let view = window.rootViewController {
                let vc: UIViewController = view
                
                if let present = vc.presentedViewController {
                    present.dismiss(animated: true, completion: {
                        //TODO - FOUND A NEW WAY
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        if let window = appDelegate.window {
                            if let view = window.rootViewController {
                                let vc: UIViewController = view
                                
                                if let nav = vc as? BaseNavigationController,
                                    let home = nav.viewControllers.first as? HomeViewController {
                                    nav.popToRootViewController(animated: false)
                                    home.moveToPosition(0, withAnimation: false)
                                    if let tabNav = home.viewControllers?.first as? BaseNavigationController,
                                        let tab = tabNav.viewControllers.first as? TabBarFriendshipViewController {
                                        tab.selectedIndex = 1
                                    }
                                    home.view.layoutIfNeeded()
                                }
                            }
                        }
                    })
                } else if let nav = vc as? BaseNavigationController,
                    let home = nav.viewControllers.first as? HomeViewController {
                    nav.popToRootViewController(animated: false)
                    home.moveToPosition(0, withAnimation: false)
                    if let tabNav = home.viewControllers?.first as? BaseNavigationController,
                        let tab = tabNav.viewControllers.first as? TabBarFriendshipViewController {
                        tab.selectedIndex = 1
                    }
                    home.view.layoutIfNeeded()
                }

            }
        }
    }
}
