//
//  NSIndexPath+toString.swift
//  panda
//
//  Created by Edouard Roussillon on 11/6/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension IndexPath {
    
    func toString() -> String {
        return "\(self.section)-\(self.row)"
    }
    
}
