//
//  UIView+anim.swift
//  panda
//
//  Created by Edouard Roussillon on 10/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20, 20, -20, 20, -10, 10, -5, 5, 0]
        layer.add(animation, forKey: nil)
    }
    
    func shakeRotation() {
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-(Double.pi/16), (Double.pi/16), -(Double.pi/16), (Double.pi/16), -(Double.pi/32), (Double.pi/32), -(Double.pi/64), (Double.pi/64), 0]
        layer.add(animation, forKey: nil)
    }
    
    func zoom(_ count: Float = 1.0, duration: TimeInterval = 0.7, toValue: NSValue = NSValue(cgSize: CGSize(width: 1.03, height: 1.03))) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        animation.fromValue = NSValue(cgSize: CGSize(width: 1, height: 1))
        animation.toValue = toValue
        animation.autoreverses = true
        animation.duration = duration
        animation.beginTime = 0.0
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        animation.repeatCount = count
        animation.isRemovedOnCompletion = true
        layer.add(animation, forKey: nil)
    }
    
    func elevate(of elevation: Double, cornerRadius: Double = 0, masksToBounds: Bool = false) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.masksToBounds = masksToBounds
        self.layer.shadowColor = Constants.COLOR.kShadowChatTextView.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = abs(CGFloat(elevation))
        self.layer.shadowOpacity = 0.24
    }
    
    func border(borderColor: CGColor = Constants.COLOR.kShadowChatTextView.cgColor, borderWidth: CGFloat = 1.0, cornerRadius: Double = 0, masksToBounds: Bool = false) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.masksToBounds = masksToBounds
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
    
    // Usage:
    // view.addBorders(edges: [.all]) -> All with default arguments
    // view.addBorders(edges: [.top], color: UIColor.greenColor()) -> Just Top, green, default thickness
    // view.addBorders(edges: [.left, .right, .bottom], color: UIColor.redColor(), thickness: 3) -> All except Top, red, thickness 3
    @discardableResult func addBorders(edges: UIRectEdge, color: UIColor = .green, thickness: CGFloat = 1.0) -> [UIView] {
        
        var borders = [UIView]()
        
        func border() -> UIView {
            let border = UIView(frame: CGRect.zero)
            border.backgroundColor = color
            border.translatesAutoresizingMaskIntoConstraints = false
            return border
        }
        
        if edges.contains(.top) || edges.contains(.all) {
            let top = border()
            addSubview(top)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[top(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["top": top]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[top]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["top": top]))
            borders.append(top)
        }
        
        if edges.contains(.left) || edges.contains(.all) {
            let left = border()
            addSubview(left)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[left(==thickness)]",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["left": left]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[left]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["left": left]))
            borders.append(left)
        }
        
        if edges.contains(.right) || edges.contains(.all) {
            let right = border()
            addSubview(right)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:[right(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["right": right]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-(0)-[right]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["right": right]))
            borders.append(right)
        }
        
        if edges.contains(.bottom) || edges.contains(.all) {
            let bottom = border()
            addSubview(bottom)
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "V:[bottom(==thickness)]-(0)-|",
                                               options: [],
                                               metrics: ["thickness": thickness],
                                               views: ["bottom": bottom]))
            addConstraints(
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-(0)-[bottom]-(0)-|",
                                               options: [],
                                               metrics: nil,
                                               views: ["bottom": bottom]))
            borders.append(bottom)
        }
        
        return borders
    }
}
