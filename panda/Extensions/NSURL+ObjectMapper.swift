//
//  NSURLUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 3/1/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper
import AlamofireImage

extension URL {
    
    static let transform = TransformOf<URL, String>(fromJSON: { (value: String?) -> URL? in
        // transform value from String? to URL?
        guard let url = value, url.length() > 0 else {
            return nil
        }
        
        return URL(string: url)
        }, toJSON: { (value: URL?) -> String? in
            // transform value from URL? to String?
            if let value = value {
                return value.absoluteString
            }
            return ""
    })
    
    func isCached() -> UIImage? {
        return UIImageView.af_sharedImageDownloader.imageCache?.image(withIdentifier: self.absoluteString) ?? nil
    }
    
}
