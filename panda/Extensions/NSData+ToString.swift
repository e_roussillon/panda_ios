//
//  NSData+ToString.swift
//  panda
//
//  Created by Edouard Roussillon on 6/19/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

extension Data {
    
    public func toString() -> String? {
        return String(data: self, encoding: String.Encoding.utf8)
    }
    
    public func toToken() -> String? {
        let tokenChars = (self as NSData).bytes.bindMemory(to: CChar.self, capacity: self.count)
        var tokenString = ""
        
        for i in 0..<self.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        return tokenString
    }
}
