//
//  UIViewControllerUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 12/19/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func traverseAndFindClass<T : UIViewController>() -> T? {
        
        var currentVC = self
        while let parentVC = currentVC.parent {
            if let result = parentVC as? T {
                return result
            }
            currentVC = parentVC
        }
        return nil
    }
}
