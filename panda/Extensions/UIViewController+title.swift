//
//  UIViewController+title.swift
//  panda
//
//  Created by Edouard Roussillon on 9/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Cartography
import AlamofireImage
import QuartzCore

extension UIViewController {
    
    //
    //MARK: Title Nav
    //
    
    internal func initNavTitleWithLogo(_ logo: URL?, title: String) {
        self.navigationItem.titleView = self.createNavTitle(logo, title: title)
    }
    
    fileprivate func createNavTitle(_ logo: URL?, title: String) -> UIView {
        let placeHolder = UIImage(named: "logo_panda")
        let icon = UIImageView(image: placeHolder)
        if let logo = logo {
            icon.af_setImage(withURL: logo,
                                  placeholderImage: placeHolder,
                                  imageTransition: UIImageView.ImageTransition.crossDissolve(0.2))
            icon.layer.cornerRadius = 18
            icon.layer.masksToBounds = true
        }
        
        let label = UILabel(frame: CGRect.zero)
        
        label.text = title.capitalized
        label.textColor = Constants.COLOR.kToolTint
        if #available(iOS 8.2, *) {
            label.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        } else {
            label.font = UIFont.boldSystemFont(ofSize: 17)
        }
        label.sizeToFit()
        
        let view = UIView(frame: CGRect.zero)
        
        view.addSubview(icon)
        view.addSubview(label)
        
        constrain(icon, label, block: { (view1, view2) -> () in
            view2.centerX == view2.superview!.centerX + 14
            view2.centerY == view2.superview!.centerY
            
            view1.height == view1.superview!.height - 10
            view1.width == view1.superview!.height - 10
            
            view1.right == view2.left - 5
            view1.centerY == view2.superview!.centerY
        })
        
        return view
    }
    
}
