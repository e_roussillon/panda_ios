//
//  Swift+print.swift
//  panda
//
//  Created by Edouard Roussillon on 10/15/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper


public func print(_ items: [String]) {
    if EnvironmentUtil.debugInfo {
        for item in items {
            log(item, withSpace: false)
        }
        Swift.print("")
    }
}

public func print(_ items: Any..., separator: String = "\n", terminator: String = "\n") {
    
    if EnvironmentUtil.debugInfo {
        let output = items.map { "\($0)" }.joined(separator: separator)
        log(output, withSpace: true)
    }
}


private func log(_ message: String, withSpace: Bool = true) {
    Swift.print("[DEBUG] - \(message)", terminator: "\n")
    if withSpace {
       Swift.print("", terminator: "\n")
    }
}
