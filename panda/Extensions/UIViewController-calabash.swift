//
//  UIViewController-calabash.swift
//  panda
//
//  Created by Edouard Roussillon on 9/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Cartography

extension UIViewController {
    
    //
    //MARK: Mode Calabash
    //
    func setModeCalabash() {
        if EnvironmentUtil.isTargetPandaQA {
            //Fucking fix to alow calabash to swipe left/right
            if let _ = self.traverseAndFindClass() as HomeViewController? {
                
                let buttonLeft = UIButton()
                buttonLeft.backgroundColor = UIColor.redColor()
                buttonLeft.accessibilityIdentifier = "BUTTON_LEFT_PAGER"
                buttonLeft.addTarget(self, action: #selector(UIViewController.clickLeft), forControlEvents: UIControlEvents.TouchUpInside)
                self.view.addSubview(buttonLeft)
                
                let buttonRight = UIButton()
                buttonRight.backgroundColor = UIColor.redColor()
                buttonRight.accessibilityIdentifier = "BUTTON_RIGHT_PAGER"
                buttonRight.addTarget(self, action: #selector(UIViewController.clickRight), forControlEvents: UIControlEvents.TouchUpInside)
                self.view.addSubview(buttonRight)
                
                constrain(buttonLeft, buttonRight, block: { (view1, view2) -> () in
                    view1.left == view1.superview!.left
                    view1.width == 10
                    view1.top == view1.superview!.top
                    view1.bottom == view1.superview!.bottom
                    
                    view2.right == view2.superview!.right
                    view2.width == 10
                    view2.top == view2.superview!.top
                    view2.bottom == view2.superview!.bottom
                })
            }
        }
    }
    
    //
    //MARK: Click Button Calabash
    //
    
    internal func clickLeft() {
        if let vc = self.traverseAndFindClass() as HomeViewController? where vc.homePageDelegate.current > 0 {
            vc.moveToPosition(vc.homePageDelegate.current-1, withAnimation: true)
        }
    }
    
    internal func clickRight() {
        if let vc = self.traverseAndFindClass() as HomeViewController? where vc.homePageDelegate.current < 2 {
            vc.moveToPosition(vc.homePageDelegate.current+1, withAnimation: true)
        }
    }
}
