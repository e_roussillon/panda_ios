//
//  FilterModel.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class FilterModel: Mappable {
    
    var filterId: Int? = 0
    var name: String? = ""
    var isHighlight: Bool? = false
    var timestamp: Date?
    
    //
    //MARK: Map
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        filterId <- map["id"]
        name <- map["name"]
        isHighlight <- map["is_highlight"]
        timestamp <- (map["timestamp"], DateFormatter.initISO8601ZDate())
    }
    
    //
    //MARK: Load
    //
    
    static func iniFilter(_ filterId: Int, name: String, isHighlight: Bool, timestamp: String) -> FilterModel {
        let disc: [String : Any] = ["id" : filterId,
            "name" : name,
            "is_highlight" : isHighlight,
            "timestamp" : timestamp]
        
        return Mapper<FilterModel>().map(JSON: disc)!
    }
}
