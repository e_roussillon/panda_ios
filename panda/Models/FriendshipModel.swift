//
//  ProfileUserModel.swift
//  panda
//
//  Created by Edouard Roussillon on 2/6/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class FriendshipModel: NSObject, Mappable {
    var minUserId: Int?
    var maxUserId: Int?
    var status: FriendshipType?
    var userLastAction: Int?
    
    //
    //MARK: Map
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        minUserId <- map["user_id"]
        maxUserId <- map["to_user_id"]
        status <- map["status"]
        userLastAction <- map["last_action"]
    }
}
