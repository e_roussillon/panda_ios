//
//  PaginatedModel.swift
//  panda
//
//  Created by Edouard Roussillon on 2016-04-21.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class Paginated<T: Mappable>: Mappable {
    
    var totalPages: Int?
    var totalEntries: Int?
    var pageSize: Int?
    var pageNumber: Int?
    var data: [T]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        totalPages <- map["total_pages"]
        totalEntries <- map["total_entries"]
        pageSize <- map["page_size"]
        pageNumber <- map["page_number"]
        data <- map["entries"]
    }

}
