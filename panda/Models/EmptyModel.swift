//
//  EmptyModel.swift
//  panda
//
//  Created by Edouard Roussillon on 6/19/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

/// Where are using this model as a default response of APIv2
open class EmptyModel: Mappable {
    
    //
    //MARK: MAP
    //
    
    public required init?(map: Map) {
        
    }
    
    open func mapping(map: Map) {
        
    }
}
