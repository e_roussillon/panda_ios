//
//  PublicRoomModel.swift
//  panda
//
//  Created by Edouard Roussillon on 3/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class PublicRoomModel: Mappable {
    
    var type: String?
    var originalName: String?
    var name: String?
    var publicRoomId: Int?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        originalName <- map["original_name"]
        name <- map["name"]
        publicRoomId <- map["id"]
    }
    
    //
    //MARK: Get String
    //
    
    func toString() -> String {
        return Mapper().toJSONString(self, prettyPrint: true) ?? ""
    }
    
    static func loadModel(_ json: String) -> PublicRoomModel? {
        
        if json.length() > 0 {
            return Mapper<PublicRoomModel>().map(JSONString: json)
        }
        
        return nil
    }
}
