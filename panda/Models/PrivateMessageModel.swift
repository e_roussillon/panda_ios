//
//  PrivateMessageModel.swift
//  panda
//
//  Created by Edouard Roussillon on 3/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper
import Foundation

class PrivateMessageModel: GenericMessageModel {

    fileprivate static let sessionManager = SessionManager.sharedInstance
    
    fileprivate static let friendshipStatusField = "friendship_status"
    fileprivate static let friendshipLastActionField = "friendship_last_action"
    fileprivate static let toUserIdField = "to_user_id"
    fileprivate static let toUserNameField = "to_user_name"
    
    var friendshipStatus: FriendshipType?
    var friendshipLastAction: Int?
    var toUserId: Int?
    var toUserName: String?
    
    //
    //MARK: MAP
    //
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        friendshipStatus <- map[PrivateMessageModel.friendshipStatusField]
        friendshipLastAction <- map[PrivateMessageModel.friendshipLastActionField]
        toUserId <- map[PrivateMessageModel.toUserIdField]
        toUserName <- map[PrivateMessageModel.toUserNameField]
    }
    
    static func initMessage(roomId: Int,
                                   rowId: Int,
                                   messageId: Int,
                                   msg: String,
                                   urlThumb: String,
                                   urlOriginal: String,
                                   urlBlur: String,
                                   type: DataType,
                                   size: Float,
                                   dataId: Int,
                                   dataLocal: String,
                                   fromUserId: Int,
                                   insertedAt: Double,
                                   index: String,
                                   active: Bool,
                                   status: StatusType) -> PrivateMessageModel {
        
        let disc: [String: Any] = [PrivateMessageModel.roomIdField : roomId,
                                         PrivateMessageModel.rowIdField : rowId,
                                         PrivateMessageModel.idField : messageId,
                                         PrivateMessageModel.msgField : msg,
                                         PrivateMessageModel.fromUserIdField : fromUserId,
                                         PrivateMessageModel.urlThumbField : urlThumb,
                                         PrivateMessageModel.urlOriginalField : urlOriginal,
                                         PrivateMessageModel.urlBlurField : urlBlur,
                                         PrivateMessageModel.typeField : type.rawValue,
                                         PrivateMessageModel.sizeField : size,
                                         PrivateMessageModel.dataIdField : dataId,
                                         PrivateMessageModel.dataLocalField : dataLocal,
                                         PrivateMessageModel.insertedAtField : insertedAt,
                                         PrivateMessageModel.indexField : index,
                                         PrivateMessageModel.activeField : active,
                                         PrivateMessageModel.statusField : status.rawValue]
        
        return Mapper<PrivateMessageModel>().map(JSON: disc)!
    }
    
    static func initMessage(roomId: Int,
                                   messageId: Int,
                                   msg: String,
                                   urlThumb: String,
                                   urlOriginal: String,
                                   urlBlur: String,
                                   type: DataType,
                                   size: Float,
                                   dataId: Int,
                                   dataLocal: String,
                                   fromUserId: Int,
                                   fromUserName: String,
                                   toUserId: Int,
                                   toUserName: String,
                                   friendshipStatus: FriendshipType,
                                   friendshipLastAction: Int,
                                   insertedAt: Double,
                                   index: String,
                                   active: Bool,
                                   status: StatusType) -> PrivateMessageModel {
        
        let disc: [String: Any] = [PrivateMessageModel.roomIdField : roomId,
                                         PrivateMessageModel.idField : messageId,
                                         PrivateMessageModel.msgField : msg,
                                         PrivateMessageModel.urlThumbField : urlThumb,
                                         PrivateMessageModel.urlOriginalField : urlOriginal,
                                         PrivateMessageModel.urlBlurField : urlBlur,
                                         PrivateMessageModel.typeField : type.rawValue,
                                         PrivateMessageModel.sizeField : size,
                                         PrivateMessageModel.dataIdField : dataId,
                                         PrivateMessageModel.dataLocalField : dataLocal,
                                         PrivateMessageModel.fromUserIdField : fromUserId,
                                         PrivateMessageModel.fromUserNameField : fromUserName,
                                         PrivateMessageModel.toUserIdField : toUserId,
                                         PrivateMessageModel.toUserNameField : toUserName,
                                         PrivateMessageModel.friendshipStatusField : friendshipStatus.rawValue,
                                         PrivateMessageModel.friendshipLastActionField : friendshipLastAction,
                                         PrivateMessageModel.insertedAtField : insertedAt,
                                         PrivateMessageModel.indexField : index,
                                         PrivateMessageModel.activeField : active,
                                         PrivateMessageModel.statusField : status.rawValue]
        
        return Mapper<PrivateMessageModel>().map(JSON: disc)!
    }
    
    static func initMessageLite(roomId: Int,
                                       msg: String,
                                       friendshipStatus: FriendshipType,
                                       friendshipLastAction: Int,
                                       toUser: UserModel,
                                       status: Int = StatusType.sending.rawValue,
                                       image: UIImage?) -> PrivateMessageModel {
        
        let insertedAt = Date().timestamp
        let userId = self.sessionManager.user.userId
        let userPseudo = self.sessionManager.user.userPseudo
        let indexTemp = "\(userId)_\(insertedAt)"
        
        var dataLocal: String = ""
        var type: DataType = DataType.none
        
        if let i = image {
            type = DataType.image
            dataLocal = "\(roomId)_\(indexTemp).jpg"
            i.saveImage(dataLocal)
        }
        
        let disc: [String: Any] = [PrivateMessageModel.roomIdField : roomId,
                                         PrivateMessageModel.msgField : msg,
                                         PrivateMessageModel.dataIdField : -1,
                                         PrivateMessageModel.dataLocalField: dataLocal,
                                         PrivateMessageModel.typeField: type.rawValue,
                                         PrivateMessageModel.sizeField: 0.0,
                                         PrivateMessageModel.friendshipStatusField : friendshipStatus.rawValue,
                                         PrivateMessageModel.friendshipLastActionField : friendshipLastAction,
                                         PrivateMessageModel.toUserIdField : toUser.userId!,
                                         PrivateMessageModel.toUserNameField : toUser.pseudo!,
                                         PrivateMessageModel.fromUserIdField : userId,
                                         PrivateMessageModel.fromUserNameField : userPseudo,
                                         PrivateMessageModel.indexField : indexTemp,
                                         PrivateMessageModel.statusField : status,
                                         PrivateMessageModel.activeField : true,
                                         PrivateMessageModel.insertedAtField : insertedAt]
        
        return Mapper<PrivateMessageModel>().map(JSON: disc)!
    }
}
