//
//  ErrorResponse.swift
//  panda
//
//  Created by Edouard Roussillon on 3/20/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class ErrorResponse: Mappable {
    
    fileprivate static let typeField = "type"
    fileprivate static let msgField = "msg"
    fileprivate static let codeField = "code"
    fileprivate static let detailsField = "details"
    fileprivate static let fieldsField = "fields"
    
    var type: ErrorType?
    var msg: String?
    var details: [String: AnyObject]?
    var code: Int?
    var fields: [FieldModel]?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        type    <- map[ErrorResponse.typeField]
        msg     <- map[ErrorResponse.msgField]
        code    <- map[ErrorResponse.codeField]
        details <- map[ErrorResponse.detailsField]
        fields  <- map[ErrorResponse.fieldsField]
    }
}
