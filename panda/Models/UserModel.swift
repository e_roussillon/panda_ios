//
//  UserModel.swift
//  panda
//
//  Created by Edouard Roussillon on 11/28/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import ObjectMapper
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


enum FriendshipType: Int {
    case notFriend = 0
    case pending
    case cancelRequest
    case refusedBeforeBeFriend
    case blockedBeforeBeFriend
    case friend
    case refusedAfterBeFriend
    case blockedAfterBeFriend
    case unBlocked
}

enum GenderType: Int {
    case none = 0
    case male
    case female
}

class UserModel: NSObject, Mappable {
        
    fileprivate static let userIdField = "id"
    fileprivate static let emailField = "email"
    fileprivate static let pseudoField = "pseudo"
    fileprivate static let genderField = "gender"
    fileprivate static let birthdayField = "birthday"
    fileprivate static let detailsField = "description"
    fileprivate static let urlBlurField = "url_blur"
    fileprivate static let urlThumbField = "url_thumb"
    fileprivate static let urlOriginalField = "url_original"
    fileprivate static let isOnlineField = "is_online"
    fileprivate static let lastSeenField = "last_seen"
    fileprivate static let friendshipStatusField = "friendship_status"
    fileprivate static let friendshipLastActionField = "friendship_last_action"
    fileprivate static let reported = "reported"
    fileprivate static let updatedAtField = "updated_at"
    
    var userId: Int? = -1
    var email: String? = ""
    var pseudo: String? = ""
    var gender: GenderType? = GenderType.none
    var birthday: Date?
    var details: String? = ""
    var urlBlur: URL?
    var urlThumb: URL?
    var urlOriginal: URL?
    var isOnline: Bool?
    var lastSeen: Date?
    var friendshipStatus: FriendshipType?
    var friendshipLastAction: Int?
    var reported: Bool?
    var updatedAt: Date?
    
    //
    //MARK: Map
    //
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        userId <- map[UserModel.userIdField]
        email <- map[UserModel.emailField]
        pseudo <- map[UserModel.pseudoField]
        gender <- map[UserModel.genderField]
        birthday <- (map[UserModel.birthdayField], DateFormatter.initSmallDate())
        details <- map[UserModel.detailsField]
        urlBlur <- (map[UserModel.urlBlurField], URL.transform)
        urlThumb <- (map[UserModel.urlThumbField], URL.transform)
        urlOriginal <- map[UserModel.urlOriginalField]
        isOnline <- map[UserModel.isOnlineField]
        lastSeen <- (map[UserModel.lastSeenField], UnixFormatterTransform())
        friendshipStatus <- map[UserModel.friendshipStatusField]
        friendshipLastAction <- map[UserModel.friendshipLastActionField]
        reported <- map[UserModel.reported]
        updatedAt <- (map[UserModel.updatedAtField], UnixFormatterTransform())
    }
    
    //
    //MARK: Methods
    //
    
    func getGenderType() -> String {
        
        if let g = self.gender {
            switch g {
            case .female:
                return ProfileGenderDelegate.stringFemal
            case .male:
                return ProfileGenderDelegate.stringMale
            default:
                return ProfileGenderDelegate.stringNone
            }
        } else {
            return ProfileGenderDelegate.stringNone
        }
    }
    
    
    //
    //MARK: Loader
    //
    
    static func initModel(_ userId: Int,  pseudo: String) -> UserModel {
        let disc: [String : Any] = ["id" : userId,
                                          "pseudo" : pseudo]
        return Mapper<UserModel>().map(JSON: disc)!
    }
    
    static func initModel(_ userId: Int,
                          email: String,
                          pseudo: String,
                          gender: GenderType?,
                          birthday: String?,
                          details: String?,
                          urlBlur: String?,
                          urlThumb: String?,
                          urlOriginal: String?,
                          isOnline: Bool?,
                          lastSeen: Double?,
                          friendshipStatus: Int,
                          friendshipLastAction: Int,
                          reported: Bool,
                          updatedAt: Double) -> UserModel {
        
        let disc: [String : Any] = [UserModel.userIdField: userId,
                                          UserModel.emailField: email,
                                          UserModel.pseudoField: pseudo,
                                          UserModel.genderField: gender?.rawValue ?? GenderType.none.rawValue,
                                          UserModel.birthdayField: birthday ?? "",
                                          UserModel.detailsField: details ?? "",
                                          UserModel.urlBlurField: urlBlur ?? "",
                                          UserModel.urlThumbField: urlThumb ?? "",
                                          UserModel.urlOriginalField: urlOriginal ?? "",
                                          UserModel.isOnlineField: isOnline ?? false,
                                          UserModel.lastSeenField: lastSeen ?? "",
                                          UserModel.friendshipStatusField: friendshipStatus,
                                          UserModel.friendshipLastActionField: friendshipLastAction,
                                          UserModel.reported: reported,
                                          UserModel.updatedAtField: updatedAt]
        
        return Mapper<UserModel>().map(JSON: disc)!
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let user = object as? UserModel {
            return self == user
        } else {
            return false
        }
    }
    
}

func ==(lhs: UserModel, rhs: UserModel) -> Bool {
    return lhs.pseudo == rhs.pseudo
}

func !=(lhs: UserModel, rhs: UserModel) -> Bool {
    return lhs.pseudo != rhs.pseudo
}

func <(lhs: UserModel, rhs: UserModel) -> Bool {
    return lhs.pseudo < rhs.pseudo
}

func >(lhs: UserModel, rhs: UserModel) -> Bool {
    return lhs.pseudo < rhs.pseudo
}

