//
//  DataModel.swift
//  panda
//
//  Created by Edouard Roussillon on 9/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class DataModel: NSObject, Mappable {
    
    fileprivate static let dataIdField = "id"
    fileprivate static let urlBlurField = "url_blur"
    fileprivate static let urlOriginalField = "url_original"
    fileprivate static let urlThumbField = "url_thumb"
    fileprivate static let typeField = "type"
    
    var dataId: Int? = -1
    var urlBlur: URL?
    var urlOriginal: URL?
    var urlThumb: URL?
    var type: DataType?
    
    
    //
    //MARK: Map
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        dataId <- map[DataModel.dataIdField]
        urlBlur <- map[DataModel.urlBlurField]
        urlOriginal <- map[DataModel.urlOriginalField]
        urlThumb <- map[DataModel.urlThumbField]
        type <- map[DataModel.typeField]
    }
}
