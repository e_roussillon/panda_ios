//
//  GenericMessageModel.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class GenericMessageModel: Mappable, Equatable {

    static let rowIdField = "row_id"
    static let idField = "id"
    static let indexField = "index"
    static let roomIdField = "room_id"
    static let msgField = "msg"
    static let statusField = "status"
    static let fromUserIdField = "from_user_id"
    static let fromUserNameField = "from_user_name"
    static let dataIdField = "data_id"
    static let urlThumbField = "url_thumb"
    static let urlOriginalField = "url_original"
    static let urlBlurField = "url_blur"
    static let typeField = "type"
    static let sizeField = "size"
    static let dataLocalField = "data_local"
    static let activeField = "active"
    static let insertedAtField = "inserted_at"


    var rowId: Int?
    var roomId: Int?
    var messageId: Int?
    var msg: String?
    var fromUserId: Int?
    var fromUserName: String?
    var dataId: Int?
    var urlThumb: URL?
    var urlOriginal: URL?
    var urlBlur: URL?
    var type: DataType?
    var size: Float?
    var dataLocal: String?
    var index: String?
    var status: StatusType?
    var insertedAt: Date?
    var uploading: CGFloat = 0.0
    var isFailed: Bool = false


    //
    //MARK: MAP
    //

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        index <- map[GenericMessageModel.indexField]
        rowId <- map[GenericMessageModel.rowIdField]
        roomId <- map[GenericMessageModel.roomIdField]
        messageId <- map[GenericMessageModel.idField]
        msg <- map[GenericMessageModel.msgField]
        fromUserId <- map[GenericMessageModel.fromUserIdField]
        fromUserName <- map[PublicMessageModel.fromUserNameField]
        dataId <- map[GenericMessageModel.dataIdField]
        urlThumb <- (map[GenericMessageModel.urlThumbField], URL.transform)
        urlOriginal <- (map[GenericMessageModel.urlOriginalField], URL.transform)
        urlBlur <- (map[GenericMessageModel.urlBlurField], URL.transform)
        type <- map[GenericMessageModel.typeField]
        size <- map[GenericMessageModel.sizeField]
        dataLocal <- map[GenericMessageModel.dataLocalField]
        insertedAt <- (map[GenericMessageModel.insertedAtField], UnixFormatterTransform())
        status <- map[GenericMessageModel.statusField]
    }


}

func ===(lhs: GenericMessageModel, rhs: GenericMessageModel) -> Bool {
    return lhs.index == rhs.index
            && lhs.rowId == rhs.rowId
            && lhs.roomId == rhs.roomId
            && lhs.messageId == rhs.messageId
            && lhs.msg == rhs.msg
            && lhs.fromUserId == rhs.fromUserId
            && lhs.fromUserName == rhs.fromUserName
            && lhs.dataId == rhs.dataId
            && lhs.urlThumb == rhs.urlThumb
            && lhs.urlOriginal == rhs.urlOriginal
            && lhs.urlBlur == rhs.urlBlur
            && lhs.type == rhs.type
            && lhs.size == rhs.size
            && lhs.dataLocal == rhs.dataLocal
            && lhs.insertedAt == rhs.insertedAt
            && lhs.status == rhs.status
}

func ==(lhs: GenericMessageModel, rhs: GenericMessageModel) -> Bool {
    return lhs.index == rhs.index
}


