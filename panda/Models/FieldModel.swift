//
//  FieldModel.swift
//  panda
//
//  Created by Edouard Roussillon on 3/20/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class FieldModel: Mappable {
    
    var field: String?
    var message: [String]?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        field <- map["field"]
        message <- map["message"]
    }
}
