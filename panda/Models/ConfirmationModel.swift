//
//  ConfirmationModel.swift
//  panda
//
//  Created by Edouard Roussillon on 9/7/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class ConfirmationModel: Mappable {
    
    fileprivate static let idField = "id"
    fileprivate static let statusField = "status"
    
    var id: Int?
    var status: Int?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
    }
    
    static func initConfirmation(id: Int, status: StatusType) -> ConfirmationModel {
        let disc: [String: Any] = [PublicMessageModel.idField : id,
                                         PublicMessageModel.statusField : status.rawValue]
        
        return Mapper<ConfirmationModel>().map(JSON: disc)!
    }
}
