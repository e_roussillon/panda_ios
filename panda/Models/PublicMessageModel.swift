//
//  PublicMessageModel.swift
//  panda
//
//  Created by Edouard Roussillon on 3/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

enum StatusType: Int {
    case error = -1
    case sending
    case sent
    case received
    case read
    case confirmed
    case deleting
    case deleted
}

enum DataType: Int {
    case none = -1
    case image
    case url
}

class PublicMessageModel: GenericMessageModel {
    
    fileprivate static let sessionManager = SessionManager.sharedInstance
    
    fileprivate static let isGeneralField = "is_general"
    fileprivate static let isHighlightField = "is_highlight"
    fileprivate static let isWelcomeMessageField = "is_welcome_message"
    
    var isGeneral: Bool?
    var isHighlight: Bool?
    var isWelcomeMessage: Bool?
    
    //
    //MARK: MAP
    //
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        isGeneral <- map[PublicMessageModel.isGeneralField]
        isHighlight <- map[PublicMessageModel.isHighlightField]
        isWelcomeMessage <- map[PublicMessageModel.isWelcomeMessageField]
    }
    
    static func initMessage(messageId: Int,
                                      msg: String,
                                      urlThumb: String,
                                      urlOriginal: String,
                                      urlBlur: String,
                                      type: DataType,
                                      isGeneral: Bool,
                                      fromUserId: Int,
                                      fromUserName: String,
                                      insertedAt: Double,
                                      index: String,
                                      status: StatusType,
                                      isHighlight: Bool,
                                      isWelcomeMessage: Bool = false) -> PublicMessageModel {
        
        let disc: [String: Any] = [PublicMessageModel.idField : messageId,
                                         PublicMessageModel.msgField : msg,
                                         PublicMessageModel.urlThumbField : urlThumbField,
                                         PublicMessageModel.urlOriginalField: urlOriginal,
                                         PublicMessageModel.urlBlurField: urlBlur,
                                         PublicMessageModel.typeField: type.rawValue,
                                         PublicMessageModel.isGeneralField : isGeneral,
                                         PublicMessageModel.fromUserIdField : fromUserId,
                                         PublicMessageModel.fromUserNameField : fromUserName,
                                         PublicMessageModel.insertedAtField : insertedAt,
                                         PublicMessageModel.indexField : index,
                                         PublicMessageModel.statusField : status.rawValue,
                                         PublicMessageModel.isHighlightField : isHighlight,
                                         PublicMessageModel.isWelcomeMessageField : isWelcomeMessage]
        
        return Mapper<PublicMessageModel>().map(JSON: disc)!
    }
    
    static func initMessageLite(msg: String,
                                    isGeneral: Bool,
                                    status: Int = StatusType.sending.rawValue,
                                    isWelcomeMessage: Bool = false) -> PublicMessageModel {
        let fromUserName = self.sessionManager.user.userPseudo
        let insertedAt = Date().timestamp
        let indexTemp = "\(fromUserName)_\(insertedAt)"
        
        let disc: [String: Any] = [PublicMessageModel.msgField : msg,
                                         PublicMessageModel.isGeneralField : isGeneral,
                                         PublicMessageModel.fromUserIdField : self.sessionManager.user.userId,
                                         PublicMessageModel.fromUserNameField : fromUserName,
                                         PublicMessageModel.insertedAtField : insertedAt,
                                         PublicMessageModel.indexField : indexTemp,
                                         PublicMessageModel.statusField : status,
                                         PublicMessageModel.isWelcomeMessageField : isWelcomeMessage]
        
        return Mapper<PublicMessageModel>().map(JSON: disc)!
    }
}
