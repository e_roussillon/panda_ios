//
//  ResponsePhoenixModel.swift
//  panda
//
//  Created by Edouard Roussillon on 7/16/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class ResponsePhoenixModel: Mappable {
    
    var status: String?
    var statusOk:Bool = false
    var response: [String: AnyObject]?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        if status == "ok" {
            statusOk = true
        }
        response <- map["response"]
    }
}
