//
//  PrivateChatModel.swift
//  panda
//
//  Created by Edouard Roussillon on 2/5/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

class PrivateChatModel {

    let rowId: Int!
    let roomId: Int!
    let isGroup: Bool!
    let nameRoom: String!
    let urlThumb: URL!
    let urlOriginal: URL!
    let urlBlur: URL!
    let type: Int!
    let userStatus: FriendshipType!
    let userStatusLastAction: Int!
    let msg: String!
    let msgSent: Date!
    let numOfMsgNotRead: Int!
    let userId: Int!
    let userIsOnline: Bool!
    let userLastSeen: Date?
    
    
    init(rowId: Int,
         roomId: Int,
         isGroup: Int,
         nameRoom: String,
         urlThumb: String,
         urlOriginal: String,
         urlBlur: String,
         type: Int,
         userStatus: Int,
         userStatusLastAction: Int,
         msg: String,
         msgSent: Double,
         numOfMsgNotRead: Int,
         userId: Int,
         userIsOnline: Bool,
         userLastSeen: Date?) {

        self.rowId = rowId
        self.roomId = roomId
        self.isGroup = isGroup == 1 ? true : false
        self.nameRoom = nameRoom
        self.urlThumb = URL(string: urlThumb)
        self.urlOriginal = URL(string: urlOriginal)
        self.urlBlur = URL(string: urlBlur)
        self.type = type
        self.userStatus = FriendshipType(rawValue: userStatus)
        self.userStatusLastAction = userStatusLastAction
        self.msg = msg
        self.msgSent = Date(timeIntervalSince1970: TimeInterval(msgSent))
        self.numOfMsgNotRead = numOfMsgNotRead
        self.userId = userId
        self.userIsOnline = userIsOnline
        self.userLastSeen = userLastSeen
    }
}

func ==(lhs: PrivateChatModel, rhs: PrivateChatModel) -> Bool {
    return lhs.rowId == rhs.rowId
        && lhs.roomId == rhs.roomId
        && lhs.isGroup == rhs.isGroup
        && lhs.nameRoom == rhs.nameRoom
        && lhs.urlThumb.absoluteString == rhs.urlThumb.absoluteString
        && lhs.urlOriginal.absoluteString == rhs.urlOriginal.absoluteString
        && lhs.urlBlur.absoluteString == rhs.urlBlur.absoluteString
        && lhs.type == rhs.type
        && lhs.userStatus == rhs.userStatus
        && lhs.userStatusLastAction == rhs.userStatusLastAction
        && lhs.msg == rhs.msg
        && lhs.msgSent == rhs.msgSent
        && lhs.numOfMsgNotRead == rhs.numOfMsgNotRead
        && lhs.userId == rhs.userId
        && lhs.userIsOnline == rhs.userIsOnline
}