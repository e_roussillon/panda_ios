//
//  BackupModel.swift
//  panda
//
//  Created by Edouard Roussillon on 2/27/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class BackupModel: Mappable {
    
    var userId: Int?
    var backup: [String]?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userId <- map["user_id"]
        backup <- map["backup"]
    }
}
