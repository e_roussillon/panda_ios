//
//  FindRoomsModel.swift
//  panda
//
//  Created by Edouard Roussillon on 3/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class FindRoomsModel: Mappable {
    
    var token: String?
    var rooms: [PublicRoomModel]?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        rooms <- map["rooms"]
    }
    
    //
    //MARK: Get String
    //
    
    func toString() -> String {
        return Mapper().toJSONString(self, prettyPrint: true) ?? ""
    }
    
    static func loadModel(_ json: String) -> FindRoomsModel {
        
        if json.length() > 0 {
            return Mapper<FindRoomsModel>().map(JSONString: json)!
        }
        
        return FindRoomsModel(map: Map(mappingType: .fromJSON, JSON: [:]))!
    }
}
