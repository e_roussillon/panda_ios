//
//  TokenModel.swift
//  panda
//
//  Created by Edouard Roussillon on 11/28/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class TokenModel: Mappable {
    
    var token:String?
    var refreshToken:String?
    
    
    fileprivate var userIdString: String?
    
    //
    //MARK: MAP
    //
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        token <- map["token"]
        refreshToken <- map["refresh_token"]
    }
}
