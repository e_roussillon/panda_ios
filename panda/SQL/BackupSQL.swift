//
//  BackupSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension BackupModel {
    
    func insert() {
        if let list = self.backup {
            BackupSQL.insertBackup(list)
        }
    }
 
    fileprivate class BackupSQL {
        
        static func insertBackup(_ backup: [String]) {
            for item in backup {
                do {
                    print("SQL -> \(item)")
                    try DBUtil.sharedInstance.prepare(item).run()
                } catch let error as NSError {
                    print("Error insertBackup -> \(item)")
                    print("Error -> \(error)")
                }
            }
        }
        
        
    }
}
