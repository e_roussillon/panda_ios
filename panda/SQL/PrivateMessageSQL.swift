//
//  PrivateMessageSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension PrivateMessageModel {
    
    static func creationTable(_ db: Connection) {
        PrivateMessageSQL.creationTable(db)
    }
    
    static func resetTable(_ db: Connection) {
        PrivateMessageSQL.resetTable(db)
    }
    
    static func getMessage(_ id: Int) -> PrivateMessageModel? {
        return PrivateMessageSQL.getMessage(id)
    }
    
    static func getMessageByIndex(_ index: String) -> PrivateMessageModel? {
        return PrivateMessageSQL.getMessageByIndex(index)
    }
    
    static func getPendingMessages() -> [PrivateMessageModel] {
        return PrivateMessageSQL.getPendingMessages()
    }
    
    static func getSentMessagesOrderAsc() -> [PrivateMessageModel] {
        return PrivateMessageSQL.getSentMessagesOrderAsc()
    }
    
    static func getMessagesByRoom(_ roomId: Int) -> [PrivateMessageModel] {
        return PrivateMessageSQL.getMessagesByRoom(roomId)
    }
    
    static func getMessagesNotSent() -> [PrivateMessageModel] {
        return PrivateMessageSQL.getMessagesNotSent()
    }
    
    func insert() {
        PrivateMessageSQL.insert(self)
    }
    
    func updateStatus() {
        PrivateMessageSQL.updateStatus(messageId: self.messageId, status: self.status)
    }
    
    func insertLite() -> Int {
        return Int(PrivateMessageSQL.insertLite(self))
    }

    func delete() {
        PrivateMessageSQL.deleteById(messageId: self.messageId)
    }
    
    //
    // MARK: PrivateMessageSQL
    //
    
    fileprivate class PrivateMessageSQL: SQLManagerDelegate {
        
        fileprivate static let privateMessageTable = Table("private_messages")
        fileprivate static let idField = Expression<Int64>("_id")
        fileprivate static let privateMessageIdField = Expression<Int?>("private_messages_id")
        fileprivate static let roomIdField = Expression<Int>("room_id")
        fileprivate static let fromUserIdField = Expression<Int>("from_user_id")
        fileprivate static let msgField = Expression<String>("msg")
        fileprivate static let statusField = Expression<Int>("status")
        fileprivate static let indexField = Expression<String>("index_row")
        fileprivate static let dataIdField = Expression<Int>("data_id")
        fileprivate static let urlThumbField = Expression<String>("url_thumb")
        fileprivate static let urlOriginalField = Expression<String>("url_original")
        fileprivate static let urlBlurField = Expression<String>("url_blur")
        fileprivate static let typeField = Expression<Int>("type")
        fileprivate static let sizeField = Expression<Float64>("size")
        fileprivate static let dataLocalField = Expression<String>("data_local")
        fileprivate static let activeField = Expression<Bool>("active")
        fileprivate static let timestampField = Expression<Double?>("inserted_at")
        
        //
        //MARK: Init
        //
        
        static func creationTable(_ db: Connection) {
            do {
                try db.run(privateMessageTable.create { t in
                    t.column(idField, primaryKey: true)
                    t.column(privateMessageIdField)
                    t.column(roomIdField)
                    t.column(fromUserIdField)
                    t.column(msgField)
                    t.column(statusField)
                    t.column(dataIdField, defaultValue: -1)
                    t.column(urlThumbField, defaultValue: "")
                    t.column(urlOriginalField, defaultValue: "")
                    t.column(urlBlurField, defaultValue: "")
                    t.column(typeField, defaultValue: DataType.none.rawValue)
                    t.column(sizeField, defaultValue: 0.0)
                    t.column(dataLocalField, defaultValue: "")
                    t.column(indexField, unique: true)
                    t.column(activeField, defaultValue: true)
                    t.column(timestampField)
                    })
            } catch {
                print("ERROR creationTable 'privateMessageTable'")
            }
        }
        
        static func resetTable(_ db: Connection) {
            do {
                try db.run(privateMessageTable.delete())
            } catch {
                print("ERROR resetTable 'privateMessageTable'")
            }
        }
        
        //
        //MARK: SQL
        //
        
        static func getMessage(_ id: Int) -> PrivateMessageModel? {
            
            let query = privateMessageTable.filter(privateMessageIdField == id)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                for item in stmt {
                    return self.initMessage(item)
                }
            } catch {
                print("Error insertFilter")
            }
            
            return nil
        }
        
        static func getMessageByIndex(_ index: String) -> PrivateMessageModel? {
            
            let query = privateMessageTable.filter(indexField == index)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                for item in stmt {
                    return self.initMessage(item)
                }
            } catch {
                print("Error insertFilter")
            }
            
            return nil
        }
        
        static func getMessagesByRoom(_ roomId: Int) -> [PrivateMessageModel] {
            var list: [PrivateMessageModel] = []
            let userId = SessionManager.sharedInstance.user.userId
            
            let select = privateMessageTable.filter(roomIdField == roomId && !(fromUserIdField == userId && (statusField == StatusType.deleting.rawValue || statusField == StatusType.deleted.rawValue)))
            do {
                let all = Array(try DBUtil.sharedInstance.prepare(select))

                for item in all {
                    if let message = self.initMessage(item) {
                        list.append(message)
                    }
                }
            } catch {
                print("Error insertFilter")
            }
            
            return list
        }
        
        static func getPendingMessages() -> [PrivateMessageModel] {
            var list: [PrivateMessageModel] = []
            let userId = SessionManager.sharedInstance.user.userId
            
//            TODO : CHANGE QUERY with '!=' see below
//            let select = privateMessageTable.filter((statusField == StatusType.sent.rawValue && fromUserIdField != userId) || (statusField == StatusType.read.rawValue && fromUserIdField == userId) || (statusField == StatusType.deleting.rawValue)
            let select = privateMessageTable.filter((statusField == StatusType.sent.rawValue && (fromUserIdField < userId || fromUserIdField > userId)) || (statusField == StatusType.read.rawValue && fromUserIdField == userId) || (statusField == StatusType.deleting.rawValue))
            do {
                let all = Array(try DBUtil.sharedInstance.prepare(select))
                
                for item in all {
                    if let message = self.initMessage(item) {
                        list.append(message)
                    }
                }
            } catch {
                print("Error insertFilter")
            }
            
            return list
        }
        
        static func getSentMessagesOrderAsc() -> [PrivateMessageModel] {
            var list: [PrivateMessageModel] = []
            let userId = SessionManager.sharedInstance.user.userId
            
            //            TODO : CHANGE QUERY with '!=' see below
            //            let select = privateMessageTable.filter((statusField == StatusType.Sent.rawValue && fromUserIdField != userId))
            let select = privateMessageTable.filter((statusField == StatusType.sent.rawValue && (fromUserIdField < userId || fromUserIdField > userId))).order(timestampField.asc)
            do {
                let all = Array(try DBUtil.sharedInstance.prepare(select))
                
                for item in all {
                    if let message = self.initMessage(item) {
                        list.append(message)
                    }
                }
            } catch {
                print("Error insertFilter")
            }
            
            return list
        }
        
        fileprivate static func initMessage(_ item: Row) -> PrivateMessageModel? {
            do {
                return try PrivateMessageModel.initMessage(roomId: item.get(roomIdField),
                                                       rowId: Int(item.get(idField) ),
                                                       messageId: Int(item.get(privateMessageIdField) ?? -1),
                                                       msg: item.get(msgField) ,
                                                       urlThumb: item.get(urlThumbField) ,
                                                       urlOriginal: item.get(urlOriginalField) ,
                                                       urlBlur: item.get(urlBlurField) ,
                                                       type: DataType(rawValue: item.get(typeField)) ?? DataType.none,
                                                       size: Float(item.get(sizeField)) ,
                                                       dataId:  item.get(dataIdField),
                                                       dataLocal: item.get(dataLocalField),
                                                       fromUserId: item.get(fromUserIdField),
                                                       insertedAt: item.get(timestampField) ?? Date().timeIntervalSince1970,
                                                       index: item.get(indexField),
                                                       active: item.get(activeField),
                                                       status: StatusType(rawValue: item.get(statusField)) ?? StatusType.error)
            } catch {
                print("Error initMessage")
                return nil
            }
        }
        
        static func insert(_ message: PrivateMessageModel) {
            
            if let roomId = message.roomId,
                let fromUserId = message.fromUserId,
                let insertedAt = message.insertedAt,
                let index = message.index {
               
                let msg = message.msg ?? ""
                let messageId = message.messageId ?? -1
                let status = message.status?.rawValue ?? StatusType.error.rawValue
                let dataId = message.dataId ?? -1
                let urlThumb = message.urlThumb?.absoluteString ?? ""
                let urlOriginal = message.urlOriginal?.absoluteString ?? ""
                let urlBlur = message.urlBlur?.absoluteString ?? ""
                let type = message.type?.rawValue ?? DataType.none.rawValue
                let dataLocal = message.dataLocal ?? ""
                let size = Float64((message.size ?? 0.0))
                
                
                let select = privateMessageTable.filter(indexField == index)
                
                do {
                    let all = Array(try DBUtil.sharedInstance.prepare(select))
                    
                    if all.count > 0 {
                        let update = select.update(privateMessageIdField <- messageId,
                                                msgField <- msg,
                                                fromUserIdField <- fromUserId,
                                                roomIdField <- roomId,
                                                statusField <- status,
                                                indexField <- index,
                                                urlThumbField <- urlThumb,
                                                urlOriginalField <- urlOriginal,
                                                urlBlurField <- urlBlur,
                                                typeField <- type,
                                                sizeField <- size,
                                                dataIdField <- dataId,
                                                dataLocalField <- dataLocal)
                        
                        do {
                            try DBUtil.sharedInstance.run(update)
                        } catch {
                            print("Error insertMessage - Private --> \(error)")
                        }
                    } else {
                        let insert = privateMessageTable.insert(or: .replace,
                                                            privateMessageIdField <- messageId,
                                                            msgField <- msg,
                                                            fromUserIdField <- fromUserId,
                                                            roomIdField <- roomId,
                                                            statusField <- status,
                                                            indexField <- index,
                                                            urlThumbField <- urlThumb,
                                                            urlOriginalField <- urlOriginal,
                                                            urlBlurField <- urlBlur,
                                                            typeField <- type,
                                                            sizeField <- size,
                                                            dataIdField <- dataId,
                                                            dataLocalField <- dataLocal,
                                                            timestampField <- Double(insertedAt.timeIntervalSince1970))
                        
                        do {
                            try DBUtil.sharedInstance.run(insert)
                        } catch {
                            print("Error insertMessage - Private --> \(error)")
                        }
                    }
                } catch {
                    print("Error insertFilter")
                }
            }
        }
        
        static func insertLite(_ message: PrivateMessageModel) -> Int64 {
            
            if let msg = message.msg,
                let roomId = message.roomId,
                let fromUserId = message.fromUserId,
                let index = message.index {
                
                let dataId = message.dataId ?? -1
                let urlThumb = message.urlThumb?.absoluteString ?? ""
                let urlOriginal = message.urlOriginal?.absoluteString ?? ""
                let urlBlur = message.urlBlur?.absoluteString ?? ""
                let type = message.type?.rawValue ?? DataType.none.rawValue
                let dataLocal = message.dataLocal ?? ""
                let size = Float64(0)
                
                let insert = privateMessageTable.insert(or: .replace,
                                                        msgField <- msg,
                                                        roomIdField <- roomId,
                                                        fromUserIdField <- fromUserId,
                                                        statusField <- StatusType.sending.rawValue,
                                                        indexField <- index,
                                                        dataIdField <- dataId,
                                                        urlThumbField <- urlThumb,
                                                        urlOriginalField <- urlOriginal,
                                                        urlBlurField <- urlBlur,
                                                        typeField <- type,
                                                        sizeField <- size,
                                                        dataLocalField <- dataLocal,
                                                        timestampField <- Double(Date().timeIntervalSince1970))
                
                do {
                    let rowid = try DBUtil.sharedInstance.run(insert)
                    return rowid
                } catch {
                     print("Error insertMessageLite - Private --> \(error)")
                    return -1
                }
            }
            
            return -1
        }
        
        static func updateStatus(messageId: Int?, status: StatusType?) {
            
            if let id = messageId,
                let statusId = status?.rawValue {
                
                let alice = privateMessageTable.filter(privateMessageIdField == id)
                let insert = alice.update(statusField <- statusId)
                
                do {
                    if try DBUtil.sharedInstance.run(insert) > 0 {
                        print("Message status updated OK")
                    }
                } catch {
                    print("Error updateStatus")
                }
            }
            
        }
        
        static func getMessagesNotSent() -> [PrivateMessageModel] {
            let sessionManager = SessionManager.sharedInstance
            let sessionUserId = sessionManager.user.userId
            let pseudo = sessionManager.user.userPseudo
            
            let sql = " ".join(["SELECT",
                                    "pm.index_row,",
                                    "pm.room_id,",
                                    "u.users_id,",
                                    "u.pseudo,",
                                    "pm.status as message_status,",
                                    "u.status,",
                                    "u.last_action,",
                                    "pm.private_messages_id,",
                                    "pm.msg,",
                                    "pm.data_id,",
                                    "pm.data_local,",
                                    "pm.url_thumb,",
                                    "pm.url_original,",
                                    "pm.url_blur,",
                                    "pm.type,",
                                    "pm.size,",
                                    "pm.active,",
                                    "pm.inserted_at",
                                "FROM private_messages AS pm",
                                "INNER JOIN private_room_users AS pru",
                                    "ON pru.room_id = pm.room_id AND pru.user_id <> \(sessionUserId)",
                                "INNER JOIN users AS u",
                                    "ON u.users_id = pru.user_id",
                                "WHERE pm.status = \(StatusType.sending.rawValue) AND pm.from_user_id = \(sessionUserId)"])
            
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(sql)
                var list: [PrivateMessageModel] = []
                for row in stmt {
                    if let index = row[0] as? String,
                        let roomId = row[1] as? Int64,
                        let userId = row[2] as? Int64,
                        let userName = row[3] as? String,
                        let status = row[4] as? Int64 {
                        
                        let friendshipStatus = row[5] as? Int64
                        let friendshipLastAction = row[6] as? Int64
                        let privateMessagesId = row[7] as? Int64
                        let msg = row[8] as? String
                        let dataId = row[9] as? Int64
                        let dataLocal = row[10] as? String
                        let urlThumb = row[11] as? String
                        let urlOriginal = row[12] as? String
                        let urlBlur = row[13] as? String
                        let type = row[14] as? Int64
                        let size = row[15] as? Float64
                        let active = row[16] as? Bool
                        let insertedAt = row[17] as? Double
                        
                        let message = PrivateMessageModel.initMessage(roomId: Int(roomId),
                                                        messageId: Int(privateMessagesId ?? -1),
                                                        msg: msg ?? "",
                                                        urlThumb: urlThumb ?? "",
                                                        urlOriginal: urlOriginal ?? "",
                                                        urlBlur: urlBlur ?? "",
                                                        type: DataType(rawValue: Int(type ?? -1)) ?? DataType.none,
                                                        size: Float(size ?? 0.0),
                                                        dataId:  Int(dataId ?? -1),
                                                        dataLocal: dataLocal ?? "",
                                                        fromUserId: sessionUserId,
                                                        fromUserName: pseudo,
                                                        toUserId: Int(userId) ,
                                                        toUserName: userName,
                                                        friendshipStatus: FriendshipType(rawValue: Int(friendshipStatus ?? -1)) ?? FriendshipType.notFriend,
                                                        friendshipLastAction: Int(friendshipLastAction ?? -1),
                                                        insertedAt: insertedAt ?? NSDate().timeIntervalSince1970,
                                                        index: index,
                                                        active: active ?? true,
                                                        status: StatusType(rawValue: Int(status)) ?? StatusType.sending)
                        
                        list.append(message)
                    }
                }
                
                return list
            } catch {
                print("error \(error)")
                return []
            }
        }

        static func deleteById(messageId: Int?) {
            if let id = messageId {
                privateMessageTable.filter(privateMessageIdField == id).delete()
            }
        }
        
    }
}

