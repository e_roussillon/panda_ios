//
//  UserSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension UserModel {
    fileprivate static let sessionManager = SessionManager.sharedInstance
 
    static func creationTable(_ db: Connection) {
        UserSQL.creationTable(db)
    }
    
    static func resetTable(_ db: Connection) {
        UserSQL.resetTable(db)
    }
    
    static func getCurrentUser() -> UserModel? {
        let user = self.sessionManager.user.userId
        return UserSQL.getUser(user)
    }
    
    static func getUser(_ userId: Int) -> UserModel {
        return UserSQL.getUser(userId)
    }
    
    static func insert(_ userId: Int, userName: String, friendshipStatus: FriendshipType, friendshipLastAction: Int) {
        UserSQL.insertUser(userId, userName: userName, friendshipStatus: friendshipStatus.rawValue, friendshipLastAction: friendshipLastAction)
    }
    
    static func getUsersByFriendshipTypeFriend() -> [UserModel] {
        return UserSQL.getUsersByFriendshipTypeFriend()
    }
    
    static func getUsersByFriendshipTypeBlocked() -> [UserModel] {
        return UserSQL.getUsersByFriendshipTypeBlocked()
    }
    
    static func getUsersByFriendshipTypePending() -> [UserModel] {
        return UserSQL.getUsersByFriendshipTypePending()
    }
    
    static func updateUsers(_ users: [UserModel]) {
        for user in users {
            user.update()
        }
    }
    
    static func getCountUsersByFriendshipTypePending() -> Int {
         return UserSQL.getCountUsersByFriendshipTypePending()
    }
    
    func update() {
        UserSQL.updateUser(self)
    }
    
    
    
    fileprivate class UserSQL: SQLManagerDelegate {
        
        fileprivate static let sessionManager = SessionManager.sharedInstance
        
        fileprivate static let userTable = Table("users")
        fileprivate static let idField = Expression<Int64>("_id")
        fileprivate static let userIdField = Expression<Int>("users_id")
        fileprivate static let emailField = Expression<String?>("email")
        fileprivate static let pseudoField = Expression<String>("pseudo")
        fileprivate static let descriptionField = Expression<String?>("description")
        fileprivate static let birthdayField = Expression<String?>("birthday")
        fileprivate static let genderField = Expression<Int?>("gender")
        fileprivate static let dataUrlThumbField = Expression<String?>("url_thumb")
        fileprivate static let dataUrlOriginalField = Expression<String?>("url_original")
        fileprivate static let dataUrlBlurField = Expression<String?>("url_blur")
        fileprivate static let isOnlineField = Expression<Bool?>("is_online")
        fileprivate static let lastSeenField = Expression<Double?>("last_seen")
        fileprivate static let friendshipStatusField = Expression<Int>("status")
        fileprivate static let friendshipLastActionField = Expression<Int>("last_action")
        fileprivate static let reportedField = Expression<Bool?>("reported")
        fileprivate static let timestampField = Expression<Double>("updated_at")
        
        //
        //MARK: Init
        //
        
        static func creationTable(_ db: Connection) {
            do {
                try db.run(userTable.create { t in
                    t.column(idField, primaryKey: true)
                    t.column(userIdField, unique: true)
                    t.column(emailField)
                    t.column(pseudoField)
                    t.column(descriptionField)
                    t.column(birthdayField)
                    t.column(genderField)
                    t.column(dataUrlThumbField)
                    t.column(dataUrlOriginalField)
                    t.column(dataUrlBlurField)
                    t.column(isOnlineField, defaultValue: false)
                    t.column(lastSeenField)
                    t.column(friendshipStatusField, defaultValue: FriendshipType.notFriend.rawValue)
                    t.column(friendshipLastActionField, defaultValue: -1)
                    t.column(reportedField, defaultValue: true)
                    t.column(timestampField)
                    })
            } catch {
                print("ERROR creationTable 'userTable'")
            }
        }
        
        static func resetTable(_ db: Connection) {
            do {
                try db.run(userTable.delete())
            } catch {
                print("ERROR resetTable 'userTable'")
            }
        }
        
        //
        //MARK: SQL
        //
        
        static func getUser(_ userId: Int) -> UserModel {
            let query = userTable.filter(userIdField == userId)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                
                for row in stmt {
                    let userId = row[userIdField]
                    let email = row[emailField]
                    let pseudo = row[pseudoField]
                    let gender = row[genderField]
                    let description = row[descriptionField]
                    let birthday = row[birthdayField]
                    let dataUrlThumb = row[dataUrlThumbField]
                    let dataUrlOriginal = row[dataUrlOriginalField]
                    let dataUrlBlur = row[dataUrlBlurField]
                    let isOnline = row[isOnlineField]
                    let lastSeen = row[lastSeenField]
                    let friendshipStatus = row[friendshipStatusField]
                    let friendshipLastAction = row[friendshipLastActionField]
                    let reported = row[reportedField]
                    let timestamp = row[timestampField]
                    
                    
                    return UserModel.initModel(Int(userId),
                                               email: email ?? "",
                                               pseudo: pseudo ,
                                               gender: GenderType(rawValue: gender ?? GenderType.none.rawValue),
                                               birthday: birthday,
                                               details: description,
                                               urlBlur: dataUrlBlur,
                                               urlThumb: dataUrlThumb,
                                               urlOriginal: dataUrlOriginal,
                                               isOnline: isOnline,
                                               lastSeen: lastSeen,
                                               friendshipStatus: friendshipStatus,
                                               friendshipLastAction: friendshipLastAction,
                                               reported: reported ?? true,
                                               updatedAt: timestamp )
                }
            } catch {
                
            }
            
            return UserModel.initModel(userId, email: "", pseudo: "", gender: GenderType.none, birthday: nil, details: nil, urlBlur: nil, urlThumb: nil, urlOriginal: nil, isOnline: false, lastSeen: 0, friendshipStatus: FriendshipType.notFriend.rawValue, friendshipLastAction: -1, reported: true, updatedAt: 0)
        }
        
        static func getUsersByFriendshipTypeFriend() -> [UserModel] {
            let user = self.sessionManager.user.userId
//            TODO : CHANGE QUERY with '!=' see below
//            let query = userTable.filter(friendshipStatusField == FriendshipType.Friend.rawValue && userIdField != user).order(pseudoField)
            
            let query = userTable.filter(friendshipStatusField == FriendshipType.friend.rawValue && (userIdField < user || userIdField > user)).order(pseudoField)
            
            return self.getUsersList(query)
        }
        
        static func getUsersByFriendshipTypeBlocked() -> [UserModel] {
            let user = self.sessionManager.user.userId
//            TODO : CHANGE QUERY with '!=' see below
//            let query = userTable.filter((friendshipStatusField == FriendshipType.BlockedBeforeBeFriend.rawValue || friendshipStatusField == FriendshipType.BlockedAfterBeFriend.rawValue) && friendshipLastActionField == user && userIdField != user).order(pseudoField)
            
            let query = userTable.filter((friendshipStatusField == FriendshipType.blockedBeforeBeFriend.rawValue || friendshipStatusField == FriendshipType.blockedAfterBeFriend.rawValue) && friendshipLastActionField == user && (userIdField < user || userIdField > user)).order(pseudoField)
            
            return self.getUsersList(query)
        }
        
        static func getUsersByFriendshipTypePending() -> [UserModel] {
            let user = self.sessionManager.user.userId
//            TODO : CHANGE QUERY with '!=' see below
//            let query = userTable.filter(friendshipStatusField == FriendshipType.Pending.rawValue || (friendshipStatusField == FriendshipType.RefusedBeforeBeFriend.rawValue && friendshipLastActionField != user) && userIdField != user).order(pseudoField)

            let query = userTable.filter(friendshipStatusField == FriendshipType.pending.rawValue && (userIdField < user || userIdField > user)).order(pseudoField)
            
            return self.getUsersList(query)
        }
        
        static func getCountUsersByFriendshipTypePending() -> Int {
            let user = self.sessionManager.user.userId

//            TODO : CHANGE QUERY with '!=' see below
//            let query = userTable.filter(friendshipStatusField == FriendshipType.Pending.rawValue && friendshipLastActionField != user && userIdField != user).order(pseudoField)

            let query = userTable.filter(friendshipStatusField == FriendshipType.pending.rawValue && (friendshipLastActionField < user || friendshipLastActionField > user) && (userIdField < user || userIdField > user)).order(pseudoField)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                var count = 0
                for _ in stmt {
                    count = count + 1
                }
                return count
            } catch {
                
            }
            return 0
        }
        
        fileprivate static func getUsersList(_ query: Table) -> [UserModel] {
            var list: [UserModel] = []
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                
                for row in stmt {
                    let userId = row[userIdField]
                    let email = row[emailField]
                    let pseudo = row[pseudoField]
                    let gender = row[genderField]
                    let description = row[descriptionField]
                    let birthday = row[birthdayField]
                    let dataUrlThumb = row[dataUrlThumbField]
                    let dataUrlOriginal = row[dataUrlOriginalField]
                    let dataUrlBlur = row[dataUrlBlurField]
                    let isOnline = row[isOnlineField]
                    let lastSeen = row[lastSeenField]
                    let friendshipStatus = row[friendshipStatusField]
                    let friendshipLastAction = row[friendshipLastActionField]
                    let reported = row[reportedField]
                    let timestamp = row[timestampField]
                    
                    let user = UserModel.initModel(Int(userId),
                                                   email: email ?? "",
                                                   pseudo: pseudo ,
                                                   gender: GenderType(rawValue: gender ?? GenderType.none.rawValue),
                                                   birthday: birthday,
                                                   details: description,
                                                   urlBlur: dataUrlBlur,
                                                   urlThumb: dataUrlThumb,
                                                   urlOriginal: dataUrlOriginal,
                                                   isOnline: isOnline,
                                                   lastSeen: lastSeen,
                                                   friendshipStatus: friendshipStatus,
                                                   friendshipLastAction: friendshipLastAction,
                                                   reported: reported ?? true,
                                                   updatedAt: timestamp )
                    
                    list.append(user)
                }
            } catch {
                
            }
            
            return list
        }
        
        static func updateUser(_ user: UserModel) {
            
            if let id = user.userId,
                let email = user.email,
                let pseudo = user.pseudo
            {
                
                let details = user.details ?? nil
                let birthday = user.birthday != nil ? user.birthday!.getDateToString() : ""
                let gender = user.gender?.rawValue ?? GenderType.none.rawValue
                let dataUrlThumb = user.urlThumb?.absoluteString ?? ""
                let dataUrlOriginal = user.urlOriginal?.absoluteString ?? ""
                let dataUrlBlur = user.urlBlur?.absoluteString ?? ""
                let isOnline = user.isOnline ?? false
                let friendshipStatus = user.friendshipStatus?.rawValue ?? FriendshipType.notFriend.rawValue
                let friendshipLastAction = user.friendshipLastAction ?? -1
                let timestamp = user.updatedAt?.timestamp ?? 0
                let reported = user.reported ?? true
                let lastSeen = user.lastSeen?.timestamp
                
                let alice = userTable.filter(userIdField == id)
                let insert = alice.update(emailField <- email,
                                          pseudoField <- pseudo,
                                          descriptionField <- details,
                                          birthdayField <- birthday,
                                          genderField <- gender,
                                          dataUrlThumbField <- dataUrlThumb,
                                          dataUrlOriginalField <- dataUrlOriginal,
                                          dataUrlBlurField <- dataUrlBlur,
                                          isOnlineField <- isOnline,
                                          lastSeenField <- lastSeen,
                                          friendshipStatusField <- friendshipStatus,
                                          friendshipLastActionField <- friendshipLastAction,
                                          reportedField <- reported,
                                          timestampField <- timestamp)
                do {
                    if try DBUtil.sharedInstance.run(insert) > 0 {
                        print("User save OK")
                    } else {
                        self.insertUser(id, userName: pseudo, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
                    }
                } catch {
                    print("Error updateUser")
                }
            }
        }
                
        static func insertUser(_ userId: Int, userName: String, friendshipStatus: Int, friendshipLastAction: Int) {
            let insert = userTable.insert(or: .replace,
                                          userIdField <- userId,
                                          pseudoField <- userName,
                                          friendshipStatusField <- friendshipStatus,
                                          friendshipLastActionField <- friendshipLastAction,
                                          timestampField <- Date().timestamp)
            
            do {
                try DBUtil.sharedInstance.run(insert)
            } catch {
                print("Error insertUser")
            }
        }
        
    }
    
}
