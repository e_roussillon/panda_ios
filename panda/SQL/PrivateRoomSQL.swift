//
//  PrivateRoomSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension PrivateChatModel {

    static func creationTable(_ db: Connection) {
        PrivateRoomSQL.creationTable(db)
        PrivateRoomUserSQL.creationTable(db)
    }

    static func resetTable(_ db: Connection) {
        PrivateRoomSQL.resetTable(db)
        PrivateRoomUserSQL.resetTable(db)
    }

    static func getRooms() -> [PrivateChatModel] {
        return PrivateRoomSQL.getRooms()
    }

    static func getRoom(_ userId: Int) -> Int {
        return PrivateRoomSQL.getRoom(userId)
    }

    static func getMessagesCount() -> Int {
        return PrivateRoomSQL.getMessagesCount()
    }

    static func insertRoom(_ roomId: Int, name: String) {
        PrivateRoomSQL.insertRoom(roomId, name: name)
    }

    static func insertUsersRoom(_ roomId: Int, toUserId: Int) {
        PrivateRoomUserSQL.insertUsersRoom(roomId, toUserId: toUserId)
    }

    fileprivate class PrivateRoomSQL: SQLManagerDelegate {
        fileprivate static let sessionManager = SessionManager.sharedInstance

        fileprivate static let privateRoomTable = Table("private_rooms")
        fileprivate static let idField = Expression<Int64>("_id")
        fileprivate static let privateRoomIdField = Expression<Int>("private_rooms_id")
        fileprivate static let nameField = Expression<String>("name")
        fileprivate static let isGroupField = Expression<Bool>("is_group")
        fileprivate static let dataIdField = Expression<Int?>("data_id")
        fileprivate static let timestampField = Expression<Double>("updated_at")

        //
        //MARK: Init
        //

        static func creationTable(_ db: Connection) {
            do {
                try db.run(privateRoomTable.create { t in
                    t.column(idField, primaryKey: true)
                    t.column(privateRoomIdField, unique: true)
                    t.column(nameField)
                    t.column(isGroupField)
                    t.column(dataIdField)
                    t.column(timestampField)
                    })
            } catch {
                print("ERROR creationTable 'privateRoomTable'")
            }
        }

        static func resetTable(_ db: Connection) {
            do {
                try db.run(privateRoomTable.delete())
            } catch {
                print("ERROR resetTable 'privateRoomTable'")
            }
        }

        //
        //MARK: SQL
        //

        static func getRooms() -> [PrivateChatModel] {
            let currentUserId = self.sessionManager.user.userId

            let sql = " ".join(["SELECT",
                                    "p_r.private_rooms_id,",
                                    "p_r.is_group,",
                                    "case when p_r.is_group = 1 then p_r.name else u.pseudo end AS name,",
                                    "case when u.url_thumb is null then '' else u.url_thumb end AS url_thumb,",
                                    "case when u.url_original is null then '' else u.url_original end AS url_original,",
                                    "case when u.url_blur is null then '' else url_blur end AS url_blur,",
                                    "u.status AS user_status,",
                                    "u.last_action AS user_status_last_action,",
                                    "p_m_l.msg,",
                                    "p_m_l.type,",
                                    "p_m_l.inserted_at AS msg_sent,",
                                    "case when p_m_c.num_of_msg_not_read is null then 0 else p_m_c.num_of_msg_not_read end AS num_of_msg_not_read,",
                                    "u.users_id,",
                                    "u.is_online,",
                                    "u.last_seen,",
                                    "p_m_l._id",
                                "FROM private_rooms AS p_r",
                                "JOIN private_room_users AS p_r_u",
                                    "ON p_r.private_rooms_id = p_r_u.room_id",
                                "JOIN users AS u",
                                    "ON p_r_u.user_id = u.users_id",
                                "JOIN ( SELECT p_m_1._id,",
                                        "p_m_1.room_id,",
                                        "p_m_1.inserted_at,",
                                        "p_m_1.msg,",
                                        "p_m_1.type",
                                        "FROM private_messages p_m_1",
                                        "ORDER BY p_m_1.private_messages_id DESC",
                                        "LIMIT 1",
                                      ") AS p_m_l ON p_m_l.room_id = p_r.private_rooms_id",
                                "LEFT JOIN ( SELECT p_m.room_id,",
                                                    "COUNT(p_m.msg) AS num_of_msg_not_read",
                                            "FROM private_messages AS p_m",
                                            "WHERE p_m.from_user_id <> \(currentUserId)",
                                                    "AND p_m.status < \(StatusType.read.rawValue)",
                                            "GROUP BY p_m.room_id",
                                           ") AS p_m_c ON p_m_c.room_id = p_r.private_rooms_id",
                                "WHERE u.users_id <> \(currentUserId)",
                                        "AND (u.status <> \(FriendshipType.blockedBeforeBeFriend.rawValue)",
                                        "AND u.status <> \(FriendshipType.blockedAfterBeFriend.rawValue))",
                                "ORDER BY p_m_l.inserted_at DESC"])

            do {
                let stmt = try DBUtil.sharedInstance.prepare(sql)
                var list: [PrivateChatModel] = []
                for row in stmt {
                    if let roomId = row[0] as? Int64,
                        let isGroup = row[1] as? Int64,
                        let nameRoom = row[2] as? String,
                        let urlThumb = row[3] as? String,
                        let urlOriginal = row[4] as? String,
                        let urlBlur = row[5] as? String,
                        let msg = row[8] as? String,
                        let type = row[9] as? Int64,
                        let msgSent = row[10] as? Double,
                        let numOfMsgNotRead = row[11] as? Int64,
                        let userId = row[12] as? Int64,
                        let rowId = row[15] as? Int64 {

                        let userStatus = row[6] as? Int64 ?? Int64(FriendshipType.notFriend.rawValue)
                        let userStatusLastAction = row[7] as? Int64 ?? Int64(-1)
                        let isOnline = row[13] as? Int64 ??  Int64(0)
                        let last_seen = (row[14] as? Double)?.toDate() ?? nil

                        list.append(PrivateChatModel(
                                                    rowId: Int(rowId),
                                                    roomId: Int(roomId),
                                                    isGroup: Int(isGroup),
                                                    nameRoom: nameRoom,
                                                    urlThumb: urlThumb,
                                                    urlOriginal: urlOriginal,
                                                    urlBlur: urlBlur,
                                                    type: Int(type),
                                                    userStatus: Int(userStatus),
                                                    userStatusLastAction: Int(userStatusLastAction),
                                                    msg: msg,
                                                    msgSent: msgSent,
                                                    numOfMsgNotRead: Int(numOfMsgNotRead),
                                                    userId: Int(userId),
                                                    userIsOnline: Int(isOnline) == 1 ? true : false,
                                                    userLastSeen: last_seen))
                    }
                }

                return list
            } catch {
                print("error \(error)")
                return []
            }

        }

        static func getRoom(_ userId: Int) -> Int {
            let currentUserId = self.sessionManager.user.userId

            let sql = "SELECT private_rooms_id, user_id from private_rooms JOIN private_room_users on private_rooms_id = room_id where user_id == \(userId) OR user_id == \(currentUserId)"

            do {
                let stmt = try DBUtil.sharedInstance.prepare(sql)

                for row in stmt {
                    if let roomId = row[0] as? Int64,
                        let userIdDB = row[1] as? Int64, Int(userIdDB) != currentUserId
                    {
                        return Int(roomId)
                    }
                }

                return -1
            } catch {
                return -1
            }
        }

        static func insertRoom(_ roomId: Int, name: String) {
            let insert = privateRoomTable.insert(or: .replace,
                                                 privateRoomIdField <- roomId,
                                                 nameField <- name,
                                                 isGroupField <- false,
                                                 timestampField <- Double(Date().timeIntervalSince1970))

            do {
                try DBUtil.sharedInstance.run(insert)
            } catch {
                print("Error insertMessage - Private")
            }
        }

        static func getMessagesCount() -> Int {
            let currentUserId = self.sessionManager.user.userId

            let sql = "SELECT  COUNT(p_m.msg) as num_of_msg_not_read FROM private_messages AS p_m WHERE p_m.from_user_id <> \(currentUserId) and p_m.status < \(StatusType.read.rawValue)"

            do {
                let stmt = try DBUtil.sharedInstance.prepare(sql)

                for row in stmt {
                    if let count = row[0] as? Int64
                    {
                        return Int(count)
                    }
                }

                return 0
            } catch {
                return 0
            }
        }


    }

    fileprivate class PrivateRoomUserSQL: SQLManagerDelegate {
        fileprivate static let sessionManager = SessionManager.sharedInstance
        fileprivate static let privateRoomUserTable = Table("private_room_users")
        fileprivate static let userIdField = Expression<Int>("user_id")
        fileprivate static let roomIdField = Expression<Int>("room_id")
        fileprivate static let timestampField = Expression<String>("updated_at")

        //
        //MARK: Init
        //

        static func creationTable(_ db: Connection) {
            do {
                try db.run(privateRoomUserTable.create { t in
                    t.column(userIdField)
                    t.column(roomIdField)
                    t.column(timestampField)
                    t.unique(userIdField, roomIdField)
                    })
            } catch {
                print("ERROR creationTable 'privateRoomUserTable'")
            }
        }

        static func resetTable(_ db: Connection) {
            do {
                try db.run(privateRoomUserTable.delete())
            } catch {
                print("ERROR resetTable 'privateRoomUserTable'")
            }
        }

        //
        //MARK: SQL
        //

        static func insertUsersRoom(_ roomId: Int, toUserId: Int) {
            let userId = sessionManager.user.userId
            let select = privateRoomUserTable.filter(roomIdField == roomId && (userIdField == userId || userIdField == toUserId))
            do {

                if Array(try DBUtil.sharedInstance.prepare(select)).count < 2 {
                    let insert = privateRoomUserTable.insert(or: .replace,
                                                             userIdField <- userId,
                                                             roomIdField <- roomId,
                                                             timestampField <- Date().getDateToFullString())
                    let insert2 = privateRoomUserTable.insert(or: .replace,
                                                              userIdField <- toUserId,
                                                              roomIdField <- roomId,
                                                              timestampField <- Date().getDateToFullString())

                    do {
                        try DBUtil.sharedInstance.run(insert)
                        try DBUtil.sharedInstance.run(insert2)
                    } catch {
                        print("Error insertUsersRoom - Private")
                    }
                }
            } catch {
                print("Error insertFilter")
            }
        }
    }


}
