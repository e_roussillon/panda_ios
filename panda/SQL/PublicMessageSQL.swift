//
//  PublicMessageSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension PublicMessageModel {
    
    static func creationTable(_ db: Connection) {
        PublicMessageSQL.creationTable(db)
    }
    
    static func resetTable(_ db: Connection) {
        PublicMessageSQL.resetTable(db)
    }
    
    static func updateMessageWithFilter(filter: FilterModel) {
        PublicMessageSQL.updateMessageWithFilter(filter: filter)
    }
    
    static func removeMessageWithFilter(filter: FilterModel) {
        PublicMessageSQL.removeMessageWithFilter(filter: filter)
    }
    
    static func getAllPublicMessageGeneral() -> [PublicMessageModel] {
        return PublicMessageSQL.getAllPublicMessage(true)
    }
    
    static func getAllPublicMessageSale() -> [PublicMessageModel] {
        return PublicMessageSQL.getAllPublicMessage(false)
    }
    
    func insert() {
        PublicMessageSQL.insert(self)
    }
    
    func insertLight() -> Int {
        return Int(PublicMessageSQL.insertLite(self))
    }
    
    func isContaineWelcomeMessage() -> Bool {
        return PublicMessageSQL.isContaineMessage(self.msg ?? "")
    }
    
    
    fileprivate class PublicMessageSQL: SQLManagerDelegate {
        fileprivate static let sessionManager = SessionManager.sharedInstance
        
        fileprivate static let publicMessageTable = Table("public_messages")
        fileprivate static let idField = Expression<Int64>("_id")
        fileprivate static let publicMessageIdField = Expression<Int?>("public_messages_id")
        fileprivate static let isGeneralField = Expression<Bool>("is_general")
        fileprivate static let msgField = Expression<String>("msg")
        fileprivate static let fromUserIdField = Expression<Int?>("from_user_id")
        fileprivate static let fromUserNameField = Expression<String>("from_user_name")
        fileprivate static let isReadField = Expression<Bool>("is_read")
        fileprivate static let urlThumbField = Expression<String>("url_thumb")
        fileprivate static let urlOriginalField = Expression<String>("url_original")
        fileprivate static let urlBlurField = Expression<String>("url_blur")
        fileprivate static let typeField = Expression<Int>("type")
        fileprivate static let indexField = Expression<String>("index_row")
        fileprivate static let statusField = Expression<Int>("status")
        fileprivate static let insertedAtField = Expression<Double?>("inserted_at")
        fileprivate static let isHightlightField = Expression<Bool>("is_hightlight")
        fileprivate static let isWelcomeMessageField = Expression<Bool?>("is_welcome_message")
        
        //
        //MARK: Init
        //
        
        static func creationTable(_ db: Connection) {
            do {
                try db.run(publicMessageTable.create { t in
                    t.column(idField, primaryKey: true)
                    t.column(publicMessageIdField)
                    t.column(msgField)
                    t.column(isGeneralField)
                    t.column(fromUserIdField)
                    t.column(fromUserNameField)
                    t.column(indexField, unique: true)
                    t.column(isReadField, defaultValue: false)
                    t.column(urlThumbField, defaultValue: "")
                    t.column(urlOriginalField, defaultValue: "")
                    t.column(urlBlurField, defaultValue: "")
                    t.column(typeField, defaultValue: DataType.none.rawValue)
                    t.column(statusField, defaultValue: StatusType.error.rawValue)
                    t.column(insertedAtField, defaultValue: Date().timestamp)
                    t.column(isHightlightField, defaultValue: false)
                    t.column(isWelcomeMessageField, defaultValue: false)
                    })
            } catch {
                print("ERROR creationTable 'publicMessageTable'")
            }
        }
        
        static func resetTable(_ db: Connection) {
            do {
                try db.run(publicMessageTable.delete())
            } catch {
                print("ERROR resetTable 'publicMessageTable'")
            }
        }
        
        //
        //MARK: SQL
        //
        fileprivate static func getAllPublicMessage(_ isGeneral: Bool) -> [PublicMessageModel] {
            let query = publicMessageTable.filter(isGeneralField == isGeneral).order(insertedAtField.asc).limit(100)
            var list: [PublicMessageModel] = []
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                
                for row in stmt {
                    list.append(self.getMessage(row: row))
                }
            } catch {
                
            }
            
            return list
        }
        
        static func getPublicMessage(_ isGeneral: Bool, index: String) -> PublicMessageModel? {
            let query = publicMessageTable.filter(isGeneralField == isGeneral && indexField == index)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                
                for row in stmt {
                    return self.getMessage(row: row)
                }
            } catch {
            }
            
            return nil
        }
        
        fileprivate static func getMessage(row: Row) -> PublicMessageModel {
            
            let id = row[publicMessageIdField]
            let msg = row[msgField]
            let isGeneral = row[isGeneralField]
            let fromUserId = row[fromUserIdField]
            let fromUserName = row[fromUserNameField]
            let urlThumb = row[urlThumbField]
            let urlBlur = row[urlBlurField]
            let urlOriginal = row[urlOriginalField]
            let type = row[typeField]
            let insertedAt = row[insertedAtField]
            let index = row[indexField]
            let isHightlight = row[isHightlightField]
            let status = row[statusField]
            let isWelcomeMessage = row[isWelcomeMessageField]
            
            return PublicMessageModel.initMessage(messageId: id ?? -1,
                                                  msg: msg,
                                                  urlThumb: urlThumb,
                                                  urlOriginal: urlOriginal,
                                                  urlBlur: urlBlur,
                                                  type: DataType(rawValue: type) ?? DataType.none,
                                                  isGeneral: isGeneral,
                                                  fromUserId: fromUserId ?? -1,
                                                  fromUserName: fromUserName,
                                                  insertedAt: insertedAt ?? Date().timestamp,
                                                  index: index,
                                                  status: StatusType(rawValue: status) ?? StatusType.error,
                                                  isHighlight: isHightlight,
                                                  isWelcomeMessage: isWelcomeMessage ?? false)
        }
        
        static func updateMessageWithFilter(filter: FilterModel) {
            let query = publicMessageTable
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                var list: [PublicMessageModel] = []
                for row in stmt {
                    let message = self.getMessage(row: row)
                    if let msg = message.msg {
                        if msg.lowercased().contains(filter.name!.lowercased()) {
                            message.isHighlight = filter.isHighlight!
                            list.append(message)
                        }
                    }
                }
                
                for item in list {
                    self.insert(item)
                }
            } catch {
                
            }
            
        }
        
        static func removeMessageWithFilter(filter: FilterModel) {
            let query = publicMessageTable
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                var list: [PublicMessageModel] = []
                for row in stmt {
                    let message = self.getMessage(row: row)
                    if let msg = message.msg {
                        if msg.lowercased().contains(filter.name!.lowercased()) {
                            message.isHighlight = false
                            list.append(message)
                        }
                    }
                }
                
                for item in list {
                    self.insert(item)
                }
            } catch {
                
            }
            
        }
        
        static func insert(_ message: PublicMessageModel) {
            
            if let msg = message.msg,
                let fromUserId = message.fromUserId,
                let fromUserName = message.fromUserName,
                let insertedAt = message.insertedAt,
                let index = message.index,
                let isGeneral = message.isGeneral,
                let status = message.status?.rawValue {
                
                let id = message.messageId ?? -1
                let isHightlight = message.isHighlight ?? false
                let urlThumb = message.urlThumb?.absoluteString ?? ""
                let urlOriginal = message.urlOriginal?.absoluteString ?? ""
                let urlBlur = message.urlBlur?.absoluteString ?? ""
                let type = message.type?.rawValue ?? DataType.none.rawValue
                let isWelcomeMessage = message.isWelcomeMessage ?? false
                
                var insert: SQLite.Insert!
                
                if let idRow = message.rowId {
                    insert = publicMessageTable.insert(or: .replace,
                                                       idField <- Int64(idRow),
                                                       indexField <- index,
                                                       publicMessageIdField <- id,
                                                       isGeneralField <- isGeneral,
                                                       msgField <- msg,
                                                       fromUserIdField <- fromUserId,
                                                       fromUserNameField <- fromUserName,
                                                       statusField <- status,
                                                       isHightlightField <- isHightlight,
                                                       urlThumbField <- urlThumb,
                                                       urlOriginalField <- urlOriginal,
                                                       urlBlurField <- urlBlur,
                                                       typeField <- type,
                                                       insertedAtField <- insertedAt.timestamp,
                                                       isWelcomeMessageField <- isWelcomeMessage)
                } else {
                    insert = publicMessageTable.insert(or: .replace,
                                                       indexField <- index,
                                                       publicMessageIdField <- id,
                                                       isGeneralField <- isGeneral,
                                                       msgField <- msg,
                                                       fromUserIdField <- fromUserId,
                                                       fromUserNameField <- fromUserName,
                                                       statusField <- status,
                                                       isHightlightField <- isHightlight,
                                                       urlThumbField <- urlThumb,
                                                       urlOriginalField <- urlOriginal,
                                                       urlBlurField <- urlBlur,
                                                       typeField <- type,
                                                       insertedAtField <- insertedAt.timestamp,
                                                       isWelcomeMessageField <- isWelcomeMessage)
                }
                
                
                do {
                    try DBUtil.sharedInstance.run(insert)
                } catch {
                    print("insert:274 Error insertMessage - \(error) | json : \(message.toJSONString() ?? "") ")
                }
            }
        }
        
        static func insertLite(_ message: PublicMessageModel) -> Int64 {
            
            if let msg = message.msg,
                let fromUserId = message.fromUserId,
                let fromUserName = message.fromUserName,
                let index = message.index,
                let isGeneral = message.isGeneral,
                let status = message.status?.rawValue {
                
                let isWelcomeMessage = message.isWelcomeMessage ?? false
                
                let insert = publicMessageTable.insert(or: .replace,
                                                       msgField <- msg,
                                                       fromUserIdField <- fromUserId,
                                                       fromUserNameField <- fromUserName,
                                                       indexField <- index,
                                                       isGeneralField <- isGeneral,
                                                       statusField <- status,
                                                       isWelcomeMessageField <- isWelcomeMessage)
                
                do {
                    let rowid = try DBUtil.sharedInstance.run(insert)
                    return rowid
                } catch {
                    print("insertLite:300 Error insertMessage - \(error) | json : \(message.toJSONString() ?? "") ")
                    return -1
                }
            }
            
            return -1
        }
        
        static func isContaineMessage(_ message: String) -> Bool {
            
            let query = publicMessageTable.filter(isWelcomeMessageField == true && msgField == message)
            
            do {
                let stmt = try DBUtil.sharedInstance.prepare(query)
                                
                for _ in stmt {
                    return true
                }
            } catch {
                print(error)
            }
            
            return false
        }
    }
}
