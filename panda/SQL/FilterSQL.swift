//
//  FilterSQL.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

extension FilterModel {
    
    static func creationTable(_ db: Connection) {
        FilterSQL.creationTable(db)
    }
    
    static func resetTable(_ db: Connection) {
        FilterSQL.resetTable(db)
    }
    
    static func getFilters(isHighlight: Bool, userId: Int? = SessionManager.sharedInstance.user.userId) -> [FilterModel] {
        return FilterSQL.getFilters(isHighlight: isHighlight, userId: userId)
    }
    
    func remove() {
        if let filterId = self.filterId {
            FilterSQL.removeFilter(filterId)
        }
    }
    
    func insert() {
        FilterSQL.insertFilter(self)
    }
 
    fileprivate class FilterSQL: SQLManagerDelegate {
        
        fileprivate static let filterTable = Table("user_filters")
        fileprivate static let idField = Expression<Int64>("_id")
        fileprivate static let filterIdField = Expression<Int>("user_filters_id")
        fileprivate static let userIdField = Expression<Int>("user_id")
        fileprivate static let nameField = Expression<String>("name")
        fileprivate static let isHighlightField = Expression<Bool>("is_highlight")
        fileprivate static let timestampField = Expression<String>("updated_at")
        
        
        //
        //MARK: Init
        //
        
        static func creationTable(_ db: Connection) {
            do {
                try db.run(filterTable.create { t in
                    t.column(idField, primaryKey: true)
                    t.column(filterIdField, unique: true)
                    t.column(userIdField)
                    t.column(nameField)
                    t.column(isHighlightField)
                    t.column(timestampField)
                    })
            } catch {
                print("ERROR creationTable 'filterTable'")
            }
        }
        
        static func resetTable(_ db: Connection) {
            do {
                try db.run(filterTable.delete())
            } catch {
                print("ERROR resetTable 'filterTable'")
            }
        }
        
        //
        //MARK: SQL
        //
        
        static func insertFilter(_ filter: FilterModel, userId: Int? = SessionManager.sharedInstance.user.userId) {
            
            if let id = filter.filterId, let name = filter.name, let isHighlight = filter.isHighlight, let timestamp = filter.timestamp {
                let insert = filterTable.insert(or: .replace, filterIdField <- id, userIdField <- userId!, nameField <- name, isHighlightField <- isHighlight, timestampField <- timestamp.getDateToFullString())
                
                do {
                    try DBUtil.sharedInstance.run(insert)
                } catch {
                    print("Error insertFilter")
                }
                
            }
        }
        
        static func getFilters(isHighlight: Bool, userId: Int? = SessionManager.sharedInstance.user.userId) -> [FilterModel] {
            let select = filterTable.filter(isHighlightField == isHighlight && userIdField == userId!).order([nameField])
            var list: [FilterModel] = []
            do {
                let all = Array(try DBUtil.sharedInstance.prepare(select))
                
                for item in all {
                    list.append(FilterModel.iniFilter(Int(item[filterIdField]), name: item[nameField], isHighlight: item[isHighlightField], timestamp: item[timestampField]))
                }
            } catch {
                print("Error insertFilter")
            }
            
            return list
        }
        
        static func removeFilter(_ filterId: Int, userId: Int? = SessionManager.sharedInstance.user.userId) {
            let select = filterTable.filter(filterIdField == Int(filterId) && userIdField == Int(userId!))
            
            do {
                try DBUtil.sharedInstance.run(select.delete())
            } catch {
                print("Error removeFilter")
            }
        }
    }
}
