//
//  MapUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class MapUtil {
    
    static func MapZero() -> Map {
        return Map(mappingType: .fromJSON, JSON: [:])
    }
    
}


