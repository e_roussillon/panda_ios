//
//  LoadingUtil.swift
//  Corretora
//
//  Created by Edouard Roussillon on 8/11/15.
//  Copyright (c) 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography

class LoadingUtil {
    
    static func showDarkBackgroundWithSpinnerWithColor(_ viewController:UIViewController!, notShowLoaderIntoWindows:Bool!, backgroundColor:UIColor? = Constants.COLOR.kBackgroundLoader, spinnerColor:UIColor? = Constants.COLOR.kSpinnerLoader) -> Array<UIView> {
        
        backgroundColor!.withAlphaComponent(0.0)
        
        let darkView:UIView = UIView(frame: viewController.view.frame)
        darkView.backgroundColor = backgroundColor
        darkView.accessibilityIdentifier = "PROGRESS"
        
        let indicatorView:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white;
        indicatorView.hidesWhenStopped = true
        indicatorView.alpha = 0.0
        indicatorView.color = spinnerColor
        indicatorView.startAnimating()
        
        if let window = viewController.view.window, notShowLoaderIntoWindows == true {
            window.addSubview(darkView)
            window.addSubview(indicatorView)
        } else {
            viewController.view.addSubview(darkView)
            viewController.view.addSubview(indicatorView)
        }
        
        constrain(darkView, indicatorView) { view, view1  in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
            
            view1.center == view1.superview!.center
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            darkView.backgroundColor = backgroundColor!.withAlphaComponent(0.6)
            indicatorView.alpha = 1.0
        })
        
        return [darkView, indicatorView];
    }
    
    static func showSpinnerWithColor(_ viewController:UIViewController!, notShowLoaderIntoWindows:Bool!, spinnerColor:UIColor? = UIColor.white) -> Array<UIView> {
        
        let darkView:UIView = UIView(frame: viewController.view.frame)
        darkView.backgroundColor = UIColor.clear
        darkView.accessibilityIdentifier = "PROGRESS"
        
        let indicatorView:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white;
        indicatorView.hidesWhenStopped = true
        indicatorView.alpha = 0.0
        indicatorView.color = spinnerColor
        indicatorView.startAnimating()
        
        if let window = viewController.view.window, notShowLoaderIntoWindows == false {
            window.addSubview(darkView)
            window.addSubview(indicatorView)
        } else {
            viewController.view.addSubview(darkView)
            viewController.view.addSubview(indicatorView)
        }
        
        constrain(darkView, indicatorView) { view, view1  in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
            
            view1.center == view1.superview!.center
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            indicatorView.alpha = 1.0
        })
        
        return [darkView, indicatorView];
    }
    
    static func removeDarkBackgroundWithSpinnerIntoView(_ array:Array<UIView>!, withAnim:Bool) {
        
        if array == nil {
            return
        }
        
        if (withAnim == true) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                for view in array {
                    view.alpha = 0
                }
                }, completion: { (isFinish) -> Void in
                    for view in array {
                        view.removeFromSuperview()
                    }
            }) 
        } else {
            for view in array {
                view.removeFromSuperview()
            }
        }
    }
}
