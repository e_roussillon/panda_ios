//
//  StoryboardUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class StoryboardUtil {
    
    //
    //MARK: Splash
    //
    
    static func splashNavViewController() -> UIViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Splash")!
    }
    
    //
    //MARK: Splash
    //
    
    static func welcomeViewController() -> WelcomeViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Welcome") as! WelcomeViewController
    }
    
    //
    //MARK: Login
    //
    
    static func loginViewController() -> LoginViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Login", controllerName:"LoginViewController") as! LoginViewController
    }
    
    static func registerViewController() -> RegisterViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Login", controllerName:"RegisterViewController") as! RegisterViewController
    }
    
    //
    //MARK: Home
    //
    
    static func homeViewController() -> UIViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Home", controllerName:"HomeViewController")!
    }
    
    //
    //MARK: Filter
    //
    
    static func tabBarFilterViewController() -> TabBarFilterViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Filter") as! TabBarFilterViewController
    }
    
    //
    //MARK: Profile
    //
    
    static func profileViewController() -> UIViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Profile", controllerName:"ProfileViewController")!
    }
    
    static func flyingProfileViewController() -> FlyingProfileViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Profile", controllerName:"FlyingProfileViewController") as! FlyingProfileViewController
    }
    
    //
    //MARK: Friendship
    //
    
    static func tabBarFriendshipViewController() -> TabBarFriendshipViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Friendship") as! TabBarFriendshipViewController
    }
    
    static func friendshipBlockedViewController() -> FriendBlockedViewController {
        return StoryboardUtil.viewControllerWithStoryboard("Friendship", controllerName:"FriendBlockedViewController") as! FriendBlockedViewController
    }
    
    //
    //MARK: Chat
    //
    
    static func tabBarChatViewController() -> TabBarChatViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Chat") as! TabBarChatViewController
    }
    
    //
    //MARK: Web
    //
    
    static func webViewController() -> WebViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Web", controllerName:"WebViewController") as! WebViewController
    }
    
    //
    //MARK: Chatting
    //
    
    static func chatViewController(_ user: UserModel, friendshipStatus: FriendshipType = FriendshipType.notFriend, friendshipLastAction: Int = -1, removedSearchAfterSelecteChat: Bool = false) -> ChatViewController  {
        
        let chattingViewController =  StoryboardUtil.viewControllerWithStoryboard("Chatting", controllerName:"ChatViewController") as! ChatViewController
        chattingViewController.channelType = ChannelType.Private
        chattingViewController.user = user
        chattingViewController.friendshipStatus = friendshipStatus
        chattingViewController.friendshipLastAction = friendshipLastAction
        chattingViewController.removedSearchAfterSelecteChat = removedSearchAfterSelecteChat
        return chattingViewController
    }
    
    //
    //MARK: Search
    //
    
    static func searchNavViewController() -> BaseNavigationController  {
        return StoryboardUtil.viewControllerWithStoryboard("Search") as! BaseNavigationController
    }
    
    static func searchViewController() -> SearchViewController  {
        return StoryboardUtil.viewControllerWithStoryboard("Search", controllerName: "SearchViewController") as! SearchViewController
    }
  
    //
    //MARK: Generic
    //
    
    static fileprivate func viewControllerWithStoryboard(_ storyboardName:String!, controllerName:String!) -> UIViewController? {
        
        if (storyboardName != nil || controllerName != nil) {
            let mainStoryboard:UIStoryboard? = UIStoryboard(name: storyboardName,  bundle:nil)
            
            if (mainStoryboard != nil) {
                return mainStoryboard!.instantiateViewController(withIdentifier: controllerName)
            }
        }
        
        return nil
    }
    
    
    static fileprivate func viewControllerWithStoryboard(_ storyboardName:String!) -> UIViewController? {
        
        guard storyboardName != nil else {
            return nil
        }
        let mainStoryboard:UIStoryboard? = UIStoryboard(name: storyboardName,  bundle:nil)
        
        guard let viewController = mainStoryboard?.instantiateInitialViewController() else {
            return nil
        }
        return viewController
    }
    
}
