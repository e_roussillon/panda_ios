//
//  KeychainUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/18/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import AlamofireImage
import CoreLocation

class KeychainUtil {
    static let sharedInstance = KeychainUtil()
    
    fileprivate init() {}
	
    func reset() {
        if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kTOKEN)) {
			KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kTOKEN)
		}
        
        if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kTOKEN_REFRESH)) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kTOKEN_REFRESH)
        }

		if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kUSER_ID)) {
			KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kUSER_ID)
		}
        
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kJUST_LOGIN) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kJUST_LOGIN)
        }
        
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kIMAGE_DOWNLOADED) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kIMAGE_DOWNLOADED)
        }
        
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LAT) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LAT)
        }
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LNG) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LNG)
        }
        
//        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kPUSH_TOKEN) {
//            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kPUSH_TOKEN)
//        }
        
		self.resetLocalisation()
        self.resetPublicChat()
        
        SQLManager.resetTables()
        self.leaveAllRooms()
	}
    
    func resetDB() {
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kCURRENT_DB) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kCURRENT_DB)
        }
    }
    
    func resetWelcomeScreen() {
        if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kWelcomeScreen) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kWelcomeScreen)
        }
    }
    
    func resetPublicChat() {
        SQLManager.resetPublicChatTables()
    }
    
    func resetLocalisation() {
        SocketManager.sharedInstance.leaveRoomPublicRooms()
        GPSManager.sharedInstance.resetLocalisation()
        
        
        if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kFIND_ROOM)) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kFIND_ROOM)
        }
        
        if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kCURRENT_ROOM)) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kCURRENT_ROOM)
        }
        
        if (KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kCURRENT_ROOM_ID)) {
            KeychainWrapper.standard.removeObject(forKey: Constants.KEYS.kCURRENT_ROOM_ID)
        }
    }
    
    func leaveAllRooms() {
        SocketManager.sharedInstance.stopSocket {
            SocketManager.sharedInstance.leaveRooms()
        }
    }
    

	var token: String {
		get {
            return KeychainWrapper.standard.string(forKey: Constants.KEYS.kTOKEN) ?? ""
		}

		set(newToken) {
            KeychainWrapper.standard.set(newToken, forKey: Constants.KEYS.kTOKEN)
		}
	}
    
    var pushToken: String {
        get {
            return KeychainWrapper.standard.string(forKey: Constants.KEYS.kPUSH_TOKEN) ?? ""
        }
        
        set(newToken) {
            KeychainWrapper.standard.set(newToken, forKey: Constants.KEYS.kPUSH_TOKEN)
        }
    }
    
    var tokenRefresh: String {
        get {
            return KeychainWrapper.standard.string(forKey: Constants.KEYS.kTOKEN_REFRESH) ?? ""
        }
        
        set(newToken) {
            KeychainWrapper.standard.set(newToken, forKey: Constants.KEYS.kTOKEN_REFRESH)
        }
    }

	var userId: Int {
		get {
			if let userId = KeychainWrapper.standard.string(forKey: Constants.KEYS.kUSER_ID), let userIdInt = Int(userId) {
				return userIdInt
			} else {
				return -1
			}
		}

		set(newUserId) {
			KeychainWrapper.standard.set("\(newUserId)", forKey: Constants.KEYS.kUSER_ID)
		}
	}
    
    var justLogin: Bool {
        get {
            if let justLogin = KeychainWrapper.standard.string(forKey: Constants.KEYS.kJUST_LOGIN), justLogin == "YES" {
                return true
            } else {
                return false
            }
        }
        
        set(newJustLogin) {
            let justLogin = newJustLogin ? "YES" : "NO"
            KeychainWrapper.standard.set("\(justLogin)", forKey: Constants.KEYS.kJUST_LOGIN)
        }
    }

	var isLogin: Bool {
		get {
			if self.token.length() > 0 {
				return true
			} else {
				return false
			}
		}
	}

    var findRooms: FindRoomsModel {
		get {
			if let json = KeychainWrapper.standard.string(forKey: Constants.KEYS.kFIND_ROOM) {
				return FindRoomsModel.loadModel(json)
			} else {
				return FindRoomsModel.loadModel("")
			}
		}

		set(newFindRooms) {
			KeychainWrapper.standard.set(newFindRooms.toString(), forKey: Constants.KEYS.kFIND_ROOM)
		}
	}

    var currentRoomId: Int {
		get {
			if let roomId = KeychainWrapper.standard.string(forKey: Constants.KEYS.kCURRENT_ROOM_ID), let roomIdInt = Int(roomId) {
				return roomIdInt
			} else {
				return -1
			}
		}

		set(newRoomId) {
			KeychainWrapper.standard.set("\(newRoomId)", forKey: Constants.KEYS.kCURRENT_ROOM_ID)
		}
	}
    
    var currentRoom: PublicRoomModel? {
        get {
            if let json = KeychainWrapper.standard.string(forKey: Constants.KEYS.kCURRENT_ROOM) {
                return PublicRoomModel.loadModel(json)
            } else {
                return nil
            }
        }
        
        set(newRoom) {
            if let room = newRoom {
                KeychainWrapper.standard.set(room.toString(), forKey: Constants.KEYS.kCURRENT_ROOM)
            } else {
                KeychainWrapper.standard.set("", forKey: Constants.KEYS.kCURRENT_ROOM)
            }
        }
    }
    
    var currentLocalisation: CLLocationCoordinate2D? {
        get {
            if let lat = KeychainWrapper.standard.double(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LAT),
                let lng = KeychainWrapper.standard.double(forKey: Constants.KEYS.kCURRENT_LOCALISATION_LNG), lat != 666 && lng != 666 {
                return CLLocationCoordinate2D(latitude: lat, longitude: lng)
            } else {
                return nil
            }
        }
        
        set(newCoordinate) {
            if let coordinate = newCoordinate {
                KeychainWrapper.standard.set(coordinate.latitude, forKey: Constants.KEYS.kCURRENT_LOCALISATION_LAT)
                KeychainWrapper.standard.set(coordinate.longitude, forKey: Constants.KEYS.kCURRENT_LOCALISATION_LNG)
            } else {
                KeychainWrapper.standard.set(666, forKey: Constants.KEYS.kCURRENT_LOCALISATION_LAT)
                KeychainWrapper.standard.set(666, forKey: Constants.KEYS.kCURRENT_LOCALISATION_LNG)
            }
        }
    }
    
    var currentDB: Int {
        get {
            if let db = KeychainWrapper.standard.integer(forKey: Constants.KEYS.kCURRENT_DB) {
                return db
            } else {
                return -1
            }
        }
        
        set(db) {
            KeychainWrapper.standard.set(db, forKey: Constants.KEYS.kCURRENT_DB)
        }
    }
    
    var welcomeScreen: Bool {
        get {
            if let welcome = KeychainWrapper.standard.bool(forKey: Constants.KEYS.kWelcomeScreen) {
                return welcome
            } else {
                return false
            }
        }
        
        set(welcome) {
            KeychainWrapper.standard.set(welcome, forKey: Constants.KEYS.kWelcomeScreen)
        }
    }
    
    var saveImages: SaveImage? {
        get {
            if KeychainWrapper.standard.hasValue(forKey: Constants.KEYS.kIMAGE_DOWNLOADED) {
                return KeychainWrapper.standard.object(forKey: Constants.KEYS.kIMAGE_DOWNLOADED) as? SaveImage
            } else {
                return nil
            }
        }
        
        set(images) {
            if let ima = images {
                KeychainWrapper.standard.set(ima, forKey: Constants.KEYS.kIMAGE_DOWNLOADED)
            }
        }
    }
}
