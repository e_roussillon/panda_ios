//
//  EnvironmentUtil.swift
//  Panda
//
//  Created by Edouard Roussillon on 9/10/15.
//  Copyright (c) 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

open class EnvironmentUtil {
    
    fileprivate static var environment: [String: AnyObject]? {
        get {
            if let env = Bundle.main.infoDictionary!["EnvironmentConfig"] as? [String: AnyObject] {
                return env
            } else {
                return nil
            }
        }
    }
    
    open static var debugInfo: Bool {
        get {
            if let val = self.environment?["MTL_ENABLE_DEBUG_INFO"] as? String, val == "YES" {
                return true
            } else {
                return false
            }
        }
    }
	
	//swiftlint:disable force_cast
	open static var protocolWS: String {
		get {
			return self.environment?["PROTOCOL_WS"] as! String
		}
	}
	//swiftlint:disable force_cast
    open static var urlAPI: String {
        get {
            return self.environment?["URL_API"] as! String
        }
    }
    //swiftlint:disable force_cast
    open static var urlWS: String {
        get {
            return self.environment?["URL_WS"] as! String
        }
    }
    
    open static var segment: String {
        get {
            return self.environment?["SEGMENT"] as! String
        }
    }
    
    open static var db: Int {
        get {
            return Int(self.environment?["DB"] as! String)!
        }
    }
	
	open static var isTargetPandaBeta: Bool {
		get {
			guard let plistFileName = Bundle.main.infoDictionary?["TargetName"] as? String else {
				return false
			}
			
			if plistFileName == "panda-beta" {
				return true
			} else {
				return false
			}
		}
	}
	
    open static var isTargetPanda: Bool {
        get {
            guard let plistFileName = Bundle.main.infoDictionary?["TargetName"] as? String else {
                return false
            }
            
            if plistFileName == "panda" {
                return true
            } else {
                return false
            }
        }
    }
    
    open static var isTargetPandaQA: Bool {
        get {
            guard let plistFileName = Bundle.main.infoDictionary?["TargetName"] as? String else {
                return false
            }
            
            if plistFileName == "panda-cal" {
                return true
            } else {
                return false
            }
        }
    }
    
    open static var isTargetPandaDev: Bool {
        get {
            guard let plistFileName = Bundle.main.infoDictionary?["TargetName"] as? String else {
                return false
            }
            
            if plistFileName == "panda-dev" {
                return true
            } else {
                return false
            }
        }
    }

    open static var isTestRunning: Bool {
        get {
            var isTestRunning = false
            if let _ = NSClassFromString("XCTest") {
                isTestRunning = true
            } else if ProcessInfo.processInfo.arguments.contains("XCTestCase") {
                isTestRunning = true
            } else if self.isTargetPandaQA {
                isTestRunning = true
            }
            
            return isTestRunning
        }
    }
}
