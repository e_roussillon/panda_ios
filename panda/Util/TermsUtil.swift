//
//  TermsUtil.swift
//  panda
//
//  Created by Edouard Roussillion on 8/12/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import TTTAttributedLabel

class TermsUtil {
    
    static func initTerms(labelTerms: TTTAttributedLabel, delegate: TTTAttributedLabelDelegate, isLight: Bool = false) {
        let fontHighlight: UIFont = UIFont.initHelveticaMedium(isLight ? 11 : 14)
        let fontNormal:UIFont = UIFont.initHelveticaLight(isLight ? 11 : 14)
        
        let text = NSLocalizedString(isLight ? "terms_to_see_label" : "terms_label", comment: "")
        let textToHighlight = NSLocalizedString("terms_label_to_highlight", comment: "")
        
        if let rangeTerms = text.range(of: textToHighlight) {
            guard let newRange: NSRange = text.nsRange(from: rangeTerms) else {
                return
            }
        
            let fullRange: NSRange = NSMakeRange(0, text.length())
            let notifyingStr = NSMutableAttributedString(string: text)
            notifyingStr.beginEditing()
            notifyingStr.addAttribute(NSAttributedStringKey.foregroundColor, value: Constants.COLOR.kTitlePlaceholder, range:fullRange)
            notifyingStr.addAttribute(NSAttributedStringKey.font, value: fontNormal, range: NSMakeRange(0, text.length()))
            notifyingStr.addAttribute(NSAttributedStringKey.font, value: fontHighlight, range: newRange)
            notifyingStr.endEditing()
            
            labelTerms.setText(notifyingStr)
            labelTerms.linkAttributes = [NSAttributedStringKey.foregroundColor : Constants.COLOR.kTitlePlaceholder, NSAttributedStringKey.font : fontHighlight]
            labelTerms.activeLinkAttributes = [NSAttributedStringKey.foregroundColor : Constants.COLOR.kTitlePlaceholder, NSAttributedStringKey.font : fontHighlight]
            labelTerms.addLink(to: URL(string: "http://terms"), with: newRange)
            labelTerms.delegate = delegate
        }
    }
    
    static func openTerms(navigationController: UINavigationController?) {
        let webController = StoryboardUtil.webViewController()
        webController.isTerms = true
        navigationController?.present(BaseNavigationController(rootViewController: webController), animated: true)
    }
    
}
