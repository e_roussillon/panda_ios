//
//  DBUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import SQLite
import Foundation


final class DBUtil {
    static let sharedInstance: Connection  = DBUtil().db
    
    fileprivate static let dbName: String = "panda_db"
    fileprivate static let dbLocalName: String = "panda_local_db.sqllite3"
    fileprivate static let rootPath: String = "Documents/DB/"
    fileprivate let sourcePath: String = Bundle.main.path(forResource: DBUtil.dbName, ofType: "sqllite3")!
    fileprivate let destinationPath = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(rootPath).appendingPathComponent(DBUtil.dbLocalName).path

    fileprivate var db: Connection!
    
    fileprivate init() {
        db = self.initDB()
    }
    
    //
    //MARK: Init
    //
    
    var databasePath = NSString()
    
    fileprivate func initDB() -> Connection {
        print("PATH DB ORIGINAL : \(self.sourcePath)")
        print("PATH NEW DB : \(self.destinationPath)")
        
        if EnvironmentUtil.db != KeychainUtil.sharedInstance.currentDB {
            if FileManager.default.fileExists(atPath: self.destinationPath) {
                do {
                    try FileManager.default.removeItem(atPath: self.destinationPath)
                    //so after it need to backup data if user is login
                    if SessionManager.sharedInstance.user.isLogin {
                        SessionManager.sharedInstance.user.justLogin = true
                    }
                }
                catch let error as NSError {
                    print("Ooops! Something went wrong: \(error)")
                }
            }
            
            do {
                let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(DBUtil.rootPath).path
                try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                try FileManager.default.copyItem(atPath: self.sourcePath, toPath: self.destinationPath)
                KeychainUtil.sharedInstance.currentDB = EnvironmentUtil.db
            } catch {
                print("Error when creation file!!")
            }
        }
        
        return try! Connection(self.destinationPath)
    }
}
