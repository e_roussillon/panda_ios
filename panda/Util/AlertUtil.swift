//
//  AlertUtil.swift
//  Corretora
//
//  Created by Edouard Roussillon on 7/30/15.
//  Copyright (c) 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class AlertUtil {
    
    static let buttonOk = NSLocalizedString("dialog_button_ok", comment: "")
    static let buttonCancel = NSLocalizedString("dialog_button_cancel", comment: "")
    
    class func showTitleWithMessage(_ title:String = "", message:String) {
        let alertController = AlertUtil.initAlertController(title, message: message)
        alertController.addAction(UIAlertAction(title: buttonOk, style: UIAlertActionStyle.cancel, handler: { (alert) -> Void in }))
        AlertUtil.launchAlert(alertController)
    }
    
    class func showTitleWithMessage(_ title:String = "", message:String, buttonOK:String = buttonOk, handlerOK:@escaping ((UIAlertAction!) -> Void), buttonCancel:String = buttonCancel,  handlerCancel:((UIAlertAction?) -> Void)!) {
        let alertController = AlertUtil.initAlertControllerWithButtons(title, message: message, buttonOK: buttonOK, handlerOK: handlerOK, buttonCancel: buttonCancel,  handlerCancel: handlerCancel)
        AlertUtil.launchAlert(alertController)
    }
    
    class func showTitleWithMessage(_ title:String = "", message:String, buttonOK:String? = buttonOk, handlerOK:@escaping ((UIAlertAction!) -> Void)) {
        let alertController = AlertUtil.initAlertController(title, message: message)
        if let buttonOK = buttonOK {
            alertController.addAction(UIAlertAction(title: buttonOK, style: UIAlertActionStyle.cancel, handler: handlerOK))
        }
        AlertUtil.launchAlert(alertController)
    }
    
    
    class func initAlertController(_ title:String = "", message:String) -> UIAlertController {
        return UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    }
    
    class func initAlertControllerWithButtons(_ title:String = "", message:String, buttonOK:String = buttonOk, handlerOK:@escaping ((UIAlertAction!) -> Void), buttonCancel:String = buttonCancel,  handlerCancel:((UIAlertAction?) -> Void)!) -> UIAlertController {
        let alertController = AlertUtil.initAlertController(title, message: message)
        alertController.addAction(UIAlertAction(title: buttonOK, style: UIAlertActionStyle.default, handler: handlerOK))
        alertController.addAction(UIAlertAction(title: buttonCancel, style: UIAlertActionStyle.cancel, handler: handlerCancel))
        
        return alertController
    }
        
    class func launchAlert(_ alertController: UIAlertController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let window = appDelegate.window {
            
            if let view = window.rootViewController {
                var lookFor = true
                var vc:UIViewController = view
                
                while(lookFor) {
                    if let present = vc.presentedViewController {
                        vc = present
                    } else if let nav = vc as? UINavigationController {
                        vc = nav.viewControllers.last!
                    } else if vc is UIAlertController {
                        //Do Nothing
                        lookFor = false
                    } else {
                        lookFor = false

                        OperationQueue.main.addOperation {
                            vc.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}
