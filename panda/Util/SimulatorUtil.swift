//
//  SimulatorUtil.swift
//  carrefour
//
//  Created by Jean Vinge on 1/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SimulatorUtil {
    class var isRunningSimulator: Bool {
        get {
            return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
        }
    }
}
