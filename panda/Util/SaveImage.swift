//
//  SaveImage.swift
//  panda
//
//  Created by Edouard Roussillon on 10/8/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import AlamofireImage

open class SaveImage: NSObject, NSCoding, ImageRequestCache {

    @objc(_TtCC9panda_dev9SaveImage11CachedImage) open class CachedImage: NSObject, NSCoding {
        let image: Image
        let identifier: String
        let totalBytes: UInt64
        var lastAccessDate: Date
        
        @objc init(_ image: Image, identifier: String) {
            self.image = image
            self.identifier = identifier
            self.lastAccessDate = Date()
            
            self.totalBytes = {
                let size = CGSize(width: image.size.width * image.scale, height: image.size.height * image.scale)
                
                let bytesPerPixel: CGFloat = 4.0
                let bytesPerRow = size.width * bytesPerPixel
                let totalBytes = UInt64(bytesPerRow) * UInt64(size.height)
                
                return totalBytes
            }()
            super.init()
        }
        
        @objc init(_ image: Image, identifier: String, totalBytes: UInt64, lastAccessDate: Date) {
            self.image = image
            self.identifier = identifier
            self.lastAccessDate = lastAccessDate
            self.totalBytes = totalBytes
            super.init()
        }
        
        func accessImage() -> Image {
            lastAccessDate = Date()
            return image
        }
        
        @objc open func encode(with aCoder: NSCoder) {
            aCoder.encode(UIImagePNGRepresentation(self.image), forKey: "image")
            aCoder.encode(self.identifier, forKey: "identifier")
            aCoder.encode(Int64(self.totalBytes), forKey: "totalBytes")
            aCoder.encode(self.lastAccessDate.getDateToFullString(), forKey: "lastAccessDate")
        }
        
        @objc required convenience public init?(coder aDecoder: NSCoder) {
            guard let data = aDecoder.decodeObject(forKey: "image") as? Data,
                let identifier = aDecoder.decodeObject(forKey: "identifier") as? String,
                let lastAccessDateString = aDecoder.decodeObject(forKey: "lastAccessDate") as? String,
                let lastAccessDate = lastAccessDateString.toDateFull()
                else { return nil }
            
            let image = UIImage(data: data)
            let totalBytes = UInt64(aDecoder.decodeInt64(forKey: "totalBytes"))
            
            
            self.init(image!,
                identifier: identifier,
                totalBytes: totalBytes,
                lastAccessDate: lastAccessDate
            )
        }
    }
    
    // MARK: Properties
    
    /// The current total memory usage in bytes of all images stored within the cache.
    open var memoryUsage: UInt64 {
        var memoryUsage: UInt64 = 0
        synchronizationQueue.sync { memoryUsage = self.currentMemoryUsage }
        
        return memoryUsage
    }
    
    /// The total memory capacity of the cache in bytes.
    open let memoryCapacity: UInt64
    
    /// The preferred memory usage after purge in bytes. During a purge, images will be purged until the memory
    /// capacity drops below this limit.
    open let preferredMemoryUsageAfterPurge: UInt64
    
    fileprivate let synchronizationQueue: DispatchQueue
    fileprivate var cachedImages: [String: CachedImage] {
        didSet {
            print("")
        }
    }
    fileprivate var currentMemoryUsage: UInt64
    
    // MARK: Initialization
    
    /**
     Initialies the `SaveImage` instance with the given memory capacity and preferred memory usage
     after purge limit.
     
     Please note, the memory capacity must always be greater than or equal to the preferred memory usage after purge.
     
     - parameter memoryCapacity:                 The total memory capacity of the cache in bytes. `100 MB` by default.
     - parameter preferredMemoryUsageAfterPurge: The preferred memory usage after purge in bytes. `60 MB` by default.
     
     - returns: The new `SaveImage` instance.
     */
    @objc public init(memoryCapacity: UInt64 = 100_000_000, preferredMemoryUsageAfterPurge: UInt64 = 60_000_000) {
        self.memoryCapacity = memoryCapacity
        self.preferredMemoryUsageAfterPurge = preferredMemoryUsageAfterPurge
        
        precondition(
            memoryCapacity >= preferredMemoryUsageAfterPurge,
            "The `memoryCapacity` must be greater than or equal to `preferredMemoryUsageAfterPurge`"
        )
        
        self.cachedImages = [:]
        self.currentMemoryUsage = 0
        
        self.synchronizationQueue = {
            let name = String(format: "com.alamofire.SaveImage-%08x%08x", arc4random(), arc4random())
            return DispatchQueue(label: name, attributes: DispatchQueue.Attributes.concurrent)
        }()
        
        super.init()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(SaveImage.removeAllImages),
            name: NSNotification.Name.UIApplicationDidReceiveMemoryWarning,
            object: nil
        )
    }
    
    @objc public init(memoryCapacity: UInt64, preferredMemoryUsageAfterPurge: UInt64, cachedImages: [String: CachedImage], currentMemoryUsage: UInt64) {
        self.memoryCapacity = memoryCapacity
        self.preferredMemoryUsageAfterPurge = preferredMemoryUsageAfterPurge
        
        precondition(
            memoryCapacity >= preferredMemoryUsageAfterPurge,
            "The `memoryCapacity` must be greater than or equal to `preferredMemoryUsageAfterPurge`"
        )
        
        self.cachedImages = cachedImages
        self.currentMemoryUsage = currentMemoryUsage
        
        self.synchronizationQueue = {
            let name = String(format: "com.alamofire.SaveImage-%08x%08x", arc4random(), arc4random())
            return DispatchQueue(label: name, attributes: DispatchQueue.Attributes.concurrent)
        }()
        
        super.init()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(SaveImage.removeAllImages),
            name: NSNotification.Name.UIApplicationDidReceiveMemoryWarning,
            object: nil
        )
    }
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc open func encode(with aCoder: NSCoder) {
        aCoder.encode(Int64(self.memoryCapacity), forKey: "memoryCapacity")
        aCoder.encode(Int64(self.preferredMemoryUsageAfterPurge), forKey: "preferredMemoryUsageAfterPurge")
        aCoder.encode(self.cachedImages, forKey: "cachedImages")
        aCoder.encode(Int64(self.currentMemoryUsage), forKey: "currentMemoryUsage")
    }
    
    @objc required convenience public init?(coder aDecoder: NSCoder) {
        let cachedImages: [String: CachedImage] = (aDecoder.decodeObject(forKey: "cachedImages") ?? [:]) as! [String : SaveImage.CachedImage]
        let memoryCapacity = UInt64(aDecoder.decodeInt64(forKey: "memoryCapacity"))
        let preferredMemoryUsageAfterPurge = UInt64(aDecoder.decodeInt64(forKey: "preferredMemoryUsageAfterPurge"))
        let currentMemoryUsage = UInt64(aDecoder.decodeInt64(forKey: "currentMemoryUsage"))
        
        
        self.init(memoryCapacity: memoryCapacity,
                  preferredMemoryUsageAfterPurge: preferredMemoryUsageAfterPurge,
                  cachedImages: cachedImages,
                  currentMemoryUsage: currentMemoryUsage)
    }
    
    // MARK: Add Image to Cache
    
    /**
     Adds the image to the cache using an identifier created from the request and optional identifier.
     
     - parameter image:      The image to add to the cache.
     - parameter request:    The request used to generate the image's unique identifier.
     - parameter identifier: The additional identifier to append to the image's unique identifier.
     */
    public func add(_ image: Image, for request: URLRequest, withIdentifier identifier: String?) {
        let requestIdentifier = imageCacheKeyFromURLRequest(request, withAdditionalIdentifier: identifier)
        add(image, withIdentifier: requestIdentifier)
    }
    
    /**
     Adds the image to the cache with the given identifier.
     
     - parameter image:      The image to add to the cache.
     - parameter identifier: The identifier to use to uniquely identify the image.
     */
    open func add(_ image: Image, withIdentifier identifier: String) {
        synchronizationQueue.async(flags: .barrier, execute: {
            let cachedImage = CachedImage(image, identifier: identifier)
            
            if let previousCachedImage = self.cachedImages[identifier] {
                self.currentMemoryUsage -= previousCachedImage.totalBytes
            }
            
            self.cachedImages[identifier] = cachedImage
            self.currentMemoryUsage += cachedImage.totalBytes
        }) 
        
        synchronizationQueue.async(flags: .barrier, execute: {
            if self.currentMemoryUsage > self.memoryCapacity {
                let bytesToPurge = self.currentMemoryUsage - self.preferredMemoryUsageAfterPurge
                
                var sortedImages = [CachedImage](self.cachedImages.values)
                sortedImages.sort {
                    let date1 = $0.lastAccessDate
                    let date2 = $1.lastAccessDate
                    
                    return date1.timeIntervalSince(date2) < 0.0
                }
                
                var bytesPurged = UInt64(0)
                
                for cachedImage in sortedImages {
                    self.cachedImages.removeValue(forKey: cachedImage.identifier)
                    bytesPurged += cachedImage.totalBytes
                    
                    if bytesPurged >= bytesToPurge {
                        break
                    }
                }
                
                self.currentMemoryUsage -= bytesPurged
            }
        }) 
    }
    
    // MARK: Remove Image from Cache
    
    /**
     Removes the image from the cache using an identifier created from the request and optional identifier.
     
     - parameter request:    The request used to generate the image's unique identifier.
     - parameter identifier: The additional identifier to append to the image's unique identifier.
     
     - returns: `true` if the image was removed, `false` otherwise.
     */
    public func removeImage(for request: URLRequest, withIdentifier identifier: String?) -> Bool {
        let requestIdentifier = imageCacheKeyFromURLRequest(request, withAdditionalIdentifier: identifier)
        return removeImage(withIdentifier: requestIdentifier)
    }
    
    /**
     Removes the image from the cache matching the given identifier.
     
     - parameter identifier: The unique identifier for the image.
     
     - returns: `true` if the image was removed, `false` otherwise.
     */
    open func removeImage(withIdentifier identifier: String) -> Bool {
        var removed = false
        
        synchronizationQueue.async(flags: .barrier, execute: {
            if let cachedImage = self.cachedImages.removeValue(forKey: identifier) {
                self.currentMemoryUsage -= cachedImage.totalBytes
                removed = true
            }
        }) 
        
        return removed
    }
    
    /**
     Removes all images stored in the cache.
     
     - returns: `true` if images were removed from the cache, `false` otherwise.
     */
    @objc public func removeAllImages() -> Bool {
        var removed = false
        
        synchronizationQueue.sync {
            if !self.cachedImages.isEmpty {
                self.cachedImages.removeAll()
                self.currentMemoryUsage = 0
                
                removed = true
            }
        }
        
        return removed
    }
    
    // MARK: Fetch Image from Cache
    
    
    /**
     Returns the image from the cache associated with an identifier created from the request and optional identifier.
     
     - parameter request:    The request used to generate the image's unique identifier.
     - parameter identifier: The additional identifier to append to the image's unique identifier.
     
     - returns: The image if it is stored in the cache, `nil` otherwise.
     */
    public func image(for request: URLRequest, withIdentifier identifier: String?) -> Image? {
        let requestIdentifier = imageCacheKeyFromURLRequest(request, withAdditionalIdentifier: identifier)
        return image(withIdentifier: requestIdentifier)
    }
    
    /**
     Returns the image in the cache associated with the given identifier.
     
     - parameter identifier: The unique identifier for the image.
     
     - returns: The image if it is stored in the cache, `nil` otherwise.
     */
    @objc public func image(withIdentifier identifier: String) -> Image? {
        var image: Image?
        
        synchronizationQueue.sync {
            if let cachedImage = self.cachedImages[identifier] {
                image = cachedImage.accessImage()
            }
        }
    
        return image
    }
    
    // MARK: Private - Helper Methods
    
    fileprivate func imageCacheKeyFromURLRequest(
        _ request: URLRequest,
        withAdditionalIdentifier identifier: String?)
        -> String
    {
        var key = request.url?.absoluteString ?? ""
        
        if let identifier = identifier {
            key += "-\(identifier)"
        }
        
        return key
    }
}
