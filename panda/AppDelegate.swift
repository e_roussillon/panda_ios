//
//  AppDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 11/14/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
//import NgKeyboardTracker
import PushKit
import ObjectMapper
import Fabric
import Crashlytics
import AlamofireImage
import Analytics
import Siren
import Firebase

public enum NotifType: Int {
    case backup = 0
    case message
    case friendship
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PKPushRegistryDelegate {
    fileprivate let sessionManager = SessionManager.sharedInstance

	var window: UIWindow?
    var voipRegistry: PKPushRegistry!

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.initKeychaine()
        self.initSiren()
        
        
        SQLManager.initTables()
        self.sessionManager.user.resetPublicChat()
        let _ = ReachabilityManager.sharedInstance
        
		self.initAppStyle()
		
//        self.keyboardAnimStart()
        self.registerVoipNotifications(application)
        self.initFabric()
        self.initSegment()
        self.initImageDownloaderAuth()
//        self.initFarebase()

        
        if let userInfo = launchOptions {
            //got notification
            self.application(application, didReceiveRemoteNotification: userInfo)
        }

		return true
	}
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.checkSerinDaily()
        
        if ReachabilityManager.sharedInstance.isConnectedToNetwork() {
            self.webSocket()
        }
    }

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.stopSocket()
        self.saveImages()
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        self.checkSerinImmediately()
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//        self.keyboardAnimStop()
        self.stopSocket()
        self.saveImages()
	}
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return false
    }

//    //
//    // MARK: Keyboard animation
//    //
//    
//    private func keyboardAnimStart() {
//        NgKeyboardTracker.sharedTracker().start()
//    }
//    
//    private func keyboardAnimStop() {
//        NgKeyboardTracker.sharedTracker().stop()
//    }
    
    //
    // MARK: Siren
    //
    
    fileprivate func initSiren() {
        let siren = Siren.shared
        siren.alertType = .force
        siren.checkVersion(checkType: .immediately)
    }
    
    fileprivate func checkSerinDaily() {
        Siren.shared.checkVersion(checkType: .daily)
    }
    
    fileprivate func checkSerinImmediately() {
        Siren.shared.checkVersion(checkType: .immediately)
    }

	//
	// MARK: App Style
	//

	fileprivate func initAppStyle() {
		// Init Style of the App
		let appDelegate = UIApplication.shared.delegate as! AppDelegate
		appDelegate.window?.tintColor = Constants.COLOR.kAppTint

		UITextField.appearance().tintColor = Constants.COLOR.kTextFieldTint
		UITextView.appearance().tintColor = Constants.COLOR.kTextViewTint
		UIToolbar.appearance().tintColor = Constants.COLOR.kToolTint
		UIToolbar.appearance().barTintColor = Constants.COLOR.kToolBarTint
        UITabBar.appearance().tintColor = Constants.COLOR.kTabbarText
        UITabBar.appearance().barTintColor = Constants.COLOR.kTabbarTint
	}

	//
	// MARK: CheckTo Removed Keychaine
	//

	fileprivate func initKeychaine() {
		let userDefaults = UserDefaults.standard

		if userDefaults.bool(forKey: "isFirstRun") == false {
            self.sessionManager.user.resetDB()
            self.sessionManager.user.resetWelcomeScreen()
            self.sessionManager.user.reset()
            
			userDefaults.set(true, forKey: "isFirstRun")
			userDefaults.synchronize() // forces the app to update the NSUserDefaults

			return
		}
	}

	//
	// MARK: WebSocket
	//

	fileprivate func webSocket() {
        let socketManager = SocketManager.sharedInstance
        
        if self.sessionManager.user.isLogin && !socketManager.isSocketConnected() {
            let _ = socketManager.startSocket()
            let userId = self.sessionManager.user.userId
            if userId > 0 && !socketManager.isJoined(ChannelType.Private, roomId: userId) {
                socketManager.joinPrivateChannel()
            }
            
        }
	}
    
    fileprivate func stopSocket() {
        SessionManager.sharedInstance.keychainUtil.resetLocalisation()
        SessionManager.sharedInstance.keychainUtil.leaveAllRooms()
        let socketManager = SocketManager.sharedInstance
        
        if socketManager.isSocketConnected() {
            socketManager.stopSocket({})
        }
    }
    
    //
    // MARK: InitFabric
    //
    
    fileprivate func initFabric() {
        if EnvironmentUtil.isTargetPanda || EnvironmentUtil.isTargetPandaBeta {
            Fabric.with([Crashlytics.self, Answers.self])
        }
    }
    
    //
    // MARK: InitFabric
    //
    
    fileprivate func initFarebase() {
        if EnvironmentUtil.isTargetPanda {
            FirebaseApp.configure()
        }
    }
    
    //
    // MARK: InitSegment
    //
    
    fileprivate func initSegment() {
        if EnvironmentUtil.isTargetPanda {
            let configuration: SEGAnalyticsConfiguration = SEGAnalyticsConfiguration.init(writeKey: EnvironmentUtil.segment)
            configuration.trackApplicationLifecycleEvents = true // Enable this to record certain application events automatically!
            configuration.recordScreenViews = true // Enable this to record screen views automatically!
            configuration.use(SEGMixpanelIntegrationFactory.init())
            SEGAnalytics.setup(with: configuration)
        }
    }
    
    
    //
    // MARK: InitPush
    //
    
    fileprivate func registerVoipNotifications(_ application: UIApplication) {
        voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
    }
    
    //
    // MARK: PKPushRegistryDelegate
    //
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        guard let token  = credentials.token.toToken() else {
            self.sessionManager.keychainUtil.token = ""
            return
        }
        
        print("token push --> \(token)")
        
        if self.sessionManager.keychainUtil.isLogin {
            self.sessionManager.uploadPushToken(token)
        } else {
            self.sessionManager.keychainUtil.pushToken = token
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        let dataPayload = payload.dictionaryPayload
        print("pushRegistry --> \(dataPayload)")
        
        guard let typeInt = dataPayload["type"] as? Int,
            let type: NotifType = NotifType(rawValue: typeInt), sessionManager.user.isLogin else {
                return
        }
        
        switch type {
        case .backup:
            BackupManager.sharedInstance.backupPending()
            break
        case .message:
            if let data = dataPayload["data"] as? [String: Any],
                let privateMessage = SocketJoinerManager.handleNotifMessage(data),
                let messageId = privateMessage.messageId,
                let status = privateMessage.status {
                
                switch status {
                case .sent:
                    ChatManager.sharedInstance.receivedPrivareMessages([(id: messageId, status: StatusType.received)])
                    UILocalNotification.showPrivateMessage(privateMessage)
                    break
                case .read:
                    ChatManager.sharedInstance.receivedPrivareMessages([(id: messageId, status: StatusType.confirmed)])
                    break
                default:
                    break
                }
            }
            break
        case .friendship:
            if let data = dataPayload["data"] as? [String: Any],
                let user = Mapper<UserModel>().map(JSON: data),
                let pseudo = user.pseudo,
                let userId = user.userId {
                user.update()
                UILocalNotification.showFriendship(pseudo, userId: userId)
            }
            break
        }
        
        
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType){
        print(type)
    }
    
    //
    // MARK: Got notification
    //
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if let userInfo = notification.userInfo {
            self.application(application, didReceiveRemoteNotification: userInfo)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]){
        guard let typeInt = userInfo["type"] as? Int,
            let type: NotifType = NotifType(rawValue: typeInt),
            let data = userInfo["data"] as? [String: Any] else {
                return
        }
        
        switch type {
        case .backup: break
        case .message:
            if let privateMessage = Mapper<PrivateMessageModel>().map(JSON: data),
                let userId = privateMessage.fromUserId,
                let friendshipStatus = privateMessage.friendshipStatus,
                let friendshipLastAction = privateMessage.friendshipLastAction {
                let user = UserModel.getUser(userId)
                UIViewController.pushNotificationMessage(user, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
            }
            break
        case .friendship:
            if let user = Mapper<UserModel>().map(JSON: data) {
                SocketNotifierManager.sharedInstance.onUpdateFriendship.dispatch(user)
                UIViewController.pushNotificationFriendship(user)
            }
            break
        }
    }
    
    //
    // MARK: Init Auth
    //
    
    fileprivate func initImageDownloaderAuth() {
        let cacheCapacity: UInt64 = 100 * 1024 * 1024
        let cachePurgeCapacity: UInt64 = 60 * 1024 * 1024
        
        let imageCache: ImageRequestCache = KeychainUtil.sharedInstance.saveImages ?? SaveImage(memoryCapacity: cacheCapacity, preferredMemoryUsageAfterPurge: cachePurgeCapacity)
        let downloader = ImageDownloader(configuration: ImageDownloader.defaultURLSessionConfiguration(), imageCache: imageCache)
        
        UIImageView.af_sharedImageDownloader = downloader
        UIButton.af_sharedImageDownloader = downloader
    }
    
    fileprivate func saveImages() {
        if let images = UIImageView.af_sharedImageDownloader.imageCache as? SaveImage {
            KeychainUtil.sharedInstance.saveImages = images
        }
    }
    
}

