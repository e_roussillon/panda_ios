//
//  BaseAPI.swift
//  carrefour
//
//  Created by Edouard Roussillon on 12/02/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Alamofire
import ObjectMapper

open class BaseAPI {
    fileprivate static let baseAPIManager: BaseAPIManager = BaseAPIManager.sharedInstance
    
    fileprivate init() {}
    
    static func GET<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.sendRequest(RequestMethod.GET, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure)
    }
    
    static func POST<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.sendRequest(RequestMethod.POST, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure)
    }
    
    static func PATCH<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.sendRequest(RequestMethod.PATCH, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure)
    }
    
    static func PUT<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.sendRequest(RequestMethod.PUT, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure)
    }
    
    static func DELETE<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        
        BaseAPI.sendRequest(RequestMethod.DELETE, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure)
    }
    
    static func UPLOAD<T: Mappable>(_ api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = Constants.WS.kAPI, type: T.Type, progress: @escaping ProgressHandler, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        
        BaseAPI.sendRequestUpload(RequestMethod.POST, api: api, parameters: parameters, header: header, baseUrl: baseUrl, type: type, successHandler: success, failureHandler: failure, progressHandler: progress)
    }
    
    //
    //MARK: Private Method
    //
    
    fileprivate static func sendRequest<T: Mappable>(_ method: RequestMethod, api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = "", type: T.Type, successHandler: @escaping SuccessHandler, failureHandler: @escaping FailHandler) {
                
        let request: RequestInfo<T> = RequestInfo(method: method, api: api, parameters: parameters, header: header, baseUrl: baseUrl, successHandler: successHandler, failureHandler: failureHandler)
        
        baseAPIManager.startRequest(request)
    }
    
    fileprivate static func sendRequestUpload<T: Mappable>(_ method: RequestMethod, api: String, parameters: [String: Any] = [:], header: [String: String] = [:], baseUrl: String = "", type: T.Type, successHandler: @escaping SuccessHandler, failureHandler: @escaping FailHandler, progressHandler: @escaping ProgressHandler) {

        let request: RequestInfo<T> = RequestInfo(method: method, api: api, parameters: parameters, header: header, baseUrl: baseUrl, successHandler: successHandler, failureHandler: failureHandler, progressHandler: progressHandler)
        
        baseAPIManager.startUpload(request)
    }
    
   

}
