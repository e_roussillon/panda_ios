//
//  HelperBaseAPI.swift
//  carrefour
//
//  Created by Edouard Roussillon on 1/20/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Alamofire
import ObjectMapper

protocol ConnectorManager {
    func startRequest<T: Mappable>(_ info: RequestInfo<T>)
    func startUpload<T: Mappable>(_ info: RequestInfo<T>)
}

class BaseAPIManager {
    static let sharedInstance: BaseAPIManager = BaseAPIManager()
    var connector: ConnectorManager = BaseAPIManagerAlamofire.instance
    
    fileprivate let sessionManager: SessionManager = SessionManager.sharedInstance
    fileprivate let analyticsManager: AnalyticsManager = AnalyticsManager.sharedInstance
    fileprivate let baseAPIHandler: BaseAPIHandler = BaseAPIHandler()
    fileprivate let resquestsQueues: [Mappable] = []
    
    
    fileprivate var isRefreshingToken: Bool = false
    
    fileprivate var requestsQueue = [RunRequest]()
    
    
    fileprivate init() {}
    
    //
    // MARK: ConnectorManager
    //
    
    func startRequest<T: Mappable>(_ info: RequestInfo<T>, forceRequest: Bool = false) {
        var request: RequestInfo<T> = info
        var mHeader: [String: String]! = request.header
        
        print("\n***** START REQUEST API - \(info.api) *****")
        
        mHeader["Content-Type"] = Constants.HEADER.JSON
        mHeader["Accept-Language"] = Constants.HEADER.LANGUAGE
        mHeader["App-Id"] = Constants.HEADER.APP_ID
        mHeader["Device-Id"] = Constants.HEADER.DEVICE_ID
        mHeader["Accept-Encoding"] = Constants.HEADER.ENCODING
        if self.sessionManager.user.isLogin {
            mHeader["Authorization"] = "Bearer " + self.sessionManager.user.token
        }
        
        request.header = mHeader
        
        if self.runRequest(info, forceRequest: forceRequest) {
            self.connector.startRequest(request)
        }
    }
    
    func startUpload<T: Mappable>(_ info: RequestInfo<T>, forceRequest: Bool = false) {
        
        var request: RequestInfo<T> = info
        var mHeader: [String: String]! = request.header
        
        print("\n***** START REQUEST API - \(info.api) *****")
        
        mHeader["Content-Type"] = Constants.HEADER.DATA
        mHeader["Accept-Language"] = Constants.HEADER.LANGUAGE
        mHeader["App-Id"] = Constants.HEADER.APP_ID
        mHeader["Device-Id"] = Constants.HEADER.DEVICE_ID
        mHeader["Accept-Encoding"] = Constants.HEADER.ENCODING
        if self.sessionManager.user.isLogin {
            mHeader["Authorization"] = "Bearer " + self.sessionManager.user.token
        }
        
        request.header = mHeader
        
        if self.runRequest(info, forceRequest: forceRequest) {
            self.connector.startUpload(request)
        }
    }
    
    fileprivate func runRequest<T: Mappable>(_ info: RequestInfo<T>, forceRequest: Bool) -> Bool {
        //run refresh token in priority
        if info.api == Constants.WS.kRenewToken || forceRequest {
            return true
        } else if isRefreshingToken {
            //add in queue new request if we are still updating token
            self.addItemToQueue(info)
            return false
        } else {
            //check if queue is empty, if not the token process is not done...
            if requestsQueue.count > 0 {
                self.addItemToQueue(info)
                return false
            } else {
                return true
            }
        }
    }
    
    //
    // MARK: Handler Request
    //
    
    func handleRequest<T: Mappable>(_ requestInfo: RequestInfo<T>, requestResponse: RequestResponse<T>) {
        self.runNextInQueue()
        
        guard let status = requestResponse.status,
            let dataString = requestResponse.data?.toString()
            else {

                consoleInfo(requestInfo, data: requestResponse.data?.toString() as AnyObject? ?? "" as AnyObject, error: requestResponse.error)
                
                if !requestInfo.failureHandler(nil, false) {
                    baseAPIHandler.genericErrorHandler()
                }
 
                return
        }
        
        consoleInfo(requestInfo, data: dataString as AnyObject, status: status, error: requestResponse.error)
        
        switch status {
        case 200...299:
            if let list = requestResponse.getObjects() {
                requestInfo.successHandler(list)
            } else if let obj = requestResponse.getObject() {
                requestInfo.successHandler(obj)
            } else {
                let i = T(map: Map.emptyMap())
                requestInfo.successHandler(i)
            }
            break
        default:
            guard let error: ErrorResponse = requestResponse.getError() else {
                let _ = requestInfo.failureHandler(nil, true)
                
                if !baseAPIHandler.handlerError(requestInfo, responseError: nil, error: requestResponse.error) {
                    baseAPIHandler.genericErrorHandler()
                }
                return
            }
            
            let isErrorComsumed = baseAPIHandler.handlerError(requestInfo, responseError: error, error: requestResponse.error, status: status)
            if !requestInfo.failureHandler(error, isErrorComsumed) {
                baseAPIHandler.genericErrorHandler()
            }
            
            break
        }
    }
    
    
    //
    // MARK: Console Info
    //
    
    fileprivate func consoleInfo<T: Mappable>(_ requestInfo: RequestInfo<T>,
                                          data: AnyObject,
                                          status: Int = -1,
                                          error: Error?) {
        
        var logs:[String] = ["***** REQUEST API - \(requestInfo.api) *****",
                               "----- STATUS ----",
                               "       * \(status)",
                               "----- URL ----",
                               "       * \(self.baseURL(requestInfo.api, baseUrl: requestInfo.baseUrl))"]
        if let header = requestInfo.header {
            logs.append("----- HEADER -----")
            for item in header {
                logs.append("       *  \(item.0) -> \(item.1)")
            }
        }
        if let parameters = requestInfo.parameters {
            logs.append("----- PARAM -----")
            for item in parameters {
                logs.append("       *  \(item.0) -> \(item.1)")
            }
        }
        logs.append("----- DATA -----")
        logs.append("       *  \(data)")
        logs.append("***** ENDED : *****\n")
        print(logs)
        
        self.sendAnalytics(requestInfo, data: data, status: status, error: error)
    }
    
    fileprivate func sendAnalytics<T: Mappable>(_ requestInfo: RequestInfo<T>,
                               data: AnyObject,
                               status: Int = -1,
                               error: Error?) {
        var param:  [String: Any] = [KeyName.RequestAPI.rawValue : requestInfo.api,
                                           KeyName.RequestParams.rawValue : requestInfo.parameters as AnyObject? ?? "",
                                           KeyName.RequestStatus.rawValue : "\(status)"]
        
        if let e = error, status <= -1 {
            param[KeyName.RequestError.rawValue] = e.localizedDescription
            self.analyticsManager.trackAction(ActionName.RequestFailed, params: param)
        } else if status > 299 {
            param[KeyName.RequestError.rawValue] = data
            self.analyticsManager.trackAction(ActionName.RequestFailed, params: param)
        }
    }
    
    //
    // MARK: Loader status bar
    //
    
    func showStatusLoading(_ showLoadding: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = showLoadding
    }
    
    //
    // MARK: init Base URL
    //
    
    func baseURL(_ api: String, baseUrl: String?) -> String {
        var url = ""
        
        if baseUrl != nil {
            url = baseUrl! + api
        } else {
            url = Constants.WS.kAPI + api
        }
        
        return url
    }
    
    //
    // MARK: Refech token
    //
    
    func addItemToQueue<T: Mappable>(_ info: RequestInfo<T>) {
        if info.api == Constants.WS.kLogout {
            return
        }
        
        requestsQueue.append({
            if info.isUploadRequest() {
                self.startUpload(info, forceRequest: true)
            } else {
                self.startRequest(info, forceRequest: true)
            }

        })
    }
    
    fileprivate func runNextInQueue() {
        if let item = self.requestsQueue.first, isRefreshingToken == false {
            item()
            let _ = self.requestsQueue.remove(at: 0)
        }
    }

    static func startRefreshToken() {
        let baseAPIManager = BaseAPIManager.sharedInstance
        
        if baseAPIManager.isRefreshingToken {
            return
        }
        
        baseAPIManager.isRefreshingToken = true
        
        baseAPIManager.sessionManager.onRefreshSessionSucceed.addOnce(baseAPIManager, handler: BaseAPIManager.onRefreshSessionSucceed)
        baseAPIManager.sessionManager.onRefreshSessionFailed.addOnce(baseAPIManager, handler: BaseAPIManager.onRefreshSessionFailed)
        
        baseAPIManager.sessionManager.refreshToken()
    }
    
    func onRefreshSessionSucceed() {
        self.isRefreshingToken = false
        self.runNextInQueue()
    }
    
    func onRefreshSessionFailed(_ error: ErrorResponse?) {
        self.isRefreshingToken = false
        self.requestsQueue = [RunRequest]()
        BaseViewController.lauchLogoutForced()
    }
    
    
}
