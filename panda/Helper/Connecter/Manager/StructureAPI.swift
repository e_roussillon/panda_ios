//
//  APIManager.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

typealias SuccessHandler = (_ data: Any?) -> Void;
typealias FailHandler = (_ response: ErrorResponse?, _ isErrorComsumed: Bool?) -> Bool;
typealias ProgressHandler = (_ bytesWritten: Int64?, _ totalBytesWritten: Int64?, _ totalBytesExpected: Int64?) -> Void;
typealias RunRequest = () -> Void

enum RequestMethod: String {
    case OPTIONS, GET, HEAD, POST, PUT, PATCH, DELETE, TRACE, CONNECT
}

struct RequestInfo<T: Mappable> {
    let method: RequestMethod
    let api: String!
    var parameters: [String: Any]? = nil
    var header: [String: String]? = nil
    var baseUrl: String? = nil
    let successHandler: SuccessHandler!
    let failureHandler: FailHandler!
    var progressHandler: ProgressHandler?
    
    init(method: RequestMethod, api: String!, parameters: [String: Any]? = nil, header: [String: String]? = nil, baseUrl: String? = nil, successHandler: SuccessHandler!, failureHandler: FailHandler!, progressHandler: ProgressHandler? = nil) {
        self.method = method
        self.api = api
        self.parameters = parameters
        self.header = header
        self.baseUrl = baseUrl
        self.successHandler = successHandler
        self.failureHandler = failureHandler
        self.progressHandler = progressHandler
    }
    
    func isUploadRequest() -> Bool {
        return progressHandler != nil
    }
}

public struct RequestResponse<T: Mappable> {
    let status: Int?
    let data: Data?
    let error: Error?
    
    init(status: Int?, data: Data?, error: Error?) {
        self.status = status
        self.data = data
        self.error = error
    }
    
    func getObject() -> T? {
        guard let data = data, let string = data.toString() else {
            return nil
        }
        
        if T.self == EmptyModel.self {
            return Mapper<T>().map(JSON: [:])
        } else {
            return Mapper<T>().map(JSONString: string)
        }
    }
    
    func getObjects() -> [T]? {
        guard let data = data,
            let string = data.toString(), string.length() > 0 && string[0] == "[" else {
            return nil
        }
        
        if let list = Mapper<T>().mapArray(JSONString: string) {
            return list
        }
        
        return nil
    }
    
    func getError() -> ErrorResponse? {
        if let data = data, let string = data.toString() {
            return Mapper<ErrorResponse>().map(JSONString: string)
        }
        
        return nil
    }
}
