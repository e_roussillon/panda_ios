//
//  BaseAPIManagerAlamofire.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Alamofire
import ObjectMapper

extension Request {
    public func debugLog() -> Self {
        if EnvironmentUtil.debugInfo {
            print("***** CURL *****")
            debugPrint(self)
            print("________________")
            
        }
        return self
    }
}

class BaseAPIManagerAlamofire: Alamofire.SessionManager, ConnectorManager {
    static let instance: BaseAPIManagerAlamofire = BaseAPIManagerAlamofire.shared
    
//    let baseAPIHelper: BaseAPIHelper = BaseAPIHelper()
    
    fileprivate override init(configuration: URLSessionConfiguration = URLSessionConfiguration.default,
                          delegate: SessionDelegate = SessionDelegate(),
                          serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        super.init(configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
    }
    
    fileprivate static let shared: BaseAPIManagerAlamofire = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "localhost": .disableEvaluation
        ]
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.WS.kTimeoutSec
        configuration.timeoutIntervalForResource = Constants.WS.kTimeoutSec
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        return BaseAPIManagerAlamofire(configuration: configuration,
                              serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }()
    
    //
    // MARK: APIManager
    //
    
    func startRequest<T: Mappable>(_ requestInfo: RequestInfo<T>) {
        let baseAPIManager = BaseAPIManager.sharedInstance
        baseAPIManager.showStatusLoading(true)
        
        let mType: ParameterEncoding = self.getMethod(requestInfo.method) == HTTPMethod.get ? URLEncoding.default : JSONEncoding.default
        let url = baseAPIManager.baseURL(requestInfo.api, baseUrl: requestInfo.baseUrl)
        
        self.request(url, method: self.getMethod(requestInfo.method), parameters: requestInfo.parameters, encoding: mType, headers: requestInfo.header).debugLog().responseJSON { (response) in
            baseAPIManager.showStatusLoading(false)
            
            let requestResponse = RequestResponse<T>(status: response.response?.statusCode, data: response.data, error: response.result.error)
            baseAPIManager.handleRequest(requestInfo, requestResponse: requestResponse)
        }
    }
    
    func startUpload<T: Mappable>(_ requestInfo: RequestInfo<T>) {
        let baseAPIManager = BaseAPIManager.sharedInstance
        baseAPIManager.showStatusLoading(true)
        
        let url = baseAPIManager.baseURL(requestInfo.api, baseUrl: requestInfo.baseUrl)
        
        
        self.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in requestInfo.parameters ?? [:] {
                if key == "path" {
                    multipartFormData.append(value as! URL, withName: key)
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        },to: url, method: .post, headers: requestInfo.header)  { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _ , _):
                
                upload.uploadProgress(closure: { (progress) in
                    DispatchQueue.main.async {
                        if let progressHandler = requestInfo.progressHandler {
                            progressHandler(0, progress.completedUnitCount, progress.totalUnitCount)
                        }
                    }
                })
                
                upload.responseJSON { response in
                    baseAPIManager.showStatusLoading(false)
                    let requestResponse = RequestResponse<T>(status: response.response?.statusCode, data: response.data, error: response.result.error)
                    baseAPIManager.handleRequest(requestInfo, requestResponse: requestResponse)
                }
                break
            case .failure(_):
                baseAPIManager.showStatusLoading(false)
                let requestResponse = RequestResponse<T>(status: nil, data: nil, error: nil)
                baseAPIManager.handleRequest(requestInfo, requestResponse: requestResponse)
            }
        }
    }
    
    //
    // MARK: Private
    //
    
    fileprivate func getMethod(_ method: RequestMethod) -> HTTPMethod {
        switch method {
        case RequestMethod.OPTIONS:
            return HTTPMethod.options
        case RequestMethod.GET:
            return HTTPMethod.get
        case RequestMethod.HEAD:
            return HTTPMethod.head
        case RequestMethod.POST:
            return HTTPMethod.post
        case RequestMethod.PUT:
            return HTTPMethod.put
        case RequestMethod.PATCH:
            return HTTPMethod.patch
        case RequestMethod.DELETE:
            return HTTPMethod.delete
        case RequestMethod.TRACE:
            return HTTPMethod.trace
        case RequestMethod.CONNECT:
            return HTTPMethod.connect
        }
    }
}
