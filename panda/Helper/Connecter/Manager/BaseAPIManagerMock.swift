//
//  BaseAPIManagerMock.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

protocol BaseAPIManagerMockDelegate {
    func response<T: Mappable>(_ requestInfo: RequestInfo<T>) -> RequestResponse<T>
}

class BaseAPIManagerMock: ConnectorManager {
    
    let baseAPIManager: BaseAPIManager = BaseAPIManager.sharedInstance
    let baseAPIManagerMockDelegate: BaseAPIManagerMockDelegate!
    
    init(delegate: BaseAPIManagerMockDelegate!) {
        self.baseAPIManagerMockDelegate = delegate
    }
    
    //
    // MARK: APIManager
    //
    
    func startRequest<T: Mappable>(_ requestInfo: RequestInfo<T>) {
        let requestResponse = self.baseAPIManagerMockDelegate.response(requestInfo)
        self.baseAPIManager.handleRequest(requestInfo, requestResponse: requestResponse)
    }
    
    func startUpload<T: Mappable>(_ requestInfo: RequestInfo<T>) {
        
    }
}
