//
//  BaseAPIHandler.swift
//  carrefour
//
//  Created by Edouard Roussillon on 1/20/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseAPIHandler {
    
    
    //
    // MARK: ERROR REQUEST
    //
    
    func handlerError<T: Mappable>(_ requestInfo: RequestInfo<T>, responseError: ErrorResponse?, error: Error? = nil, status: Int = -1) -> Bool {
        if status != -1 {
            return self.handlerError400(requestInfo, responseError: responseError, status: status)
        } else if self.hadNoConnection(error) {
            return self.handleNoConnection(error!)
        } else if self.hadTimeOut(error) {
            return self.handleTimeOut(error!)
        } else if self.hadInternalErrorStatusCode(responseError, error: error) {
            return self.handleInternalErrorStatusCode(responseError, error: error)
        }
        
        return false
    }
    
    //
    // MARK: GENERIC DIALOG
    //
    
    func genericErrorHandler() {
        BaseViewController.showAlertGeneric()
    }
    
    //
    // MARK - TEST ERROR
    //
    
    fileprivate func hadInternalErrorStatusCode(_ response: ErrorResponse?, error: Error?) -> Bool {
        if let httpResponse: Int = response?.code {
            if httpResponse == 500 {
                return true
            }
        }
        
//        if let errorCode: Int = error?.code {
//            if errorCode == 500 {
//                return true
//            }
//        }
        
        return false
    }
    
    
    
    fileprivate func hadTimeOut(_ error: Error?) -> Bool {
//        if let errorCode: Int = error?.code {
//            if errorCode == -1001 || errorCode == -1003 {
//                return true
//            }
//        }
        
        return false
    }
    
    fileprivate func hadNoConnection(_ error: Error?) -> Bool {
        
//        if let errorCode: Int = error?.code {
//            if errorCode == -1009 || errorCode == -1004 {
//                return true
//            }
//        }
        
        return false
    }
    
    //
    //MARK - HANDLE ERROR
    //
    
    fileprivate func handleInternalErrorStatusCode(_ response: ErrorResponse?, error: Error?) -> Bool {
        //TODO SHOW MESSAGE
        //logger.error("InternalError STATUS 500")
        self.genericErrorHandler()
        return true
    }
    
    
    
    fileprivate func handleTimeOut(_ error: Error?) -> Bool {
        //TODO SHOW MESSAGE
        //        logger.error("Time Out")
        self.genericErrorHandler()
        return true
    }
    
    fileprivate func handleNoConnection(_ error: Error?) -> Bool {
        //TODO SHOW MESSAGE
        //        logger.error("Internet connection")
        self.genericErrorHandler()
        return true
    }

    
    //
    // MARK - Handler 400 - 499
    //
    
    fileprivate func handlerError400<T: Mappable>(_ requestInfo: RequestInfo<T>, responseError: ErrorResponse?, status: Int) -> Bool {
        guard let type = responseError?.type, let codeInt = responseError?.code else {
            return false
        }
        
        switch status {
        case 401:
            switch type {
            case .auth:
                guard let code = ErrorAuth(rawValue: codeInt) else {
                    return false
                }
                
                switch code {
                case .user_REPORTED:
                    guard let time = responseError?.details?["time"] as? Int else {
                        BaseViewController.lauchLogoutForced()
                        return false
                    }
                    
                    let _ = requestInfo.failureHandler(responseError, true)
                    BaseViewController.lauchLogoutReported(time, logoutUser: !(requestInfo.api == Constants.WS.kLogin))
                    return false
                case .token_INVALID:
                    
                    let baseAPIManager = BaseAPIManager.sharedInstance
                    baseAPIManager.addItemToQueue(requestInfo)
                    
                    BaseAPIManager.startRefreshToken()
                    return false
                case .token_NOT_FOUND:
                    BaseViewController.lauchLogoutForced()
                    return false
                default:
                    return false
                }
            default:
                return false
            }
        case 409:
            switch type {
            case .auth:
                guard let code = ErrorAuth(rawValue: codeInt) else {
                    BaseViewController.lauchLogoutForced()
                    return false
                }
                
                switch code {
                case .user_DUPLICATED:
                    BaseViewController.lauchLogoutForced()
                default:
                    return false
                }
            default:
                return false
            }
        default:
            return false
        }
        
        return true
    }
    
    

}
