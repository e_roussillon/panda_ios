//
//  EnumAPI.swift
//  panda
//
//  Created by Edouard Roussillon on 11/16/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

public enum ErrorType: Int {
    case db = 0
    case changeset
    case localization
    case public_ROOM
    case auth
    case user_FRIENDSHIP
    case private_MESSAGE
    case message_SENDER
    case user
}

public enum ErrorAuth: Int {
    case password_OR_LOGIN = 0
    case token_NOT_FOUND
    case token_INVALID
    case token_NOT_RENEW
    case user_REPORTED
    case user_DUPLICATED
}
