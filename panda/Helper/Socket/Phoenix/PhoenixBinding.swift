//
//  PhoenixBinding.swift
//  panda
//
//  Created by Edouard Roussillon on 6/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class PhoenixBinding {
    var event: String
    var callback: ([String: Any]) -> Void?
    
    init(event: String, callback: @escaping ([String: Any]) -> Void?) {
        (self.event, self.callback) = (event, callback)
    }
    
}
