//
//  PhoenixChannel.swift
//  panda
//
//  Created by Edouard Roussillon on 6/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class PhoenixChannel {
    fileprivate var bindings: [PhoenixBinding] = []
    fileprivate(set) var topic: String?
    fileprivate(set) var callback: ((PhoenixChannel) -> Void?)
    fileprivate var socket: PhoenixSocket?
    fileprivate let socketConnectorManager = SocketConnectorManager.connectorManager
    
    init(topic: String, callback: @escaping ((PhoenixChannel) -> Void), socket: PhoenixSocket) {
        (self.topic, self.callback, self.socket) = (topic, { callback($0) }, socket)
        reset()
    }
    
    func reset() {
        bindings = []
    }
    
    func on(_ event: String, callback: @escaping ([String: Any]) -> Void) {
        bindings.append(PhoenixBinding(event: event, callback: { callback($0) }))
    }
    
    func isMember(topic: String) -> Bool {
        return self.topic == topic
    }
    
    fileprivate func off(_ event: String) {
        var newBindings: [PhoenixBinding] = []
        for binding in bindings {
            if binding.event != event {
                newBindings.append(PhoenixBinding(event: binding.event, callback: binding.callback))
            }
        }
        bindings = newBindings
    }
    
    func trigger(_ triggerEvent: String, msg: [String: Any]) {
        for binding in bindings {
            if binding.event == triggerEvent {
                binding.callback(msg)
            }
        }
    }
    
    fileprivate func send(_ event: String, message: PhoenixMessage) {
        print("conn sending")
        let payload = PhoenixPayload.initPlayload(topic: topic!, event: event, playload: message)
        let _ = self.socketConnectorManager.send(payload)
    }
    
    func leave(_ message: PhoenixMessage) {
        if let sock = socket {
            sock.leave(topic: topic!, message: message)
        }
        reset()
    }
}
