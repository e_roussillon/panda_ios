//
//  PhoenixMessage.swift
//  panda
//
//  Created by Edouard Roussillon on 6/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class PhoenixMessage: Mappable {
    fileprivate(set) var subject: String?
    fileprivate(set) var body: String?
    fileprivate(set) var message: AnyObject?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        subject <- map["subject"]
        body <- map["body"]
        message <- map["message"]
    }
    
    static func initMessage(subject: String, body: String) -> PhoenixMessage {
        let disc: [String: Any] = ["subject" : subject, "body" : body]
        return Mapper<PhoenixMessage>().map(JSON: disc)!
    }
    
    static func initMessage(message: [String: Any]) -> PhoenixMessage {
        let disc: [String: Any] = ["message" : message]
        return Mapper<PhoenixMessage>().map(JSON: disc)!
    }
}
