//
//  Path.swift
//  SwiftPhoenix
//
//  Created by Kyle Oba on 8/23/15.
//  Copyright (c) 2015 David Stump. All rights reserved.
//

import Foundation

// http://stackoverflow.com/a/24888789/1935440
// String's stringByAddingPercentEncodingWithAllowedCharacters doesn't encode + sign,
// which is ofter used in Phoenix tokens.
private let URLEncodingAllowedChars = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~/?")

private func encodePair(_ pair: (String, String)) -> String? {
    if let key = pair.0.addingPercentEncoding(withAllowedCharacters: URLEncodingAllowedChars),
        let value = pair.1.addingPercentEncoding(withAllowedCharacters: URLEncodingAllowedChars)
    { return "\(key)=\(value)" } else { return nil }
}


private func resolveUrl(_ url: URL, params: [String: String]?) -> URL {
    guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false),
        let params = params else { return url }
    
    let queryString = params.flatMap(encodePair).joined(separator: "&")
    components.percentEncodedQuery = queryString
    print("socket connection url : \(components.url ?? url)")
    return components.url ?? url
}



public struct Path {
public static func removeTrailingSlash(_ path: String) -> String {
		if path.characters.count == 0 { return path }
		if path.substring(with: path.characters.index(path.endIndex, offsetBy: -1)..<path.endIndex) == "/" {
			return path.substring(with: path.startIndex..<path.characters.index(path.endIndex, offsetBy: -1))
		}
		return path
	}

	public static func removeLeadingSlash(_ path: String) -> String {
		if path.characters.count == 0 { return path }
		if path.substring(with: path.startIndex..<path.characters.index(path.startIndex, offsetBy: 1)) == "/" {
			return path.substring(with: path.characters.index(path.startIndex, offsetBy: 1)..<path.endIndex)
		}
		return path
	}

	public static func removeLeadingAndTrailingSlashes(_ path: String) -> String {
		return Path.removeTrailingSlash(Path.removeLeadingSlash(path))
	}

    public static func endpointWithProtocol(_ prot: String, domainAndPort: String, path: String, transport: String, params: [String: String]? = nil) -> URL {
		var theProt = ""
		switch prot {
		case "ws":
			theProt = "http"
		case "wss":
			theProt = "https"
		default:
			theProt = prot
		}

		let theDomAndPort = removeLeadingAndTrailingSlashes(domainAndPort)
		let thePath = removeLeadingAndTrailingSlashes(path)
		let theTransport = removeLeadingAndTrailingSlashes(transport)
		let url = URL(string: "\(theProt)://\(theDomAndPort)/\(thePath)/\(theTransport)")
        
        return resolveUrl(url!, params: params)
	}
}
