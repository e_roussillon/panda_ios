//
//  PhoenixPayload.swift
//  panda
//
//  Created by Edouard Roussillon on 6/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class PhoenixPayload: Mappable {
    
    fileprivate static let topicLabel = "topic"
    fileprivate static let eventLabel = "event"
    fileprivate static let refLabel = "ref"
    fileprivate static let playloadLabel = "payload"
    
    fileprivate(set) var topic: String?
    fileprivate(set) var event: String?
    fileprivate(set) var ref: String?
    fileprivate(set) var payload: [String: AnyObject]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        topic <- map[PhoenixPayload.topicLabel]
        event <- map[PhoenixPayload.eventLabel]
        ref <- map[PhoenixPayload.refLabel]
        payload <- map[PhoenixPayload.playloadLabel]
    }
    
    static func initPlayload(topic: String, event: String, playload: PhoenixMessage) -> PhoenixPayload {        
        let disc: [String : Any] = [topicLabel : topic,
                    eventLabel : event,
                    playloadLabel : playload.toJSON()]
        return Mapper<PhoenixPayload>().map(JSON: disc)!
    }
    
    func toJsonString(_ ref: UInt64) -> String {
        var json = "{\"\(PhoenixPayload.topicLabel)\": \"\(self.topic!)\", \"\(PhoenixPayload.eventLabel)\": \"\(self.event!)\", \"\(PhoenixPayload.refLabel)\": \"\(ref)\", "
        
        if var disc = self.payload {
            if disc.keys.contains("message") {
                disc.removeValue(forKey: "subject")
                disc.removeValue(forKey: "body")
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: disc["message"]!, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
                    json += "\"\(PhoenixPayload.playloadLabel)\": \(jsonString)"
                    json += "}"
                    
                    return json
                } catch let error as NSError{
                    print(error.description)
                }
                
            } else {
                disc.removeValue(forKey: "message")
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: disc, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
                    json += "\"\(PhoenixPayload.playloadLabel)\": \(jsonString)"
                    json += "}"
                    
                    return json
                } catch let error as NSError{
                    print(error.description)
                }
            }

            return ""
            
        } else {
            return ""
        }
    }
}
