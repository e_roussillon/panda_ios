//
//  PhoenixSocket.swift
//  panda
//
//  Created by Edouard Roussillon on 6/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Starscream
import ObjectMapper

class PhoenixSocket: NSObject, WebSocketDelegate {
    
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let analyticsManager = AnalyticsManager.sharedInstance
    fileprivate let chatManager = ChatManager.sharedInstance
    
    fileprivate var conn: WebSocket?
    fileprivate var endPoint: URL?
    fileprivate(set) var channels: [PhoenixChannel] = []
    fileprivate var sendBuffer: [PhoenixPayload] = []
    fileprivate var sendBufferTimer: Timer?
    fileprivate let flushEverySec = 0.1
    fileprivate var reconnectTimer: Timer?
    fileprivate let reconnectAfterSec = 5
    fileprivate var heartbeatTimer: Timer?
    fileprivate let heartbeatAfterSec = 30
    fileprivate var messageReference: UInt64 = UInt64.min // 0 (max: 18,446,744,073,709,551,615)
    fileprivate var joinMessages: [String:PhoenixMessage] = [:]
    
    var numberOfTime = 0
    var forceDisconnection:Bool = false
    
    init(domainAndPort: String, path: String, transport: String, prot: String = "http", params: [String: String]? = nil) {
        super.init()
        self.updateEndPoint(domainAndPort, path: path, transport: transport, prot: prot, params: params)
    }
    
    func updateEndPoint(_ domainAndPort: String, path: String, transport: String, prot: String = "http", params: [String: String]? = nil) {
        self.endPoint = Path.endpointWithProtocol(prot, domainAndPort: domainAndPort, path: path, transport: transport, params: params)
        resetBufferTimer()
        reconnect()
    }
    
    func close(_ callback: () -> ()) {
        if let connection = self.conn {
            connection.delegate = nil
            connection.disconnect()
            self.sendAnalytics(ActionName.DisconnectionChat)
        }
        
        if !self.forceDisconnection {
            self.heartbeatTimer?.invalidate()
            self.reconnectTimer?.invalidate()
        }
        callback()
    }
    
    @objc fileprivate func reconnect() {
        self.forceDisconnection = false
        close() {
            if !self.forceDisconnection {
                self.conn = WebSocket(url: self.endPoint!)
                if let connection = self.conn {
                    connection.delegate = self
                    connection.connect()
                }
            } else {
                self.heartbeatTimer?.invalidate()
                self.reconnectTimer?.invalidate()
            }
        }
    }
    
    fileprivate func resetBufferTimer() {
        sendBufferTimer?.invalidate()
        sendBufferTimer = Timer.scheduledTimer(timeInterval: TimeInterval(flushEverySec), target: self, selector: #selector(PhoenixSocket.flushSendBuffer), userInfo: nil, repeats: true)
    }
    
    fileprivate func onOpen() {
        self.socketNotifierManager.onStopTimerNotification.dispatch(Void())
        reconnectTimer?.invalidate()
        heartbeatTimer?.invalidate()
        heartbeatTimer = Timer.scheduledTimer(timeInterval: TimeInterval(heartbeatAfterSec), target: self, selector: #selector(PhoenixSocket.sendHeartbeat), userInfo: nil, repeats: true)
        rejoinAll()
    }
    
    fileprivate func onClose(_ event: String) {
        let time: Int = reconnectAfterSec * self.numberOfTime
        self.socketNotifierManager.onResetTimerNotification.dispatch(time)
        print("Try to Connect again in : \(time) sec")
        heartbeatTimer?.invalidate()
        reconnectTimer?.invalidate()
        reconnectTimer = Timer.scheduledTimer(timeInterval: TimeInterval(time + 1), target: self, selector: #selector(PhoenixSocket.reconnect), userInfo: nil, repeats: true)
    }
    
    fileprivate func onError(_ error: NSError) {
        print("Error: \(error)")
    }
    
    func isConnected() -> Bool {
        if let connection = self.conn {
            return connection.isConnected
        } else {
            return false
        }
    }
    
    fileprivate func rejoinAll() {
        for chan in channels {
            rejoin(chan as PhoenixChannel)
        }
    }
    
    fileprivate func rejoin(_ chan: PhoenixChannel) {
        chan.reset()
        
        if let topic = chan.topic {
            for (topic_message, joinMessage) in joinMessages {
                if topic == topic_message {
                    let payload = PhoenixPayload.initPlayload(topic: topic, event: "phx_join", playload: joinMessage)
                    send(payload)
                    chan.callback(chan)
                }
            }
        }
    }
    
    func refreshToken(_ topic: String, message: PhoenixMessage) {
        if self.joinMessages.keys.contains(topic) {
            let oldMessage = self.joinMessages[topic]
            
            if oldMessage?.subject != message.subject || oldMessage?.body != message.body {
                self.joinMessages[topic] = message
                for chan in channels {
                    if chan.topic == topic {
                        rejoin(chan)
                        break
                    }
                }
            }
        }
    }
    
    func isJoined(topic: String) -> Bool {
        for chan in channels {
            if chan.isMember(topic: topic) {
                return true
            }
        }
        
        return false
    }
    
    func join(topic: String, message: PhoenixMessage, callback: @escaping ((PhoenixChannel) -> Void)) {
        if !isJoined(topic: topic) {
            let chan = PhoenixChannel(topic: topic, callback: callback, socket: self)
            self.joinMessages[topic] = message
            channels.append(chan)
            print("joining topic: -> \(topic)")
            rejoin(chan)
        }
    }
    
    func leave(topic: String, message: PhoenixMessage) {
        let payload = PhoenixPayload.initPlayload(topic: topic, event: "phx_leave", playload: message)
        print("leave topic: -> \(topic)")
        send(payload)
        
        var newChannels: [PhoenixChannel] = []
        for chan in channels {
            let c = chan as PhoenixChannel
            if !c.isMember(topic: topic) {
                newChannels.append(c)
            }
        }
        channels = newChannels
    }
    
    func leaveAllWithoutMessage() {
        self.channels = []
        self.sendBuffer = []
        self.joinMessages = [:]
    }
    
    func send(_ data: PhoenixPayload) {
        
        if isConnected() {
            doSendBuffer(data)
        } else {
            if let e = data.event, e != "phx_join" && e != "phx_leave" {
                sendBuffer.append(data)
            }
        }
    }
    
    @objc fileprivate func flushSendBuffer() {
        if isConnected() && sendBuffer.count > 0 {
            for data in sendBuffer {
                doSendBuffer(data)
            }
            sendBuffer = []
            resetBufferTimer()
        }
    }
    
    fileprivate func doSendBuffer(_ data: PhoenixPayload) {
        if let connection = self.conn {
            let json = data.toJsonString(makeRef())
            connection.write(string: json)
            print("send to channel: \(json)")
        }
    }
    
    @objc fileprivate func sendHeartbeat() {
        let heartbeatMessage = PhoenixMessage.initMessage(subject: "status", body: "heartbeat")
        let payload = PhoenixPayload.initPlayload(topic: "phoenix", event: "heartbeat", playload: heartbeatMessage)
        send(payload)
    }
    
    fileprivate func onMessage(_ payload: PhoenixPayload) {
        let (topic, event, message) = (payload.topic!, payload.event!, payload.payload!)
        for chan in channels {
            if chan.isMember(topic: topic) {
                chan.trigger(event, msg: message)
            }
        }
    }
    
    //
    // MARK: WebSocketDelegate
    //
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("socket message: \(text)")
        let payload = Mapper<PhoenixPayload>().map(JSONString: text)!
        self.onMessage(payload)
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
         print("got some data: \(data.count)")
    }
    
    func websocketDidConnect(socket: WebSocketClient) {
        self.sendAnalytics(ActionName.ConnectionChat)
        self.numberOfTime = 0
        print("socket opened")
        onOpen()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("socket closed: \(String(describing: error?.localizedDescription))")
        
        //TODO - Need to check
//        if error?.code == 403 {
//            //Invalid HTTP upgrade
//            BaseAPIManager.startRefreshToken()
//        } else {
            self.sendAnalytics(ActionName.LostConnectionChat)
            self.numberOfTime += (self.numberOfTime == 0) ? 1 : self.numberOfTime
        onClose("reason: \(String(describing: error?.localizedDescription))")
//        }
    }
    
    func websocketDidWriteError(_ error: NSError?) {
        onError(error!)
    }
    
    fileprivate func unwrappedJsonString(_ string: String?) -> String {
        if let stringVal = string {
            return stringVal
        } else {
            return ""
        }
    }
    
    fileprivate func makeRef() -> UInt64 {
        let newRef = messageReference + 1
        messageReference = (newRef == UINT64_MAX) ? 0 : newRef
        return newRef
    }
    
    //
    // MARK: Analytics
    //
    
    
    fileprivate func sendAnalytics(_ action: ActionName) {
        let param: [String: Any] = [KeyName.UserId.rawValue: self.sessionManager.user.userId,
                                          KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo]
        self.analyticsManager.trackAction(action, params: param)
    }
    
}
