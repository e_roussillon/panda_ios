//
//  GenericMessageSender.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

enum ChannelType: String {
    case Generic = "general_channel"
    case Sale = "sale_channel"
    case Broadcast = "broadcast_channel"
    case Private = "private_channel"
}
//
//protocol GenericMessageSender {
//    var topicType: ChannelType! { get }
//    var message: String!  { get }
//}
