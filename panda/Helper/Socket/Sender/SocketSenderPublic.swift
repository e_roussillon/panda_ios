//
//  SocketSenderPublic.swift
//  panda
//
//  Created by Edouard Roussillon on 5/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

protocol SocketSenderPublic {
    func sendMessage(publicMessage: PublicMessageModel, socketConnector: SocketConnector)
    func resendMessage(publicMessage: PublicMessageModel, socketConnector: SocketConnector)
    func sendWelcomeMessage(publicMessage: PublicMessageModel)
}

extension SocketSenderPublic {
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    fileprivate var socketNotifierManager: SocketNotifierManager {
        get {
            return SocketNotifierManager.sharedInstance
        }
    }
    
    fileprivate var chatManager: ChatManager {
        get {
            return ChatManager.sharedInstance
        }
    }
    
    func sendMessage(publicMessage: PublicMessageModel, socketConnector: SocketConnector) {
        let rowId: Int = publicMessage.insertLight()
        
        if rowId > 0 && self.chatManager.findRooms.token != nil && self.sessionManager.user.userId > 0 {
            
            publicMessage.rowId = rowId
            publicMessage.insert()
            
            self.send(publicMessage, socketConnector: socketConnector)
            self.socketNotifierManager.onClearTextNotification.dispatch(Void())
        } 
    }
    
    func sendWelcomeMessage(publicMessage: PublicMessageModel) {
        
        guard let isWelcomeMessage = publicMessage.isWelcomeMessage,
                let isGeneral = publicMessage.isGeneral else {
            return
        }
        
        if isWelcomeMessage && !publicMessage.isContaineWelcomeMessage() {
            publicMessage.insert()
            
            if isGeneral {
                self.socketNotifierManager.onNewGeneralMessageNotification.dispatch(publicMessage)
            } else {
                self.socketNotifierManager.onNewSaleMessageNotification.dispatch(publicMessage)
            }
        }
        
    }
    
    func resendMessage(publicMessage: PublicMessageModel, socketConnector: SocketConnector) {
        self.send(publicMessage, socketConnector: socketConnector)
    }
    
    
    //
    // MARK: Private
    //
    
    fileprivate func send(_ publicMessage: PublicMessageModel, socketConnector: SocketConnector) {
        let disc = ["message": publicMessage.msg!,
                    "user": publicMessage.fromUserName!,
                    "index": publicMessage.index!]
        
        let isGeneral = publicMessage.isGeneral!
        let message = PhoenixMessage.initMessage(message: disc as [String : AnyObject])
        let roomType = isGeneral ? ChannelType.Generic : ChannelType.Sale
        let payload = PhoenixPayload.initPlayload(topic: "\(roomType.rawValue):\(self.chatManager.currentRoomId)", event: "news:msg", playload: message)
        if socketConnector.send(payload) {
            if isGeneral {
                self.socketNotifierManager.onNewGeneralMessageNotification.dispatch(publicMessage)
            } else {
                self.socketNotifierManager.onNewSaleMessageNotification.dispatch(publicMessage)
            }
        } else {
            publicMessage.status = StatusType.sending
            publicMessage.insert()
            if isGeneral {
                self.socketNotifierManager.onFailGeneralMessageNotification.dispatch(publicMessage)
            } else {
                self.socketNotifierManager.onFailSaleMessageNotification.dispatch(publicMessage)
            }
        }
    }
    
}
