//
//  SocketSender.swift
//  panda
//
//  Created by Edouard Roussillon on 5/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

class SocketSenderManager: SocketSender {}

protocol SocketSender: SocketSenderPrivate, SocketSenderPublic {
    
}