//
//  SocketSenderPrivate.swift
//  panda
//
//  Created by Edouard Roussillon on 5/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit

protocol SocketSenderPrivate {
    func sendMessage(privateMessage: PrivateMessageModel, socketConnector: SocketConnector, withNotification: Bool)
    func sendMessages(_ messages: [PrivateMessageModel], socketConnector: SocketConnector)
}

extension SocketSenderPrivate {

    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }

    fileprivate var socketNotifierManager: SocketNotifierManager {
        get {
            return SocketNotifierManager.sharedInstance
        }
    }

    func sendMessage(privateMessage: PrivateMessageModel, socketConnector: SocketConnector, withNotification: Bool) {
        let sessionUserId = sessionManager.user.userId
        let pseudo = sessionManager.user.userPseudo
        let rowId: Int = privateMessage.insertLite()

        if let userId = privateMessage.toUserId,
           let userName = privateMessage.toUserName,
           let index = privateMessage.index,
           rowId > 0 && sessionUserId > 0 {

            privateMessage.rowId = rowId

            if let type = privateMessage.type,
               let dataId = privateMessage.dataId,
               let dataLocal = privateMessage.dataLocal, dataLocal.length() > 0 && type == DataType.image && dataId == -1 && privateMessage.urlBlur == nil {

                APIData().uploadImage(dataLocal, progress: { (bytesWritten, totalBytesWritten, totalBytesExpected) in
                    self.socketNotifierManager.onNewImageUploadingNotification.dispatch((bytesWritten: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpected: totalBytesExpected, message: privateMessage))
                }, success: { (data) in
                    let image = data as! DataModel
                    privateMessage.type = DataType.image
                    privateMessage.dataId = image.dataId
                    privateMessage.insert()
                    self.sendMessage(index: index, pseudo: pseudo, roomId: privateMessage.roomId!, userId: userId, userName: userName, msg: privateMessage.msg!, dataId: image.dataId!, socketConnector: socketConnector)
                }, failure: { (response, isErrorComsumed) -> Bool in
                    self.socketNotifierManager.onNewPrivateMessageFailNotification.dispatch(privateMessage)
                    return true
                })
            } else {
                self.sendMessage(index: index, pseudo: pseudo, roomId: privateMessage.roomId!, userId: userId, userName: userName, msg: privateMessage.msg!, dataId: privateMessage.dataId ?? -1, socketConnector: socketConnector)
            }

            privateMessage.insert()
            if withNotification {
                DispatchQueue.main.async(execute: {
                    self.socketNotifierManager.onNewPrivateMessageNotification.dispatch((isNewItem: true, message: privateMessage))
                    self.socketNotifierManager.onClearTextNotification.dispatch(Void())
                })
            }
        }
    }

    func sendMessages(_ messages: [PrivateMessageModel], socketConnector: SocketConnector) {
        guard messages.count > 0 else {
            return
        }

        let sessionUserId = sessionManager.user.userId
        var newMessages: [[String: AnyObject]] = [[:]]

        for item in messages {
            if let messageId: Int = item.messageId,
               let status: StatusType = item.status {
                let data: [String: AnyObject] = ["id": messageId as AnyObject,
                                                 "status": status.rawValue as AnyObject]
                newMessages.append(data)
                item.updateStatus()
            }
        }

        let disc: [String: AnyObject] = ["messages": newMessages as AnyObject]

        let message = PhoenixMessage.initMessage(message: disc)
        let payload = PhoenixPayload.initPlayload(
                topic: "\(ChannelType.Private.rawValue):\(sessionUserId)",
                event: "news:msg:confirmation",
                playload: message)
        let _ = socketConnector.send(payload)
    }

    func deleteMessage(roomId: Int, listMessageIds: [Int], socketConnector: SocketConnector) {
        let sessionUserId = sessionManager.user.userId

        let disc: [String: AnyObject] = ["room_id": roomId as AnyObject,
                                         "message_ids": listMessageIds as AnyObject]

        let message = PhoenixMessage.initMessage(message: disc)
        let payload = PhoenixPayload.initPlayload(
                topic: "\(ChannelType.Private.rawValue):\(sessionUserId)",
                event: "news:msg:deleting",
                playload: message)

        let _ = socketConnector.send(payload)
    }

    //
    // MARK: Private
    //

    fileprivate func sendMessage(index: String, pseudo: String, roomId: Int, userId: Int, userName: String, msg: String, dataId: Int, socketConnector: SocketConnector) {
        let sessionUserId = sessionManager.user.userId

        let disc: [String: AnyObject] = ["index": index as AnyObject,
                                         "user_name": pseudo as AnyObject,
                                         "private_room_id": roomId as AnyObject,
                                         "to_user_id": userId as AnyObject,
                                         "to_user_name": userName as AnyObject,
                                         "message": msg as AnyObject,
                                         "data_id": dataId as AnyObject]

        let message = PhoenixMessage.initMessage(message: disc)
        let payload = PhoenixPayload.initPlayload(
                topic: "\(ChannelType.Private.rawValue):\(sessionUserId)",
                event: "news:msg",
                playload: message)

        let _ = socketConnector.send(payload)
    }
}
