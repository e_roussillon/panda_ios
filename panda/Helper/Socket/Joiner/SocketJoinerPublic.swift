//
//  SocketJoinerPublic.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

protocol SocketJoinerPublic {
    func joinPublicChannel(_ channelType: ChannelType, roomId: Int, token: String, socketConnector: SocketConnector)
    func refreshToken(_ channelType: ChannelType, roomId: Int, token: String, socketConnector: SocketConnector)
}

extension SocketJoinerPublic {
    
    fileprivate var analyticsManager: AnalyticsManager {
        get {
            return AnalyticsManager.sharedInstance
        }
    }
    
    fileprivate var chatManager: ChatManager {
        get {
            return ChatManager.sharedInstance
        }
    }
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    fileprivate var socketNotifierManager: SocketNotifierManager {
        get {
            return SocketNotifierManager.sharedInstance
        }
    }
    
    func refreshToken(_ channelType: ChannelType, roomId: Int, token: String, socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: self.sessionManager.user.token, body: token)
        socketConnector.refreshToken(channelType, roomId: roomId, message: message)
    }
    
    func joinPublicChannel(_ channelType: ChannelType, roomId: Int, token: String, socketConnector: SocketConnector) {
        switch channelType {
        case ChannelType.Generic:
            self.joinGeneralChannel(roomId, token: token, socketConnector: socketConnector)
            break
        case ChannelType.Sale:
            self.joinSaleChannel(roomId, token: token, socketConnector: socketConnector)
            break
        case ChannelType.Broadcast:
            self.joinBroadcastChannel(roomId, token: token, socketConnector: socketConnector)
            break
        default:
            break
        }
    }
    
    fileprivate func joinGeneralChannel(_ roomId: Int, token: String, socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: self.sessionManager.user.token, body: token)
        socketConnector.joinChanel(ChannelType.Generic, roomId: roomId, message: message, callback: { (channel) in
            
            channel.on("news:msg:error") { message in
                self.sendAnalytics(ActionName.ErrorGeneralMessage)
                self.handleFailMessage(message, isGeneral: true)
            }
            
            channel.on("news:msg") { message in
                self.handleSuccessMessage(message, channelType: ChannelType.Generic)
            }
            
            channel.on("phx_reply") { message in
                guard let reponse = Mapper<ResponsePhoenixModel>().map(JSON: message) else {
                    return
                }
                
                if reponse.statusOk {
                    self.sendAnalytics(ActionName.JoinGeneralChat)
                    self.socketNotifierManager.onJoinedChannelGeneral.dispatch(Void())
                    self.socketNotifierManager.onJoinPublicRoomSucceed.dispatch(Void())
                }
            }
        })
    }
    
    fileprivate func joinSaleChannel(_ roomId: Int, token: String, socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: self.sessionManager.user.token, body: token)
        socketConnector.joinChanel(ChannelType.Sale, roomId: roomId, message: message, callback: { (channel) in
            
            channel.on("news:msg:error") { message in
                self.sendAnalytics(ActionName.ErrorTradeMessage)
                self.handleFailMessage(message, isGeneral: false)
            }
            
            channel.on("news:msg") { message in
                self.handleSuccessMessage(message, channelType: ChannelType.Sale)
            }
            
            channel.on("phx_reply") { message in
                guard let reponse = Mapper<ResponsePhoenixModel>().map(JSON: message) else {
                    return
                }
                
                if reponse.statusOk {
                    self.sendAnalytics(ActionName.JoinTradeChat)
                    self.socketNotifierManager.onJoinedChannelSale.dispatch(Void())
                    self.socketNotifierManager.onJoinPublicRoomSucceed.dispatch(Void())
                }
            }

        })
    }
    
    fileprivate func joinBroadcastChannel(_ roomId: Int, token: String, socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: self.sessionManager.user.token, body: token)
        socketConnector.joinChanel(ChannelType.Broadcast, roomId: roomId, message: message, callback: { (channel) in
                        
            channel.on("news:msg") { message in
                self.handleSuccessMessage(message, channelType: ChannelType.Broadcast)
            }
            
            channel.on("phx_reply") { message in
                guard let reponse = Mapper<ResponsePhoenixModel>().map(JSON: message) else {
                    return
                }
                
                if reponse.statusOk {
                    self.sendAnalytics(ActionName.JoinBroadcastChat)
                    self.socketNotifierManager.onJoinedChannelBroadcast.dispatch(Void())
                }
            }
            
        })
    }
    
    //
    // MARK: Generic
    //
    
    fileprivate func handleSuccessMessage(_ object: [String: Any], channelType: ChannelType) {
        var disc = object
        disc["send_type"] = StatusType.sent.rawValue
        
        if var publicMessage = Mapper<PublicMessageModel>().map(JSON: disc),
            let msg = publicMessage.msg, msg.length() > 0 {
            
            if channelType != ChannelType.Broadcast {
                if SocketManager.sharedInstance.isPublicMessageHavetoBeBlocked(&publicMessage, channelType: channelType) {
                    return
                }
            }
            
            publicMessage.status = StatusType.sent
            publicMessage.insert()
            
            switch channelType {
            case ChannelType.Generic:
                self.sendAnalytics(ActionName.ReceivedGeneralMessage)
                self.socketNotifierManager.onNewGeneralMessageNotification.dispatch(publicMessage)
                break
            case ChannelType.Sale:
                self.sendAnalytics(ActionName.ReceivedTradeMessage)
                self.socketNotifierManager.onNewSaleMessageNotification.dispatch(publicMessage)
                break
            case ChannelType.Broadcast:
                self.sendAnalytics(ActionName.ReceivedBroadcastMessage)
                self.socketNotifierManager.onNewBroadcastMessageNotification.dispatch(publicMessage)
                break
            default:
                print("[Receive message] - but this message hae been managed!!!")
                break
            }
        } else {
            return
        }
    }
    
    fileprivate func handleFailMessage(_ object: Any, isGeneral: Bool) {
//        guard let message = object as? PhoenixMessage,
//            let (index, _) = PublicMessageSender.socketResponseToErrorMessage(message),
//            let publicMessage = PublicMessageSQL.getPublicMessage(isGeneral, index: index) else {
//                //TODO CHECK ERROR
//                return
//        }
//        
//        publicMessage.sendType = SendType.Error
//        PublicMessageSQL.insertMessageLite(publicMessage)
//        
//        if isGeneral {
//            self.socketNotifierManager.onFailGeneralMessageNotification.dispatch(publicMessage)
//        } else {
//            self.socketNotifierManager.onFailSaleMessageNotification.dispatch(publicMessage)
//        }
    }
    
    fileprivate func sendAnalytics(_ actionName: ActionName, params: [String: AnyObject] = [:]) {
        var param: [String: Any] = [KeyName.RoomId.rawValue: "\(self.chatManager.currentRoomId)",
                                          KeyName.RoomName.rawValue: self.sessionManager.keychainUtil.currentRoom?.originalName ?? "",
                                          KeyName.UserId.rawValue : "\(self.sessionManager.user.userId)",
                                          KeyName.UserPseudo.rawValue : self.sessionManager.user.userPseudo]
        
        for p in params {
            param[p.0] = p.1
        }
        
        self.analyticsManager.trackAction(actionName, params: param)
    }
}
