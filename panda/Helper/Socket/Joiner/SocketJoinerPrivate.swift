//
//  SocketJoinerPrivate.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

protocol SocketJoinerPrivate {
    func joinPrivateChannel(_ socketConnector: SocketConnector)
    func refreshToken(_ socketConnector: SocketConnector)
}

extension SocketJoinerPrivate {
    
    fileprivate var analyticsManager: AnalyticsManager {
        get {
            return AnalyticsManager.sharedInstance
        }
    }
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    fileprivate var socketManager: SocketManager {
        get {
            return SocketManager.sharedInstance
        }
    }
    
    fileprivate var socketNotifierManager: SocketNotifierManager {
        get {
            return SocketNotifierManager.sharedInstance
        }
    }
    
    func refreshToken(_ socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: sessionManager.user.token, body: UserModel.getCurrentUser()?.pseudo ?? "")
        socketConnector.refreshToken(ChannelType.Private, roomId: sessionManager.user.userId, message: message)
    }
    
    func joinPrivateChannel(_ socketConnector: SocketConnector) {
        let message = PhoenixMessage.initMessage(subject: sessionManager.user.token, body: UserModel.getCurrentUser()?.pseudo ?? "")
        socketConnector.joinChanel(ChannelType.Private, roomId: sessionManager.user.userId, message: message, callback: { (channel) in
            
            channel.on("news:msg") { message in
                 self.handleSuccessMessage(message)
            }
            
            channel.on("news:msg:confirmation") { message in
                self.handleSuccessMessagesStatus(message)
            }
            
            channel.on("news:users:connect") { message in                
                guard let list = message["list"], let users = Mapper<UserModel>().mapArray(JSONObject: list) else {
                    return
                }
                
                UserModel.updateUsers(users)
                self.socketNotifierManager.onNewConnection.dispatch(users)
                
                self.sendAnalytics(ActionName.UsersConnect)
            }
            
            channel.on("news:user:connect") { message in
                guard let user_id = message["user_id"] as? Int,
                    let lastSeen = message["last_seen"] as? Double,
                    let user: UserModel = UserModel.getUser(user_id) else {
                    return
                }
                
                user.isOnline = true
                user.lastSeen = lastSeen.toDate()
                user.update()
                self.socketNotifierManager.onNewConnection.dispatch([user])
                
                let param: [String: Any] = [KeyName.ToUserId.rawValue: "\(user.userId ?? -1)",
                                                  KeyName.ToUserPseudo.rawValue: user.pseudo ?? ""]
                self.sendAnalytics(ActionName.UserConnect, params: param)
            }
            
            channel.on("news:user:disconnect") { message in
                guard let user_id = message["user_id"] as? Int,
                    let lastSeen = message["last_seen"] as? Double,
                    let user: UserModel = UserModel.getUser(user_id) else {
                    return
                }
                
                user.isOnline = false
                user.lastSeen = lastSeen.toDate()
                user.update()
                self.socketNotifierManager.onNewDisconnection.dispatch(user)
                
                let param: [String: Any] = [KeyName.ToUserId.rawValue: "\(user.userId ?? -1)",
                                                  KeyName.ToUserPseudo.rawValue: user.pseudo ?? ""]
                self.sendAnalytics(ActionName.UserDisconnect, params: param)
            }
            
            channel.on("news:user:friendship") { message in
                guard let user = Mapper<UserModel>().map(JSON: message) else {
                    return
                }
                user.update()
                self.socketNotifierManager.onUpdateFriendship.dispatch(user)
                self.sendNotifFriendship(user)
                
                
                let param: [String: Any] = [KeyName.ToUserId.rawValue: "\(user.userId ?? -1)",
                                                  KeyName.ToUserPseudo.rawValue: user.pseudo ?? "",
                                                  KeyName.FriendshipStatus.rawValue: user.friendshipStatus?.rawValue ?? -1,
                                                  KeyName.FriendshipLastAction.rawValue: user.friendshipLastAction ?? -1]
                self.sendAnalytics(ActionName.UserFriendship, params: param)
            }
            
            channel.on("news:user:duplicate") { message in
                self.sendAnalytics(ActionName.UserDuplicate)
                self.forceDisconnect()
            }
            
            channel.on("news:user:reported") { message in
                guard let time = message["time"] as? Int else {
                    return
                }
                
                self.sendAnalytics(ActionName.UserReported)
                self.disconnectUser(time)
            }
            
            channel.on("news:msg:pending") { message in
                guard let backup = Mapper<BackupModel>().map(JSON: message) else {
                    return
                }
                
                backup.insert()
                
                let userId = self.sessionManager.user.userId
                let list = PrivateMessageModel.getPendingMessages()
                var newList: [PrivateMessageModel] = []
                var newListDelete: [Int:[Int]] = [:]

                for message in list {
                    if let fromUserId = message.fromUserId {
                        if let roomId = message.roomId, let messageId = message.messageId, message.status == StatusType.deleting {
                            var newList = [messageId]
                            if newListDelete.count != 0 && newListDelete.keys.contains(roomId) {
                                newList += newListDelete[roomId] ?? []
                            }
                            newListDelete.updateValue(newList, forKey: roomId)
                        } else if userId == fromUserId  {
                            message.status = StatusType.confirmed
                        } else {
                            message.status = StatusType.received
                        }
                        newList.append(message)
                    }
                }
                
                self.sendAnalytics(ActionName.UserBackup)
                self.socketManager.sendPrivateMessages(newList)
                self.socketManager.deletePrivateMessages(newListDelete)
                self.socketNotifierManager.onGetBackupMessagesNotification.dispatch(Void())
            }

            channel.on("news:msg:deleted") { messages in
                guard let list = messages["list"], let messageIds = Mapper<PrivateMessageModel>().mapArray(JSONObject: list) else {
                    return
                }

                for item in messageIds {
                    if let id = item.messageId, let message = PrivateMessageModel.getMessage(id) {
                        message.status = StatusType.deleted
                        message.delete()
                        self.socketNotifierManager.onDeletePrivateMessageNotification.dispatch(message)
                    }
                }

                self.sendAnalytics(ActionName.UserDeleted)
            }

            channel.on("news:msg:hidden") { messages in
                guard let list = messages["list"], let messageIds = Mapper<PrivateMessageModel>().mapArray(JSONObject: list) else {
                    return
                }

                for item in messageIds {
                    if let id = item.messageId, let message = PrivateMessageModel.getMessage(id) {
                        message.msg = ""
                        message.type = DataType.none
                        message.urlBlur = nil
                        message.urlOriginal = nil
                        message.urlThumb = nil
                        message.status = StatusType.deleted
                        message.insert()
                        self.socketNotifierManager.onHiddenPrivateMessageNotification.dispatch(message)
                    }
                }

                self.sendAnalytics(ActionName.UserDeleted)
            }
            
            channel.on("phx_reply") { message in
                guard let reponse = Mapper<ResponsePhoenixModel>().map(JSON: message) else {
                    return
                }
                
                if reponse.statusOk {
                    self.sendAnalytics(ActionName.JoinPrivateChat)
                    self.socketNotifierManager.onJoinedChannelPrivate.dispatch(Void())
                } else {
                    if let time = reponse.response?["time"] as? Int {
                        self.disconnectUser(time)
                    } else {
                        BaseAPIManager.startRefreshToken()
//                        BaseViewController.lauchLogoutForced()
                    }
                }
            }
        })
    }
    
    //
    // MARK: Generic
    //
    
    fileprivate func handleSuccessMessagesStatus(_ object: [String: Any]) {
        var newList:[PrivateMessageModel] = []
        if let message = object["messages"] as? [[String: Any]] {
            for message in message {
                if let msg = self.savePrivateMessage(message) {
                    newList.append(msg)
                }
            }
        }
        
        let userId = self.sessionManager.user.userId
        
        var listToSave: [PrivateMessageModel] = []
        for message in newList {
            if message.fromUserId == userId && message.status == StatusType.read {
                message.status = StatusType.confirmed
                listToSave.append(message)
            } else if message.fromUserId != userId && (message.status == StatusType.received || message.status == StatusType.sent) {
                message.status = StatusType.read
                listToSave.append(message)
            }   
        }
        
        self.socketManager.sendPrivateMessages(listToSave)
        
        for item in newList {
            self.socketNotifierManager.onNewPrivateMessageNotification.dispatch((isNewItem: false, message: item))
        }
    }
    
    fileprivate func handleSuccessMessageStatus(_ object: [String: Any]) {
        if let message = self.savePrivateMessage(object) {
            self.socketNotifierManager.onNewPrivateMessageNotification.dispatch((isNewItem: false, message: message))
        }
    }
    
    fileprivate func savePrivateMessage(_ object: [String: Any]) -> PrivateMessageModel? {
        if let id = object["id"] as? Int,
            let statusId = object["status"] as? Int,
            let status = StatusType(rawValue: statusId),
            let privateMessage = PrivateMessageModel.getMessage(id) {
            
            privateMessage.status = status
            privateMessage.updateStatus()
            return privateMessage
        }
        
        return nil
    }
    
    fileprivate func handleSuccessMessage(_ object: [String: Any]) {
        guard let privateMessage = SocketJoinerManager.handleNotifMessage(object),
            let toUserId = privateMessage.toUserId else {
            return
        }
        
        if self.sessionManager.user.userId == toUserId {
            privateMessage.status = StatusType.received
            self.socketManager.sendPrivateMessages([privateMessage])
            self.socketNotifierManager.onNewPrivateMessageNotification.dispatch((isNewItem: true, message: privateMessage))
            self.sendNotifMessage(privateMessage)
        } else {
            self.socketNotifierManager.onNewPrivateMessageNotification.dispatch((isNewItem: false, message: privateMessage))
        }
    }
    
    fileprivate func disconnectUser(_ time: Int) {
        BaseViewController.lauchLogoutReported(time)
    }
    
    fileprivate func forceDisconnect() {
        BaseViewController.lauchLogoutForced()
    }
    
    fileprivate func sendNotifFriendship(_ user: UserModel) {
        if let pseudo = user.pseudo,
            let userId = user.userId, user.friendshipStatus == FriendshipType.pending {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let window = appDelegate.window,
                let root = window.rootViewController,
                let nav = root as? BaseNavigationController {
                
                if let home = nav.viewControllers.last as? HomeViewController {
                    if let navPre = home.presentedViewController as? BaseNavigationController {
                        if let fly = navPre.viewControllers.last as? FlyingProfileViewController, fly.user == user {
                            //DO NOTHING WE ARE IN THE RIGHT PLACE
                        } else {
                            UILocalNotification.showFriendship(pseudo, userId: userId)
                        }
                    } else if home.homePageDelegate.current != 0 {
                        UILocalNotification.showFriendship(pseudo, userId: userId)
                    }
                } else if let navPre = nav.presentedViewController as? BaseNavigationController,
                    let fly = navPre.viewControllers.last as? FlyingProfileViewController {
                    
                    if fly.user != user {
                        UILocalNotification.showFriendship(pseudo, userId: userId)
                    }
                } else {
                    UILocalNotification.showFriendship(pseudo, userId: userId)
                }
                
            } else {
                UILocalNotification.showFriendship(pseudo, userId: userId)
            }
        }
    }
    
    fileprivate func sendNotifMessage(_ message: PrivateMessageModel) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let window = appDelegate.window,
            let root = window.rootViewController,
            let nav = root as? BaseNavigationController,
            let userIdFrom = message.fromUserId {
            
            let userFrom = UserModel.getUser(userIdFrom)
            
            if let chat = nav.viewControllers.last as? ChatViewController, userFrom == chat.user {
                //DO NOTHING WE ARE IN THE RIGHT PLACE
            } else if let navPre = nav.presentedViewController as? BaseNavigationController,
                let chat = navPre.viewControllers.last as? ChatViewController, userFrom == chat.user {
                //DO NOTHING WE ARE IN THE RIGHT PLACE
            } else {
                UILocalNotification.showPrivateMessage(message)
            }
            
//            if let _ = home.presentedViewController {
//                UILocalNotification.showPrivateMessage(false, privateMessage: message)
//            } else if home.homePageDelegate.current != 1 {
//                UILocalNotification.showPrivateMessage(false, privateMessage: message)
//            }
//            
            
        } else {
            UILocalNotification.showPrivateMessage(message)
        }
    }
    
    fileprivate func sendAnalytics(_ action: ActionName, params: [String : Any] = [:]) {
        var param: [String: Any] = [KeyName.UserId.rawValue : "\(self.sessionManager.user.userId)",
                                          KeyName.UserPseudo.rawValue : self.sessionManager.user.userPseudo]
        
        for item in params {
            param[item.0] = item.1
        }
        
        self.analyticsManager.trackAction(action, params: param)
    }
}
