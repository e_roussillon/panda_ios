//
//  SocketJoiner.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import ObjectMapper

class SocketJoinerManager: SocketJoiner {

    static func handleNotifMessage(_ object: [String: Any]) -> PrivateMessageModel?  {
        let sessionManager = SessionManager.sharedInstance
        if let privateMessage = Mapper<PrivateMessageModel>().map(JSON: object),
            let roomId = privateMessage.roomId,
            let toUserName = privateMessage.toUserName,
            let toUserId = privateMessage.toUserId,
            let index = privateMessage.index,
            let fromUserId = privateMessage.fromUserId,
            let fromUserName = privateMessage.fromUserName,
            let friendshipStatus = privateMessage.friendshipStatus,
            let friendshipLastAction = privateMessage.friendshipLastAction {
            
            let message = PrivateMessageModel.getMessageByIndex(index)
            
            privateMessage.rowId = message?.rowId
            privateMessage.dataLocal = message?.dataLocal
            privateMessage.insert()
            
            if sessionManager.user.userId == toUserId {
                //new message
                PrivateChatModel.insertRoom(roomId, name: fromUserName)
                PrivateChatModel.insertUsersRoom(roomId, toUserId: fromUserId)
                UserModel.insert(fromUserId, userName: fromUserName, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
                return privateMessage
            } else {
                //update message
                PrivateChatModel.insertRoom(roomId, name: toUserName)
                PrivateChatModel.insertUsersRoom(roomId, toUserId: toUserId)
                UserModel.insert(toUserId, userName: toUserName, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
                
                return privateMessage
            }
        } else {
            return nil
        }
    }
}


protocol SocketJoiner: SocketJoinerPublic, SocketJoinerPrivate {
    func isJoined(_ channelType: ChannelType, roomId: Int, socketConnector: SocketConnector) -> Bool
}

extension SocketJoiner {
    
    func isJoined(_ channelType: ChannelType, roomId: Int, socketConnector: SocketConnector) -> Bool {
        return socketConnector.isJoined("\(channelType.rawValue):\(roomId)")
    }
}
