//
//  SocketNotifier.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SocketNotifierManager {
    static fileprivate(set) var sharedInstance: SocketNotifierManager = SocketNotifierManager()
    
    let onJoinPublicRoomSucceed = Event<Void>()
    let onJoinPublicRoomFailed = Event<Void>()

    let onJoinPrivateRoomSucceed = Event<Void>()
    let onJoinPrivateRoomFailed = Event<Void>()
    
    let onFindPrivateRoomSucceed = Event<Void>()
    let onFindPrivateRoomFailed = Event<Void>()
    
    let onResetTimerNotification = Event<Int>()
    let onUpdateTimerNotification = Event<Int>()
    let onStopTimerNotification = Event<Void>()
    
    let onNewGeneralMessageNotification = Event<PublicMessageModel>()
    let onFailGeneralMessageNotification = Event<PublicMessageModel>()
    
    let onNewSaleMessageNotification = Event<PublicMessageModel>()
    let onFailSaleMessageNotification = Event<PublicMessageModel>()
    
    let onNewBroadcastMessageNotification = Event<PublicMessageModel>()
    let onFailBroadcastMessageNotification = Event<PublicMessageModel>()
    
    let onNewImageUploadingNotification = Event<(bytesWritten: Int64?, totalBytesWritten: Int64?, totalBytesExpected: Int64?, message: PrivateMessageModel)>()
    let onNewPrivateMessageNotification = Event<(isNewItem: Bool, message: PrivateMessageModel)>()
    let onNewPrivateMessageFailNotification = Event<PrivateMessageModel>()
    let onDeletePrivateMessageNotification = Event<PrivateMessageModel>()
    let onHiddenPrivateMessageNotification = Event<PrivateMessageModel>()
    let onGetBackupMessagesNotification = Event<(Void)>()
    
    let onNewConnection = Event<[UserModel]>()
    let onNewDisconnection = Event<UserModel>()
    let onUpdateFriendship = Event<UserModel>()
    
    let onJoinedChannelPrivate = Event<Void>()
    let onJoinedChannelSale = Event<Void>()
    let onJoinedChannelGeneral = Event<Void>()
    let onJoinedChannelBroadcast = Event<Void>()
    
    let onClearTextNotification = Event<Void>()
    
    let onRemoveChatFindRoomNotification = Event<Void>()

    fileprivate init() {}
}
