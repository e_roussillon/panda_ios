//
//  SocketPublicHighlighter.swift
//  panda
//
//  Created by Edouard Roussillon on 5/29/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

protocol SocketPublicHighlighter: SocketHighlighter {
    func checkIsIncomingPublicMessageHaveToBeBlocked(_ publicMessage: inout PublicMessageModel, channelType: ChannelType) -> Bool
}

extension SocketPublicHighlighter {
    
    fileprivate var analyticsManager: AnalyticsManager {
        get {
            return AnalyticsManager.sharedInstance
        }
    }
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    func checkIsIncomingPublicMessageHaveToBeBlocked(_ publicMessage: inout PublicMessageModel, channelType: ChannelType) -> Bool {
        guard let userId = publicMessage.fromUserId,
            let pseudo = publicMessage.fromUserName,
            let msg = publicMessage.msg else {
                return false
        }
        
        if !self.isMessageNeedToBeBlocked(userId, channelType: channelType) && !self.isMessageNeedToBeBlockedByFilters(msg, userId: userId, pseudo: pseudo, channelType: channelType) {
            self.isMessageNeedToBeHighlighted(publicMessage: &publicMessage, message: msg, userId: userId, pseudo: pseudo, channelType: channelType)
            return false
        } else {
            return true
        }
    }
    
    
    //
    // Private
    //
    
    fileprivate func isMessageNeedToBeBlockedByFilters(_ message: String, userId: Int, pseudo: String, channelType: ChannelType) -> Bool {
        let list:[FilterModel] = FilterModel.getFilters(isHighlight: false)
        
        for item: FilterModel in list {
            if message.lowercased().contains(item.name?.lowercased() ?? "") {
                
                let param = [KeyName.ToUserId.rawValue: "\(userId)",
                             KeyName.ToUserPseudo.rawValue: pseudo,
                             KeyName.Word.rawValue: item.name?.lowercased() ?? "",
                             KeyName.Message.rawValue: message.lowercased()]
                
                switch channelType {
                case ChannelType.Generic:
                    self.sendAnalytics(ActionName.BlockedUserGeneralMessage, params: param as [String : AnyObject])
                    break
                case ChannelType.Private:
                    self.sendAnalytics(ActionName.BlockedUserTradeMessage, params: param as [String : AnyObject])
                    break
                default:
                    break
                }
                
                return true
            }
        }
        
        return false
    }
    
    fileprivate func isMessageNeedToBeHighlighted(publicMessage: inout PublicMessageModel, message: String, userId: Int, pseudo: String, channelType: ChannelType) {
        publicMessage.isHighlight = false
        let list:[FilterModel] = FilterModel.getFilters(isHighlight: true)
        
        for item: FilterModel in list {
            if message.lowercased().contains(item.name?.lowercased() ?? "") {
                publicMessage.isHighlight = true
                
                let param = [KeyName.ToUserId.rawValue: "\(userId)",
                             KeyName.ToUserPseudo.rawValue: pseudo,
                             KeyName.Word.rawValue: item.name?.lowercased() ?? "",
                             KeyName.Message.rawValue: message.lowercased()]
                
                switch channelType {
                case ChannelType.Generic:
                    self.sendAnalytics(ActionName.BlockedUserGeneralMessage, params: param as [String : AnyObject])
                    break
                case ChannelType.Private:
                    self.sendAnalytics(ActionName.BlockedUserTradeMessage, params: param as [String : AnyObject])
                    break
                default:
                    break
                }
                
                break
            }
        }
    }
    
    fileprivate func sendAnalytics(_ actionName: ActionName, params: [String: AnyObject] = [:]) {
        var param: [String: Any] = [KeyName.RoomId.rawValue: "\(self.sessionManager.keychainUtil.currentRoomId)",
                                          KeyName.RoomName.rawValue: self.sessionManager.keychainUtil.currentRoom?.originalName ?? "",
                                          KeyName.UserId.rawValue : "\(self.sessionManager.user.userId)",
                                          KeyName.UserPseudo.rawValue : self.sessionManager.user.userPseudo]
        
        for p in params {
            param[p.0] = p.1
        }
        
        self.analyticsManager.trackAction(actionName, params: param)
    }
}
