//
//  SocketHighlighter.swift
//  panda
//
//  Created by Edouard Roussillon on 5/29/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

class SocketHighlighterManager: SocketPublicHighlighter {}

protocol SocketHighlighter {
    func isMessageNeedToBeBlocked(_ fromUserId: Int, channelType: ChannelType) -> Bool
}

extension SocketHighlighter {
    
    fileprivate var analyticsManager: AnalyticsManager {
        get {
            return AnalyticsManager.sharedInstance
        }
    }
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    func isMessageNeedToBeBlocked(_ fromUserId: Int, channelType: ChannelType) -> Bool {
        if let user: UserModel = UserModel.getUser(fromUserId),
            let status = user.friendshipStatus,
            let lastAction = user.friendshipLastAction, (status == FriendshipType.blockedBeforeBeFriend ||  status == FriendshipType.blockedAfterBeFriend) && lastAction == sessionManager.user.userId {
            
            let param = [KeyName.ToUserId.rawValue: "\(user.userId ?? -1)",
                         KeyName.ToUserPseudo.rawValue: user.pseudo ?? ""]
            
            switch channelType {
            case ChannelType.Generic:
                self.sendAnalytics(ActionName.BlockedUserGeneralMessage, params: param as [String : AnyObject])
                break
            case ChannelType.Private:
                self.sendAnalytics(ActionName.BlockedUserTradeMessage, params: param as [String : AnyObject])
                break
            default:
                break
            }
            
            return true
        }

        return false
    }
    
    fileprivate func sendAnalytics(_ actionName: ActionName, params: [String: AnyObject] = [:]) {
        var param: [String: Any] = [KeyName.RoomId.rawValue: "\(self.sessionManager.keychainUtil.currentRoomId)",
                                          KeyName.RoomName.rawValue: self.sessionManager.keychainUtil.currentRoom?.originalName ?? "",
                                          KeyName.UserId.rawValue : "\(self.sessionManager.user.userId)",
                                          KeyName.UserPseudo.rawValue : self.sessionManager.user.userPseudo]
        
        for p in params {
            param[p.0] = p.1
        }
        
        self.analyticsManager.trackAction(actionName, params: param)
    }
}
