//
//  SocketConnector.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SocketConnectorPhoenix: NSObject, SocketConnector {
    
    static let sharedInstance = SocketConnectorPhoenix()
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let bag = DisposableBag()
    
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate var socket: PhoenixSocket!
    fileprivate var timer: Timer?
    fileprivate var currentTimer: Int = -1
    fileprivate var sendBufferSaleMessage: [PhoenixPayload] = []
    fileprivate var sendBufferGeneralMessage: [PhoenixPayload] = []
    
    fileprivate override init() {
        super.init()
        
        self.bag.add(self.socketNotifierManager.onResetTimerNotification.add(self, handler: SocketConnectorPhoenix.onResetTimerNotification))
        self.bag.add(self.socketNotifierManager.onJoinedChannelGeneral.add(self, handler: SocketConnectorPhoenix.onJoinedChannelGeneral))
        self.bag.add(self.socketNotifierManager.onJoinedChannelSale.add(self, handler: SocketConnectorPhoenix.onJoinedChannelSale))
        self.bag.add(self.socketNotifierManager.onGetBackupMessagesNotification.add(self, handler: SocketConnectorPhoenix.onGetBackupMessagesNotification))
    }
    
    deinit {
        self.bag.dispose()
    }
    
    //
    // MARK: NOTIF
    //
    
    func onResetTimerNotification(_ time: Int) {
        self.currentTimer = time
            
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(SocketConnectorPhoenix.updateTimer), userInfo: nil, repeats: true)
    }
    
    func onJoinedChannelGeneral() {
        SocketManager.sharedInstance.sendPublicMessageWelcome(true)
        if isSocketConnected() && sendBufferGeneralMessage.count > 0 {
            for data in sendBufferGeneralMessage {
                let _ = self.send(data)
            }
            sendBufferGeneralMessage = []
        }
    }
    
    func onJoinedChannelSale() {
        SocketManager.sharedInstance.sendPublicMessageWelcome(false)
        if isSocketConnected() && sendBufferSaleMessage.count > 0 {
            for data in sendBufferSaleMessage {
                let _ = self.send(data)
            }
            sendBufferSaleMessage = []
        }
    }
    
    func onGetBackupMessagesNotification() {
        let socketManager = SocketManager.sharedInstance
        let list = PrivateMessageModel.getMessagesNotSent()

        if isSocketConnected() && list.count > 0 {
            for data in list {
                socketManager.resendPrivateMessages(data)
            }
        }
    }
    
    @objc func updateTimer() {
        if self.currentTimer == 0 {
            timer?.invalidate()
        }
        
        self.socketNotifierManager.onUpdateTimerNotification.dispatch(self.currentTimer)
        self.currentTimer -= 1
    }
}

//
// Extention Connection
//

extension SocketConnectorPhoenix {
    func isSocketConnected() -> Bool {
        if socket != nil {
            return socket.isConnected()
        }
        return false
    }
    
    func startSocket() -> Bool {
        if sessionManager.user.isLogin {
            
            if socket == nil {
                socket = PhoenixSocket(domainAndPort: Constants.SOCKET.kURL, path: Constants.SOCKET.kPath, transport: Constants.SOCKET.kTransport, prot: Constants.SOCKET.kProt, params: ["token" : sessionManager.user.token])
            } else {
                self.stopSocket({ () -> () in
                    self.socket.updateEndPoint(Constants.SOCKET.kURL, path: Constants.SOCKET.kPath, transport: Constants.SOCKET.kTransport, prot: Constants.SOCKET.kProt, params: ["token" : self.sessionManager.user.token])
                })
            }
            
            return true
        }
        
        return false
    }
    
    func stopSocket(_ callback: @escaping () -> ()) {
        if socket != nil {
            socket.close({ () -> () in
                callback()
            })
        }
    }
    
    func forceStopSocket(_ callback: @escaping () -> ()) {
        if socket != nil {
            socket.forceDisconnection = true
            socket.numberOfTime = 0
            timer?.invalidate()
            socket.close({ () -> () in
                callback()
            })
        }
    }
    
    func refreshToken(_ channel: ChannelType, roomId: Int, message: PhoenixMessage) {
        if socket != nil {
            socket.refreshToken("\(channel.rawValue):\(roomId)", message: message)
        }
    }
    
}

//
// extension Send Message
//

extension SocketConnectorPhoenix {
    
    func send(_ data: PhoenixPayload) -> Bool {
        if isSocketConnected() {
            self.socket.send(data)
            return true
        } else {
            guard let topic = data.topic else {
                return false
            }
            
            if topic.contains(ChannelType.Generic.rawValue) {
                self.sendBufferSaleMessage.append(data)
            } else if topic.contains(ChannelType.Sale.rawValue) {
                self.sendBufferGeneralMessage.append(data)
            }
            
            return false
        }
    }
    
}

//
// extension Join
// 

extension SocketConnectorPhoenix {
    
    func isJoined(_ topic: String) -> Bool {
        if socket != nil {
            return socket.isJoined(topic: topic)
        }
        return false
    }
    
    func joinChanel(_ channel: ChannelType, roomId: Int, message: PhoenixMessage, callback: @escaping ((PhoenixChannel) -> Void)) {
        if socket != nil {
            self.socket.join(topic: "\(channel.rawValue):\(roomId)", message: message, callback: callback)
            
        } else {
            print("ERROR !!! SOCKET NOT CONNECTED YET")
        }
    }
    
    func leaveAllRoom() {
        if socket != nil {
            self.socket.leaveAllWithoutMessage()
        }
    }
    
    func leaveRoomPublicRooms() {
        if socket != nil {
            for channel in self.socket.channels {
                if let topic = channel.topic, !topic.contains(ChannelType.Private.rawValue) {
                    self.socket.leave(topic: topic, message: PhoenixMessage.initMessage(message: ["leave":"room" as AnyObject]))
                }
            }
        }
    }
}



