//
//  SocketConnectorMocked.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

protocol SocketConnectorMockedDelegate {
    func isSocketConnected() -> Bool
    func startSocket() -> Bool
    func stopSocket(_ callback: () -> ())
    func forceStopSocket(_ callback: () -> ())
    func send(_ data: PhoenixPayload) -> Bool
    func isJoined(_ topic: String) -> Bool
    func joinChanel(_ channel: ChannelType, roomId: Int, message: PhoenixMessage, callback: ((PhoenixChannel) -> Void))
}

class SocketConnectorMocked: SocketConnector {
    let socketConnectorMockedDelegate: SocketConnectorMockedDelegate
    
    init(delegate: SocketConnectorMockedDelegate) {
        self.socketConnectorMockedDelegate = delegate
    }
    
    func isSocketConnected() -> Bool {
        return self.socketConnectorMockedDelegate.isSocketConnected()
    }
    
    func startSocket() -> Bool {
        return self.socketConnectorMockedDelegate.startSocket()
    }
    
    func stopSocket(_ callback: @escaping () -> ()) {
        return self.socketConnectorMockedDelegate.stopSocket(callback)
    }
    
    func forceStopSocket(_ callback: @escaping () -> ()) {
        return self.socketConnectorMockedDelegate.forceStopSocket(callback)
    }
    
    func send(_ data: PhoenixPayload) -> Bool {
        return self.socketConnectorMockedDelegate.send(data)
    }
    
    func isJoined(_ topic: String) -> Bool {
        return self.socketConnectorMockedDelegate.isJoined(topic)
    }
    
    func joinChanel(_ channel: ChannelType, roomId: Int, message: PhoenixMessage, callback: @escaping ((PhoenixChannel) -> Void)) {
       return self.socketConnectorMockedDelegate.joinChanel(channel, roomId: roomId, message: message, callback: callback)
    }
    
    func leaveAllRoom() {
        
    }
    
    
    func leaveRoomPublicRooms() {
        
    }
    
    func refreshToken(_ channel: ChannelType, roomId: Int, message: PhoenixMessage) {
        
    }
    
}
