//
//  SocketConnector.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SocketConnectorManager {
    static var connectorManager: SocketConnector = SocketConnectorPhoenix.sharedInstance
}

protocol SocketConnector {
    func isSocketConnected() -> Bool
    func startSocket() -> Bool
    func stopSocket(_ callback: @escaping () -> ())
    func forceStopSocket(_ callback: @escaping () -> ())
    func send(_ data: PhoenixPayload) -> Bool
    func isJoined(_ topic: String) -> Bool
    func joinChanel(_ channel: ChannelType, roomId: Int, message: PhoenixMessage, callback: @escaping ((PhoenixChannel) -> Void))
    func leaveAllRoom()
    func leaveRoomPublicRooms()
    func refreshToken(_ channel: ChannelType, roomId: Int, message: PhoenixMessage)
}
