//
//  AnalyticsManager.swift
//  panda
//
//  Created by Edouard Roussillon on 10/13/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Crashlytics
import Analytics

public enum ScreenName: String {
    case Splash = "screen_splash"
    case BackupSplash = "screen_backup_splash"
    case SignUp = "screen_signUp"
    case SignIn = "screen_signIn"
    case GeneralChat = "screen_general_chat"
    case TradeChat = "screen_trade_chat"
    case PrivateListChat = "screen_private_list_chat"
    case PrivateChat = "screen_private_chat"
    case Profile = "screen_profile"
    case FlyingProfile = "screen_flying_profile"
    case FilterLike = "screen_filter_like"
    case FilterDislike = "screen_filter_dislike"
    case FriendshipAccepted = "screen_friendship_accepted"
    case FriendshipPending = "screen_friendship_pending"
    case FriendshipBlocked = "screen_friendship_blocked"
    case Search = "screen_search"
    case Welcome = "screen_welcome"
}

public enum ActionName: String {
    case ClickSignUp = "click_sign_up"
    case SendSignUp = "send_sign_up"
    case SendSignIn = "send_sign_in"
    case MissingInfoSignIn = "missing_info_sign_in"
    case MissingInfoSignUp = "missing_info_sign_up"
    case ClickForgotPassword = "click_forgot_password"
    case RequestSucceed = "request_succeed"
    case RequestFailed = "request_failed"
    case SendGeneralMessage = "send_general_message"
    case HighlightGeneralMessage = "highlight_general_message"
    case BlockedGeneralMessage = "blocked_general_message"
    case BlockedUserGeneralMessage = "blocked_user_general_message"
    case ReceivedGeneralMessage = "received_general_message"
    case ErrorGeneralMessage = "error_general_message"
    case SendTradeMessage = "send_trading_message"
    case HighlightTradeMessage = "highlight_trade_message"
    case BlockedTradeMessage = "blocked_trading_message"
    case BlockedUserTradeMessage = "blocked_user_trading_message"
    case ReceivedTradeMessage = "received_trading_message"
    case ReceivedBroadcastMessage = "received_broadcast_message"
    case ErrorTradeMessage = "error_trading_message"
    case SendPrivateMessage = "send_private_message"
    case ConfirmPrivateMessage = "confirm_private_message"
    case JoinGeneralChat = "join_general_chat"
    case JoinTradeChat = "join_trade_chat"
    case JoinBroadcastChat = "join_broadcast_chat"
    case JoinPrivateChat = "join_private_chat"
    case LostConnectionChat = "lost_connection_chat"
    case DisconnectionChat = "disconnection_chat"
    case ConnectionChat = "connection_chat"
    case UserDuplicate = "user_duplicate"
    case UserReported = "user_reported"
    case UserBackup = "user_backup"
    case UserDeleted = "user_deleted"
    case UserFriendship = "user_friendship"
    case UserDisconnect = "user_disconnect"
    case UserConnect = "user_connect"
    case UsersConnect = "users_connect"
    case ClickUserFromGeneralChat = "click_user_from_general_chat"
    case ClickUserFromTradeChat = "click_user_from_trade_chat"
    case ClickBlockUser = "click_block_user"
    case ClickUnblockUser = "click_unblock_user"
    case ClickReportUser = "click_report_user"
    case ClickProfileUser = "click_profile_user"
    case ClickLogout = "click_logout"
    case ClickDismissProfile = "click_dismiss_profile"
    case ClickEditProfile = "click_edit_profile"
    case ClickCancelEditProfile = "click_cancel_edit_profile"
    case ClickSaveEditProfile = "click_save_edit_profile"
    case ClickEditPictureProfile = "click_edit_picture_profile"
    case ClickCancelSelectPictureProfile = "click_cancel_select_picture_profile"
    case ClickCancelSelectPicturePrivateMessage = "click_cancel_select_picture_private_message"
    case ClickCameraProfile = "click_camera_profile"
    case ClickCameraPrivateMessage = "click_camera_private_message"
    case ClickGalleryProfile = "click_gallery_profile"
    case ClickGalleryPrivateMessage = "click_gallery_private_message"
    case PictureSelectedProfile = "picture_selected_profile"
    case PictureSelectedPrivateMessage = "picture_selected_private_message"
    case ClickDeleteFilterLike = "click_delete_filter_like"
    case ClickNewFilterLike = "click_new_filter_like"
    case ClickEditFilterLike = "click_edit_filter_like"
    case ClickDeleteFilterDislike = "click_delete_filter_dislike"
    case ClickNewFilterDislike = "click_new_filter_dislike"
    case ClickEditFilterDislike = "click_edit_filter_dislike"
}

public enum KeyName: String {
    case UserId = "user_id"
    case UserPseudo = "user_pseudo"
    case ToUserId = "to_user_id"
    case ToUserPseudo = "to_user_pseudo"
    case Word = "word"
    case Message = "message"
    case FriendshipStatus = "friendship_status"
    case FriendshipLastAction = "friendship_last_action"
    case RequestAPI = "request_api"
    case RequestParams = "request_params"
    case RequestStatus = "request_status"
    case RequestError = "request_error"
    case EmailError = "email_error"
    case PasswordError = "password_error"
    case GenderError = "gender_error"
    case BirthdayError = "birthday_error"
    case RoomId = "room_id"
    case RoomName = "room_name"
}



class AnalyticsManager {
    static let sharedInstance = AnalyticsManager()
    
    fileprivate init() {}
    
    func trackUserLogin() {
        if EnvironmentUtil.isTargetPanda {
            if let user = UserModel.getCurrentUser(),
                let userId = user.userId,
                let pseudo = user.pseudo,
                let email = user.email
            {
                let disc: [String : String] = ["pseudo" : pseudo, "email" : email]
                self.printTrackActionUser("\(userId)", params: disc as [String : AnyObject]?)
                SEGAnalytics.shared().identify("\(userId)", traits: disc)
            }

        }
    }
    
    func trackScreen( _ screen: ScreenName, params: [String: Any]? = nil) {
        if EnvironmentUtil.isTargetPanda || EnvironmentUtil.isTargetPandaBeta {
            self.printTrackScreen(screen, params: params)
            Answers.logContentView(withName: screen.rawValue, contentType: nil, contentId: nil, customAttributes: params)
            
            self.sendSegment(screen.rawValue, params: params)
        }
    }
    
    func trackAction( _ action: ActionName, params: [String: Any]? = nil) {
        if EnvironmentUtil.isTargetPanda || EnvironmentUtil.isTargetPandaBeta {
            self.printTrackAction(action, params: params)
            Answers.logCustomEvent(withName: action.rawValue, customAttributes: params)
            
            self.sendSegment(action.rawValue, params: params)
        }
    }
    
    fileprivate func printTrackScreen( _ screen: ScreenName, params: [String: Any]? = nil) {
        var logs: [String] = ["***** Analytics (ScreenName) *****",
                              "----- SCREEN NAME -----",
                              "       *  \(screen.rawValue)"]
        if let p = params {
            logs.append("----- PARAM -----")
            for item in p {
                logs.append("       *  \(item.0) -> \(item.1)")
            }
        }
        print(logs)
    }
    
    fileprivate func printTrackAction( _ action: ActionName, params: [String: Any]? = nil) {
        var logs: [String] = ["***** Analytics (ActionName) *****",
                              "----- ACTION NAME -----",
                              "       *  \(action.rawValue)"]
        if let p = params {
            logs.append("----- PARAM -----")
            for item in p {
                logs.append("       *  \(item.0) -> \(item.1)")
            }
        }
        print(logs)
    }
    
    fileprivate func printTrackActionUser( _ userId: String, params: [String: Any]? = nil) {
        var logs: [String] = ["***** Analytics (ActionName) *****",
                              "----- UserId -----",
                              "       *  \(userId)"]
        if let p = params {
            logs.append("----- PARAM -----")
            for item in p {
                logs.append("       *  \(item.0) -> \(item.1)")
            }
        }
        print(logs)
    }
    
    fileprivate func sendSegment(_ track: String, params: [String: Any]? = nil) {
        if EnvironmentUtil.isTargetPanda {
            if let p = params {
               SEGAnalytics.shared().track(track, properties: p)
            } else {
               SEGAnalytics.shared().track(track)
            }
            
        }
    }
}
