//
//  UserManager.swift
//  panda
//
//  Created by Edouard Roussillon on 6/18/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import UIKit


class UserManager {
    static fileprivate(set) var sharedInstance: UserManager = UserManager()
    
    let onUpdateUserSucceed = Event<UserModel>()
    let onUpdateUserFailed = Event<ErrorResponse?>()
    
    let onGetUserSucceed = Event<UserModel>()
    let onGetUserFailed = Event<ErrorResponse?>()
    
    let onReportUserSucceed = Event<UserModel>()
    let onReportUserFailed = Event<ErrorResponse?>()
    
    fileprivate init() {}
}

extension UserManager {
    
    func updateLocalUser(_ userModel: UserModel) {
        userModel.update()
    }
    
}

extension UserManager {
    
    func reportUser(_ userId: Int) {
        APIUser().reportUserBy(id: userId, success: { (data) in
            let user = UserModel.getUser(userId)
            user.reported = true
            user.update()
            self.onReportUserSucceed.dispatch(user)
        }) { (response, isErrorComsumed) -> Bool in
            self.onReportUserFailed.dispatch(response)
            return true
        }
    }
    
    func updateUser(_ speudo:String, email:String, gender:GenderType, birthday:Date, description:String, image: UIImage?) {
        
        if let i = image {
            let time = "\(Date().timeIntervalSince1970)".replacingOccurrences(of: ".", with: "")
            let imageName = "\(time).jpg"
            i.saveImage(imageName)
            
            APIData().uploadImage(imageName, success: { (data) in
                let image = data as! DataModel
                self.updateInfoUser(speudo, email: email, gender: gender, birthday: birthday, description: description, imageId: image.dataId)
            }, failure: { (response, isErrorComsumed) -> Bool in
                self.onUpdateUserFailed.dispatch(response)
                return true
            })
            
            
        } else {
            self.updateInfoUser(speudo, email: email, gender: gender, birthday: birthday, description: description, imageId: nil)
        }
        
       
    }
    
    fileprivate func updateInfoUser(_ speudo:String, email:String, gender:GenderType, birthday:Date, description:String, imageId: Int?) {
        APIUser().updateUser(speudo, email: email, gender: gender, birthday: birthday, description: description, imageId: imageId, success: { (data) in
            let user = data as! UserModel
            self.updateLocalUser(user)
            self.onUpdateUserSucceed.dispatch(user)
        }) { (response, isErrorComsumed) -> Bool in
            self.onUpdateUserFailed.dispatch(response)
            return true
        }
    }
    
    //
    //MARK: User Profile
    //
    
    func getUserBy(id: String) {
        APIUser().getUserBy(id: id, success: { (data) in
            let user = data as! UserModel
            self.updateLocalUser(user)
            self.onGetUserSucceed.dispatch(user)
        }) { (response, isErrorComsumed) -> Bool in
            self.onGetUserFailed.dispatch(response)
            return true
        }
    }
}
