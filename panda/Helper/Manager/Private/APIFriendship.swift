//
//  APIFriendship.swift
//  panda
//
//  Created by Edouard Roussillon on 2016-04-21.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

enum FriendshipList: Int {
    case accepted = 0
    case pending
    case blocked
}

class APIFriendship {
    

//    static func frienships(type: FriendshipList, page: Int, success: SuccessHandler, failure: FailHandler) {
//        var api:String = ""
//        
//        switch type {
//        case .Accepted:
//            api = Constants.WS.kAcceptedFriendship
//            break
//        case .Pending:
//            api = Constants.WS.kPendingFriendship
//            break
//        default:
//            api = Constants.WS.kBlockedFriendship
//        }
//        
//        let disc = ["page": page]
//        
//        BaseAPI.POST(api, parameters: disc, success: { (data) -> Void in
//            guard let mData = data as? String, let paginated = Mapper<Paginated<UserModel>>().map(mData) else {
//                return success(data: nil)
//            }
//            
//            return success(data: paginated)
//        }) { (response, isErrorComsumed) -> Bool in
//            return failure(response: response, isErrorComsumed: isErrorComsumed)
//        }
//    }
    
    func updateFriendship(_ toUserId: Int, status: FriendshipType, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        
        let disc = ["to_user_id":toUserId, "status":status.rawValue]
        
        BaseAPI.POST(Constants.WS.kUpdateFriendship, parameters: disc, type: UserModel.self, success: { (data) -> Void in
            
//            guard let mData = data as? String, let friendship = Mapper<FriendshipModel>().map(mData) else {
//                success(data: nil)
//                return
//            }
            
            return success(data as! UserModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
}
