//
//  APIData.swift
//  panda
//
//  Created by Edouard Roussillon on 9/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import UIKit

class APIData {
    
    func uploadImage(_ imageName: String, progress: ProgressHandler? = nil, success: @escaping SuccessHandler, failure: @escaping FailHandler) {

        if let path = UIImage.getPath(imageName),
            let data = try? Data.init(contentsOf: path) {
            let imageSize = Double(data.count) / 1024.0
            let param: [String: Any] = ["file_type": ".jpg",
                                              "file_name": imageName,
                                              "size": imageSize,
                                              "path": path]
            BaseAPI.UPLOAD(Constants.WS.kUploadImage, parameters: param, type: DataModel.self, progress: { (bytesWritten, totalBytesWritten, totalBytesExpected) in
                if let pro = progress {
                    pro(bytesWritten, totalBytesWritten, totalBytesExpected)
                }
                
                }, success: { (data) in
                    success(data as! DataModel)
                }, failure: { (response, isErrorComsumed) -> Bool in
                    return failure(response, isErrorComsumed)
            })
            
        } else {
            let _ = failure(nil, false)
        }
        
    }
    
}
