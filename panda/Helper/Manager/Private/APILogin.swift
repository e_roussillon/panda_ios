//
//  APILogin.swift
//  panda
//
//  Created by Edouard Roussillon on 11/16/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class APILogin {
    
    //
    //MARK: Login
    //
    
    func login(_ user:String, password:String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        
        let disc = ["email":user,
                    "platform_device":Constants.PLATFORM]
        
        let header:[String:String] = ["Password":password]
        
        BaseAPI.POST(Constants.WS.kLogin, parameters: disc, header: header, type: TokenModel.self, success: { (data) in
            success(data as! TokenModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    //
    //MARK: Register
    //
    
    func register(_ speudo:String, email:String, birthday:Date, pass:String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        
        let user = ["user": ["pseudo":speudo.trim().lowercased(), "email":email.trim().lowercased(), "birthday":birthday.getDateToString()]]
        let header:[String:String] = ["Password":pass]
        
        
        BaseAPI.POST(Constants.WS.kRegister, parameters: user, header: header, type: TokenModel.self, success: { (data) -> Void in
            success(data as! TokenModel)
        }, failure: {(response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        })
        
    }
    
    //
    //MARK: New Token
    //
    
    func newToken(_ refreshToken: String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let header = ["authorization_refesh": "Bearer " + refreshToken]
        
        BaseAPI.POST(Constants.WS.kRenewToken,
                     header: header,
                     type: TokenModel.self,
                     success: { (data) in
                        success(data)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    //
    //MARK: Push Token
    //
    
    func pushToken(_ token: String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let data = ["token": token, "os": "ios"]
        
        BaseAPI.POST(Constants.WS.kPushToken, parameters: data, type: EmptyModel.self, success: { (data) in
            success(data as! EmptyModel)
        }, failure: {(response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        })
    }
    
    //
    //MARK: Logout
    //
    
    func logout(_ success: @escaping SuccessHandler, failure: FailHandler) {
        BaseAPI.POST(Constants.WS.kLogout, type: EmptyModel.self, success: { (data) in
            success(nil)
        }, failure: {(response, isErrorComsumed) -> Bool in
            success(nil)
            return true
        })
    }
    
    //
    //MARK: ResetPassword
    //
    
    func resetPassword(_ email: String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let data = ["email": email]
        
        BaseAPI.POST(Constants.WS.kResetPassword, parameters: data, type: EmptyModel.self, success: { (data) in
            success(nil)
            }, failure: {(response, isErrorComsumed) -> Bool in
                let _ = failure(response, isErrorComsumed)
                return true
        })
    }
}
