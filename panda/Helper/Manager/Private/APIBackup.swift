//
//  APIBackup.swift
//  panda
//
//  Created by Edouard Roussillon on 7/17/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class APIBackup {
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    func backup(_ success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.GET(Constants.WS.kBackup, type: BackupModel.self, success: { (data) in
            success(data as! BackupModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    func backupPending(_ success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        BaseAPI.GET(Constants.WS.kBackupPending, type: BackupModel.self, success: { (data) in
            success(data as! BackupModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
}
