//
//  APISearch.swift
//  panda
//
//  Created by Edouard Roussillon on 7/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class APISearch {
    
    func search(_ pseudo: String, page: Int, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let disc: [String: AnyObject] = ["pseudo": pseudo.lowercased() as AnyObject,
                    "page": page as AnyObject]
        
        BaseAPI.POST(Constants.WS.kFiendFriendship, parameters: disc, type: Paginated<UserModel>.self, success: { (data) in
            success(data as! Paginated<UserModel>)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
}
