//
//  APIFilter.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class APIFilter {
    
    //
    //MARK: Filter
    //
    
    func addFilter(_ name: String, isHighlight: Bool, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let filter = ["name" : name, "is_highlight" : isHighlight] as [String : Any]
        let disc = ["user_filter": filter]
        
        BaseAPI.POST(Constants.WS.kFilter, parameters: disc, type: FilterModel.self, success: { (data) -> Void in
            success(data as! FilterModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    func updateFilter(_ filterId: Int, name: String, isHighlight: Bool, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let url = Constants.WS.kFilter + "/\(filterId)"
        
        let filter = ["name" : name, "is_highlight" : isHighlight] as [String : Any]
        let disc = ["id" : filterId, "user_filter": filter] as [String : Any]
        
        BaseAPI.PUT(url, parameters: disc, type: FilterModel.self, success: { (data) -> Void in
            success(data as! FilterModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    func removeFilter(_ filterId: Int, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let url = Constants.WS.kFilter + "/\(filterId)"
        let disc = ["id" : filterId]
        
        BaseAPI.DELETE(url, parameters: disc, type: EmptyModel.self, success: { (data) -> Void in
            return success(nil)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
}
