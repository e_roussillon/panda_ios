//
//  APIChat.swift
//  panda
//
//  Created by Edouard Roussillon on 2/27/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import CoreLocation
import Foundation
import ObjectMapper

class APIChat {
    
    fileprivate var sessionManager: SessionManager {
        get {
            return SessionManager.sharedInstance
        }
    }
    
    func findPublicRoom(_ success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        if let loc = GPSManager.sharedInstance.getCurrentLocalisation() {
            let disc = ["latitude" : "\(loc.latitude)", "longitude" : "\(loc.longitude)"]
            
            BaseAPI.POST(Constants.WS.kFindRoom, parameters: disc, type: FindRoomsModel.self, success: { (data) -> Void in
                success(data as! FindRoomsModel)
                }, failure: {(response, isErrorComsumed) -> Bool in
                    failure(response, isErrorComsumed)
            })
        } else {
            let _ = failure(nil, true)
        }
    }
    
    func confirmationsPrivateMessages(_ messages: [ConfirmationModel], success: @escaping SuccessHandler, failure: FailHandler) {
        let disc = ["messages" : messages.toJSON()]
        
        BaseAPI.POST(Constants.WS.kConfirmationMessages, parameters: disc, type: EmptyModel.self, success: { (data) in
            
            for message in messages {
                if let messageId: Int = message.id,
                    let statusInt: Int = message.status,
                    let status = StatusType(rawValue: statusInt) {
                    if let message = PrivateMessageModel.getMessage(messageId) {
                        message.status = status
                        message.updateStatus()
                    }
                }
            }
            
            success(data as! EmptyModel)
        }) { (response, isErrorComsumed) -> Bool in
            return true
        }
        
    }
    
}
