//
//  APIUser.swift
//  panda
//
//  Created by Edouard Roussillon on 11/29/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class APIUser {
    
    let sessionManager = SessionManager.sharedInstance
    
    //
    //MARK: Save user profile
    //
    
    func updateUser(_ speudo:String, email:String, gender:GenderType, birthday:Date, description:String, imageId: Int?, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let userId = self.sessionManager.user.userId
        let url = Constants.WS.kUser + "/\(userId)"
        
        var user: [String: Any] = ["pseudo": speudo, "email": email, "gender": gender.rawValue, "birthday": birthday.getDateToString(), "description": description]
        
        if let dataId = imageId {
            user["data_id"] = dataId as AnyObject?
        }
        
        let disc: [String: Any] = ["id": userId, "user": user]
        
        BaseAPI.PUT(url, parameters: disc, type: UserModel.self, success: { (data) -> Void in
            success(data as! UserModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    //
    //MARK: User Profile
    //
    
    func getUserBy(id: String, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let url = Constants.WS.kUser + "/\(id)"
        
        BaseAPI.GET(url, type: UserModel.self, success: { (data) -> Void in
            success(data as! UserModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
    //
    //MARK: Report user
    //
    
    func reportUserBy(id: Int, success: @escaping SuccessHandler, failure: @escaping FailHandler) {
        let reported: [String: AnyObject] = ["user_id_reported": id as AnyObject]
        let disc: [String: AnyObject] = ["user_report": reported as AnyObject]
        
        BaseAPI.POST(Constants.WS.kReportUser, parameters: disc, type: EmptyModel.self, success: { (data) -> Void in
            success(data as! EmptyModel)
        }) { (response, isErrorComsumed) -> Bool in
            return failure(response, isErrorComsumed)
        }
    }
    
}
