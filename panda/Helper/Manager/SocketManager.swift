//
//  SocketManager.swift
//  panda
//
//  Created by Edouard Roussillon on 5/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SocketManager {
    static let sharedInstance: SocketManager = SocketManager()
    fileprivate let socketJoiner: SocketJoiner = SocketJoinerManager()
    fileprivate let socketSender: SocketSender = SocketSenderManager()
    fileprivate let socketConnector: SocketConnector = SocketConnectorManager.connectorManager
    fileprivate let socketHighlighter: SocketHighlighterManager = SocketHighlighterManager()
    fileprivate let chatManager: ChatManager = ChatManager.sharedInstance
    fileprivate let sessionManager: SessionManager = SessionManager.sharedInstance
    fileprivate let socketNotification: SocketNotifierManager = SocketNotifierManager.sharedInstance

    public var updatedToken: Bool = false;

    //Have to be private to be singleton
    fileprivate init() {
    }

    //
    // Connector
    //

    func isSocketConnected() -> Bool {
        return socketConnector.isSocketConnected()
    }

    func startSocket() -> Bool {
        let result = socketConnector.startSocket()
        joinRooms()
        return result
    }

    func stopSocket(_ callback: @escaping () -> ()) {
        return socketConnector.stopSocket(callback)
    }

    func forceStopSocket(_ callback: @escaping () -> ()) {
        return socketConnector.forceStopSocket(callback)
    }

    //
    // Send Message
    //

    func sendPrivateMessages(_ messages: [PrivateMessageModel]) {
        socketSender.sendMessages(messages, socketConnector: self.socketConnector)
    }

    func sendPrivateMessage(_ roomId: Int, message: String, image: UIImage?, friendshipStatus: FriendshipType, friendshipLastAction: Int, toUser: UserModel) {
        let message = PrivateMessageModel.initMessageLite(roomId: roomId,
                msg: message,
                friendshipStatus: friendshipStatus,
                friendshipLastAction: friendshipLastAction,
                toUser: toUser,
                image: image)
        socketSender.sendMessage(privateMessage: message, socketConnector: self.socketConnector, withNotification: true)
    }

    func resendPrivateMessages(_ message: PrivateMessageModel) {
        socketSender.sendMessage(privateMessage: message, socketConnector: self.socketConnector, withNotification: false)
    }

    func sendPublicMessage(_ isGeneralTopic: Bool, message: String) {
        let message = PublicMessageModel.initMessageLite(msg: message, isGeneral: isGeneralTopic)
        socketSender.sendMessage(publicMessage: message, socketConnector: self.socketConnector)
    }

    func sendPublicMessageWelcome(_ isGeneralTopic: Bool) {
        if let title = self.chatManager.currentRoom?.originalName {
            if isGeneralTopic {
                let msg = NSLocalizedString("welcome_message_general", comment: "").replacingOccurrences(of: "%1", with: title.capitalized)

                let message = PublicMessageModel.initMessageLite(msg: msg, isGeneral: isGeneralTopic, status: StatusType.sent.rawValue, isWelcomeMessage: true)
                socketSender.sendWelcomeMessage(publicMessage: message)
            } else {
                let msg = NSLocalizedString("welcome_message_sale", comment: "").replacingOccurrences(of: "%1", with: title.capitalized)

                let message = PublicMessageModel.initMessageLite(msg: msg, isGeneral: isGeneralTopic, status: StatusType.sent.rawValue, isWelcomeMessage: true)
                socketSender.sendWelcomeMessage(publicMessage: message)
            }
        }
    }

    func resendPublicMessage(_ publicMessageModel: PublicMessageModel) {
        socketSender.resendMessage(publicMessage: publicMessageModel, socketConnector: self.socketConnector)
    }

    //
    // Delete Message
    //

    func deletePrivateMessages(_ list: [Int: [Int]]) {
        for item in list {
            deletePrivateMessages(roomId: item.key, listMessageIds: item.value)
        }
    }

    func deletePrivateMessages(roomId: Int, listMessageIds: [Int]) {
        socketSender.deleteMessage(roomId: roomId, listMessageIds: listMessageIds, socketConnector: self.socketConnector)
    }

    //
    // Join Channel
    //

    func leaveRooms() {
        socketConnector.leaveAllRoom()
    }

    func leaveRoomPublicRooms() {
        socketConnector.leaveRoomPublicRooms()
    }

    func joinRooms() {
        let userId = self.sessionManager.user.userId

        if userId > 0 {
            if !self.isJoined(ChannelType.Private, roomId: userId) {
                self.joinPrivateChannel()
            } else if (updatedToken) {
                self.refreshTokenPrivateChannel()
            }

            self.socketNotification.onJoinPrivateRoomSucceed.dispatch(Void())
        } else {
            self.socketNotification.onJoinPrivateRoomFailed.dispatch(Void())
        }

        if let token = self.chatManager.findRooms.token,
           let rooms = self.chatManager.findRooms.rooms, self.chatManager.currentRoomId > 0 && self.chatManager.isCountryFind {
            let id = self.chatManager.currentRoomId

            if !self.isJoined(ChannelType.Generic, roomId: id) {
                self.joinGeneralChannel(id, token: token)
            } else if (updatedToken) {
                self.refreshTokenGeneralChannel(id, token: token)
            }

            if !self.isJoined(ChannelType.Sale, roomId: id) {
                self.joinSaleChannel(id, token: token)
            } else if (updatedToken) {
                self.refreshTokenSaleChannel(id, token: token)
            }

            for room in rooms {
                guard let roomId = room.publicRoomId else {
                    return
                }

                if !self.isJoined(ChannelType.Broadcast, roomId: roomId) {
                    self.joinBroadcastChannel(roomId, token: token)
                } else if (updatedToken) {
                    self.refreshTokenBroadcastChannel(roomId, token: token)
                }
            }

//            } else {
//                self.socketNotification.onJoinPublicRoomFailed.dispatch()
//            }
        } else {
            self.socketNotification.onJoinPublicRoomFailed.dispatch(Void())
            GPSManager.sharedInstance.getLastLocalisation()
        }

        if (updatedToken) {
            updatedToken = false;
        }

    }

    func isJoined(_ channelType: ChannelType, roomId: Int) -> Bool {
        return socketJoiner.isJoined(channelType, roomId: roomId, socketConnector: self.socketConnector)
    }

    func joinGeneralChannel(_ roomId: Int, token: String) {
        socketJoiner.joinPublicChannel(ChannelType.Generic, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func refreshTokenGeneralChannel(_ roomId: Int, token: String) {
        socketJoiner.refreshToken(ChannelType.Generic, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func joinSaleChannel(_ roomId: Int, token: String) {
        socketJoiner.joinPublicChannel(ChannelType.Sale, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func refreshTokenSaleChannel(_ roomId: Int, token: String) {
        socketJoiner.refreshToken(ChannelType.Sale, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func joinBroadcastChannel(_ roomId: Int, token: String) {
        socketJoiner.joinPublicChannel(ChannelType.Broadcast, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func refreshTokenBroadcastChannel(_ roomId: Int, token: String) {
        socketJoiner.refreshToken(ChannelType.Broadcast, roomId: roomId, token: token, socketConnector: self.socketConnector)
    }

    func joinPrivateChannel() {
        return socketJoiner.joinPrivateChannel(self.socketConnector)
    }

    func refreshTokenPrivateChannel() {
        return socketJoiner.refreshToken(self.socketConnector)
    }

    //
    // Message Highlighter
    //

    func isPublicMessageHavetoBeBlocked(_ publicMessage: inout PublicMessageModel, channelType: ChannelType) -> Bool {
        return socketHighlighter.checkIsIncomingPublicMessageHaveToBeBlocked(&publicMessage, channelType: channelType)
    }
}
