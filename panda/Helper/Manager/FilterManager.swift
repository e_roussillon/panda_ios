//
//  FilterManager.swift
//  panda
//
//  Created by Edouard Roussillon on 6/18/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class FilterManager {
//    static private(set) var sharedInstance: FilterManager = FilterManager()
    static let sharedInstance: FilterManager = FilterManager()
    
    let onAddFilterSucceed = Event<FilterModel>()
    let onAddFilterFailed = Event<ErrorResponse?>()
    
    let onUpdateFilterSucceed = Event<FilterModel>()
    let onUpdateFilterFailed = Event<ErrorResponse?>()
    let onUpdateFilterFailedMissingParam = Event<ErrorResponse?>()
    
    let onRemoveFilterSucceed = Event<Void>()
    let onRemoveFilterFailed = Event<ErrorResponse?>()
    
    fileprivate init() {}
}

//
// MARK: API Filter
//

extension FilterManager {
    
    func addFilter(_ name: String, isHighlight: Bool) {
        
        APIFilter().addFilter(name, isHighlight: isHighlight, success: { (data) in 
            if let filter = self.updateOfInsertFilter(data) {
                PublicMessageModel.updateMessageWithFilter(filter: filter)
                self.onAddFilterSucceed.dispatch(filter)
            } else {
                self.onAddFilterFailed.dispatch(nil)
            }
        }) { (response, isErrorComsumed) -> Bool in
            self.onAddFilterFailed.dispatch(response)
            return true
        }
        
    }
    
    func updateFilter(_ filterModel: FilterModel, name: String) {
        
        guard let id = filterModel.filterId, let isHighlight = filterModel.isHighlight else {
            self.onUpdateFilterFailedMissingParam.dispatch(nil)
            return
        }
        
        APIFilter().updateFilter(id, name: name, isHighlight: isHighlight, success: { (data) in
            if let filter = self.updateOfInsertFilter(data) {
                PublicMessageModel.updateMessageWithFilter(filter: filter)
                self.onUpdateFilterSucceed.dispatch(filter)
            } else {
                self.onUpdateFilterFailed.dispatch(nil)
            }
        }) { (response, isErrorComsumed) -> Bool in
            self.onUpdateFilterFailed.dispatch(response)
            return true
        }
        
    }
    
    func removeFilter(_ filterModel: FilterModel) {
        
        if let filterId = filterModel.filterId {
            PublicMessageModel.removeMessageWithFilter(filter: filterModel)
            
            APIFilter().removeFilter(filterId, success: { (data) in
                filterModel.remove()
                self.onRemoveFilterSucceed.dispatch(Void())
            }) { (response, isErrorComsumed) -> Bool in
                self.onRemoveFilterFailed.dispatch(response)
                return true
            }
        }

    }
    
    fileprivate func updateOfInsertFilter(_ data: Any?) -> FilterModel? {
        guard let filter = data as? FilterModel,
            let filterId = filter.filterId,
            let name = filter.name,
            let isHighlight = filter.isHighlight
            else {
                return nil
        }
        
        FilterModel.iniFilter(filterId, name: name, isHighlight: isHighlight, timestamp: Date().getDateToISO8601ZString()).insert()
        
        return filter
    }
    
}
