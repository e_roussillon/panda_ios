//
//  FriendshipManager.swift
//  panda
//
//  Created by Edouard Roussillon on 6/18/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FriendshipManager {
    static fileprivate(set) var sharedInstance: FriendshipManager = FriendshipManager()
    
    let onUpdateFriendshipSucceed = Event<UserModel>()
    let onUpdateFriendshipFailed = Event<ErrorResponse?>()
    let onUpdateFriendshipFailedUserIdNotFound = Event<ErrorResponse?>()
    
    fileprivate init() {}
}

//
// MARK: Get friends list local
//

extension FriendshipManager {
    func getUsersByFriendshipTypeFriend() -> [UserModel] {
        return UserModel.getUsersByFriendshipTypeFriend()
    }
    
    func getUsersByFriendshipTypeBlocked() -> [UserModel] {
        return UserModel.getUsersByFriendshipTypeBlocked()
    }
    
    func getUsersByFriendshipTypePending() -> [UserModel] {
        return UserModel.getUsersByFriendshipTypePending()
    }
}

//
// MARK: API Update Friendship
//

extension FriendshipManager {
    
    fileprivate var userManager: UserManager {
        get {
            return UserManager.sharedInstance
        }
    }
    
    func updateFriendship(_ userId: Int?, status: FriendshipType) {
        guard let id = userId else {
            self.onUpdateFriendshipFailedUserIdNotFound.dispatch(nil)
            return
        }
        
        APIFriendship().updateFriendship(id, status: status, success: { (data) -> Void in
            
            let user = data as! UserModel
//            user.friendshipStatus = friendship.status
//            user.friendshipLastAction = friendship.userLastAction
            user.update()
            self.onUpdateFriendshipSucceed.dispatch(user)
            
        }, failure: { (response, isErrorComsumed) -> Bool in
            self.onUpdateFriendshipFailed.dispatch(response)
            return true
        })
    }
    
}

