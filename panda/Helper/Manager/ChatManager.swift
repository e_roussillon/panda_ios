//
//  ChatManager.swift
//  panda
//
//  Created by Edouard Roussillon on 6/19/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation

class ChatManager {
    static fileprivate(set) var sharedInstance: ChatManager = ChatManager()
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let sessionManager = SessionManager.sharedInstance
    
    fileprivate(set) var isCountryFind: Bool = false
    
    var currentRoomId: Int {
        get {
            return self.sessionManager.keychainUtil.currentRoomId
        }
        
        set {
            return self.sessionManager.keychainUtil.currentRoomId = newValue
        }
    }
    
    var currentRoom: PublicRoomModel? {
        get {
            return self.sessionManager.keychainUtil.currentRoom
        }
        
        set {
            return self.sessionManager.keychainUtil.currentRoom = newValue
        }
    }
    
    var findRooms: FindRoomsModel {
        get {
            return self.sessionManager.keychainUtil.findRooms
        }
        
        set {
            return self.sessionManager.keychainUtil.findRooms = newValue
        }
    }
    
    fileprivate init() {}
    
    let onFindPublicRoomSucceed = Event<FindRoomsModel>()
    let onFindPublicRoomFailed = Event<Void>()
}

extension ChatManager {
    
    func findPublicRoom() {
        APIChat().findPublicRoom({ (data) in
            self.isCountryFind = true
            self.findRooms = data as! FindRoomsModel
            
            if let rooms = self.findRooms.rooms {
                for room in rooms {
                    if let type = room.type, let id = room.publicRoomId, type == "city" {
                        self.currentRoomId = id
                        self.currentRoom = room
                        SocketManager.sharedInstance.joinRooms()
                        break
                    }
                }
            }
            
            self.onFindPublicRoomSucceed.dispatch(self.findRooms)
            
        }) { (response, isErrorComsumed) -> Bool in
            self.sessionManager.user.resetLocalisation()
            self.isCountryFind = false
            
            self.onFindPublicRoomFailed.dispatch(Void())
            return true
        }
        
    }
    
    func refreshPublicRoom() {
        let refreshPublicRoom = RefreshPublicRoom()
        let gpdManager = GPSManager.sharedInstance
        
        gpdManager.onGetLocalisation.addOnce(refreshPublicRoom, handler: RefreshPublicRoom.onGetLocalisation)
        gpdManager.getLastLocalisation()
    }
    
    func receivedPrivareMessages(_ messages: [(id: Int, status: StatusType)]) {
        var list: [ConfirmationModel] = []
        
        for (id, status) in messages {
            list.append(ConfirmationModel.initConfirmation(id: id, status: status))
        }
        
        
        APIChat().confirmationsPrivateMessages(list, success: { (data) in
        }) { (response, isErrorComsumed) -> Bool in
            return true
        }
    }
    
    fileprivate class RefreshPublicRoom {
    
        func onGetLocalisation(_ local: CLLocationCoordinate2D) {
            ChatManager.sharedInstance.findPublicRoom()
        }
    
    }
    
}
