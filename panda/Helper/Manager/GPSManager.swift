//
//  GPSManager.swift
//  panda
//
//  Created by Edouard Roussillon on 9/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import MapKit
import CoreLocation

public enum GPSState: Int {
    case restricted = 0
    case denied
    case notDetermined
    case failed
    case lookingFor
    case gotIt
}

class GPSManager {
    static let sharedInstance = GPSManager()
    
    fileprivate var manager = LocalisationManager()
    fileprivate init() {}
    
    //
    /// MARK: Events
    //
    let onGetLocalisation = Event<CLLocationCoordinate2D>()
    //The app is not allowed to use location services.
    let onGetLocalisationDenied = Event<Void>()
    //The user has not yet chosen whether to allow location services.
    let onGetLocalisationNotDetermined = Event<Void>()
    //Location services are not available and the user cannot change the authorization (e.g., constrained by parental controls).
    let onGetLocalisationRestricted = Event<Void>()
    let onGetLocalisationFail = Event<Error>()
}


extension GPSManager {
    
    func resetLocalisation() {
        manager.lastLocalisation = nil
    }
    
    func getGPSState() -> GPSState {
        
        if manager.gpsState == GPSState.notDetermined {
            getLastLocalisation()
        }
        
        return manager.gpsState
    }
    
    func getCurrentLocalisation() -> CLLocationCoordinate2D? {
        if let loc = self.manager.lastLocalisation?.coordinate {
            return loc
        } else {
            return SessionManager.sharedInstance.user.currentLocalisation
        }
    }
    
    func getLastLocalisation() {
        
        guard let localisation = self.manager.lastLocalisation else {
            if SimulatorUtil.isRunningSimulator {
                self.manager.gpsState = .gotIt
                //Sao Paulo
//                self.manager.lastLocalisation = CLLocation(latitude: -23.625450, longitude: -46.701587)
                //Mogi
//                self.manager.lastLocalisation = CLLocation(latitude: -23.526463, longitude: -46.196695)
                //New York
//                self.manager.lastLocalisation = CLLocation(latitude: 40.669188, longitude: -73.972992)
                //Paris
                self.manager.lastLocalisation = CLLocation(latitude: 48.8507085, longitude: 2.3067613)
            
                ChatManager.sharedInstance.findPublicRoom()
                self.onGetLocalisation.dispatch(self.manager.lastLocalisation!.coordinate)
            } else {
                let status = CLLocationManager.authorizationStatus()
                if status == .authorizedAlways || status == .authorizedWhenInUse {
                    self.manager.startUpdatingLocation()
                } else if status == .notDetermined || status == .denied {
                    self.manager.requestWhenInUseAuthorization()
                }
            }

            return
        }
        
        self.onGetLocalisation.dispatch(localisation.coordinate)
    }
}

private class LocalisationManager: NSObject, CLLocationManagerDelegate {
    let locationManager: CLLocationManager = CLLocationManager()
    var lastLocalisation: CLLocation? {
        didSet {
            SessionManager.sharedInstance.user.currentLocalisation = lastLocalisation?.coordinate
        }
    }
    
    var gpsState: GPSState!
    
    override init() {
        super.init()
        gpsState = .notDetermined
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    //
    //MARK: CLLocationManagerDelegate
    //
    
    @objc func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if SimulatorUtil.isRunningSimulator {
            return
        }
        
        switch status {
        case CLAuthorizationStatus.restricted:
            gpsState = .restricted
            GPSManager.sharedInstance.onGetLocalisationRestricted.dispatch(Void())
        case CLAuthorizationStatus.denied:
            gpsState = .denied
            GPSManager.sharedInstance.onGetLocalisationDenied.dispatch(Void())
        case CLAuthorizationStatus.notDetermined:
            gpsState = .notDetermined
            GPSManager.sharedInstance.onGetLocalisationNotDetermined.dispatch(Void())
        default:
            self.lastLocalisation = nil
            startUpdatingLocation()
        }
        
        //self.storeSearchPromotionViewController.removedLoader()
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        gpsState = .gotIt
        if let _ = self.lastLocalisation {
            //Do nothing you already have the geo
            self.stopUpdatingLocation()
        } else if let local = locations.first {
            self.lastLocalisation = local
            ChatManager.sharedInstance.findPublicRoom()
            GPSManager.sharedInstance.onGetLocalisation.dispatch(local.coordinate)
            self.stopUpdatingLocation()
        }
        
    }
    
    @objc func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        gpsState = .failed
        GPSManager.sharedInstance.onGetLocalisationFail.dispatch(error)
        self.stopUpdatingLocation()
    }
    
    func startUpdatingLocation() {
        gpsState = .lookingFor
        locationManager.startUpdatingLocation()
    }
    
    func requestWhenInUseAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
}
