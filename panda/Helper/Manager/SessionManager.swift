//
//  SessionManager.swift
//  panda
//
//  Created by Edouard Roussillon on 6/19/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation


class SessionManager {
    static fileprivate(set) var sharedInstance: SessionManager = SessionManager()
    let keychainUtil: KeychainUtil = KeychainUtil.sharedInstance
    
    let onGetSessionSucceed = Event<Void>()
    let onGetSessionFailed = Event<ErrorResponse?>()
    let onGetSessionFailedNotFoundEmailOrPassword = Event<Void>()
    let onGetSessionFailedRateLimit = Event<Void>()
    let onGetSessionFailedNotActive = Event<Void>()
    
    let onRefreshSessionSucceed = Event<Void>()
    let onRefreshSessionFailed = Event<ErrorResponse?>()
    
    let onResetPasswordSessionSucceed = Event<Void>()
    let onResetPasswordSessionFailed = Event<Void>()
    
    fileprivate init() {}
}

//
// MARK: Session Param
//

extension SessionManager {
    var user: User {
        get {
            return User.sharedInstence
        }
    }
    
    class User {
        fileprivate static let sharedInstence: User = User()
        fileprivate let sessionManager = SessionManager.sharedInstance

        fileprivate init() {}
        
        fileprivate(set) var token: String {
            get {
                return sessionManager.keychainUtil.token
            }
            set {
                sessionManager.keychainUtil.token = newValue
            }
        }
        
        fileprivate(set) var pushToken: String {
            get {
                return sessionManager.keychainUtil.pushToken
            }
            set {
                sessionManager.keychainUtil.pushToken = newValue
            }
        }
        
        fileprivate(set) var tokenRefresh: String {
            get {
                return sessionManager.keychainUtil.tokenRefresh
            }
            set {
                sessionManager.keychainUtil.tokenRefresh = newValue
            }
        }
        
        var justLogin: Bool {
            get {
                return sessionManager.keychainUtil.justLogin
            }
            set {
                sessionManager.keychainUtil.justLogin = newValue
            }
        }
        
        var welcomeScreen: Bool {
            get {
                return sessionManager.keychainUtil.welcomeScreen
            }
            set {
                sessionManager.keychainUtil.welcomeScreen = newValue
            }
        }
        
        fileprivate(set) var userId: Int {
            get {
                return sessionManager.keychainUtil.userId
            }
            set {
                sessionManager.keychainUtil.userId = newValue
            }
        }
        
        var currentLocalisation: CLLocationCoordinate2D? {
            get {
                return sessionManager.keychainUtil.currentLocalisation
            }
            set {
                sessionManager.keychainUtil.currentLocalisation = newValue
            }
        }
        
        var isLogin: Bool {
            get {
                return sessionManager.keychainUtil.isLogin
            }
        }
        
        var userPseudo: String {
            get {
                return UserModel.getCurrentUser()?.pseudo ?? ""
            }
        }
        
        func reset() {
            sessionManager.keychainUtil.reset()
        }
        
        func resetDB() {
            sessionManager.keychainUtil.resetDB()
        }
        
        func resetWelcomeScreen() {
            sessionManager.keychainUtil.resetWelcomeScreen()
        }
        
        func resetLocalisation() {
            sessionManager.keychainUtil.resetLocalisation()
        }
        
        func resetPublicChat() {
            sessionManager.keychainUtil.resetPublicChat()
        }
    }
    
}


//
// MARK: API Login
//

extension SessionManager {
    
    func uploadPushToken(_ token: String?) {
        if let t = token, self.user.isLogin {
            APILogin().pushToken(t, success: { (data) in
                self.user.pushToken = t
            }, failure: { (response, isErrorComsumed) -> Bool in
                return true
            })
        } else {
            self.user.pushToken = ""
        }
    }
    
    func updateUserId(_ userId: Int?) {
        if let userId = userId {
            self.user.userId = userId
            AnalyticsManager.sharedInstance.trackUserLogin()
        } else {
            BaseViewController.logoutPanda(true)
        }
        
    }
    
    func login(_ user:String, password:String) {
        APILogin().login(user, password: password, success: { (data) in
            self.checkToken(data)
        }) { (response, isErrorComsumed) -> Bool in
            self.clearUser()
            guard let errorModel: ErrorResponse = response, let code = errorModel.code else {
                self.onGetSessionFailed.dispatch(response)
                return false
            }
            
            if let code = LoginError(rawValue: code), errorModel.type == .auth {
                switch code {
                case .error_EMAIL_OR_PASSWORD_EMPTY:
                    self.onGetSessionFailedNotFoundEmailOrPassword.dispatch(Void())
                    return true
                case .error_RATE_LIMIT:
                    self.onGetSessionFailedRateLimit.dispatch(Void())
                    return true
                case .error_NOT_ACTIVE:
                    self.onGetSessionFailedNotActive.dispatch(Void())
                    return true
                }
            }
            
            self.onGetSessionFailed.dispatch(response)
            return false
        }
    }
    
    func register(_ speudo:String, email:String, birthday:Date, password:String) {
        APILogin().register(speudo, email: email, birthday: birthday, pass: password, success: { (data) in
            self.checkToken(data)
        }) { (response, isErrorComsumed) -> Bool in
            self.clearUser()
            self.onGetSessionFailed.dispatch(response)
            return true
        }
    }

    func logout() {
        self.clearUser()
        APILogin().logout({ (data) in
        }) { (response, isErrorComsumed) -> Bool in
            return true
        }
    }
    
    func refreshToken() {
        APILogin().newToken(self.user.tokenRefresh, success: { (data) in
            if self.loginUser(data as! TokenModel, uploadPushToken: false) {
                SocketManager.sharedInstance.updatedToken = true;
                ChatManager.sharedInstance.refreshPublicRoom()
                let _ = SocketConnectorPhoenix.sharedInstance.startSocket()

                self.onRefreshSessionSucceed.dispatch(Void())
            } else {
                self.onRefreshSessionFailed.dispatch(nil)
            }
        }) { (response, isErrorComsumed) -> Bool in
            self.onRefreshSessionFailed.dispatch(response)
            return true
        }
    }
    
    func resetPassword(_ email: String) {
        
        APILogin().resetPassword(email, success: { (data) in
            self.onResetPasswordSessionSucceed.dispatch(Void())
        }) { (response, isErrorComsumed) -> Bool in
            self.onResetPasswordSessionFailed.dispatch(Void())
            return true
        }
        
    }
    
    //
    //MARK: Generic
    //
    
    fileprivate func checkToken(_ any: Any?) {
        if let token = any as? TokenModel, self.loginUser(token) {
            self.onGetSessionSucceed.dispatch(Void())
        } else {
            self.onGetSessionFailed.dispatch(nil)
        }
    }
    
    fileprivate func loginUser(_ tokenModel: TokenModel, uploadPushToken: Bool = true) -> Bool {
        guard let token = tokenModel.token, let refreshToken = tokenModel.refreshToken else {
            self.logout()
            return false
        }
        
        self.user.token = token
        self.user.tokenRefresh = refreshToken
        self.user.justLogin = true
        if uploadPushToken {
            self.uploadPushToken(self.user.pushToken)
        }
        return true
    }
    
    fileprivate func clearUser() {
        self.user.reset()
    }
    
    //
    // MARK: New Token Room
    //
    
    func onFindPublicRoomFailed() {
        
    }
    
}
