//
//  SearchManager.swift
//  panda
//
//  Created by Edouard Roussillon on 7/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SearchManager {
    static fileprivate(set) var sharedInstance: SearchManager = SearchManager()
    
    
    let onSearchSucceed = Event<Paginated<UserModel>>()
    let onSearchFailed = Event<ErrorResponse?>()
    
    fileprivate init() {}
}

//
// MARK: API Login
//

extension SearchManager {
    
    func searchSpeudo(_ speudo: String, page: Int) {
        APISearch().search(speudo, page: page, success: { (data) in
            
            self.onSearchSucceed.dispatch(data as! Paginated<UserModel>)
            
        }) { (response, isErrorComsumed) -> Bool in
            self.onSearchFailed.dispatch(response)
            return true
        }
    }
    
}
