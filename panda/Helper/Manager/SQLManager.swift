//
//  SQLManager.swift
//  panda
//
//  Created by Edouard Roussillon on 2/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import SQLite

protocol SQLManagerDelegate {
    static func creationTable(_ db: Connection)
}

class SQLManager {
    
    static func initTables() {
        FilterModel.creationTable(DBUtil.sharedInstance)
        UserModel.creationTable(DBUtil.sharedInstance)
        PrivateMessageModel.creationTable(DBUtil.sharedInstance)
        PrivateChatModel.creationTable(DBUtil.sharedInstance)
        PublicMessageModel.creationTable(DBUtil.sharedInstance)
    }
    
    static func resetTables() {
        FilterModel.resetTable(DBUtil.sharedInstance)
        UserModel.resetTable(DBUtil.sharedInstance)
        PrivateMessageModel.resetTable(DBUtil.sharedInstance)
        PrivateChatModel.resetTable(DBUtil.sharedInstance)
        PublicMessageModel.resetTable(DBUtil.sharedInstance)
    }
    
    static func resetPublicChatTables() {
        PublicMessageModel.resetTable(DBUtil.sharedInstance)
    }
    
}
