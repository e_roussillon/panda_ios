//
//  ReachabilityManager.swift
//  panda
//
//  Created by Edouard Roussillon on 12/10/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Reachability

enum ReachabilityManagerType {
    case wifi
    case cellular
    case none
}

class ReachabilityManager {
    static let sharedInstance = ReachabilityManager()
    
    fileprivate var reachability: Reachability!
    fileprivate var reachabilityManagerType: ReachabilityManagerType = .none
    
    
    fileprivate init() {
        self.reachability = Reachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReachabilityManager.reachabilityChanged(_:)),name: Notification.Name.reachabilityChanged,object: self.reachability)
        do{
            try self.reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc fileprivate func reachabilityChanged(_ note: Notification) {
        
        let reachability = note.object as! Reachability
                
        if reachability.connection != .none {
            if reachability.connection == .wifi {
                self.reachabilityManagerType = .wifi
            } else {
                self.reachabilityManagerType = .cellular
            }
        } else {
            self.reachabilityManagerType = .none
        }
    }
}

extension ReachabilityManager {

    func isConnectedToNetwork() -> Bool {
        return reachabilityManagerType != .none
    }
    
}
