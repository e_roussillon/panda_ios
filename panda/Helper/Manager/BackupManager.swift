//
//  BackupManager.swift
//  panda
//
//  Created by Edouard Roussillon on 7/17/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class BackupManager {
    static fileprivate(set) var sharedInstance: BackupManager = BackupManager()
    
    
    let onBackupSucceed = Event<Void>()
    let onBackupFailed = Event<ErrorResponse?>()
    
    fileprivate init() {}
}

//
// MARK: API Login
//

extension BackupManager {
    
    fileprivate var sessionManager: SessionManager {
        return SessionManager.sharedInstance
    }
    
    func backup() {
        APIBackup().backup({ (data) in
            self.checkBackup(data)
        }) { (response, isErrorComsumed) -> Bool in
            self.sessionManager.logout()
            self.onBackupFailed.dispatch(response)
            return true
        }
    }
    
    func backupPending() {
        APIBackup().backupPending({ (data) in
            self.backupPending(data)
        }) { (response, isErrorComsumed) -> Bool in
            return true
        }
    }
    
    fileprivate func checkBackup(_ any: Any?) {
        if let backup = any as? BackupModel, self.backupUser(backup) {
            self.onBackupSucceed.dispatch(Void())
        } else {
            self.sessionManager.logout()
            self.onBackupFailed.dispatch(nil)
        }
    }
    
    fileprivate func backupUser(_ backupModel: BackupModel) -> Bool {
        guard let userId: Int = backupModel.userId, userId > 0 else {
            return false
        }
        
        backupModel.insert()
        self.sessionManager.updateUserId(userId)
        let _ = SocketManager.sharedInstance.startSocket()
        return true
    }
    
    fileprivate func backupPending(_ any: Any?) {
        guard let backup = any as? BackupModel else {
            return
        }
        
        let listMessagesBefore = PrivateMessageModel.getSentMessagesOrderAsc()
        let listFriendsBefore = UserModel.getUsersByFriendshipTypePending()
        backup.insert()
        var listMessagesAfter = PrivateMessageModel.getSentMessagesOrderAsc()
        var listFriendsAfter = UserModel.getUsersByFriendshipTypePending()
        
        listMessagesAfter.removeObjectsInArray(listMessagesBefore)
        listFriendsAfter.removeObjectsInArray(listFriendsBefore)
        
        var listIds: [(id: Int, status: StatusType)] = []
        for privateMessage in listMessagesAfter {
            if let messageId = privateMessage.messageId {
                listIds.append((id: messageId, status: StatusType.received))
                UILocalNotification.showPrivateMessage(privateMessage)
            }
        }
        
        for user in listFriendsAfter {
            if let pseudo = user.pseudo,
                let userId = user.userId {
                UILocalNotification.showFriendship(pseudo, userId: userId)
            }
        }
        
        if listIds.count > 0 {
             ChatManager.sharedInstance.receivedPrivareMessages(listIds)
        }
    }
}
