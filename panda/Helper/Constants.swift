//
//  Constants.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.

import UIKit

class Constants {
    
    static let PLATFORM = "ios"
    
    struct HEADER {
        static let APP_ID = "123456qwerty"
        static let DEVICE_ID = UIDevice.current.identifierForVendor?.uuidString ?? "";
        static let LANGUAGE = "en"
        static let JSON = "application/json"
        static let DATA = "application/x-www-form-urlencoded"
        static let ENCODING = "gzip"
    }
    
    struct SOCKET {
        static let kURL = EnvironmentUtil.urlWS
        static let kPath = "socket"
        static let kTransport = "websocket"
        static let kProt = EnvironmentUtil.protocolWS
    }
    
    struct WS {
        static let kTimeoutSec: TimeInterval = 20
        static let kAPI = EnvironmentUtil.urlAPI
        
        //ResetPassword
        static let kResetPassword = "api/v1/reset_password"
        
        //Login
        static let kLogin = "api/v1/auth"
        
        //Logout
        static let kLogout = "api/v1/logout"
        
        //Renew Token
        static let kRenewToken = "api/v1/auth/renew"
        
        //Push Notification - Token
        static let kPushToken = "api/v1/push_token"
        
        //Register
        static let kRegister = "api/v1/register"
        
        //Backup
        static let kBackup = "api/v1/auth/backup"
        static let kBackupPending = "api/v1/pending/backup"
        
        //User
        static let kUser = "api/v1/users"
        static let kReportUser = "api/v1/report"
        
        //PublicRoom
        static let kFindRoom = "api/v1/find_room"
        
        static let kConfirmationMessages = "api/v1/confirmation/messages"
        
        //Filter
        static let kFilter = "api/v1/filters"
        
        //Friendship
        static let kUpdateFriendship = "api/v1/update_friendship"
        static let kFiendFriendship = "api/v1/find_friendships"
        
        //Data Image
        static let kUploadImage = "api/v1/upload/image"
        
        
//        ##################
//        ##################
//        ##################
        
        //Token
        static let kNewToken = "api/v1/auth/renew"
        
        //Private Chat
        static let kPrivateChatsList = "api/chat/get_all_conversations"
        static let kCreateChats = "api/chat/create_conversations"
        
        //Users Chat
        static let kUsersChatList = "api/friend/users"
    }
    
    struct COLOR {
        //App Tint
        static let kAppTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
        static let kTextFieldTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
        static let kTextViewTint:UIColor = UIColor.UIColorFromRGB(0x3d3d3d)
        static let kToolTint:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kToolBarTint:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
        static let kTabbarTint:UIColor = UIColor.UIColorFromRGB(0xF5F5F5)
        static let kTabbarShadow:UIColor = UIColor.UIColorFromRGB(0x474747)
        
        //Tabbar
        static let kTabbarText:UIColor = UIColor.UIColorFromRGB(0x474747)
        
        //Loader
        static let kBackgroundLoader:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.0)
        static let kSpinnerLoader:UIColor = UIColor.UIColorFromRGB(0x474747)
        
        //Navigation Bar
        static let kNavigationBar:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
        static let kNavigationButton:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kNavigationBarDarck:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kNavigationButtonDarck:UIColor = UIColor.UIColorFromRGB(0xFFFFFF, alpha:0.8)
        
        //View COntroller
        static let kBackgroundViewController:UIColor = UIColor.UIColorFromRGB(0xF5F5F5)
        
        //Text Filed
        static let kTitlePlaceholder:UIColor = UIColor.UIColorFromRGB(0x858585)
        static let kTitleError:UIColor = UIColor.UIColorFromRGB(0xF40606)
        static let kViewLine:UIColor = UIColor.UIColorFromRGB(0x474747)
        
        //Text Button
        static let kTitleButtonLight:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kTitleButtonDarck:UIColor = UIColor.white
        
        //Gender Button
        static let kBorderSelected:UIColor = UIColor.UIColorFromRGB(0x1F1F1F)
        static let kBorderError:UIColor = UIColor.UIColorFromRGB(0xF40606)
        
        //Chat General
        static let kBorderColorSending:UIColor = UIColor.UIColorFromRGB(0x999999, alpha: 0.15)
        static let kBorderColorError:UIColor = UIColor.UIColorFromRGB(0xF40606)
        
        //Chat Connection View
        static let kBorderColorConnection:UIColor = UIColor.UIColorFromRGB(0x999999, alpha: 0.25)
        
        static let kBorderChatTextView:UIColor = UIColor.UIColorFromRGB(0xCCC7C7)
        static let kShadowChatTextView:UIColor = UIColor.UIColorFromRGB(0x000000)
        
        
        static let kTextColorDate:UIColor = UIColor.UIColorFromRGB(0x6e6e6e)
        static let kTextColorHighlight:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kTextColorNormal:UIColor = UIColor.UIColorFromRGB(0x474747)
        static let kTextColorNotInterested:UIColor = UIColor.UIColorFromRGB(0x474747, alpha: 0.7)
        
    }
    
    struct KEYS {
        static let kTOKEN: String = "token"
        static let kTOKEN_REFRESH: String = "tokenRefresh"
        static let kPUSH_TOKEN: String = "pushToken"
        static let kUSER_ID: String = "userId"
        static let kJUST_LOGIN: String = "justLogin"
        static let kIMAGE_DOWNLOADED: String = "imagesDownloaded"
        static let kFIND_ROOM: String = "findRoom"
        static let kCURRENT_ROOM_ID: String = "currentRoomId"
        static let kCURRENT_ROOM: String = "currentRoom"
        static let kCURRENT_LOCALISATION_LNG: String = "currentLocaliationLng"
        static let kCURRENT_LOCALISATION_LAT: String = "currentLocaliationLat"
        static let kCURRENT_DB: String = "currentDB"
        static let kWelcomeScreen: String = "welcomeScreen"
    }
    
    struct FIELD {
        static let kPseudoMin:Int = 4
        static let kPseudoMax:Int = 30
    }
    
    struct SEGUE {
        static let SHOW_PROFILE = "SHOW_PROFILE"
        static let OPEN_CONVERSATION = "OPEN_CONVERSATION"
        static let ACTION_FRIENDSHIP = "ACTION_FRIENDSHIP"
        static let HIDE_KEYBOARD = "HIDE_KEYBOARD"
    }
    
    struct SIZE {
        static let IMAGE = 1024.0 //Kilobyte
    }
}
