//
//  Event.swift
//  panda
//
//  Created by Edouard Roussillon on 5/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

open class Event<U> {
	
	public typealias Handler = (U) -> ()
	open var listeners = [Invocable]()
	
	open func add<T: AnyObject>(_ target: T, handler: @escaping (T) -> Handler) -> Disposable {
		let listener = Listener(target: target, handler: handler, event: self)
		listeners.append(listener)
		return listener
	}
    
	open func addOnce<T: AnyObject>(_ target: T, handler: @escaping (T) -> Handler) {
		let listener = Listener(target: target, handler: handler, event: self, once: true)
		listeners.append(listener)
	}
	
	open func dispatch( _ data: U ) {
		for listener in listeners {
			listener.invoke(data)
		}
	}
    	
	open func removeAll() {
		listeners.removeAll()
	}
	
}
