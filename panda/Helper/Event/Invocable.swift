//
//  Invocable.swift
//  panda
//
//  Created by Edouard Roussillon on 5/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

public protocol Invocable: class {
	func invoke(_ data: Any)
}
