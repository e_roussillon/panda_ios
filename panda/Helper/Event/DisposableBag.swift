//
//  DisposableBag.swift
//  panda
//
//  Created by Edouard Roussillon on 5/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

open class DisposableBag : Disposable {
	open fileprivate(set) var disposables = [Disposable]()
	
	open func add( _ disposable: Disposable ) {
		self.disposables.append(disposable)
	}
	
	open func dispose() {
		for disposable in disposables {
			disposable.dispose()
		}
		self.disposables.removeAll(keepingCapacity: false)
		self.disposables = [Disposable]()
	}
	
	deinit {
		dispose()
	}
}
