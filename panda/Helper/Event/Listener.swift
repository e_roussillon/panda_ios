//
//  EventHandler.swift
//  panda
//
//  Created by Edouard Roussillon on 5/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

public protocol Disposable {
    func dispose()
}

open class Listener<T: AnyObject, U>: Invocable, Disposable {
	
	/// Here we will insert the target that will receive the event
	open weak var target: T?
	
	/**
	*  This handler will be called when the event occours
	*
	*  @return return object type defined on event
	*/
	open var handler: (T) -> (U) -> ()
	
	/// Here the event associate with the handler
	open var event: Event<U>
	
	
	/// Inform if the handler will be called at once or more than one time
	open let once: Bool
	
	public init(target: T, handler: @escaping (T) -> (U) -> (), event: Event<U>, once:Bool = false) {
		self.target = target
		self.handler = handler
		self.event = event
		self.once = once
	}
    
	/**
	Here where the event will be invoked
	
	- parameter data: the parameter of event
	*/
	//swiftlint:disable force_cast
	open func invoke(_ data: Any) {
		
        if let t = target,
            let d = data as? U {
            handler(t)(d)
        }
        
		if self.once {
			self.dispose()
		}
	}

	/**
	Remove the handler from listeners
	*/
	open func dispose() {
		event.listeners = event.listeners.filter { $0 !== self }
	}
}
