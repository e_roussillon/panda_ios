//
//  ErrorAPI.swift
//  panda
//
//  Created by Edouard Roussillon on 12/5/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

public enum GenericUserError:Int {
    case error_USER_BLOCKED = 0
    case error_USER_NOT_FOUND
    case error_PLATFORM_DEVICE
    case error_DEVICE_ID
}

public enum LoginError:Int {
    case error_EMAIL_OR_PASSWORD_EMPTY = 0
    case error_RATE_LIMIT = 7
    case error_NOT_ACTIVE = 8
}

public enum RegisterError: String {
    case ERROR_PSEUDO = "pseudo"
    case ERROR_EMAIL = "email"
    case ERROR_BIRTHDAY = "birthday"
    case ERROR_PASSWORD = "password"
}

public enum FieldError: String {
    case ERROR_BLANK = "can't be blank"
    case ERROR_INVALID = "is invalid"
    case ERROR_FORMAT = "has invalid format"
    case ERROR_USED = "has already been taken"
    case ERROR_MIN_6_CHARACTER = "should be at least 6 character(s)"
    case ERROR_MIN_4_CHARACTER = "should be at least 4 character(s)"
    case ERROR_MAX_30_CHARACTER = "should be at most 30 character(s)"
    case ERROR_BIRTDAY_UNDER_16 = "birthday under 16"
}

public enum ProfileError:Int {
    case error_USER_EMPTY = 0
    case error_USER_NOT_FOUND
    case error_STATUS_NOT_FOUND
    case error_MESSAGE_NOT_FOUND
}

public enum UpdateProfileError:Int {
    case error_USER_EMPTY = 0
    case error_USER_NOT_FOUND
    case error_PSEUDO_EMPTY
    case error_EMAIL_EMPTY
    case error_EMAIL_NOT_VALID
    case error_BIRTHDAY_EMPTY
    case error_BIRTHDAY_NOT_VALID
    case error_USER_EXIST
    case error_GENDER_EMPTY
}

public enum FilterError:Int {
    case error_FILTER_NOT_FOUND = 0
    case error_FILTER_EMPTY
    case error_NAME_EMPTY
    case error_USER_EMPTY
}

public enum RoomError: Int {
    case error_ROOM_EMPTY = 0
    case error_ROOM_INVALID
    case error_USER_EMPTY
    case error_USER_INVALID
    case error_CONVERSATION_WITH_YOUR_SELF
}

class ErrorAPI {
 
    
    static func checkGenericUserError(_ response:ErrorResponse?) -> Bool {
        
        let result = false
        
//        if let statuRequest:StatuRequest = response, let code = statuRequest.code, let mCode = GenericUserError(rawValue: code) where statuRequest.method == .GENERIC_USER {
//            switch mCode {
//            case .ERROR_USER_BLOCKED:
//                result = true
//                AlertUtil.showTitleWithMessage(message: NSLocalizedString("generic_error_user_bocker", comment: ""))
//            case .ERROR_USER_NOT_FOUND:
//                result = true
//                AlertUtil.showTitleWithMessage(message: NSLocalizedString("generic_error_user_not_found", comment: ""))
//            case .ERROR_PLATFORM_DEVICE:
//                result = true
//                AlertUtil.showTitleWithMessage(message: NSLocalizedString("generic_error_platform", comment: ""))
//            case .ERROR_DEVICE_ID:
//                result = true
//                AlertUtil.showTitleWithMessage(message: NSLocalizedString("generic_error_device_id", comment: ""))
//            }
//        }
        
        return result        
    }
    

    static func checkGenericFilterError(_ response:ErrorResponse?) -> Bool {
        
        var result: Bool = false
        
        guard let type = response?.type, let list = response?.fields, type == ErrorType.changeset && list.count > 0 else {
            return result
        }
        
        
        for item in list {
            
            if item.field == "name_user_id" {
                result = true
                AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_title", comment: ""), message: NSLocalizedString("dialog_filter_message_error_duplica", comment: ""))
            }
            
        }
        
        return result
    }
    

}







