/*
  Localizable.strings
  panda

  Created by Edouard Roussillon on 11/29/15.
  Copyright © 2015 Edouard Roussillon. All rights reserved.
*/

//
//MARK: Dialog
//

"dialog_title" = "Alert";
"dialog_title_error_server" = "Unstable Connection";
"dialog_message_error_server" = "Your internet connection is unstable, try again in a few minutes.";
"dialog_title_alert_gps" = "GPS is disable";
"dialog_message_alert_gps" = "To use the chat, please enable the application's location service.";
"dialog_button_ok" = "OK";
"dialog_button_cancel" = "Cancel";
"dialog_message_reported" = "You have been reported :(. You will be able to reconnect only in %1";
"dialog_message_logout_froced" = "You will be disconnected :(, we can not identify you!";
"dialog_message_logout_close_chat" = "Sorry you cannot communicate with this user!";
"dialog_message_image_too_big" = "Please choice a smaller image.";
"dialog_message_copied" = "The message has been copied";
"dialog_message_rate_limit" = "Rate limits for authentication";
"dialog_message_not_active" = "Your account is not active, please check your email to active your account.";


//
//MARK: Generic
//

"generic_label_email" = "Email";
"generic_label_password" = "Password";
"generic_label_pseudo" = "Pseudo";
"generic_label_birthday" = "Birthday";
"generic_label_male" = "Male";
"generic_label_female" = "Female";
"generic_label_gender" = "Gender";
"generic_label_none" = "None";
"generic_label_description" = "Description";
"generic_label_refresh" = "Pull to refresh";
"generic_label_now" = "now";
"generic_label_retry_button" = "Retrying Now";
"generic_label_upload_image" = "Try again";

"generic_error_empty_field" = "Field is empty or invalid";
"generic_error_user_bocker" = "Sorry you can not login because you have been blocked by the administrator";
"generic_error_user_not_found" = "Please check again your email or password!";
"generic_error_device_id" = "Opss!! missing the Device ID";
"generic_error_platform" = "Opss!! missing the Platform";
"generic_error_filter" = "Opss!! We did not succeed to save your filter, pls try again!";

"generic_gps_loogin_for" = "Waiting signal GPS...";
"generic_room_connection" = "Connection...";
"generic_impossible_connection" = "Impossible to connect!";

//
//MARK: ActionSheet
//

"action_sheet_title" = "Send a picture";
"action_sheet_cancel" = "Cancel";
"action_sheet_camera" = "Camera";
"action_sheet_gallery" = "Gallery";


//
//MARK: Login
//

"login_screen_bt_login" = "Log In";
"login_screen_bt_register" = "Sign Up";
"login_screen_bt_login_on_panda" = "Log In on .Panda";
"login_screen_bt_register_on_panda" = "Sign Up on .Panda";
"login_screen_bt_forgot_pass" = "forgot your password?";

"dialog_reset_password_title" = "forgot your password?";
"dialog_reset_password_message" = "We sent an email to reset your password.";

"dialog_reset_password_success" = "An email have been send";
"dialog_reset_password_fail" = "You need to verify your email!";



//
//MARK: Register
//

"register_screen_title" = "Register";
"register_screen_label_or" = "Or";
"register_screen_bt_create" = "Create";

"register_screen_error_empty_pseudo_max_min" = "Your speudo have to be between 4 to 30 characters";
"register_screen_error_email" = "This email is already used";
"register_screen_error_email_invalid" = "Your email is invalid";
"register_screen_error_password_to_small" = "Your password have to be set with minimum 6 characters";
"register_screen_error_pseudo_invalid" = "Your pseudo must be set with characters, numbers or symbol - _";
"register_screen_error_pseudo" = "This pseudo is already used";
"register_screen_error_birthday" = "Please enter a valid birthday.";

"register_screen_alert_title_birthday" = "Why do I need to provide my birthday?";
"register_screen_alert_message_birthday" = "Providing your birthday helps make sure you get the right .Panda experience for your age.";

"register_screen_error_message_birthday" = "We are not able to process your registration at this time. Please try again later";

//
//MARK: Profile
//

"dialog_profile_message_cancel" = "Your profile will not be save?";

"profile_screen_title" = "Profile";
"profile_label_info" = "Informations";
"profile_label_logout" = "Logout";

//
//MARK: Flying Profile
//

"flying_profile_header_actions" = "Actions";
"flying_profile_header_filter" = "Filters";
"flying_profile_header_details" = "Personnel Info";

"dialog_flying_profile_title_add_friend" = "Add Friend";
"dialog_flying_profile_message_add_friend" = "Send a message to your future friend.";
"dialog_flying_profile__placeholder" = "Do you want to be my friend ?";

//
//MARK: GeneralChat
//

"general_screen_title" = "General";
"welcome_message_general" = "Welcome to %1 general channel.";

//
//MARK: SaleChat
//

"sale_screen_title" = "Trade";
"welcome_message_sale" = "Welcome to %1 trade channel.";


//
//MARK: ChatConnectionView
//

"chat_connection_title" = "Impossible to connecte into the Chat";
"chat_connection_retry_timer" = "Retrying in %1";
"chat_connection_retry_timer_unit" = "sec";

//
//MARK: ChatGPSView
//

"chat_gps_title" = "Enabling your location will let you connect to the chat";

//
//MARK: ChatNotMappedView
//

"chat_not_mapped_title" = "Your current localization do not have any Chat available. Please try again later!";


//
//MARK: Filter
//

"dialog_add_filter_title" = "Add Filter";
"dialog_update_filter_title" = "Update Filter";
"dialog_remove_filter_title" = "Delete Filter";
"dialog_filter_message_want" = "We will highlight any messages into General/Trade chats that match with your filter";
"dialog_filter_message_not_want" = "We will remove any messages into General/Trade chats that match with your filter";
"dialog_filter_message_remove" = "Are you sure to delete this filter ?";
"dialog_filter_message_error_duplica" = "You already added this filter.";

"filter_screen_title" = "Filters";
"filter_tabbar_like_title" = "Like";
"filter_tabbar_not_like_title" = "Not Like";
"filter_add_new_filter_title" = "add new filter";
"filter_header_want" = "Filters you do want to see";
"filter_header_not_want" = "Filters you do NOT want to see";

//
//MARK: Filter Empty State
//

"filter_empty_state_like" = "You do not have any filter saved";
"filter_empty_state_dislile" = "You do not have any filter saved";

//
//MARK: PrivateChat
//

"private_chat_screen_title" = "Private";
"private_chat_notification_title" = "new message from: %1";

//
//MARK: NewChat
//

"new_chat_screen_title" = "Friends";

//
//MARK: Friendship
//
"friendship_notification" = "%1 want to be your friend";
"friendship_screen_title" = "Friendship";
"friend_accepted_tabbar_title" = "Accepted";
"friend_pending_tabbar_title" = "Pending";
"friend_blocked_tabbar_title" = "Users blocked";

"friend_time_online" = "online";
"friend_time_last_seen_today" = "'last seen today at 'HH:mm";
"friend_time_last_seen" = "'last seen 'MMM', 'd' at 'HH:mm";

//
//MARK: Friendship Empty State
//

"friend_empty_state_friend" = "Soon you will have some friends";
"friend_empty_state_pending" = "Nobody is asking you as a friend";
"friend_empty_state_blocked" = "That great you did not block anyone.\nTo block a user click on his name from the general/trade channels and click 'block'";


//
//MARK: Friendship Actions
//

"dialog_block_user_title" = "Block this user";
"dialog_block_user_message" = "Are you sure to block this user?";
"dialog_report_user_title" = "Report this user";
"dialog_report_user_message" = "Are you sure to report this user?";


"friendship_action_refuce_title" = "Refuse";
"friendship_action_cancel_title" = "Cancel Request";
"friendship_action_add_title" = "Add Friend";
"friendship_action_accept_title" = "Accept";
"friendship_action_remove_title" = "Remove";
"friendship_action_block_title" = "Block";
"friendship_action_unblock_title" = "Unblock";
"friendship_action_report_title" = "Report";

//
//MARK: Search
//

"search_cancel_button_nav" = "Cancel";
"search_accepted_title" = "Your Friends";
"search_pending_title" = "Friends Pending";
"search_other_title" = "Future Friends :)...";

//
//MARK: Welcome
//

"welcome_title" = "You can control the content showing into General and Trade channels";
"welcome_swipe_right" = "Swipe on the right to add any words to filter the content (General/Trade)";
"welcome_swipe_left" = "Swipe on the left to see all users you already blocked";
"welcome_button" = "Go it!";
