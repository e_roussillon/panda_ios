//
//  HomeViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class HomeViewController: BasePageViewController, BaseViewControllerSegue {
    
    static let resetToSplash = "resetToSplash"
    static let enableSwiping = "enableSwiping"
    static let disableSwiping = "disableSwiping"
    fileprivate let sessionManager = SessionManager.sharedInstance
    
    var isInit: Bool = false
    
    let page = 1
    @IBOutlet var homePageDelegate: HomePageDelegate!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constants.COLOR.kBackgroundViewController
        
        if sessionManager.user.isLogin {
            self.startInit()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
                
        if sessionManager.user.isLogin && !sessionManager.user.justLogin {
            sessionManager.user.justLogin = false
            if !isInit {
                isInit = true
                self.startInit()
            }
            
//            if !NSUserDefaults.boolWithKey(Constants.KEYS.kIS_EDITING) {
//                self.initPage()
//            }
        }
        else {
//            BaseViewController.logoutPanda(false)
            BaseViewController.launchSplash(false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if sessionManager.user.isLogin && !sessionManager.user.welcomeScreen {
            BaseViewController.launchWelcome()
        }
    }
    
    fileprivate func startInit() {
        self.initViewName()
        self.initDelegate()
        self.initBackgroundColor()
        self.initPage()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initViewName() {
        self.view.accessibilityIdentifier = "HOME_SCREEN"
    }
    
    fileprivate func initBackgroundColor() {
        self.view.backgroundColor = Constants.COLOR.kBackgroundViewController
    }
    
    fileprivate func initDelegate() {
        self.homePageDelegate.baseViewControllerSegue = self;
    }
    
    fileprivate func initPage(){
        self.moveToPosition(self.page, withAnimation:false);
    }
    
    //
    //MARK: MoveToPage
    //
    
    func moveToPosition(_ position:Int, withAnimation:Bool) {
        
        let direction:UIPageViewControllerNavigationDirection = homePageDelegate.current > position ? UIPageViewControllerNavigationDirection.reverse : UIPageViewControllerNavigationDirection.forward;
        
        if let deg = homePageDelegate.baseViewControllerSegue {
            let viewCoontroller = HomePageDelegate.addViewControllerAtIndex(position, baseViewControllerSegue:deg)
            homePageDelegate.current = position
            
            self.setViewControllers([viewCoontroller], direction: direction, animated: withAnimation) { (isFinish) -> Void in}
        }
        
    }
    
    
    //
    //MARK: BaseViewControllerSegue
    //
    
    func launchSegue(_ name: String, values:[Any]?) {
        if (name == HomeViewController.enableSwiping) {
            self.enableSwipeGesture(true)
        } else if (name == HomeViewController.disableSwiping) {
            self.enableSwipeGesture(false)
        }
        
    }
}
