//
//  HomePageDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 11/27/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class HomePageDelegate : BasePageDelegate {
    
    static var arrayViewPager:[UIViewController] = []
    
    static func addViewControllerAtIndex(_ index:Int, baseViewControllerSegue:BaseViewControllerSegue) -> UIViewController
    {
        
        if arrayViewPager.count > index {
            return arrayViewPager[index]
        } else {
            var viewController:UIViewController!
            
            for i in HomePageDelegate.arrayViewPager.count...index {
                
                switch (i){
                case 0:
                    let friendshipViewController = StoryboardUtil.tabBarFriendshipViewController()
                    friendshipViewController.positionViewController = 0;
                    friendshipViewController.baseViewControllerSegue = baseViewControllerSegue
                    let nav = BaseNavigationController(rootViewController: friendshipViewController)
                    nav.setNavigationBarHidden(true, animated: false)
                    viewController = nav
                    HomePageDelegate.arrayViewPager.insert(viewController, at: i)
                    break
                case 1:
                    let tabBarChatViewController: TabBarChatViewController = StoryboardUtil.tabBarChatViewController()
                    tabBarChatViewController.positionViewController = 1;
                    tabBarChatViewController.baseViewControllerSegue = baseViewControllerSegue
                    let nav = BaseNavigationController(rootViewController: tabBarChatViewController)
                    nav.setNavigationBarHidden(true, animated: false)
                    viewController = nav
                    HomePageDelegate.arrayViewPager.insert(viewController!, at: i)
                    break
                case 2:
                    let tabBarFilterViewController = StoryboardUtil.tabBarFilterViewController()
                    tabBarFilterViewController.positionViewController = 2;
                    tabBarFilterViewController.baseViewControllerSegue = baseViewControllerSegue
                    viewController = tabBarFilterViewController
                    HomePageDelegate.arrayViewPager.insert(viewController!, at: i)
                    break
                default:

                    break
                }
            }
            
            return viewController
        }
    }
    
    //
    //MARK: BasePageDelegate
    //
    
    override func viewControllerAtIndex(_ index:Int, baseViewControllerSegue:BaseViewControllerSegue) -> UIViewController
    {
        return HomePageDelegate.addViewControllerAtIndex(index, baseViewControllerSegue: baseViewControllerSegue);
    }
    
    override func maxViewController() -> Int {
        return 3
    }
    
}
