//
//  WebViewController.swift
//  panda
//
//  Created by Edouard Roussillion on 8/12/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation

class WebViewController: BaseViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    var url: URL?
    var isTerms: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isTerms {
            url = URL(string: NSLocalizedString("terms_link", comment: ""))
            self.title = NSLocalizedString("terms_label_to_highlight", comment: "").capitalizingFirstLetter()
            self.navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(named: "dissmis_arrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(WebViewController.clickDismiss)), animated: true)
        }
        
        if let baseUrl = url {
            webView.loadRequest(URLRequest(url: baseUrl))
        }
    }
    
    
    //
    //MARK: Click
    //
    
    @objc internal func clickDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
}
