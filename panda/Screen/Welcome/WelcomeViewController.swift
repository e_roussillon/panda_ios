//
//  WelcomeViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 12/14/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class WelcomeViewController: BaseViewController {
    
    fileprivate let fontBold = UIFont.initHelveticaBold(18)
    fileprivate let fontRegular = UIFont.initHelvetica(18)
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var lableSwipeRight: UILabel!
    @IBOutlet weak var lableSwipeLeft: UILabel!
    @IBOutlet weak var buttonWelcome: BaseButtonLight!
    
    @IBOutlet var constraintBottom: NSLayoutConstraint!
    @IBOutlet var constraintTop: NSLayoutConstraint!
    
    override var screenName: ScreenName? {
        return ScreenName.Welcome
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initSize()
        self.initFont()
        self.initLocalizedString()
    }
    
    //
    // MARK: Init
    //
    
    fileprivate func initSize() {
        if DeviceType.IS_IPHONE_5 {
            self.constraintTop.constant = 70
            self.constraintBottom.constant = 70
        } else if DeviceType.IS_IPHONE_678 {
            self.constraintTop.constant = 90
            self.constraintBottom.constant = 90
        } else if DeviceType.IS_IPHONE_678P {
            self.constraintTop.constant = 120
            self.constraintBottom.constant = 120
        }
        
    }
    
    fileprivate func initFont() {
        self.labelTitle.font = fontBold
        self.lableSwipeRight.font = fontRegular
        self.lableSwipeLeft.font = fontRegular
    }
    
    fileprivate func initLocalizedString() {
        self.labelTitle.text = NSLocalizedString("welcome_title", comment: "")
        self.lableSwipeRight.text = NSLocalizedString("welcome_swipe_right", comment: "")
        self.lableSwipeLeft.text = NSLocalizedString("welcome_swipe_left", comment: "")
        self.buttonWelcome.setTitle(NSLocalizedString("welcome_button", comment: ""), for: UIControlState())
    }
    
    //
    // MARK: Click
    //
    
    @IBAction func clickGoIt(_ sender: AnyObject) {
        self.sessionManager.user.welcomeScreen = true
        self.dismiss(animated: true, completion: nil)
    }
    
}
