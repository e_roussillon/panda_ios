//
//  FriendCell.swift
//  panda
//
//  Created by Edouard Roussillon on 2/6/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import AlamofireImage
import QuartzCore

class FriendCell: UITableViewCell {
    fileprivate let sessionManager = SessionManager.sharedInstance
    static let chatHeight: CGFloat = 50.0
    static let chatHeightBig: CGFloat = 110.0
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonUser: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    @IBOutlet weak var actionsView: UIView!
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var timeLable: UILabel!
    
    @IBOutlet weak var rightButton: BaseButtonDark!
    @IBOutlet weak var leftButton: BaseButtonLight!
    @IBOutlet weak var centerButton: BaseButtonDark!
    
    
    @IBOutlet weak var constraintBottomUserContent: NSLayoutConstraint!
    
    var baseViewControllerSegue: BaseViewControllerSegue!
    var userModel: UserModel?
    
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat {
        guard let statusInt = objects?.first as? Int, let status = FriendshipList(rawValue: statusInt) else {
            return chatHeight
        }
        
        switch status {
        case .pending:
            return chatHeightBig
        default:
            return chatHeight
        }
    }
    
    override func configureCell(_ objects: [Any]?) {
        let placeHolder = UIImage(named: "logo_panda")
        if let user = objects?.first as? UserModel,
            let status = user.friendshipStatus {
            self.userModel = user
            self.labelName.text = user.pseudo ?? ""
            let lastConnection = user.lastSeen ?? Date()
            
            if let imageUrl = user.urlThumb {
                self.buttonUser.af_setImage(for: UIControlState.normal, url: imageUrl, placeholderImage: placeHolder)
            } else {
                self.buttonUser.setImage(placeHolder, for: UIControlState())
            }
            self.buttonUser.layer.cornerRadius = self.buttonUser.frame.width / 2
            self.buttonUser.layer.masksToBounds = true
            
            switch status {
            case FriendshipType.blockedBeforeBeFriend, FriendshipType.blockedAfterBeFriend:
                self.timeLable.text = ""
                self.buttonMessage.isHidden = true
                self.constraintBottomUserContent.constant = 0
                self.actionsView.isHidden = true
                self.actionView.isHidden = true
                break
            case FriendshipType.pending:
                self.timeLable.text = ""
                self.buttonMessage.isHidden = false
                if user.friendshipLastAction == sessionManager.user.userId {
                    self.actionsView.isHidden = true
                    self.actionView.isHidden = false
                } else {
                    self.actionsView.isHidden = false
                    self.actionView.isHidden = true
                }
                break
            default:
                if let isOnline = self.userModel?.isOnline, isOnline {
                    self.timeLable.text = NSLocalizedString("friend_time_online", comment: "")
                } else {
                    self.timeLable.text = lastConnection.getDateUserConnection()
                }
                
                self.actionsView.isHidden = true
                self.constraintBottomUserContent.constant = 0
                self.buttonMessage.isHidden = false
                self.actionView.isHidden = true
            }
            
        } else {
            self.labelName.text = ""
            self.buttonMessage.isHidden = true
        }
        
        if let del = objects?[1] as? BaseViewControllerSegue {
            self.baseViewControllerSegue = del
        }
        
        self.initLocalizedString()
    }
    
    fileprivate func initLocalizedString() {
        self.leftButton.setTitle(NSLocalizedString("friendship_action_accept_title", comment: ""), for: UIControlState())
        self.leftButton.tag = FriendshipType.friend.rawValue
        self.rightButton.setTitle(NSLocalizedString("friendship_action_refuse_title", comment: ""), for: UIControlState())
        self.rightButton.tag = FriendshipType.refusedBeforeBeFriend.rawValue
        self.centerButton.setTitle(NSLocalizedString("friendship_action_cancel_title", comment: ""), for: UIControlState())
        self.centerButton.tag = FriendshipType.cancelRequest.rawValue
    }
    
    //
    //Click
    //
    
    @IBAction func clickMessage(_ sender: AnyObject) {
        if let user = userModel {
            self.baseViewControllerSegue.launchSegue(Constants.SEGUE.OPEN_CONVERSATION, values: [user])
        }
    }

    @IBAction func clickActions(_ sender: AnyObject) {
    
        guard sender is UIButton,
            let user = self.userModel else {
            return
        }
        
        let tag: Int = sender.tag
        
        self.baseViewControllerSegue.launchSegue(Constants.SEGUE.ACTION_FRIENDSHIP, values: [tag, user])
    }
    
}
