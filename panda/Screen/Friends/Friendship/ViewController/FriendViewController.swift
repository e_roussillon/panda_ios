//
//  FriendViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 4/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FriendViewController: BaseFriendshipViewController {
    
    override var screenName: ScreenName? {
        if self.sessionManager.user.isLogin {
            return ScreenName.FriendshipAccepted
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLocalizedString()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bag.add(self.socketNotifierManager.onNewConnection.add(self, handler: FriendViewController.onNewConnection))
        self.bag.add(self.socketNotifierManager.onNewDisconnection.add(self, handler: FriendViewController.onNewDesconnection))
        
        self.initLayout()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLocalizedString() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = NSLocalizedString("friendship_screen_title", comment: "")
    }
    
    fileprivate func initLayout() {
        self.baseFriendshipDelegate.friendshipList = FriendshipList.accepted
        
        self.initList()
    }
    
    
    //
    //MARK: Handler
    //
    
    func onNewConnection(_ users: [UserModel]) {
        self.initList()
    }
    
    func onNewDesconnection(_ user: UserModel) {
        self.initList()
    }
}
