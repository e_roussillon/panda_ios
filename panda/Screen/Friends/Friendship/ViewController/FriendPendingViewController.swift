//
//  FriendPendingViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 4/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FriendPendingViewController: BaseFriendshipViewController {
    
    override var screenName: ScreenName? {
        if self.sessionManager.user.isLogin {
            return ScreenName.FriendshipPending
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLocalizedString()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initLayout()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLocalizedString() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = NSLocalizedString("friend_pending_tabbar_title", comment: "")
    }
    
    fileprivate func initLayout() {
        self.baseFriendshipDelegate.friendshipList = FriendshipList.pending
        
        self.initList()
    }
}
