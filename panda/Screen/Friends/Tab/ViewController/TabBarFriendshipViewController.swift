//
//  TabBarFriendshipViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 4/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class TabBarFriendshipViewController: BaseTabBarViewController {
    
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let sessionManager = SessionManager.sharedInstance
    
    override func loadView() {
        super.loadView()
        
        self.initLocalizedString()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initListener()
        self.getFriendshipCount()
    }
        
    //
    // MARK: Init
    //
    
    fileprivate func initListener() {
        self.bag.add(self.socketNotifierManager.onUpdateFriendship.add(self, handler: TabBarFriendshipViewController.onUpdateFriendship))
    }
    
    fileprivate func initLocalizedString() {
        let itemAccepted: UITabBarItem = self.tabBar.items![0]
        itemAccepted.title = NSLocalizedString("friend_accepted_tabbar_title", comment: "")
        
        let itemPending: UITabBarItem = self.tabBar.items![1]
        itemPending.title = NSLocalizedString("friend_pending_tabbar_title", comment: "")
        
        let itemBlocked: UITabBarItem = self.tabBar.items![2]
        itemBlocked.title = NSLocalizedString("friend_blocked_tabbar_title", comment: "")
    }
    
    //
    //MARK: ListChatsViewController
    //
    
    func onUpdateFriendship(_ user: UserModel) {
        self.getFriendshipCount()
    }
    
    func getFriendshipCount() {
        let count = UserModel.getCountUsersByFriendshipTypePending()
        if count > 0 {
            self.tabBar.items![1].badgeValue = "\(count)"
        } else {
            self.tabBar.items![1].badgeValue = nil
        }
    }
}
