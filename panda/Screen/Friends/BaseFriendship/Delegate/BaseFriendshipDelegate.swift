//
//  BaseFriendshipDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 4/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class BaseFriendshipDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var list:[UserModel] = []
    var baseViewControllerSegue: BaseViewControllerSegue!
    var friendshipList: FriendshipList!
    
    //
    // MARK: UITableViewDataSource
    //
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return FriendCell.initCell(tableView, indexPath: indexPath, objects: [list[indexPath.row], self.baseViewControllerSegue])
    }
    
    //
    // MARK: UITableViewDelegate
    //
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FriendCell.heightWithObjects([friendshipList.rawValue])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.baseViewControllerSegue.launchSegue(Constants.SEGUE.SHOW_PROFILE, values: [list[indexPath.row]])
    }
    
}
