//
//  FriendshipViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 4/21/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Infinity

class BaseFriendshipViewController: BaseViewController {
    
    let socketNotifierManager = SocketNotifierManager.sharedInstance
    
    fileprivate let friendshipManager: FriendshipManager = FriendshipManager.sharedInstance
    let baseInfinitRefreshView = BaseInfinitRefreshView()
    let basePullRefreshView = BasePullRefreshView()
    
    var paginated: Paginated<UserModel>? = nil
    
    var tabBarFriendshipViewController: TabBarFriendshipViewController {
        get {
            return self.tabBarController as! TabBarFriendshipViewController
        }
    }
    
    @IBOutlet var baseFriendshipDelegate: BaseFriendshipDelegate!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bag.add(self.friendshipManager.onUpdateFriendshipSucceed.add(self, handler: BaseFriendshipViewController.onUpdateFriendshipSucceed))
        self.bag.add(self.friendshipManager.onUpdateFriendshipFailed.add(self, handler: BaseFriendshipViewController.onUpdateFriendshipFailed))
        self.bag.add(self.friendshipManager.onUpdateFriendshipFailedUserIdNotFound.add(self, handler: BaseFriendshipViewController.onUpdateFriendshipFailedUserIdNotFound))
        self.bag.add(self.socketNotifierManager.onUpdateFriendship.add(self, handler: BaseFriendshipViewController.onUpdateFriendship))
        
        
//        if paginated != nil {
//            self.hiddenLoader()
//        }
        
//        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        self.initPullRefreshScroll()
//        self.initInfinitRefreshScroll()
    }
        
    deinit {
        self.tableView.fty.pullToRefresh.remove()
        self.tableView.fty.infiniteScroll.remove()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initTableView() {
        FriendCell.registerNibIntoTableView(tableView)
        self.baseFriendshipDelegate.baseViewControllerSegue = self
    }
    
//    private func initPullRefreshScroll() {
//        
//        self.tableView.addPullToRefresh(animator: basePullRefreshView, action: { [weak self] () -> Void in
//            self?.initAPI(true, isPullRefresh: true)
//        })
//    }
//    
//    private func initInfinitRefreshScroll() {
//        
//        self.tableView.addInfiniteScroll(animator: baseInfinitRefreshView, action: { [weak self] () -> Void in
//            self?.initAPI(true, isPullRefresh: false)
//        })
//        self.tableView.infiniteStickToContent = false
//    }
    
//    private func hiddenLoader(){
//        baseInfinitRefreshView.stopAnimating()
//        basePullRefreshView.stopAnimating()
//    }

    fileprivate func showEmptyView() {
        
    }
    
    //
    //MARK: API
    //
    
    func initList() {
        switch self.baseFriendshipDelegate.friendshipList! {
        case FriendshipList.accepted:
            self.baseFriendshipDelegate.list = self.friendshipManager.getUsersByFriendshipTypeFriend()
            
            if self.baseFriendshipDelegate.list.count == 0 {
                self.showEmptyState(UIImage(named: "friend")!, message: NSLocalizedString("friend_empty_state_friend", comment: ""))
            } else {
                self.removeEmptyState()
            }
            break
        case FriendshipList.pending:
            self.baseFriendshipDelegate.list = self.friendshipManager.getUsersByFriendshipTypePending()
            
            if self.baseFriendshipDelegate.list.count == 0 {
                self.showEmptyState(UIImage(named: "pending")!, message: NSLocalizedString("friend_empty_state_pending", comment: ""))
            } else {
                self.removeEmptyState()
            }
            break
        case FriendshipList.blocked:
            self.baseFriendshipDelegate.list = self.friendshipManager.getUsersByFriendshipTypeBlocked()
            
            if self.baseFriendshipDelegate.list.count == 0 {
                self.showEmptyState(UIImage(named: "ic_blocked_big")!, message: NSLocalizedString("friend_empty_state_blocked", comment: ""))
            } else {
                self.removeEmptyState()
            }
            break
        }
        self.tableView.reloadData()
    }
    
//    func initAPI(fromTouchUser: Bool, isPullRefresh: Bool) {
//        
//        guard let page = paginated,
//                let pageNumber = page.pageNumber else {
//            self.startAPI(1, isPullRefresh: isPullRefresh)
//            return
//        }
//        
//        if isPullRefresh {
//            self.startAPI(1, isPullRefresh: isPullRefresh)
//        } else {
//            self.startAPI(pageNumber + 1, isPullRefresh: isPullRefresh)
//        }
//    }
    
    
//    private func startAPI(page: Int, isPullRefresh: Bool) {
//        APIFriendship.frienships(self.baseFriendshipDelegate.friendshipList, page: page, success: { (data) -> Void in
//            self.tableView.endRefreshing()
//            self.tableView.endInfiniteScrolling()
//            
//            guard let paginated = data as? Paginated<UserModel> else {
//                self.baseFriendshipListsDelegate.initFriendshipFromList([], status: self.baseFriendshipDelegate.friendshipList)
//                self.tableView.enableInfiniteScroll = false
//                self.tableView.reloadData()
//                return
//            }
//            
//            self.tableView.enableInfiniteScroll = true
//            self.paginated = paginated
//            
//            if isPullRefresh {
//                self.baseFriendshipListsDelegate.initFriendshipFromList(paginated.data ?? [], status: self.baseFriendshipDelegate.friendshipList)
//            } else {
//                self.baseFriendshipListsDelegate.addFriendshipFromList(paginated.data ?? [], oldStatus: nil)
//            }
//            
//            if self.baseFriendshipListsDelegate.list(self.baseFriendshipDelegate.friendshipList).count == 0 {
//                self.hiddenLoader()
//            }
//            
//            self.tableView.reloadData()
//        }, failure: {(response, isErrorComsumed) -> Bool in
//            self.tableView.endRefreshing()
//            self.tableView.endInfiniteScrolling()
//            
//            return isErrorComsumed
//        })
//    }
    
    fileprivate func updateFriendship(_ user: UserModel, friendshipType: FriendshipType) {
        self.showLoader()
        self.friendshipManager.updateFriendship(user.userId, status: friendshipType)
    }
    
    //
    // MARK: BaseViewControllerSegue
    //
    
    override func launchSegue(_ name: String, values:[Any]?) {
        if name == Constants.SEGUE.SHOW_PROFILE {
            if let user = values?.first as? UserModel {
                UIViewController.launchProfileUser(user)
            }
        } else if name == Constants.SEGUE.OPEN_CONVERSATION {
            if let user = values?.first as? UserModel,
               let status = user.friendshipStatus {
                let lastAction = user.friendshipLastAction ?? -1
                UIViewController.launchChatting(user, friendshipStatus: status, friendshipLastAction: lastAction)
            }
        } else if name == Constants.SEGUE.ACTION_FRIENDSHIP {
            if let tag = values?.first as? Int,
                let user = values?.last as? UserModel,
                let friendshipType = FriendshipType(rawValue: tag), values?.count == 2 {
                updateFriendship(user, friendshipType: friendshipType)
            }
        }
    }
    
    //
    // MARK: Handler
    //
    
    func onUpdateFriendshipSucceed(_ userModel: UserModel) {
        if let tab = self.tabBarController as? TabBarFriendshipViewController {
            tab.getFriendshipCount()
        }
        self.removedLoader()
        self.initList()
        self.tableView.reloadData()
    }
    
    func onUpdateFriendshipFailed(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    func onUpdateFriendshipFailedUserIdNotFound(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    //
    // MARK: Handle
    //
    
    func onUpdateFriendship(_ user: UserModel) {
        self.initList()
    }
    
    //
    // MARK: Click Search
    //
   
    @IBAction func clickSearch(_ sender: AnyObject) {
        UIViewController.launchSearch()
    }
    
    @IBAction func clickSetting(_ sender: AnyObject) {
        UIViewController.launchProfileUser(sessionManager.user.userId, userName: "")
        let param: [String : Any]? = [KeyName.UserId.rawValue: "\(self.sessionManager.user.userId)",
                     KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo]
        
        self.analyticsManager.trackAction(ActionName.ClickProfileUser, params: param)
    }
}
