//
//  RegisterViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation
import TTTAttributedLabel

class RegisterViewController: BaseViewController, UITextFieldDelegate, TTTAttributedLabelDelegate {

    let picker = UIDatePicker()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var imageSplash: UIImageView!
    @IBOutlet weak var textFieldPseudo: BaseTextFieldView!
    @IBOutlet weak var textFieldBirthday: BaseTextFieldView!
    @IBOutlet weak var textFieldEmail: BaseTextFieldView!
    @IBOutlet weak var textFieldPassword: BaseTextFieldView!
    @IBOutlet weak var buttonCreate: BaseButtonDark!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonForgotPassword: UIButton!
    @IBOutlet weak var switcherTerms: UISwitch!
    @IBOutlet weak var labelTerms: TTTAttributedLabel!
    @IBOutlet weak var viewTerms: UIView!

    @IBOutlet weak var animationRegisterDelegate: AnimationRegisterDelegate!

    @IBOutlet weak var constraintHeightLogoSplash: NSLayoutConstraint!
    @IBOutlet weak var constraintHorizontalLogoSplash: NSLayoutConstraint!
    @IBOutlet weak var constraintTopLogo: NSLayoutConstraint!
    @IBOutlet weak var constraintTopFieldEmail: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLogo: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPseudo: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightBirthday: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightEmail: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPassword: NSLayoutConstraint!
    @IBOutlet weak var constraintTopButton: NSLayoutConstraint!
    
    var currentTextField:UITextField?
    var currentBirthday:Date?

    override var screenName: ScreenName? {
        return ScreenName.SignUp
    }

    override func loadView() {
        super.loadView()

        self.initLocalizedString()
        self.initLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.addObservers()
        self.animationRegisterDelegate.animFromLogin()

        self.bag.add(self.sessionManager.onGetSessionSucceed.add(self, handler: RegisterViewController.onGetSessionSucceed))
        self.bag.add(self.sessionManager.onGetSessionFailed.add(self, handler: RegisterViewController.onGetSessionFailed))
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.removeObservers()
    }

    //
    //MARK: Init
    //

    fileprivate func initLocalizedString() {
        self.title = NSLocalizedString("register_screen_title", comment: "")

        self.textFieldPseudo.setPlaceholder(NSLocalizedString("generic_label_pseudo", comment: ""))
        self.textFieldBirthday.setPlaceholder(NSLocalizedString("generic_label_birthday", comment: ""))
        self.textFieldEmail.setPlaceholder(NSLocalizedString("generic_label_email", comment: ""))
        self.textFieldPassword.setPlaceholder(NSLocalizedString("generic_label_password", comment: ""))
        self.buttonForgotPassword.setTitle(NSLocalizedString("login_screen_bt_forgot_pass", comment: ""), for: UIControlState())
        TermsUtil.initTerms(labelTerms: self.labelTerms, delegate: self)
    }

    fileprivate func initLayout() {
        self.automaticallyAdjustsScrollViewInsets = false;
        self.animationRegisterDelegate.initLogo()

        self.textFieldPseudo.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: false, withNext: true)
        self.textFieldPseudo.removeConstraint(constraintHeightPseudo)
        self.textFieldPseudo.setKeyboard(UIKeyboardType.default)
        self.textFieldPseudo.delegate = self
        self.textFieldPseudo.autocorrectionType(UITextAutocorrectionType.no)
        self.textFieldEmail.baseTextField.returnKeyType = .next

        self.textFieldEmail.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: true)
        self.textFieldEmail.removeConstraint(constraintHeightEmail)
        self.textFieldEmail.setKeyboard(UIKeyboardType.emailAddress)
        self.textFieldEmail.delegate = self
        self.textFieldEmail.autocorrectionType(UITextAutocorrectionType.no)
        self.textFieldEmail.baseTextField.returnKeyType = .next

        self.textFieldBirthday.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: true)
        self.textFieldBirthday.removeConstraint(constraintHeightBirthday)
        picker.setDate(Date(), animated: false)
        picker.maximumDate = Date()
        picker.minimumDate = "01/01/1900".toDate() as Date
        picker.datePickerMode = UIDatePickerMode.date
        picker.addTarget(self, action: #selector(RegisterViewController.picketUpdate(_:)), for: UIControlEvents.valueChanged)
        picker.accessibilityIdentifier = "DATE_PICKER"
        self.textFieldBirthday.baseTextField.inputView = picker
        self.textFieldBirthday.delegate = self
        self.textFieldBirthday.updateButtonStyle(ButtonTextFieldType.info)

        self.textFieldPassword.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: false)
        self.textFieldPassword.removeConstraint(constraintHeightPassword)
        self.textFieldPassword.isSecureTextEntry(true)
        self.textFieldPassword.delegate = self
        self.textFieldPassword.baseTextField.returnKeyType = .done
        self.textFieldPassword.updateButtonStyle(ButtonTextFieldType.secure)
    }

    //
    //MARK: UITextFieldDelegate
    //

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.textFieldBirthday.baseTextField {
            self.picketUpdate(picker)
        }

        self.currentTextField = textField
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.currentTextField = nil
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == self.textFieldPseudo.baseTextField {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        } else if textField == self.textFieldEmail.baseTextField {
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }

        return true
    }



    //
    //MARK: Keyboard
    //

    override func keyboardWillShow(_ notif: Notification) {
        super.keyboardWillShow(notif)

        if (self.currentTextField == nil) {
            self.currentTextField = self.textFieldBirthday.baseTextField
        }

        self.animationRegisterDelegate.animKeyboardShow()

        if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
            let keyboardFrame = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardFrame!.size.height, 0)
            let newHeight: CGFloat = 380

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.scrollView.contentInset = contentInsets
                self.scrollView.contentSize = CGSize(width: 0, height: newHeight)
                self.scrollView.scrollIndicatorInsets = contentInsets
            }) 
        }
    }

    override func keyboardWillHide(_ notif: Notification) {
        super.keyboardWillHide(notif)
        self.currentTextField = nil

        self.animationRegisterDelegate.animKeyboardHide()

        if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
            let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, 0, 0)

            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.scrollView.contentInset = contentInsets
                self.scrollView.contentSize = CGSize(width: 0, height: 0)
                self.scrollView.scrollIndicatorInsets = contentInsets

            }) 
        }
    }

    //
    //MARK: ToolBar
    //

    override func clickPres(_ sender: UIBarButtonItem) {
        if self.currentTextField == self.textFieldPassword.baseTextField {
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
        } else if self.currentTextField == self.textFieldEmail.baseTextField {
            self.textFieldPseudo.baseTextField.becomeFirstResponder()
        } else {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        }
    }

    override func clickNext(_ sender: UIBarButtonItem) {

        if self.currentTextField == self.textFieldPseudo.baseTextField {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        } else if self.currentTextField == self.textFieldEmail.baseTextField {
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
            self.currentTextField = self.textFieldBirthday.baseTextField
        } else  {
            self.textFieldPassword.baseTextField.becomeFirstResponder()
        }

    }

    //
    //MARK: UIDatePicker
    //

    @objc internal func picketUpdate(_ sender: UIDatePicker) {
        self.currentTextField = self.textFieldBirthday.baseTextField
        self.currentBirthday = sender.date
        self.textFieldBirthday.baseTextField.text = sender.date.getMediumStyleDate()
    }

    //
    //MARK: Cheking Validation
    //

    fileprivate func isNotMissingInfo() -> Bool {
        var canNotLogin = false

        if (textFieldPseudo.text.length() == 0) {
            canNotLogin = true
            textFieldPseudo.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else if (textFieldPseudo.text.characters.count < Constants.FIELD.kPseudoMin || textFieldPseudo.text.characters.count > Constants.FIELD.kPseudoMax) {
            canNotLogin = true
            textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_empty_pseudo_max_min", comment: ""))
        } else {
            textFieldPseudo.setMessage(nil)
        }

        if (textFieldEmail.text.length() == 0) {
            canNotLogin = true
            textFieldEmail.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else if !textFieldEmail.text.isValidEmail() {
            canNotLogin = true
            textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email_invalid", comment: ""))
        } else {
            textFieldEmail.setMessage(nil)
        }

        if (textFieldBirthday.text.length() == 0) {
            canNotLogin = true
            textFieldBirthday.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        // } else if let date = self.currentBirthday where date.getAge() <= 6 {
        //     textFieldBirthday.setMessage(NSLocalizedString("register_screen_error_birthday", comment: ""))
        } else {
            textFieldBirthday.setMessage(nil)
        }

        if (textFieldPassword.text.length() == 0) {
            canNotLogin = true
            textFieldPassword.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else if (textFieldPassword.text.length() < 6) {
            canNotLogin = true
            self.textFieldPassword.setMessage(NSLocalizedString("register_screen_error_password_to_small", comment: ""))
        } else {
            textFieldPassword.setMessage(nil)
        }
        
        if (!switcherTerms.isOn) {
            canNotLogin = true
            labelTerms.textColor = Constants.COLOR.kTitleError
        } else {
            labelTerms.textColor = Constants.COLOR.kTitlePlaceholder
        }

        return canNotLogin
    }

    //
    //MARK: Click
    //

    @IBAction func clickLogin(_ sender: AnyObject) {

        if !isNotMissingInfo() {
            self.view.endEditing(true)
            self.showLoader()
            self.sessionManager.register(self.textFieldPseudo.text, email: self.textFieldEmail.text, birthday: self.currentBirthday!, password: self.textFieldPassword.text)
            self.analyticsManager.trackAction(ActionName.SendSignUp)
        } else {
            self.animationRegisterDelegate.animLogoError()
        }
    }

    @IBAction func clickBack(_ sender: AnyObject) {
        self.animationRegisterDelegate.animBeforLogin {
            let _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    //
    // MARK: TTTAttributedLabelDelegate
    //
    
    func attributedLabel(_ label: TTTAttributedLabel, didSelectLinkWith url: URL) {
        TermsUtil.openTerms(navigationController: self.navigationController)
    }

    //
    //MARK: Handle
    //

    func onGetSessionSucceed() {
        self.removedLoader()
        self.animationRegisterDelegate.animBeforSplash {
            let _ = self.navigationController?.popToRootViewController(animated: false)
        }
    }

    func onGetSessionFailed(_ response: ErrorResponse?) {
        self.removedLoader()
        self.animationRegisterDelegate.animLogoError()

        if let statuRequest: ErrorResponse = response,
            let fields = statuRequest.fields,
            let type = statuRequest.type, type == .changeset {

            for item in fields {
                guard let name = item.field,
                    let field = RegisterError(rawValue: name),
                    let msg = item.message?.first,
                    let message = FieldError(rawValue: msg) else {
                    continue
                }

                switch field {
                case .ERROR_PSEUDO:
                    switch message {
                    case .ERROR_BLANK:
                        self.textFieldPseudo.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
                        break
                    case .ERROR_USED:
                        self.textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_pseudo", comment: ""))
                        break
                    case .ERROR_MIN_4_CHARACTER, .ERROR_MAX_30_CHARACTER:
                        self.textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_empty_pseudo_max_min", comment: ""))
                        break
                    default:
                        self.textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_pseudo_invalid", comment: ""))
                        break
                    }
                case .ERROR_EMAIL:
                    switch message {
                    case .ERROR_BLANK:
                        self.textFieldEmail.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
                        break
                    case .ERROR_USED:
                        self.textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email", comment: ""))
                        break
                    default:
                        self.textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email_invalid", comment: ""))
                        break
                    }
                case .ERROR_BIRTHDAY:
                    AlertUtil.showTitleWithMessage(message: NSLocalizedString("register_screen_error_message_birthday", comment: ""))
                    break
                case .ERROR_PASSWORD:
                    switch message {
                    case .ERROR_MIN_6_CHARACTER:
                        self.textFieldPassword.setMessage(NSLocalizedString("register_screen_error_password_to_small", comment: ""))
                        break
                    default:
                        self.textFieldPassword.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
                        break
                    }
                }
            }
        }
    }
}
