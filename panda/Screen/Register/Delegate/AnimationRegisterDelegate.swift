//
//  AnimationRegisterDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 10/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class AnimationRegisterDelegate: NSObject {
    
    fileprivate let defaultTopMin: CGFloat = 10.0
    fileprivate let defaultTopMax4s: CGFloat = 15.0
    fileprivate let defaultTopMax: CGFloat = 50.0
    
    @IBOutlet weak var registerViewController: RegisterViewController!
    
    
    //
    // MARK: Animations
    //
    
    func animFromLogin() {
        self.registerViewController.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.registerViewController.constraintTopFieldEmail.constant = 5
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                self.registerViewController.constraintTopLogo.constant = self.defaultTopMax4s
            }
            self.registerViewController.constraintTopButton.constant = 64
            self.initLocalizedStringRegister()
            self.registerViewController.textFieldPseudo.alpha = 0.0
            self.registerViewController.textFieldBirthday.alpha = 0.0
            self.registerViewController.viewTerms.isHidden = true
            self.registerViewController.buttonForgotPassword.alpha = 0.0
            self.registerViewController.viewTerms.isHidden = true
            self.registerViewController.viewTerms.alpha = 0.0
            self.registerViewController.view.layoutIfNeeded()
        }, completion: { (finish) in
            self.registerViewController.viewTerms.isHidden = false
            self.registerViewController.textFieldPseudo.isHidden = false
            self.registerViewController.textFieldBirthday.isHidden = false
            self.registerViewController.viewTerms.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.registerViewController.buttonForgotPassword.isHidden = true
                self.registerViewController.textFieldPseudo.alpha = 1.0
                self.registerViewController.textFieldBirthday.alpha = 1.0
                self.registerViewController.viewTerms.alpha = 1.0
                }, completion: { (finish) in
                    self.registerViewController.imageLogo.zoom(.greatestFiniteMagnitude)
            })
        }) 
    }
    
    func animBeforLogin(_ animDone: @escaping () -> Void) {
        self.registerViewController.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.initLocalizedStringLogin()
            self.registerViewController.imageLogo.layer.removeAllAnimations()
            self.registerViewController.textFieldPseudo.setMessage("")
            self.registerViewController.textFieldBirthday.setMessage("")
            self.registerViewController.textFieldEmail.setMessage("")
            self.registerViewController.textFieldPassword.setMessage("")
            self.registerViewController.textFieldPseudo.alpha = 0.0
            self.registerViewController.textFieldBirthday.alpha = 0.0
            self.registerViewController.buttonForgotPassword.alpha = 0.0
            self.registerViewController.buttonForgotPassword.isHidden = false
            self.registerViewController.viewTerms.alpha = 0.0
            self.registerViewController.view.layoutIfNeeded()
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.registerViewController.constraintTopLogo.constant = self.defaultTopMax
                self.registerViewController.constraintTopFieldEmail.constant = -77
                self.registerViewController.buttonForgotPassword.alpha = 1.0
                self.registerViewController.constraintTopButton.constant = 10
                self.registerViewController.view.layoutIfNeeded()
            }, completion: { (finish) in
                animDone()
            }) 
        }) 
    }
    
    func animBeforSplash(_ done:@escaping () -> Void) {
        let possitionLogoLogin = self.registerViewController.scrollView.frame.origin.y + self.registerViewController.imageLogo.frame.origin.y + (self.registerViewController.constraintHeightLogo.constant / 2)
        let hor = ((ScreenSize.SCREEN_HEIGHT / 2) - possitionLogoLogin) * -1
        self.registerViewController.constraintHorizontalLogoSplash.constant = hor
        self.registerViewController.imageSplash.isHidden = false
        self.registerViewController.imageSplash.alpha = 0.0
        self.registerViewController.view.layoutIfNeeded()
        
        
        UIView.animate(withDuration: 0.1, animations: {
            self.registerViewController.imageSplash.alpha = 1.0
            self.registerViewController.imageLogo.alpha = 0.0
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.registerViewController.scrollView.alpha = 0.0
                self.registerViewController.constraintHorizontalLogoSplash.constant = 0
                self.registerViewController.constraintHeightLogoSplash.constant = 180
                self.registerViewController.view.layoutIfNeeded()
                }, completion: { (finish) in
                    self.registerViewController.imageSplash.zoom(FLT_MAX)
                    done()
            })
        }) 
    }
    
    func initLogo() {
        if (DeviceType.IS_IPHONE_5 == true) {
            self.registerViewController.constraintHeightLogo.constant = 130
        } else if (DeviceType.IS_IPHONE_678 == true) {
            self.registerViewController.constraintHeightLogo.constant = 160
        } else if (DeviceType.IS_IPHONE_678P == true) {
            self.registerViewController.constraintHeightLogo.constant = 180
        } else {
            self.registerViewController.constraintHeightLogo.constant = 120
        }
    }
    
    func initLogoSmall() {
        if (DeviceType.IS_IPHONE_678 == true) {
            self.registerViewController.constraintHeightLogo.constant = 90
        } else if (DeviceType.IS_IPHONE_678P == true) {
            self.registerViewController.constraintHeightLogo.constant = 130
        } else {
            self.registerViewController.constraintHeightLogo.constant = 80
        }
    }
    
    
    func animKeyboardShow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.registerViewController.constraintTopLogo.constant = self.defaultTopMin
            self.registerViewController.buttonBack.alpha = 0.0
            self.initLogoSmall()
            self.registerViewController.view.layoutIfNeeded()
        })
    }
    
    func animKeyboardHide() {
        UIView.animate(withDuration: 0.3, animations: {
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                self.registerViewController.constraintTopLogo.constant = self.defaultTopMax4s
            } else {
                self.registerViewController.constraintTopLogo.constant = self.defaultTopMax
            }
            
            self.registerViewController.buttonBack.alpha = 1.0
            self.initLogo()
            self.registerViewController.view.layoutIfNeeded()
        })
    }
    
    func animLogoError() {
        self.registerViewController.imageLogo.shakeRotation()
    }
    
    
    fileprivate func initLocalizedStringRegister() {
        self.registerViewController.buttonCreate.setTitle(NSLocalizedString("login_screen_bt_register", comment: ""), for: UIControlState())
        self.registerViewController.buttonBack.setTitle(NSLocalizedString("login_screen_bt_login_on_panda", comment: ""), for: UIControlState())
        self.registerViewController.buttonForgotPassword.setTitle(NSLocalizedString("login_screen_bt_forgot_pass", comment: ""), for: UIControlState())
    }
    
    fileprivate func initLocalizedStringLogin() {
        self.registerViewController.buttonCreate.setTitle(NSLocalizedString("login_screen_bt_login", comment: ""), for: UIControlState())
        self.registerViewController.buttonBack.setTitle(NSLocalizedString("login_screen_bt_register_on_panda", comment: ""), for: UIControlState())
        self.registerViewController.buttonForgotPassword.setTitle(NSLocalizedString("login_screen_bt_forgot_pass", comment: ""), for: UIControlState())
    }
    
}
