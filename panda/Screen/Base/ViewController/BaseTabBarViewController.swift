//
//  BaseTabBarViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class BaseTabBarViewController: UITabBarController {
    
    let bag = DisposableBag()
    var positionViewController:Int! = -1
    var baseViewControllerSegue:BaseViewControllerSegue!
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.bag.dispose()
    }
    
}
