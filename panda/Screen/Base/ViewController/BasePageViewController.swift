//
//  BasePageViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/28/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class BasePageViewController: UIPageViewController {
    
    
    func enableSwipeGesture(_ enable:Bool){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = enable
            }
        }
    }
    
}
