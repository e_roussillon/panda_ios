//
//  BaseNavigationController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    override func loadView() {
        super.loadView()
        
        self.loadStyle()
    }
    
    //
    //MARK: Init
    //
    
    func loadStyle() {
        self.styleNavigationToColor(Constants.COLOR.kNavigationBar, buttonColor: Constants.COLOR.kNavigationButton)
        
    }
    
    func loadStyleDarck() {
        self.styleNavigationToColor(Constants.COLOR.kNavigationBarDarck, buttonColor: Constants.COLOR.kNavigationButtonDarck)
    }
    
    fileprivate func styleNavigationToColor(_ color:UIColor, buttonColor:UIColor) {
        self.navigationBar.barStyle = UIBarStyle.default
        self.navigationBar.isTranslucent = false
        self.view.backgroundColor = UIColor.clear
        self.navigationBar.barTintColor = color
        self.navigationBar.tintColor = buttonColor
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : buttonColor]
        
        self.initShadow()
    }
    
    fileprivate func initShadow() {
        self.navigationBar.elevate(of: 1.0)
//        self.navigationBar.layer.shadowOpacity = 0.9;
//        self.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0);
//        self.navigationBar.layer.shadowRadius = 5;
//        self.navigationBar.layer.masksToBounds = false;
//
//        let shadowRect = self.navigationBar.frame.insetBy(dx: 10, dy: 0);  // inset top/bottom
//        self.navigationBar.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath
    }
    
}
