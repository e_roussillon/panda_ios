//
//  BaseViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import RxSwift
import RxCocoa

@objc protocol BaseViewControllerSegue {
    func launchSegue(_ name: String, values:[Any]?)
}

class BaseViewController: UIViewController, BaseViewControllerSegue {
    
    private(set) var disposeBag = DisposeBag()
    let bag = DisposableBag()
    var positionViewController:Int! = -1
    let analyticsManager = AnalyticsManager.sharedInstance
    let sessionManager = SessionManager.sharedInstance
    
    var screenName: ScreenName? {
        return nil
    }
    
    var paramsAnalytics: [String: Any]? {
        return nil
    }
    
    fileprivate var arrayLoader: Array<UIView>?
    fileprivate var emptyState: BaseEmptyState!
    
    override func loadView() {
        super.loadView()
        
        self.loadStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Fix calabash
//        self.setModeCalabash()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.sendScreenEvent()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.bag.dispose()
        self.disposeBag = DisposeBag()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func loadStyle() {
        self.view.backgroundColor = Constants.COLOR.kBackgroundViewController
    }
    
    //
    //MARK: Actions
    //
    
    func showLoader() {
        if (arrayLoader != nil) {
            removedLoader()
        }
        
        arrayLoader = LoadingUtil.showDarkBackgroundWithSpinnerWithColor(self, notShowLoaderIntoWindows: true)
        
    }
    
    func showLoaderClearBackground() {
        if (arrayLoader != nil) {
            removedLoader()
        }
        
        arrayLoader = LoadingUtil.showSpinnerWithColor(self, notShowLoaderIntoWindows: false)
        
    }
    
    func removedLoader(_ anim: Bool = true) {
        if (arrayLoader != nil) {
            LoadingUtil.removeDarkBackgroundWithSpinnerIntoView(arrayLoader, withAnim: anim)
        }
        
        arrayLoader = nil
    }
    
    //
    //MARK: EmptyState
    //
    
    func showEmptyState(_ image: UIImage, message: String) {
        
        if self.emptyState == nil {
           self.emptyState = BaseEmptyState(frame: self.view.frame)
        } else {
            self.removeEmptyState()
        }
        
        self.emptyState.showMessage(image, message: message)
        self.view.addSubview(self.emptyState)
        
        constrain(self.emptyState) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    func removeEmptyState() {
        if self.emptyState != nil {
            self.emptyState.removeFromSuperview()
        }
    }
    
    //
    //MARK: Observer
    //
    
    func addObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeObservers()
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(_ notif:Notification) {
        // Do something here
    }
    
    @objc func keyboardWillHide(_ notif:Notification) {
        // Do something here
    }
    
    //
    //MARK: BaseViewControllerSegue
    //
    
    func launchSegue(_ name: String, values:[Any]?) {
        
    }
    
    //
    // MARK: Analytics
    //
    
    fileprivate func sendScreenEvent() {
        if let screen = screenName {
            self.analyticsManager.trackScreen(screen, params: self.paramsAnalytics)
        }
    }
}
