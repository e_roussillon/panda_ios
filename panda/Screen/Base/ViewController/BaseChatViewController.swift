//
//  BaseChatViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 3/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa
import RxKeyboard

class BaseChatViewController: BaseViewController, ButtonClickDelegate {

    let socketManager: SocketManager = SocketManager.sharedInstance
    let socketNotifierManager = SocketNotifierManager.sharedInstance
    let chatManager = ChatManager.sharedInstance
    let baseTextChatView: BaseTextChatView = BaseTextChatView()
    let baseToolBarView: BaseToolBarView = BaseToolBarView()
    var channelType: ChannelType!
    @IBOutlet weak var tableView: UITableView!

    lazy var tabBarHeight: CGFloat = {
        return self.tabBarController?.tabBar.frame.height ?? 0
    }()

    lazy var baseTextChatViewHeight: CGFloat = {
        return self.self.baseTextChatView.height
    }()

    fileprivate var oldBottomPadding: CGFloat = -1.0

    override func loadView() {
        super.loadView()
        self.baseTextChatView.loadView(viewController: self, constant: -tabBarHeight)
        self.baseToolBarView.loadView(viewController: self, buttonClickDelegate: self, constant: -tabBarHeight)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initBaseLayout()
        self.baseTextChatView.viewDidLoad()
        self.baseToolBarView.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.initNotif()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.removeObservers()
    }

    //
    //MARK: Init
    //

    private func initBaseLayout() {
        self.initObservables()
        self.initChatLayout()
    }

    private func initChatLayout() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tableTappedTable))
//        tap.cancelsTouchesInView = false
//        self.tableView.addGestureRecognizer(tap)

        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(tableLongPressTable))
        longPress.cancelsTouchesInView = false
        self.tableView.addGestureRecognizer(longPress)

        let contentInset = UIEdgeInsetsMake(0, 0, self.baseTextChatViewHeight, 0)
        self.tableView.contentInset = contentInset
        self.tableView.scrollIndicatorInsets = contentInset
        self.tableView.alwaysBounceVertical = true

        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive
    }

//    internal func chatPrivateLayout() {
//        let contentInset = UIEdgeInsetsMake(0, 0, self.baseTextChatViewHeight, 0)
//        self.tableView.contentInset = contentInset
//        self.tableView.scrollIndicatorInsets = contentInset
//        self.tableView.alwaysBounceVertical = true
//
//        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.interactive
//    }

    fileprivate func initNotif() {
        self.bag.add(self.socketNotifierManager.onClearTextNotification.add(self, handler: BaseChatViewController.onClearTextNotification))
    }

    //
    // MARK: Observable
    //

    func initObservables() {
        Observable.combineLatest(self.baseTextChatView.heightObservable,
                        RxKeyboard.instance.visibleHeight.asObservable()) { (height, keyboardVisibleHeight) -> (CGFloat, CGFloat) in
                    return (height, keyboardVisibleHeight)
                }
                .asDriver(onErrorJustReturn: (CGFloat(0), CGFloat(0)))
                .drive(onNext: { [weak self] (height, keyboardHeight) in
                    guard let `self` = self else {
                        return
                    }
                    var keyboardVisibleHeight = keyboardHeight

                    if #available(iOS 11.0, *), DeviceType.IS_IPHONE_X, keyboardVisibleHeight <= self.view.safeAreaInsets.bottom {
                        keyboardVisibleHeight = 0
                    } else if #available(iOS 11.0, *), DeviceType.IS_IPHONE_X {
                        keyboardVisibleHeight = keyboardVisibleHeight - self.view.safeAreaInsets.bottom
                    }

                    self.view.layoutIfNeeded()

                    UIView.animate(withDuration: 0, animations: {
                        self.self.baseTextChatView.updateBottomConstraint(constant: keyboardVisibleHeight)

                        if !DeviceType.IS_IPHONE_X {
                            keyboardVisibleHeight = keyboardVisibleHeight - self.tabBarHeight
                            keyboardVisibleHeight = max(keyboardVisibleHeight, 0)
                        }

                        let contentInset = UIEdgeInsetsMake(0, 0, height + keyboardVisibleHeight, 0)
                        self.tableView.contentInset = contentInset
                        self.tableView.scrollIndicatorInsets = contentInset
                        self.view.layoutIfNeeded()
                    })
                })
                .disposed(by: self.disposeBag)

        RxKeyboard.instance.willShowVisibleHeight
                .asObservable()
                .asDriver(onErrorJustReturn: 0)
                .drive(onNext: { [weak self]  height in
                    guard let `self` = self, self.self.baseTextChatView.isTextFirstResponder() else {
                        return
                    }
                    let contentSize = self.tableView.contentSize.height
                    let bounds = self.tableView.bounds.size.height
                    let heightLeft = max((contentSize - bounds), 0)
                    if (heightLeft > height) {
                        var keyboardVisibleHeight = height
                        if !DeviceType.IS_IPHONE_X {
                            keyboardVisibleHeight = keyboardVisibleHeight - self.tabBarHeight
                            keyboardVisibleHeight = max(keyboardVisibleHeight, 0)
                        }

                        self.tableView.contentOffset = CGPoint(x: 0, y: self.tableView.contentOffset.y + keyboardVisibleHeight)
                    }
                })
                .disposed(by: self.disposeBag)
    }

    //
    //MARK: BaseViewControllerSegue
    //

    override func launchSegue(_ name: String, values: [Any]?) {
        if name == Constants.SEGUE.HIDE_KEYBOARD {
            self.hideKeyboard()
        }
    }

    func hideKeyboard() {
        self.view.endEditing(true)
    }

    //
    // MARK: Tap gesture
    //

    func tableTappedTable(_ tap: UITapGestureRecognizer) {
        if (tap.state == UIGestureRecognizerState.began) {
            let location = tap.location(in: self.tableView)
            if let path = self.tableView.indexPathForRow(at: location) {
                self.handlePress(path)
            }

            self.hideKeyboard()
        }
    }

    func handlePress(_ path: IndexPath) {
    }

    @objc func tableLongPressTable(_ gesture: UILongPressGestureRecognizer) {
        if (gesture.state == UIGestureRecognizerState.began) {
            let location = gesture.location(in: self.tableView)
            if let path = self.tableView.indexPathForRow(at: location) {
                self.handleLongPress(path)
            }

            self.hideKeyboard()
        }
    }

    func handleLongPress(_ path: IndexPath) {
    }

    //
    // MARK: Clear text
    //

    func onClearTextNotification() {
        self.self.baseTextChatView.clearView()
    }

    //
    // MARK: ButtonClickDelegate
    //

    func buttonDeleteClicked() {}

    func buttonCopyClicked() {}

    func buttonReplyClicked() {}

    func buttonForwardClicked() {}
}

