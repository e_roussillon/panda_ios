//
//  BaseCollectionBinder.swift
//  panda
//
//  Created by Edouard Roussillion on 10/5/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class BaseCollectionBinder : NSObject, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    private weak var collection: UICollectionView?
    
    private let listVariable: Variable<[BaseCollectionListBinder]> = Variable([])
    let list: Observable<[BaseCollectionListBinder]>
    
    private let disposeBag = DisposeBag()
    private var dataSource: RxCollectionViewSectionedAnimatedDataSource<BaseCollectionListBinder>?
    private var noAnimation: Bool = false
    
    init(collection collectionView: UICollectionView) {
        self.collection = collectionView
        self.list = listVariable.asObservable()
        super.init()
        
        self.dataSource = RxCollectionViewSectionedAnimatedDataSource<BaseCollectionListBinder>(decideViewTransition: { [weak self] (collectionViewSectionDataSource, collectionView, changesets) in
            guard let `self` = self else { return .reload }
            
            if (self.noAnimation) {
                return .reload
            } else {
                return .animated
            }
            }, configureCell: { collectionViewSectionDataSource, collectionView, indexPath, item in
                return item.initCell(collectionView: collectionView, indexPath: indexPath)
        }, configureSupplementaryView:{ collectionViewSectionDataSource, collectionView, string, indexPath in
            return UICollectionReusableView()
        })
        
        self.initBinder()
    }
    
    private func initBinder() {
        self.collection?.rx.setDelegate(self).disposed(by: disposeBag)
        
        if let collectionView = self.collection, let dataSource = dataSource {
            self.list
                .map({ (list) -> [BaseCollectionListBinder] in
                    list
                })
                .bind(to: collectionView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
        }
    }
    
    public func add(item: BaseCollectionCellViewModel) {
        if (self.listVariable.value.count > 0) {
            self.listVariable.value[0].items = self.listVariable.value[0].items + [item]
        }
    }
    
    public func remove(item: BaseCollectionCellViewModel) {
        if (self.listVariable.value.count > 0) {
            self.listVariable.value[0].items = self.listVariable.value[0].items.filter { $0.identity != item.identity }
        }
    }
    
    public func add(item: BaseCollectionListBinder) {
        let list = self.listVariable.value
        
        if let index = list.index(where: { (section) -> Bool in section.identity == item.identity }), index > -1 {
            self.listVariable.value[index].items = self.listVariable.value[index].items + item.items
        } else {
            self.listVariable.value = list + [item]
        }
    }
    
    public func reload(item: BaseCollectionListBinder, withAnimation: Bool) {
        var list = self.listVariable.value
        
        if let indexSection = list.index(where: { (section) -> Bool in section.identity == item.identity }), indexSection > -1 {
        
            for (index, oldBaseCollectionCellViewModel) in list[indexSection].items.enumerated() {
                
                for newBaseCollectionCellViewModel in item.items {
                    
                    if (oldBaseCollectionCellViewModel.identity == newBaseCollectionCellViewModel.identity) {
                        list[indexSection].items[index] = newBaseCollectionCellViewModel
                    }
                }
            }
        }
        
        self.updateList(newList: list, withAnimation: withAnimation)
    }
    
    public func remove(item: BaseCollectionListBinder) {
//        let list = self.listVariable.value
//        if let index = list.index(where: { (section) -> Bool in section.identity == item.identity }) {
//
//            self.listVariable.value[index].items = self.listVariable.value[index].items.
//
//        }
    }
    
    public func updateList(newList: [BaseCollectionListBinder], withAnimation: Bool) {
        noAnimation = withAnimation
        self.listVariable.value = newList
    }
    
    //
    // MARK: UICollectionViewDelegate
    //
    
    //TODO Added mesthods
    
    //
    // MARK: UICollectionViewFlowLayout
    //
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.listVariable.value[indexPath.section].items[indexPath.row].sizeCell()
    }
    
}
