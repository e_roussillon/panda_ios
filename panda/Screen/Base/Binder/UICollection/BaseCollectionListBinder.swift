//
//  BaseCollectionListBinder.swift
//  panda
//
//  Created by Edouard Roussillion on 10/5/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxDataSources

public func ==(lhs: BaseCollectionHeaderViewModel, rhs: BaseCollectionHeaderViewModel) -> Bool {
    return lhs.identity == rhs.identity
}

open class BaseCollectionHeaderViewModel : NSObject {
    public var identity: String = ""
}

public func ==(lhs: BaseCollectionCellViewModel, rhs: BaseCollectionCellViewModel) -> Bool {
    return lhs.equalTo(rhs: rhs)
}

open class BaseCollectionCellViewModel : IdentifiableType, Equatable {
    public var identity: String = ""
    public typealias Identity = String
    
    func equalTo(rhs: BaseCollectionCellViewModel) -> Bool {
        return self.identity == rhs.identity
    }

    public func initCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }

    public func sizeCell() -> CGSize {
        return CGSize.zero
    }
}

struct BaseCollectionListBinder {
    var header: BaseCollectionHeaderViewModel
    var items: [Item]
}

extension BaseCollectionListBinder: AnimatableSectionModelType {
    typealias Item = BaseCollectionCellViewModel
    
    var identity: String {
        return self.header.identity
    }
    
    init(original: BaseCollectionListBinder, items: [Item]) {
        self = original
        self.items = items
    }
}
