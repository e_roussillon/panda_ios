//
//  BaseTableBinder.swift
//  panda
//
//  Created by Edouard Roussillion on 10/5/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class BaseTableBinder : NSObject, UITableViewDelegate {
    private weak var tableView: UITableView?

    private var heightAtIndexPath = NSMutableDictionary()
    private let listVariable: Variable<[BaseTableListBinder]> = Variable([])
    let list: Observable<[BaseTableListBinder]>

    private let didSelectRowSubject: PublishSubject<(BaseTableHeaderViewModel, BaseTableCellViewModel)> = PublishSubject()
    let didSelectRow: Driver<(BaseTableHeaderViewModel, BaseTableCellViewModel)>

    private let disposeBag = DisposeBag()
    private var dataSource: RxTableViewSectionedAnimatedDataSource<BaseTableListBinder>?
    private var withAnimation: Bool = false
    private let listCellsLoaded = NSMutableDictionary()

    init(tableView: UITableView) {
        self.tableView = tableView
        self.list = listVariable.asObservable()
        self.didSelectRow = didSelectRowSubject.asDriver(onErrorJustReturn: (BaseTableHeaderViewModel(), BaseTableCellViewModel()))
        super.init()
        
        self.dataSource = RxTableViewSectionedAnimatedDataSource<BaseTableListBinder>(decideViewTransition: { [weak self] (collectionViewSectionDataSource, collectionView, changesets) in
                guard let `self` = self else { return .reload }
            
                if (self.withAnimation) {
                    return .reload
                } else {
                    return .animated
                }
            }, configureCell: { tableViewSectionDataSource, tableView, indexPath, item in
                return item.initCell(tableView: tableView, indexPath: indexPath)
            })

        self.initBinder()
    }

    private func initBinder() {
        self.tableView?.delegate = nil
        self.tableView?.rx.setDelegate(self).disposed(by: disposeBag)

        if let tableView = self.tableView, let dataSource = dataSource {
            self.list
                .bind(to: tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
        }
    }

    public func add(item: BaseTableCellViewModel) {
        if (self.listVariable.value.count > 0) {
            self.listVariable.value[0].items = self.listVariable.value[0].items + [item]
        }
    }

    public func removeCellOrSection(item: BaseTableCellViewModel, atSection: BaseTableHeaderViewModel) {
        if let indexSection = self.listVariable.value.index(where:  { $0.header == atSection }) {
            self.listCellsLoaded.removeObject(forKey: item.identity)
            let newListCells = self.listVariable.value[indexSection].items.filter { $0.identity != item.identity }
            if newListCells.count > 0 {
                self.listVariable.value[indexSection].items = newListCells
            } else {
                self.listVariable.value.remove(at: indexSection)
            }
        }
    }

    public func replace(item: BaseTableCellViewModel, atSection: BaseTableHeaderViewModel) {
        if let indexSection = self.listVariable.value.index(where:  { $0.header == atSection }),
            let indexItem = self.listVariable.value[indexSection].items.index(where: { $0.identity == item.identity }) {
            self.listCellsLoaded.removeObject(forKey: item.identity)
            self.listVariable.value[indexSection].items[indexItem] = item
        }
    }

    public func replace(item: BaseTableCellViewModel, atPosition: Int, atSection: Int = 0) {
        if (self.listVariable.value.count > atSection && self.listVariable.value[atSection].items.count > atPosition) {
            self.listCellsLoaded.removeObject(forKey: item.identity)
            self.listVariable.value[atSection].items[atPosition] = item
        }
    }

    public func add(item: BaseTableListBinder) {
        let list = self.listVariable.value

        if let index = list.index(where: { (section) -> Bool in section.identity == item.identity }), index > -1 {
            self.listVariable.value[index].items = self.listVariable.value[index].items + item.items
        } else {
            self.listVariable.value = list + [item]
        }
    }

    public func reload(item: BaseTableListBinder, withAnimation: Bool) {
        var list = self.listVariable.value
        self.listCellsLoaded.removeAllObjects()

        if let indexSection = list.index(where: { (section) -> Bool in section.identity == item.identity }), indexSection > -1 {

            for (index, oldBaseTableCellViewModel) in list[indexSection].items.enumerated() {

                for newBaseTableCellViewModel in item.items {

                    if (oldBaseTableCellViewModel.identity == newBaseTableCellViewModel.identity) {
                        list[indexSection].items[index] = newBaseTableCellViewModel
                    }
                }
            }
        }

        self.updateList(newList: list, withAnimation: withAnimation)
    }

    public func updateList(newList: [BaseTableListBinder], withAnimation: Bool) {
        self.withAnimation = withAnimation
        self.listVariable.value = newList
    }
    
    public func itemAt(indexPath indexPah: IndexPath) -> BaseTableCellViewModel {
        return self.listVariable.value[indexPah.section].items[indexPah.row]
    }

    public func findItem(itemToFind: BaseTableCellViewModel, atSection: Int = 0) -> Int {
        if self.listVariable.value.count > 0 {
            return self.listVariable.value[atSection].items.index(where: {(item) -> Bool in
                item.identity == itemToFind.identity
            }) ?? -1
        }

        return -1
    }

    //
    // MARK: UITableViewDelegate
    //
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = listVariable.value[indexPath.section]
        self.didSelectRowSubject.onNext((section.header, section.items[indexPath.row]))
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        self.heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }

    //
    // INFO: take on this website `https://useyourloaf.com/blog/table-view-cells-with-varying-row-heights/` or github `https://github.com/kharrison/CodeExamples/blob/master/Huckleberry/Huckleberry/UYLTableViewController.m`
    //
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = listVariable.value[indexPath.section]
        let item = section.items[indexPath.row]
        let key = item.identity

        if let height = listCellsLoaded.object(forKey: key) as? CGFloat {
            return height
        } else if let cell = item.initCell(tableView: tableView) {
            cell.bounds = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: cell.bounds.height)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()

            let height = cell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height + 1
            self.listCellsLoaded.setValue(height, forKey: key)
            return height
        } else {
            return 0
        }
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return listVariable.value[section].header.initHeight()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return listVariable.value[section].header.initView()
    }

}
