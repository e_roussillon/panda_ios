//
//  BaseTableListBinder.swift
//  panda
//
//  Created by Edouard Roussillion on 10/5/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxDataSources

public func ==(lhs: BaseTableHeaderViewModel, rhs: BaseTableHeaderViewModel) -> Bool {
    return lhs.identity == rhs.identity
}

open class BaseTableHeaderViewModel : BaseViewModel {
    public var identity: String = ""
    
    public func initView() -> UIView {
        return UIView()
    }
    
    public func initHeight() -> CGFloat {
        return CGFloat(0)
    }
}

public func ==(lhs: BaseTableCellViewModel, rhs: BaseTableCellViewModel) -> Bool {
    return lhs.equalTo(rhs: rhs)
}

open class BaseTableCellViewModel : BaseViewModel, IdentifiableType {
    public var identity: String = ""
    public typealias Identity = String

    func equalTo(rhs: BaseTableCellViewModel) -> Bool {
        return self.identity == rhs.identity
    }

    func cellIdentifier() -> String {
        return String(describing: type(of: self))
    }

    public func initCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        assert(false, "You have to implement it")
        return UITableViewCell()
    }

    public func initCell(tableView: UITableView) -> UITableViewCell? {
        assert(false, "You have to implement it")
        return nil
    }
}

struct BaseTableListBinder {
    var header: BaseTableHeaderViewModel
    var items: [Item]
}

extension BaseTableListBinder: AnimatableSectionModelType {
    typealias Item = BaseTableCellViewModel

    var identity: String {
        return self.header.identity
    }

    init(original: BaseTableListBinder, items: [Item]) {
        self = original
        self.items = items
    }
}
