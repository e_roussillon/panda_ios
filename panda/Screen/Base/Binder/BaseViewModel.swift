//
//  BaseViewModel.swift
//  panda
//
//  Created by Edouard Roussillion on 10/6/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

open class BaseViewModel: NSObject {
    var sessionManager: SessionManager { get { return SessionManager.sharedInstance} }

    let disposeBag = DisposeBag()
    let bag = DisposableBag()
    let socketNotifierManager = SocketNotifierManager.sharedInstance

    
    deinit {
        bag.dispose()
    }
    
    
}
