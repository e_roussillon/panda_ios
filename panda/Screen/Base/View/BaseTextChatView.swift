//
//  BaseTextChatView.swift
//  panda
//
//  Created by Edouard Roussillon on 1/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import Toolbar
import RxCocoa
import RxSwift

class BaseTextChatView: NSObject, UITextViewDelegate {
    static let constantHeightMax: CGFloat = 100
    static let constantHeightMin: CGFloat = 44
    
    private let toolbar: Toolbar = Toolbar()
    private let textView: UITextView!
    
    private var disposeBag = DisposeBag()
    private let isHiddenPictureBehavior: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    var isHiddenPicture: Bool = true {
        didSet {
            isHiddenPictureBehavior.onNext(isHiddenPicture)
        }
    }

    var isHiddenToolbar: Bool = false {
        didSet {
            self.toolbar.isHidden = isHiddenToolbar
        }
    }
    
    var heightObservable: Observable<CGFloat>!
    lazy var height: CGFloat = {
        return self.toolbar.frame.height
    }()
    
    private let sendSubject: PublishSubject<String> = PublishSubject()
    var sendDriver: Driver<String>!
    
    private let pictureSubject: PublishSubject<Void> = PublishSubject()
    var pictureDriver: Driver<Void>!

    private var constraint: NSLayoutConstraint?
    private var toolbarBottomConstraint: NSLayoutConstraint?
    private var defaultBottomConstraint: CGFloat = 0
    
    private lazy var itemSend: ToolbarItem = {
        return ToolbarItem(title: "send", target: self, action: #selector(clickSend))
    }()

    private lazy var itemPicture: ToolbarItem = {
        return ToolbarItem(image: #imageLiteral(resourceName: "ic_gallery"), target: self, action: #selector(clickPicture))
    }()

    private lazy var itemTextView: ToolbarItem = {
        self.textView.delegate = self
        self.textView.font = UIFont.systemFont(ofSize: 14)
        self.textView.backgroundColor = UIColor.white
        self.textView.border(borderColor: Constants.COLOR.kShadowChatTextView.withAlphaComponent(0.2).cgColor, cornerRadius: 8, masksToBounds: true)
        return ToolbarItem(customView: self.textView)
    }()
    
    func isTextFirstResponder() -> Bool {
        return textView.isFirstResponder
    }

	override init() {
        self.textView = UITextView(frame: .zero)
        super.init()
	
        self.sendDriver = self.sendSubject.asDriver(onErrorJustReturn: "")
        self.pictureDriver = self.pictureSubject.asDriver(onErrorJustReturn: Void())
        self.heightObservable = self.toolbar.rx
            .observe(CGRect.self, "bounds")
            .map({ return max($0?.size.height ?? BaseTextChatView.constantHeightMin, BaseTextChatView.constantHeightMin) })
            .distinctUntilChanged()
    
        self.toolbar.backgroundColor = UIColor.clear
	}
    
    func loadView(viewController: BaseViewController, constant: CGFloat = 0) {
        self.defaultBottomConstraint = constant
        self.disposeBag = viewController.disposeBag
        viewController.view.addSubview(toolbar)
        let borders: [UIView] = self.toolbar.addBorders(edges: [.top], color: Constants.COLOR.kBackgroundViewController)
        borders.forEach { (view) in
            view.elevate(of: -1.0)
        }
        self.toolbarBottomConstraint = self.toolbar.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor, constant: self.defaultBottomConstraint)
        self.toolbarBottomConstraint?.isActive = true
        
        self.initObservable()
    }
    
    func viewDidLoad() {
        self.toolbar.maximumHeight = BaseTextChatView.constantHeightMax
        self.toolbar.minimumHeight = BaseTextChatView.constantHeightMin
        self.itemPicture.setHidden(isHiddenPicture, animated: false)
        self.toolbar.setItems([self.itemPicture, self.itemTextView, self.itemSend], animated: false)
    }
    
    func updateBottomConstraint(constant: CGFloat) {
        let newConstant = (constant * -1)
        if (newConstant < self.defaultBottomConstraint) {
            self.toolbarBottomConstraint?.constant = newConstant
        } else {
            self.toolbarBottomConstraint?.constant = self.defaultBottomConstraint
        }
    }
    
	//
	// MARK: Init
	//
    
    private func initObservable() {
        Observable
            .combineLatest(isHiddenPictureBehavior.asObservable(),
                       self.textView.rx.text.asObservable()) { (isHiddenPicture, text) -> Bool in
                        if !isHiddenPicture {
                            return (text?.length() ?? 0) > 0
                        }
                        return isHiddenPicture
            }
            .observeOn(MainScheduler.asyncInstance)
            .bind(onNext: { (isMenuHidden) in
                self.toolbar.layoutIfNeeded()
                self.itemPicture.setHidden(isMenuHidden, animated: true)
                UIView.animate(withDuration: 0.3) {
                    self.toolbar.layoutIfNeeded()
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    func clearView() {
        self.textView.text = ""
        self.textViewDidChange(self.textView)
    }
    
    //
    // MARK: Click
    //
    
    @objc private func clickSend() {
        if let msg = textView.text, !msg.isBlank {
            self.sendSubject.onNext(msg)
        }
    }
    
    @objc private func clickPicture() {
        self.pictureSubject.onNext(Void())
    }
    
	//
	// MARK: UITextViewDelegate
	//
    
    func textViewDidChange(_ textView: UITextView) {
        let size: CGSize = textView.sizeThatFits(textView.bounds.size)
        if let constraint: NSLayoutConstraint = self.constraint {
            textView.removeConstraint(constraint)
        }
        self.constraint = textView.heightAnchor.constraint(equalToConstant: size.height)
        self.constraint?.priority = UILayoutPriority.defaultHigh
        self.constraint?.isActive = true
    }
}
