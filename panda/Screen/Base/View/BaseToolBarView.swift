//
//  BaseToolBarView.swift
//  panda
//
//  Created by Edouard Roussillon on 1/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import Toolbar
import RxCocoa
import RxSwift

class BaseToolBarView: NSObject {
    static let constantHeight: CGFloat = 44

    private let toolbar: Toolbar = Toolbar()
    private var disposeBag = DisposeBag()
    private var buttonClickDelegate: ButtonClickDelegate?
    
    lazy var height: CGFloat = {
        return self.toolbar.frame.height
    }()

    private let isHiddenDeleteBehavior: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var isHiddenDelete: Bool = false {
        didSet {
            isHiddenDeleteBehavior.onNext(isHiddenDelete)
        }
    }
    let buttonDeleteClicked = (() -> Void).self

    private let isHiddenCopyBehavior: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    var isHiddenCopy: Bool = true {
        didSet {
            isHiddenCopyBehavior.onNext(isHiddenCopy)
        }
    }
    let buttonCopyClicked = (() -> Void).self
    
    private let isHiddenReplyBehavior: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    var isHiddenReply: Bool = true {
        didSet {
            isHiddenReplyBehavior.onNext(isHiddenReply)
        }
    }
    let buttonReplyClicked = (() -> Void).self

    private let isHiddenForwardBehavior: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    var isHiddenForward: Bool = true {
        didSet {
            isHiddenForwardBehavior.onNext(isHiddenForward)
        }
    }
    let buttonForwardClicked = (() -> Void).self

    var isHiddenToolbar: Bool = false {
        didSet {
            self.toolbar.isHidden = isHiddenToolbar
        }
    }

    private var constraint: NSLayoutConstraint?
    private var toolbarBottomConstraint: NSLayoutConstraint?
    private var defaultBottomConstraint: CGFloat = 0

    private lazy var itemDelete: ToolbarItem = {
        return ToolbarItem(image: #imageLiteral(resourceName: "ic_delete"), target: self, action: #selector(clickDelete))
    }()

    private lazy var itemCopy: ToolbarItem = {
        return ToolbarItem(image: #imageLiteral(resourceName: "ic_copy"), target: self, action: #selector(clickCopy))
    }()

    private lazy var itemReply: ToolbarItem = {
        return ToolbarItem(image: #imageLiteral(resourceName: "ic_reply"), target: self, action: #selector(clickReply))
    }()

    private lazy var itemForward: ToolbarItem = {
        return ToolbarItem(image: #imageLiteral(resourceName: "ic_forward"), target: self, action: #selector(clickForward))
    }()

	override init() {
        super.init()
    
        self.toolbar.backgroundColor = UIColor.clear
	}
    
    func loadView(viewController: BaseViewController, buttonClickDelegate: ButtonClickDelegate, constant: CGFloat = 0) {
        self.buttonClickDelegate = buttonClickDelegate
        self.disposeBag = viewController.disposeBag
        viewController.view.addSubview(toolbar)
        let borders: [UIView] = self.toolbar.addBorders(edges: [.top], color: Constants.COLOR.kBackgroundViewController)
        borders.forEach { (view) in
            view.elevate(of: -1.0)
        }
        self.toolbarBottomConstraint = self.toolbar.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor, constant: constant)
        self.toolbarBottomConstraint?.isActive = true
        
        self.initObservable()
    }
    
    func viewDidLoad() {
        self.toolbar.maximumHeight = BaseToolBarView.constantHeight
        self.toolbar.minimumHeight = BaseToolBarView.constantHeight
        self.itemDelete.setHidden(isHiddenDelete, animated: false)
        self.itemReply.setHidden(isHiddenReply, animated: false)
        self.itemForward.setHidden(isHiddenForward, animated: false)
        self.toolbar.setItems([self.itemCopy, self.itemReply, self.itemForward, self.itemDelete], animated: false)
    }

    func updateBottomConstraint(constant: CGFloat) {

    }

	//
	// MARK: Init
	//
    
    private func initObservable() {
        Observable
                .combineLatest(
                        self.isHiddenDeleteBehavior.asObservable(),
                        self.isHiddenCopyBehavior.asObservable(),
                        self.isHiddenForwardBehavior.asObservable(),
                        self.isHiddenReplyBehavior.asObservable())
                .flatMapLatest {
                    return Driver.just(($0, $1, $2, $3))
                }
                .observeOn(MainScheduler.asyncInstance)
                .bind(onNext: { [weak self] (isHiddenDelete, isHiddenCopy, isHiddenForward, isHiddenReply) in
                    guard let `self` = self else { return }

                    self.toolbar.layoutIfNeeded()
                    self.itemDelete.setHidden(isHiddenDelete, animated: false)
                    self.itemCopy.setHidden(isHiddenCopy, animated: false)
                    self.itemForward.setHidden(isHiddenForward, animated: false)
                    self.itemReply.setHidden(isHiddenReply, animated: false)
                    UIView.animate(withDuration: 0.3) {
                        self.toolbar.layoutIfNeeded()
                    }
                })
                .disposed(by: self.disposeBag)
    }
    
    //
    // MARK: Click
    //
    
    @objc private func clickDelete() {
        self.buttonClickDelegate?.buttonDeleteClicked()
    }

    @objc private func clickCopy() {
        self.buttonClickDelegate?.buttonCopyClicked()
    }

    @objc private func clickReply() {
        self.buttonClickDelegate?.buttonReplyClicked()
    }

    @objc private func clickForward() {
        self.buttonClickDelegate?.buttonForwardClicked()
    }

}

protocol ButtonClickDelegate {
    func buttonDeleteClicked()
    func buttonCopyClicked()
    func buttonReplyClicked()
    func buttonForwardClicked()
}
