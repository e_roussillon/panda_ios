//
//  BaseButtonTextField.swift
//  panda
//
//  Created by Edouard Roussillon on 10/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

public enum ButtonTextFieldType: Int {
    case none = 0
    case secure
    case info
}

open class BaseButtonTextField: UIButton {
    open var buttonTextFieldType: ButtonTextFieldType = ButtonTextFieldType.none
    
    fileprivate var imageNoneName = "ic_clear"
    fileprivate var imageNoneSelectedName = "ic_clear"
    
    fileprivate var imageSecureName = "ic_eye_close"
    fileprivate var imageSecureSelectedName = "ic_eye"
    
    fileprivate var imageInfoName = "ic_info"
    fileprivate var imageInfoSelectedName = "ic_info"
    
    open fileprivate(set) var isSelectedTerms: Bool = false {
        didSet {
            self.forceToUpdate(nil)
        }
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.isSelectedTerms = false
    }
    
    
    //
    // MARK : Public
    //
    
    open func clickButton(_ baseTextFieldView: UITextField) {
        self.isSelectedTerms = !self.isSelectedTerms
        
        switch buttonTextFieldType {
        case .none:
            self.clickClear(baseTextFieldView)
            break
        case .secure:
            self.clickSecure(baseTextFieldView)
            break
        case .info:
            self.clickInfo(baseTextFieldView)
            break
        }
    }
    
    open func refreshButton(_ baseTextField: UITextField?, currentText: String) {
        switch buttonTextFieldType {
        case .none:
            if currentText.length() > 0 {
                self.isHidden = false
            } else {
                self.isHidden = true
            }
            break
        default:
            self.isHidden = false
            break
        }
        
        self.forceToUpdate(baseTextField)
    }
    
    //
    // MARK: Private
    //
    
    fileprivate func forceToUpdate(_ baseTextField: UITextField?) {
        switch buttonTextFieldType {
        case .none:
            self.setImage(self.imageNoneName, imageSelected: self.imageNoneSelectedName)
            break
        case .secure:
            self.setImage(self.imageSecureName, imageSelected: self.imageSecureSelectedName)
            break
        case .info:
            self.setImage(self.imageInfoName, imageSelected: self.imageInfoSelectedName)
            break
        }
    }
    
    fileprivate func setImage(_ imageNotSelected: String, imageSelected: String) {
        if isSelectedTerms {
            self.setImage(UIImage(named: imageNotSelected), for: UIControlState())
            self.setImage(UIImage(named: imageNotSelected), for: UIControlState.disabled)
        } else {
            self.setImage(UIImage(named: imageSelected), for: UIControlState())
            self.setImage(UIImage(named: imageSelected), for: UIControlState.disabled)
        }
    }
    
    
    //
    // MARK: Click
    //
    
    fileprivate func clickSecure(_ baseTextFieldView: UITextField) {
        baseTextFieldView.isSecureTextEntry = !self.isSelectedTerms
        
        // We have to apply our font to the text field again, via attributedText.
        let text = baseTextFieldView.text ?? ""
        baseTextFieldView.attributedText = NSAttributedString(string: text)
    }
    
    fileprivate func clickClear(_ baseTextFieldView : UITextField) {
        baseTextFieldView.text = ""
        self.isHidden = true
    }
    
    fileprivate func clickInfo(_ baseTextFieldView: UITextField) {  
        AlertUtil.showTitleWithMessage(NSLocalizedString("register_screen_alert_title_birthday", comment: ""), message: NSLocalizedString("register_screen_alert_message_birthday", comment: ""))
    }
}
