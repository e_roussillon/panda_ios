//
//  BaseTextViewWithPlaceholder.swift
//  Corretora
//
//  Created by Edouard Roussillon on 8/25/15.
//  Copyright (c) 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography

class BaseTextViewWithPlaceholder: UIView, UITextViewDelegate {
    
    var delegate:UITextViewDelegate?
    let defaultFont14 = UIFont.initHelveticaLight(16)
    let defaultFont12 = UIFont.initHelveticaLight(11)
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var baseTextField: BaseTextView!
    @IBOutlet weak var holder: UILabel!
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var constraintCenterView: NSLayoutConstraint!

    
    let animationDuration = 0.3
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseTextViewWithPlaceholder", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        self.moveToCenter(false)
        self.initLayout()
        self.addShadow()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if (textView.text.isEmpty == false) {
            self.moveToTop(true)
        } else {
            self.moveToCenter(true)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return self.delegate!.textView!(textView, shouldChangeTextIn:range, replacementText:text)
    }
    
    //
    //MARK: Shadow
    //
    
    fileprivate func addShadow() {
        self.viewLine.layer.shadowOffset = CGSize(width: 0.0, height: 2);
        self.viewLine.layer.shadowRadius = 3
        self.viewLine.layer.shadowOpacity = 0.4;
        self.viewLine.layer.shouldRasterize = true
    }
    
    //
    //MARK: Access TextField
    //
    
    var text:String {
        get {
            return baseTextField.text!
        }
        set(newCenter) {
            baseTextField.text! = newCenter
            self.moveToTop(true)
        }
    }
    
    func setPlaceholder(_ text:String) {
        self.holder.text = text
    }
    
    func setKeyboard(_ type:UIKeyboardType) {
        self.baseTextField.keyboardType = type
    }
    
    func isSecureTextEntry(_ isSecure:Bool) {
        self.baseTextField.isSecureTextEntry = isSecure
    }
    
    func autocorrectionType(_ type:UITextAutocorrectionType) {
        self.baseTextField.autocorrectionType = type
    }
    
    //
    //MARK: Enable
    //
    
    func enable(_ show:Bool) {
        self.baseTextField.isEditable = show
        self.viewLine.isHidden = !show
    }

    //
    //MARK: animation
    //
    
    fileprivate func moveToTop(_ animated:Bool) {
        self.layoutIfNeeded()
        self.constraintCenterView.constant = 0
        self.holder.textColor = Constants.COLOR.kTitlePlaceholder
        self.holder.font = defaultFont12
        if (animated == true) {
            UIView.animate(withDuration: 0.3, delay:0, options: [.beginFromCurrentState, .curveEaseOut], animations:{
                self.layoutIfNeeded()
            }, completion:nil)
        }
    }
    
    fileprivate func moveToCenter(_ animated:Bool) {
        self.layoutIfNeeded()
        self.constraintCenterView.constant = (self.frame.size.height/2) - (self.holder.frame.size.height/2)
        self.holder.textColor = Constants.COLOR.kTitlePlaceholder
        self.holder.font = defaultFont14
        
        if (animated == true) {
            UIView.animate(withDuration: 0.3, delay:0, options: [.beginFromCurrentState, .curveEaseIn], animations:{
                self.layoutIfNeeded()
                }, completion:nil)
        }
    }
    
}
