//
//  BaseButton.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class BaseButtonLight: UIButton {
    
    let defaultColor = Constants.COLOR.kTitleButtonLight
    let defaultFont = UIFont.initHelvetica(17)
    
    @IBOutlet var rootView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initBackground()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initBackground()
    }
    
    //
    //MARK : Init
    //
    
    func initBackground() {
        
        self.titleLabel?.font = defaultFont
        self.setTitleColor(defaultColor, for: UIControlState())
        self.setTitleColor(defaultColor, for: UIControlState.selected)
        self.setTitleColor(defaultColor, for: UIControlState.highlighted)
        
        self.setBackgroundImage(UIImage(named: "button_light"), for: UIControlState())
        self.setBackgroundImage(UIImage(named: "button_light"), for: UIControlState.selected)
        self.setBackgroundImage(UIImage(named: "button_light"), for: UIControlState.highlighted)
    }
    
    //
    //MARK : Click
    //
    
    override var isHighlighted: Bool {
        didSet { if isHighlighted { isHighlighted = false } }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.contentEdgeInsets = UIEdgeInsetsMake(-2.0,0.0,0.0,0.0);
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5);
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.2;
        
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.contentEdgeInsets = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
        self.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0;
        
        super.touchesEnded(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.contentEdgeInsets = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
        self.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0;
        
        super.touchesCancelled(touches, with: event)
    }
    
    
}
