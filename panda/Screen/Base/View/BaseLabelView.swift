//
//  BaseLabelView.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography

class BaseLabelView: UIView {
    
    static let height: CGFloat = 27.0
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewLine: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseLabelView", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    required init() {
        super.init(frame: CGRect.zero)
        
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseLabelView", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.backgroundColor = Constants.COLOR.kBackgroundViewController
        self.initLayout2()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    fileprivate func initLayout2() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
            view.height == 27
        }
    }

    //
    //MARK: Access Label
    //
    
    var text:String {
        get {
            return labelTitle.text!
        }
        set(newCenter) {
            labelTitle.text! = newCenter
        }
    }
    
}
