//
//  BaseSimpleEmptyStateView.swift
//  panda
//
//  Created by Edouard Roussillon on 3/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Cartography


class BaseSimpleEmptyStateView: UIView {
    @IBOutlet var rootView: UIView!
    
    @IBOutlet weak var smallContentTransparentView: UIView!
    @IBOutlet weak var smallContentView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var button: BaseButtonDark!
    @IBOutlet weak var labelSubtitle: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseSimpleEmptyStateView", owner: self, options: nil)
        
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    //
    // MARK: Init
    //
    
    fileprivate func initLayout() {
        self.initLocalizedString()

        self.smallContentTransparentView.layer.cornerRadius = 5
        self.button.isEnabled = true
        
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    //
    // MARK: Click
    //

    @IBAction func clickButton(_ sender: AnyObject) {
        self.click()
    }
    
    
    //
    // MARK: To Override
    //
    
    func initLocalizedString() {}
    func click() {}
}
