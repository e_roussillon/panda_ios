//
//  BaseEmptyState.swift
//  panda
//
//  Created by Edouard Roussillon on 11/11/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Cartography

class BaseEmptyState: UIView {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelMessage: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseEmptyState", owner: self, options: nil)
        
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseEmptyState", owner: self, options: nil)
        
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    //
    // MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    //
    // MARK: Public Method
    //
    
    func showMessage(_ image: UIImage, message: String) {
        self.imageLogo.image = image
        self.labelMessage.text = message
    }
    
}
