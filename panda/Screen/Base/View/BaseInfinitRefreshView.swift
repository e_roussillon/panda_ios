//
//  BaseInfinitRefreshView.swift
//  panda
//
//  Created by Edouard Roussillon on 2016-04-24.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import Infinity

class BaseInfinitRefreshView: UIView, CustomInfiniteScrollAnimator {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseInfinitRefreshView", owner: self, options: nil)
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    required init() {
        super.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 30))
        
        Bundle.main.loadNibNamed("BaseInfinitRefreshView", owner: self, options: nil)
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        self.backgroundColor = Constants.COLOR.kBackgroundViewController
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    //
    // MARK: CustomInfiniteScrollAnimator
    //

    func animateState(_ state: InfiniteScrollState) {
        switch state {
        case .none:
            stopAnimating()
//        case .Releasing(let progress):
//            print("progress -> \(progress)")
//            break
        case .loading:
            startAnimating()
        }
    }
    
    //
    // MARK: Animation
    //
    
    func startAnimating() {
        indicatorView.isHidden = false
        indicatorView.startAnimating()
    }
    
    func stopAnimating() {
        indicatorView.stopAnimating()
        indicatorView.isHidden = true
    }
    
}
