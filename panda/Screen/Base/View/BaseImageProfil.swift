//
//  BaseImageProfil.swift
//  panda
//
//  Created by Edouard Roussillon on 1/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import AlamofireImage
import QuartzCore

class BaseImageProfil: UIView, GalleryDelegate {
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    var baseGalleryDelegate: BaseGalleryDelegate!
    var imagePicker = UIImagePickerController()
    var isNewImage: Bool = false

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseImageProfil", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
        self.setImage(nil)
        self.editImage(false)
        self.isNewImage = false
    }
    
    //
    //MARK: Setup
    //
    
    func editImage(_ edit: Bool) {
        self.editButton.isHidden = !edit
    }
    
    func setImage(_ image: URL?) {
        self.isNewImage = false
        let placeHolder = UIImage(named: "logo_panda")
        
        if let newImage: URL = image {
            self.imageView.image = placeHolder
            self.imageView.af_setImage(withURL: newImage,
                                              placeholderImage: placeHolder,
                                              imageTransition: UIImageView.ImageTransition.crossDissolve(0.2), runImageTransitionIfCached: true)
        } else {
            self.imageView.image = placeHolder
        }
            
        self.layoutIfNeeded()
        self.imageView.layer.cornerRadius = self.imageView.frame.width / 2
        self.imageView.layer.masksToBounds = true
    }
    
    //
    //MARK: UIImagePickerControllerDelegate
    //
    
    func sendPicture(_ image: UIImage) {
        self.imageView.image = image
        self.imageView.layer.cornerRadius = self.imageView.frame.width / 2
        self.imageView.layer.masksToBounds = true
        self.isNewImage = true
    }
    
    func pictureToBig() {
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_image_too_big", comment: ""))
    }
    
    func cancelSelectPicture() {
        
    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickEdit(_ sender: AnyObject) {
        self.baseGalleryDelegate.galleryDelegate = self
        self.baseGalleryDelegate.showActionSheet()
        
        let sessionManager = SessionManager.sharedInstance
        
        let param: [String : Any] = [KeyName.UserId.rawValue: "\(sessionManager.user.userId)",
                     KeyName.UserPseudo.rawValue: sessionManager.user.userPseudo]
        AnalyticsManager.sharedInstance.trackAction(ActionName.ClickEditPictureProfile, params: param)
    }

}
