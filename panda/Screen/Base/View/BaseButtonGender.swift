//
//  BaseImageGender.swift
//  panda
//
//  Created by Edouard Roussillon on 12/5/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class BaseButtonGender: UIButton {
    
    var isGenderSelected = false {
        didSet {
            self.checkSelection(isGenderSelected)
        }
    }
    
    var isErrorShowed = false
        {
        didSet {
            if isErrorShowed == true {
                self.showError()
            }
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //
    //MARK: Generic
    //
    
    func checkSelection(_ isSelected:Bool) {
        
        if (isSelected == true) {
            self.select()
        } else {
            self.unselect()
        }
        
    }
    
    fileprivate func select() {
        self.isErrorShowed = false
        
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.borderColor = Constants.COLOR.kBorderSelected.cgColor
        self.layer.borderWidth = 3
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5);
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.27;
    }
    
    fileprivate func unselect() {
        self.isErrorShowed = false
        
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0
        self.layer.shadowOffset = CGSize(width: 0, height: 0);
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 0;
    }
    
    fileprivate func showError() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.borderColor = Constants.COLOR.kBorderError.cgColor
        self.layer.borderWidth = 3
        self.layer.shadowOffset = CGSize(width: 0.0, height: 5);
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.27;
    }
    
}
