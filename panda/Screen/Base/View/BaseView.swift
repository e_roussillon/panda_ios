//
//  BaseView.swift
//  panda
//
//  Created by Edouard Roussillion on 10/27/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class BaseView: UIView {
    let disposeBag = DisposeBag()
}
