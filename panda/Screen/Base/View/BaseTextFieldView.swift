//
//  BaseTextFieldView.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import QuartzCore
import Cartography
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class BaseTextFieldView: UIView, UITextFieldDelegate {
    
    var delegate:UITextFieldDelegate?
    let defaultColorError = Constants.COLOR.kNavigationBar
    
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var baseTextField: BaseTextField!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var buttonTextField: BaseButtonTextField!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("BaseTextFieldView", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.initLayout()
        self.addShadow()
        self.initClick()
        self.initField()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
    }
    
    fileprivate func initField() {
        self.resetMessage(false)
        self.baseTextField.delegate = self
        self.tintColor = UIColor.white
    }
    
    fileprivate func initClick() {
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BaseTextFieldView.clickView)))
    }
    
    fileprivate func refreshButton(_ currentText: String) {
        self.buttonTextField.refreshButton(self.baseTextField, currentText: currentText)
    }
    
    func updateButtonStyle( _ newStyle: ButtonTextFieldType ) {
        self.buttonTextField.buttonTextFieldType = newStyle
        self.refreshButton(self.baseTextField.text ?? "")
    }
    
    //
    //MARK: UITextFieldDelegate
    //
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.setMessage("")
        
        if let del:UITextFieldDelegate = self.delegate {
           del.textFieldDidBeginEditing?(textField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let del:UITextFieldDelegate = self.delegate {
            del.textFieldDidEndEditing?(textField)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let del:UITextFieldDelegate = self.delegate {
            return del.textFieldShouldBeginEditing?(textField) ?? true
        } else {
            return true
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let del:UITextFieldDelegate = self.delegate {
            return del.textFieldShouldClear?(textField) ?? true
        } else {
            return true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let del:UITextFieldDelegate = self.delegate {
            return del.textFieldShouldEndEditing?(textField) ?? true
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let del:UITextFieldDelegate = self.delegate {
            return del.textFieldShouldReturn?(textField) ?? true
        } else {
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.setMessage("")
        
        if string != "\n" {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
            self.refreshButton(newString as String)
        }
        
        if let del:UITextFieldDelegate = self.delegate {
            return del.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
        } else {
            return true
        }
    }
    
    //
    //MARK: Shadow
    //
    
    fileprivate func addShadow() {
        self.viewLine.layer.shadowOffset = CGSize(width: 0.0, height: 2);
        self.viewLine.layer.shadowRadius = 3
        self.viewLine.layer.shadowOpacity = 0.4;
        self.viewLine.layer.shouldRasterize = true
    }
    
    //
    //MARK: Access TextField
    //
    
    var text:String {
        get {
            return baseTextField.text!
        }
        set(newCenter) {
            baseTextField.text! = newCenter
        }
    }
    
    func setPlaceholder(_ text:String) {
        self.baseTextField.placeholder = text
    }
    
    func setKeyboard(_ type:UIKeyboardType) {
        self.baseTextField.keyboardType = type
    }
    
    func isSecureTextEntry(_ isSecure:Bool) {
        self.baseTextField.isSecureTextEntry = isSecure
    }
    
    func autocorrectionType(_ type:UITextAutocorrectionType) {
        self.baseTextField.autocorrectionType = type
    }
    
    
    //
    //MARK: Alert Message
    //
    fileprivate func resetMessage(_ withAnim:Bool) {
        self.viewLine.backgroundColor = Constants.COLOR.kViewLine
        self.labelError.text = ""
        
        if (withAnim == true) {
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            }) 
        }
    
    }
    
    fileprivate func showMessage(_ msg:String) {
        self.viewLine.backgroundColor = Constants.COLOR.kTitleError
        self.labelError.text = msg
        
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }) 
    }
    
    func setMessage(_ msg:String?) {
        if let message = msg {
            if message.isEmpty {
                if self.labelError.text?.length() > 0 {
                    self.resetMessage(true)
                }
            } else {
                self.showMessage(message)
            }
        } else {
            self.resetMessage(true)
        }
    }
    
    //
    //MARK: Click
    //
    
    @objc internal func clickView() {
        self.baseTextField.becomeFirstResponder()
    }
    
    @IBAction func clickButton(_ sender: AnyObject) {
        self.buttonTextField.clickButton(self.baseTextField)
    }
    
    
    //
    //MARK: Enable
    //
    
    func enable(_ show:Bool) {
        self.baseTextField.isEnabled = show
        self.viewLine.isHidden = !show
    }
    
}
