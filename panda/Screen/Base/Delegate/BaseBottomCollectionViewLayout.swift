//
//  BaseBottomCollectionViewLayout.swift
//  panda
//
//  Created by Edouard Roussillon on 11/5/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit

private var heightAtIndexPathKey: UInt8 = 0

extension UICollectionViewFlowLayout {
    
    func reloadVisibleCells() {
        super.invalidateLayout()
//        if let list: [NSIndexPath] = self.collectionView!.indexPathsForVisibleItems() where list.count > 0 {
//            // Do we need to stick cells to the bottom or not
//            var shiftDownNeeded = false
//            
//            // Size of all cells without modifications
//            let allContentSize = super.collectionViewContentSize()
//            
//            // If there are not enough cells to fill collection view vertically we shift them down
//            let diff = self.collectionView!.bounds.size.height - allContentSize.height - self.collectionView!.contentInset.bottom
//            if Double(diff) > DBL_EPSILON {
//                shiftDownNeeded = true
//            }
//            
//            if shiftDownNeeded {
//                self.collectionView!.reloadItemsAtIndexPaths(list)
//            }
//        }
    }
    
}



class BaseBottomCollectionViewLayout: UICollectionViewFlowLayout {
        
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        // Do we need to stick cells to the bottom or not
        var shiftDownNeeded = false
        
        // Size of all cells without modifications
        let allContentSize = super.collectionViewContentSize
        
        // If there are not enough cells to fill collection view vertically we shift them down
        let diff = self.collectionView!.bounds.size.height - allContentSize.height - self.collectionView!.contentInset.bottom
        if Double(diff) > DBL_EPSILON {
            shiftDownNeeded = true
        }
        
        // Ask for common attributes
        let attributes = super.layoutAttributesForElements(in: rect)
        
        if let attributes = attributes {
            if shiftDownNeeded {
                for element in attributes {
                    let frame = element.frame;
                    // shift all the cells down by the difference of heights
                    element.frame = frame.offsetBy(dx: 0, dy: diff);
                }
            }
        }

        return attributes;
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attribute = super.layoutAttributesForItem(at: indexPath)
        
        var shiftDownNeeded = false
        
        // Size of all cells without modifications
        let allContentSize = super.collectionViewContentSize
        
        // If there are not enough cells to fill collection view vertically we shift them down
        let diff = self.collectionView!.bounds.size.height - allContentSize.height - self.collectionView!.contentInset.bottom
        if Double(diff) > DBL_EPSILON {
            shiftDownNeeded = true
        }
        
        if shiftDownNeeded {
            let frame = attribute!.frame
            // shift all the cells down by the difference of heights
            attribute!.frame = frame.offsetBy(dx: 0, dy: diff);
        }
        
        return attribute
    }
}
