//
//  BaseGalleryDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 10/4/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import UIKit

protocol GalleryDelegate {
    func sendPicture(_ image: UIImage)
    func pictureToBig()
    func cancelSelectPicture()
}

class BaseGalleryDelegate: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var baseViewController: BaseViewController!
    
    var galleryDelegate: GalleryDelegate?
    fileprivate let imagePicker = UIImagePickerController()
    fileprivate var actionSheet: UIAlertController!
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let analyticsManager = AnalyticsManager.sharedInstance
    
    func showActionSheet() {
        actionSheet = UIAlertController(title: NSLocalizedString("action_sheet_title", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("action_sheet_cancel", comment: ""), style: .cancel) { (action) in
            self.cancelAction()
        })
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("action_sheet_camera", comment: ""), style: .default) { (action) in
            self.cameraAction()
        })
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("action_sheet_gallery", comment: ""), style: .default) { (action) in
            self.galleryAction()
        })
        self.baseViewController.present(self.actionSheet, animated: true, completion:nil)
    }
    
    
    //
    // MARK: Actions
    //

    fileprivate func cancelAction() {
        if baseViewController is ProfileViewController {
            self.sendAnalytics(ActionName.ClickCancelSelectPictureProfile)
        } else {
            self.sendAnalytics(ActionName.ClickCancelSelectPicturePrivateMessage)
        }
    }
    
    fileprivate func cameraAction() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = true
            self.baseViewController.present(imagePicker, animated: true, completion:nil)
            
            if baseViewController is ProfileViewController {
                self.sendAnalytics(ActionName.ClickCameraProfile)
            } else {
                self.sendAnalytics(ActionName.ClickCameraPrivateMessage)
            }
        }
    }
    
    fileprivate func galleryAction() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = true
            self.baseViewController.present(imagePicker, animated: true, completion:nil)
            
            if baseViewController is ProfileViewController {
                self.sendAnalytics(ActionName.ClickGalleryProfile)
            } else {
                self.sendAnalytics(ActionName.ClickGalleryPrivateMessage)
            }
        }
    }
    
    //
    // MARK: UIImagePickerControllerDelegate
    //
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        picker.dismiss(animated: true) {
            
            if let deg = self.galleryDelegate {
                let size = Double(image.compressImage().count) / 1024.0
            
                if  size <= Constants.SIZE.IMAGE {
                    deg.sendPicture(image)
                } else {
                    deg.pictureToBig()
                }
            }
        }
        
        if self.baseViewController is ProfileViewController {
            self.sendAnalytics(ActionName.PictureSelectedProfile)
        } else {
            self.sendAnalytics(ActionName.PictureSelectedPrivateMessage)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        if let deg = self.galleryDelegate {
            deg.cancelSelectPicture()
        }
        
        if baseViewController is ProfileViewController {
            self.sendAnalytics(ActionName.ClickCancelSelectPictureProfile)
        } else {
            self.sendAnalytics(ActionName.ClickCancelSelectPicturePrivateMessage)
        }
    }
    
    //
    // MARK: Analytics
    //
    
    func sendAnalytics(_ actionName: ActionName) {
        let param: [String : Any] = [KeyName.UserId.rawValue: "\(self.sessionManager.user.userId)",
                     KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo]
        self.analyticsManager.trackAction(actionName, params: param)
    }

    
}
