//
//  BasePageDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 11/27/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

protocol BasePagePositionDelegate {
    func pageMoveToPosition(_ position:Int)
}

class BasePageDelegate: NSObject,  UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    var delegate:BasePagePositionDelegate?
    var baseViewControllerSegue:BaseViewControllerSegue?
    var current:Int = 0
    
    func viewControllerAtIndex(_ index:Int, baseViewControllerSegue:BaseViewControllerSegue) -> UIViewController
    {
        return BaseViewController()
    }
    
    //
    //MARK: UIPageViewControllerDataSource
    //
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = -1;
        
        if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BasePageContentViewController  {
            index = vc.positionViewController
        } else if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BaseTabBarViewController  {
            index = vc.positionViewController
        } else if let viewController = viewController as? BasePageContentViewController {
            index = viewController.positionViewController
        } else if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BaseViewController  {
            index = vc.positionViewController
        } else {
            let viewController = viewController as! BaseTabBarViewController
            index = viewController.positionViewController
        }
        
        if ((index == 0) || (index == NSNotFound)) {
            return nil;
        }
        
        return self.viewControllerAtIndex(index-1, baseViewControllerSegue:baseViewControllerSegue!);
        
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = -1;
        
        if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BasePageContentViewController  {
            index = vc.positionViewController
        } else if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BaseTabBarViewController  {
            index = vc.positionViewController
        } else if let viewController = viewController as? BaseViewController { //BasePageContentViewController
            index = viewController.positionViewController
        } else if let nav = viewController as? BaseNavigationController, let vc = nav.viewControllers.first as? BaseViewController  {
            index = vc.positionViewController
        } else {
            let viewController = viewController as! BaseTabBarViewController
            index = viewController.positionViewController
        }
        
        if (index == NSNotFound || index == maxViewController() - 1) {
            return nil;
        }
        
        
        return self.viewControllerAtIndex(index+1, baseViewControllerSegue:baseViewControllerSegue!);
        
    }
    
    //
    //MARK: UIPageViewControllerDelegate
    //
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if (completed == false) {
            return
        }
        
        if let nav = pageViewController.viewControllers?.first as? BaseNavigationController,
            let vc = nav.viewControllers.last as? BaseTabBarViewController {
            current = vc.positionViewController
            delegate?.pageMoveToPosition(current)
        } else if let nav = pageViewController.viewControllers?.first as? BaseNavigationController,
            let vc = nav.viewControllers.first as? BaseViewController  {
            current = vc.positionViewController
            delegate?.pageMoveToPosition(current)
        }
    }
    
    //
    //MARK: To Implement
    //
    
    func maxViewController() -> Int {
        return 0
    }
    
}
