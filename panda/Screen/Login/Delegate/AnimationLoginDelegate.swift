//
//  AnimationLoginDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 10/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class AnimationLoginDelegate: NSObject {
    
    fileprivate let defaultTopMin4s: CGFloat = 10.0
    fileprivate let defaultTopMin: CGFloat = 30.0
    fileprivate let defaultTopMax: CGFloat = 50.0
    
    @IBOutlet weak var loginViewController: LoginViewController!
    
    //
    // MARK: Animations
    //
    
    func initLogo() {
        if (DeviceType.IS_IPHONE_5 == true) {
            self.loginViewController.constraintHeightLogo.constant = 130
        } else if (DeviceType.IS_IPHONE_678 == true) {
            self.loginViewController.constraintHeightLogo.constant = 160
        } else if (DeviceType.IS_IPHONE_678P == true) {
            self.loginViewController.constraintHeightLogo.constant = 180
        } else {
            self.loginViewController.constraintHeightLogo.constant = 120
        }
    }
    
    func initLogoSmall() {
        if (DeviceType.IS_IPHONE_678 == true) {
            self.loginViewController.constraintHeightLogo.constant = 130
        } else if (DeviceType.IS_IPHONE_678P == true) {
            self.loginViewController.constraintHeightLogo.constant = 150
        } else {
            self.loginViewController.constraintHeightLogo.constant = 80
        }
    }
    
    func animAfterSplash() {
        self.loginViewController.view.layoutIfNeeded()
        
        let possitionLogoLogin = self.loginViewController.scrollView.frame.origin.y + self.loginViewController.logoLogin.frame.origin.y + (self.loginViewController.constraintLogoLoginWidth.constant / 2)
        let hor = ((ScreenSize.SCREEN_HEIGHT / 2) - possitionLogoLogin) * -1
        
        UIView.animate(withDuration: 0.5, animations: {
            self.loginViewController.scrollView.alpha = 0.0
            self.loginViewController.logoLogin.alpha = 0.0
            self.loginViewController.constraintLogoSplashHorizontal.constant = hor
            self.loginViewController.constraintLogoSplashWidth.constant = self.loginViewController.constraintLogoLoginWidth.constant
            self.loginViewController.view.layoutIfNeeded()
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.loginViewController.scrollView.isHidden = false
                self.loginViewController.scrollView.alpha = 1.0
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.01, animations: {
                    self.loginViewController.logoSplash.alpha = 0.0
                    self.loginViewController.logoLogin.alpha = 1.0
                }, completion: { (finish) in
                    self.loginViewController.logoSplash.isHidden = true
                    self.loginViewController.logoLogin.zoom(FLT_MAX)
                })
            })
        }) 
    }
    
    func animBeforSplash(_ done:@escaping () -> Void) {
        let possitionLogoLogin = self.loginViewController.scrollView.frame.origin.y + self.loginViewController.logoLogin.frame.origin.y + (self.loginViewController.constraintLogoLoginWidth.constant / 2)
        let hor = ((ScreenSize.SCREEN_HEIGHT / 2) - possitionLogoLogin) * -1
        self.loginViewController.constraintLogoSplashHorizontal.constant = hor
        self.loginViewController.logoSplash.isHidden = false
        self.loginViewController.logoSplash.alpha = 0.0
        self.loginViewController.view.layoutIfNeeded()
        
        
        UIView.animate(withDuration: 0.1, animations: {
            self.loginViewController.logoSplash.alpha = 1.0
            self.loginViewController.logoLogin.alpha = 0.0
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.loginViewController.scrollView.alpha = 0.0
                self.loginViewController.constraintLogoSplashHorizontal.constant = 0
                self.loginViewController.constraintLogoSplashWidth.constant = 180
                self.loginViewController.view.layoutIfNeeded()
            }, completion: { (finish) in
                self.loginViewController.logoSplash.zoom(FLT_MAX)
                done()
            })
        }) 
    }
    
    func animKeyboardShow() {
        UIView.animate(withDuration: 0.3, animations: {
            if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPHONE_5 {
                self.loginViewController.constraintLogoLoginTop.constant = self.defaultTopMin4s
            } else {
                self.loginViewController.constraintLogoLoginTop.constant = self.defaultTopMin
            }
            self.loginViewController.buttonRegister.alpha = 0.0
            self.initLogoSmall()
            self.loginViewController.view.layoutIfNeeded()
        })
    }
    
    func animKeyboardHide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.loginViewController.constraintLogoLoginTop.constant = self.defaultTopMax
            self.loginViewController.buttonRegister.alpha = 1.0
            self.initLogo()
            self.loginViewController.view.layoutIfNeeded()
        })
    }
    
    func animLogoError() {
        self.loginViewController.logoLogin.shakeRotation()
    }
}
