//
//  LoginViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import ObjectMapper
import Cartography

class LoginViewController: BaseViewController, UITextFieldDelegate {

    fileprivate let sessionManaget: SessionManager = SessionManager.sharedInstance

    @IBOutlet weak var logoLogin: UIImageView!
    @IBOutlet weak var logoSplash: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textFieldEmail: BaseTextFieldView!
    @IBOutlet weak var textFieldPassword: BaseTextFieldView!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var buttonForgot: UIButton!
    @IBOutlet weak var animationLoginDelegate: AnimationLoginDelegate!
    
    @IBOutlet weak var constraintLogoLoginTop: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightEmail: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPassword: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightLogo: NSLayoutConstraint!
    @IBOutlet weak var constraintLogoSplashHorizontal: NSLayoutConstraint!
    @IBOutlet weak var constraintLogoSplashWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintLogoLoginWidth: NSLayoutConstraint!

    var statuRequestSocialError:ErrorResponse?
    var userSocial:UserModel?
    
    override var screenName: ScreenName? {
        return ScreenName.SignIn
    }
    
    override func loadView() {
        super.loadView()
        
        self.initLayout()
        self.animationLoginDelegate.initLogo()
        self.initLocalizedString()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.addObservers()
        
        
        self.bag.add(self.sessionManaget.onGetSessionSucceed.add(self, handler: LoginViewController.onGetSessionSucceed))
        self.bag.add(self.sessionManaget.onGetSessionFailed.add(self, handler: LoginViewController.onGetSessionFailed))
        self.bag.add(self.sessionManaget.onGetSessionFailedNotFoundEmailOrPassword.add(self, handler: LoginViewController.onGetSessionFailedNotFoundEmailOrPassword))
        self.bag.add(self.sessionManaget.onGetSessionFailedRateLimit.add(self, handler: LoginViewController.onGetSessionFailedRateLimit))
        self.bag.add(self.sessionManaget.onGetSessionFailedNotActive.add(self, handler: LoginViewController.onGetSessionFailedNotActive))
        
        self.bag.add(self.sessionManaget.onResetPasswordSessionSucceed.add(self, handler: LoginViewController.onResetPasswordSessionSucceed))
        self.bag.add(self.sessionManaget.onResetPasswordSessionFailed.add(self, handler: LoginViewController.onResetPasswordSessionFailed))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if self.isMovingToParentViewController && !self.isMovingFromParentViewController {
            self.animationLoginDelegate.animAfterSplash()
        } else {
            self.logoLogin.zoom(FLT_MAX)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    
        self.removeObservers()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLocalizedString() {        
        self.textFieldEmail.setPlaceholder(NSLocalizedString("generic_label_email", comment: ""))
        self.textFieldPassword.setPlaceholder(NSLocalizedString("generic_label_password", comment: ""))
        self.buttonLogin.setTitle(NSLocalizedString("login_screen_bt_login", comment: ""), for: UIControlState())
        self.buttonRegister.setTitle(NSLocalizedString("login_screen_bt_register_on_panda", comment: ""), for: UIControlState())
        self.buttonForgot.setTitle(NSLocalizedString("login_screen_bt_forgot_pass", comment: ""), for: UIControlState())
    }
    
    fileprivate func initLayout() {
        self.automaticallyAdjustsScrollViewInsets = false;
        
        self.textFieldEmail.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: false, withNext: true)
        self.textFieldEmail.removeConstraint(constraintHeightEmail)
        self.textFieldEmail.setKeyboard(UIKeyboardType.emailAddress)
        self.textFieldEmail.delegate = self
        self.textFieldEmail.autocorrectionType(UITextAutocorrectionType.no)
        self.textFieldEmail.baseTextField.returnKeyType = .next
        
        self.textFieldPassword.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: false)
        self.textFieldPassword.removeConstraint(constraintHeightPassword)
        self.textFieldPassword.isSecureTextEntry(true)
        self.textFieldPassword.delegate = self
        self.textFieldPassword.baseTextField.returnKeyType = .done
        self.textFieldPassword.updateButtonStyle(ButtonTextFieldType.secure)
    }
    
    //
    //MARK: UITextFieldDelegate
    //
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.textFieldEmail.baseTextField {
            self.textFieldPassword.baseTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
            self.clickLogin("" as AnyObject)
        }
        
        return true
    }
    
    //
    //MARK: Keyboard
    //
    
    override func keyboardWillShow(_ notif: Notification) {
        super.keyboardWillShow(notif)
        
        self.animationLoginDelegate.animKeyboardShow()
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            let keyboardFrame = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardFrame!.size.height, 0)
            let newHeight: CGFloat = 280
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.scrollView.contentInset = contentInsets
                self.scrollView.contentSize = CGSize(width: 0, height: newHeight)
                self.scrollView.scrollIndicatorInsets = contentInsets
            }) 
        }
    }
    
    override func keyboardWillHide(_ notif: Notification) {
        super.keyboardWillHide(notif)
        
        self.animationLoginDelegate.animKeyboardHide()
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, 0, 0)
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.scrollView.contentInset = contentInsets
                self.scrollView.contentSize = CGSize(width: 0, height: 0)
                self.scrollView.scrollIndicatorInsets = contentInsets
                
            }) 
        }
    }
    
    //
    //MARK: ToolBar
    //
    
    override func clickPres(_ sender: UIBarButtonItem) {
        self.textFieldEmail.baseTextField.becomeFirstResponder()
    }
    
    override func clickNext(_ sender: UIBarButtonItem) {
        self.textFieldPassword.baseTextField.becomeFirstResponder()
    }
    
    //
    //MARK: Cheking Validation
    //
    
    fileprivate func isNotMissingInfo() -> Bool {
        var canNotLogin = false
        
        if (textFieldEmail.text.characters.count == 0) {
            canNotLogin = true
            textFieldEmail.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else {
            textFieldEmail.setMessage(nil)
        }
        
        if (textFieldPassword.text.characters.count == 0) {
            canNotLogin = true
            textFieldPassword.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else {
            textFieldPassword.setMessage(nil)
        }
        
        return canNotLogin
        
    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickLogin(_ sender: AnyObject) {
        if !isNotMissingInfo() {
            self.analyticsManager.trackAction(ActionName.SendSignIn)
            self.view.endEditing(true)
            self.showLoader()
            self.sessionManaget.login(self.textFieldEmail.text, password: self.textFieldPassword.text)
        } else {
            self.animationLoginDelegate.animLogoError()
            self.sendAnanylitcs()
        }
    }
    
    @IBAction func clickRegister(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.3, animations: {
            self.textFieldEmail.setMessage("")
            self.textFieldPassword.setMessage("")
            self.logoLogin.layer.removeAllAnimations()
        }, completion: { (finish) in
            self.analyticsManager.trackAction(ActionName.ClickSignUp)
            let registerViewController = StoryboardUtil.registerViewController()
            self.navigationController?.pushViewController(registerViewController, animated: false)
        }) 
    }
    
    
    @IBAction func clickForgot(_ sender: AnyObject) {
        self.analyticsManager.trackAction(ActionName.ClickForgotPassword)
        self.showAlertForgotPassword(self.textFieldEmail.text)
    }
    
    //
    //MARK: Handle
    //
    
    func onGetSessionSucceed() {
        self.removedLoader()
        self.animationLoginDelegate.animBeforSplash {
            let _ = self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func onGetSessionFailed(_ response: ErrorResponse?) {
        self.removedLoader()
        self.animationLoginDelegate.animLogoError()
    }
    
    func onGetSessionFailedNotFoundEmailOrPassword() {
        self.removedLoader()
        self.animationLoginDelegate.animLogoError()
        self.textFieldEmail.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        self.textFieldPassword.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        self.sendAnanylitcs()
    }
    
    func onGetSessionFailedRateLimit() {
        self.removedLoader()
        self.animationLoginDelegate.animLogoError()
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_rate_limit", comment: ""))
    }
    
    func onGetSessionFailedNotActive() {
        self.removedLoader()
        self.animationLoginDelegate.animLogoError()
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_not_active", comment: ""))
    }
    
    func onResetPasswordSessionSucceed() {
        self.removedLoader()
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_reset_password_success", comment: ""))
    }
    
    func onResetPasswordSessionFailed() {
        self.removedLoader()
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_reset_password_fail", comment: ""))
    }
    
    //
    // MARK: Dialog Forgot password
    //
    
    fileprivate func showAlertForgotPassword(_ email: String) {
        let alertController = AlertUtil.initAlertController(NSLocalizedString("dialog_reset_password_title", comment: ""), message: NSLocalizedString("dialog_reset_password_message", comment: ""))
        
        alertController.addAction(UIAlertAction(title: AlertUtil.buttonOk, style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in
            guard let text = alertController.textFields?.first?.text, text.length() > 0 else {
                return
            }
            
            self.showLoader()
            self.sessionManaget.resetPassword(text)
        }))
        
        alertController.addAction(UIAlertAction(title: AlertUtil.buttonCancel, style: UIAlertActionStyle.cancel, handler: { (alertAction) -> Void in
        }))
        
        alertController.addTextField { (textField) -> Void in
            textField.placeholder =  NSLocalizedString("generic_label_email", comment: "")
            textField.text = email
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        
        AlertUtil.launchAlert(alertController)
    }
    
    //
    // MARK: Analytics
    //
    
    fileprivate func sendAnanylitcs() {
        var param: [String: Any] = [:]
        if let emailError = self.textFieldEmail.labelError.text, emailError.length() > 0 {
            param[KeyName.EmailError.rawValue] = emailError
        }
        
        if let passwordError = self.textFieldPassword.labelError.text, passwordError.length() > 0 {
            param[KeyName.PasswordError.rawValue] = passwordError as AnyObject?
        }
        self.analyticsManager.trackAction(ActionName.MissingInfoSignIn, params: param)
    }
}
