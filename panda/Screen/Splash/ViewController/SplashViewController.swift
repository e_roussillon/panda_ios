//
//  SplashViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/15/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    fileprivate let backupManager: BackupManager = BackupManager.sharedInstance

    override var screenName: ScreenName? {
        if sessionManager.user.justLogin {
            return ScreenName.BackupSplash
        } else {
            return ScreenName.Splash
        }
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.bag.add(self.backupManager.onBackupSucceed.add(self, handler: SplashViewController.onBackupSucceed))
        self.bag.add(self.backupManager.onBackupFailed.add(self, handler: SplashViewController.onBackupFailed))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.addHeartBeatAnimation()
        
        if sessionManager.user.justLogin {
            self.initBackup()
        } else {
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SplashViewController.launchLogin), userInfo: nil, repeats: false)
        }
    }
    
    @objc internal func launchLogin() {
        self.navigationController?.pushViewController(StoryboardUtil.loginViewController(), animated: false)
    }
    
    //
    //MARK: Backup
    //
    
    fileprivate func initBackup() {
        self.backupManager.backup()
    }
    
    //
    //MARK: Handle
    //
    
    func onBackupSucceed() {
        self.sessionManager.user.justLogin = false
        self.navigationController!.dismiss(animated: true, completion: nil)
    }
    
    func onBackupFailed(_ response: ErrorResponse?) {
        self.launchLogin()
    }
    
    
    /**
     Heart beating animation
     */
    fileprivate func addHeartBeatAnimation() {
        self.imageView.zoom(FLT_MAX)
    }
}
