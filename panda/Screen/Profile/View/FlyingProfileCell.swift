//
//  FlyingProfileCell.swift
//  panda
//
//  Created by Edouard Roussillon on 2/7/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FlyingProfileCell: UITableViewCell {

    let none = NSLocalizedString("generic_label_none", comment: "")
    static let filterHeight: CGFloat = 44.0
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func configureCell(_ objects: [Any]?) {

        guard let list = objects, list.count == 2 else {
            self.labelTitle.text = ""
            self.labelDescription.text = ""
            return
        }
        
        if let label = list.first as? String {
            self.labelTitle.text = label
        } else {
            self.labelTitle.text = none
        }
        
        if let desc = list[1] as? String {
            self.labelDescription.text = desc
        } else if let descDate = list[1] as? Date {
            self.labelDescription.text = descDate.getMediumStyleDate()
        } else {
            self.labelDescription.text = none
        }
    }
}
