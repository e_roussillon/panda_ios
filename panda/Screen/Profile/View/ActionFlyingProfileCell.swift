//
//  FlyingProfileCell.swift
//  panda
//
//  Created by Edouard Roussillon on 2/7/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

typealias HandlerFirendshipAlert = (_ alertController: UIAlertController, _ alertAction: UIAlertAction) -> Void;

enum ActionMode {
    case positive
    case negative
}

class ActionFlyingProfileCell: UITableViewCell {
    fileprivate static let sessionManager: SessionManager = SessionManager.sharedInstance
    static let filterHeight: CGFloat = 44.0
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var buttonActionPositive: BaseButtonLight!
    @IBOutlet weak var buttonActionNegatice: BaseButtonDark!
    
    var user: UserModel!
    var baseViewControllerSegue: BaseViewControllerSegue!
    var index: Int!
    
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func configureCell(_ objects: [Any]?) {

        guard let list = objects, list.count >= 2 else {
            self.resetView()
            return
        }
        
        self.rootView.layer.masksToBounds = false
        self.contentView.layer.masksToBounds = false
        
        if let _ = list.first as? Bool, let segue = list.last as? BaseViewControllerSegue, list.count == 2 {
            self.baseViewControllerSegue = segue
            self.addIconReportedUser()
        } else if let user = list.first as? UserModel, let index = list[1] as? Int, let segue = list[2] as? BaseViewControllerSegue, let blocked = list[3] as? Bool {
            self.baseViewControllerSegue = segue
            self.user = user
            self.index = index
            if blocked {
                self.initButtonBlocked()
            } else {
                self.initButton()
            }
        } else {
            self.resetView()
        }
    }
    
    //
    //MARK: Init
    //
    
    static func numberOfCells(_ status: FriendshipType, userLastAction: Int) -> Int {
        switch status {
        case .pending:
            if userLastAction == sessionManager.user.userId {
                return 1
            } else {
                return 2
            }
        case .friend:
            return 1
        default:
            return 1
        }
    }
    
    fileprivate func resetView() {
        self.buttonActionPositive.isHidden = true
        self.buttonActionNegatice.isHidden = true
    }
    
    fileprivate func initButton() {
        if let status = self.user.friendshipStatus, let row = index {
            let lastActionUserId =  self.user.friendshipLastAction ?? -1
            
            switch status {
            case .notFriend, .cancelRequest, .refusedAfterBeFriend:
                self.addIconAddUser()
                break
            case .pending:
                if lastActionUserId == ActionFlyingProfileCell.sessionManager.user.userId {
                    self.addIconPendingUser()
                } else {
                    self.addIconsAccepteFriend(row)
                }
                break
            case .refusedBeforeBeFriend:
                self.addIconAddUser()
                break
            case .friend:
                self.addIconRemoveUser()
                break
            default:
                break
            }
        }
    }
    
    fileprivate func initButtonBlocked() {
        if let status = self.user.friendshipStatus {
            let lastActionUserId =  self.user.friendshipLastAction ?? -1
            
            switch status {
            case .blockedAfterBeFriend, .blockedBeforeBeFriend:
                if lastActionUserId == ActionFlyingProfileCell.sessionManager.user.userId {
                    self.addIconUnblockUser()
                }
                break
            case .friend:
                self.addIconBlockedUserAfterBeFriend()
            default:
                self.addIconBlockedUserBeforeBeFriend()
                break
            }
        }
    }
    
    fileprivate func addIconsAccepteFriend(_ row: Int!) {
        switch row {
        case 0:
            self.initButtonImageWithTest(.positive, title: NSLocalizedString("friendship_action_accept_title", comment: ""), image: "user_add", tag: FriendshipType.friend.rawValue)
            break
        case 1:
            self.initButtonImageWithTest(.negative, title: NSLocalizedString("friendship_action_refuse_title", comment: ""), image: "user_remove", tag:  FriendshipType.refusedBeforeBeFriend.rawValue)
            break
        default:
            self.addIconBlockedUserBeforeBeFriend()
            break
        }
    }
    
    fileprivate func addIconPendingUser() {
        self.initButtonImageWithTest(.negative, title: NSLocalizedString("friendship_action_cancel_title", comment: ""), image: "user_add", tag: FriendshipType.cancelRequest.rawValue)
    }
    
    fileprivate func addIconAddUser() {
        self.initButtonImageWithTest(.positive, title: NSLocalizedString("friendship_action_add_title", comment: ""), image: "user_add", tag: FriendshipType.pending.rawValue)
    }
    
    fileprivate func addIconRemoveUser() {
        self.initButtonImageWithTest(.negative, title: NSLocalizedString("friendship_action_remove_title", comment: ""), image: "user_remove", tag: FriendshipType.refusedAfterBeFriend.rawValue)
    }
    
    fileprivate func addIconBlockedUserBeforeBeFriend() {
        self.addIconBlockWith(FriendshipType.blockedBeforeBeFriend.rawValue)
    }
    
    fileprivate func addIconBlockedUserAfterBeFriend() {
        self.addIconBlockWith(FriendshipType.blockedAfterBeFriend.rawValue)
    }
    
    fileprivate func addIconBlockWith(_ status: Int) {
        self.initButtonImageWithTest(.negative, title: NSLocalizedString("friendship_action_block_title", comment: ""), image: "user_block", tag: status)
    }
    
    fileprivate func addIconUnblockUser() {
        self.initButtonImageWithTest(.positive, title: NSLocalizedString("friendship_action_unblock_title", comment: ""), image: "user_unblock", tag: FriendshipType.unBlocked.rawValue)
    }
    
    fileprivate func addIconReportedUser() {
        self.initButtonImageWithTest(.negative, title: NSLocalizedString("friendship_action_report_title", comment: ""), image: "user_reported", tag: -1)
    }
    
    
    fileprivate func initButtonImageWithTest(_ actions: ActionMode, title: String, image: String, tag: Int) {
        
        switch actions {
        case .positive:
            self.buttonActionPositive.tag = tag
            self.buttonActionPositive.setImage(UIImage(named: image), for: UIControlState())
            self.buttonActionPositive.setTitle(title, for: UIControlState())
            self.buttonActionPositive.isHidden = false
            self.buttonActionNegatice.isHidden = true
            break
        case .negative:
            self.buttonActionNegatice.tag = tag
            self.buttonActionNegatice.setImage(UIImage(named: image), for: UIControlState())
            self.buttonActionNegatice.setTitle(title, for: UIControlState())
            self.buttonActionPositive.isHidden = true
            self.buttonActionNegatice.isHidden = false
            break
        }
    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickAction(_ sender: AnyObject) {
        guard sender is UIButton else {
            return
        }
        
        let tag: Int = sender.tag
        
        self.baseViewControllerSegue.launchSegue(Constants.SEGUE.ACTION_FRIENDSHIP, values: [tag])
    }
    
}
