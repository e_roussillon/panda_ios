//
//  FlyingProfileDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 2/7/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FlyingProfileDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    let sessionManager = SessionManager.sharedInstance
    let none = NSLocalizedString("generic_label_none", comment: "")
    var user: UserModel?
    var baseViewControllerSegue: BaseViewControllerSegue!
    var isModeRemove: Bool = false
    var numberOfSection: Int = 1
    
    var actionSection: Bool = false
    var buttonBlockedSection: Bool = false
    
    //
    //MARK: UITableViewDataSource
    //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let user = user else {
            numberOfSection = 1
            return numberOfSection
        }

//TODO: Uncomment code for v2 when there is friendship
        self.actionSection = self.getActionSection(user)
        self.buttonBlockedSection = self.getButtonsBottomSection(user)
        
        if self.actionSection && self.buttonBlockedSection {
            numberOfSection = 3
        } else if self.actionSection || self.buttonBlockedSection {
            numberOfSection = 2
        } else {
            numberOfSection = 1
        }
        
//TODO: Cooment code if v2
//        self.buttonBlockedSection = self.getButtonsBottomSection(user)
//        
//        if self.buttonBlockedSection {
//            numberOfSection = 2
//        } else {
//            numberOfSection = 1
//        }
   
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        guard let mUser = user else {
            return 0
        }

//TODO: Uncomment code for v2 when there is friendship
        switch self.numberOfSection {
        case 1:
            return 3
        case 2:
            if section == 0 && self.actionSection {
                return self.getFriendshipRow(mUser)
            } else if section == 1 && self.buttonBlockedSection {
                return self.getButtonsBottomRow(mUser)
            } else {
                return 3
            }
        default:
            if section == 0 {
                return self.getFriendshipRow(mUser)
            } else if section == 1 {
                return 3
            } else {
                return getButtonsBottomRow(mUser)
            }
        }
        
//TODO: Cooment code if v2
//        switch self.numberOfSection {
//        case 1:
//            return 3
//        default:
//            if section == 0 {
//                return 3
//            } else {
//                return getButtonsBottomRow(mUser)
//            }
//        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let mUser = user else {
            return UITableViewCell()
        }
      
        switch self.numberOfSection {
        case 1:
            return getDetailsUser(tableView: tableView, user: mUser, indexPath: indexPath)
        case 2:
            if indexPath.section == 0 && self.actionSection {
                return ActionFlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [mUser, indexPath.row, baseViewControllerSegue, false])
            } else if indexPath.section == 1 && self.buttonBlockedSection {
                return self.getButtonBottom(tableView: tableView, user: mUser, indexPath: indexPath)
            } else {
                return getDetailsUser(tableView: tableView, user: mUser, indexPath: indexPath)
            }
        default:
            if indexPath.section == 0 {
                return ActionFlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [mUser, indexPath.row, baseViewControllerSegue, false])
            } else if indexPath.section == 1 {
                return self.getDetailsUser(tableView: tableView, user: mUser, indexPath: indexPath)
            } else {
                return self.getButtonBottom(tableView: tableView, user: mUser, indexPath: indexPath)
            }
        }
    }
    
    //
    //MARK: UITableViewDelegate
    //
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.numberOfSection {
        case 1:
            return FlyingProfileCell.heightWithObjects(nil)
        case 2:
            if indexPath.section == 0 && self.actionSection {
                return ActionFlyingProfileCell.heightWithObjects(nil)
            } else if indexPath.section == 1 && self.buttonBlockedSection {
                return ActionFlyingProfileCell.heightWithObjects(nil)
            } else {
                return FlyingProfileCell.heightWithObjects(nil)
            }
        default:
            if indexPath.section == 0 {
                return ActionFlyingProfileCell.heightWithObjects(nil)
            } else if indexPath.section == 1 {
                return FlyingProfileCell.heightWithObjects(nil)
            } else {
                return ActionFlyingProfileCell.heightWithObjects(nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return BaseLabelView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BaseLabelView()
    
//TODO: Uncomment code for v2 when there is friendship
        switch self.numberOfSection {
        case 1:
            view.text = NSLocalizedString("flying_profile_header_details", comment: "")
        case 2:
            if section == 0 && self.actionSection {
                view.text = NSLocalizedString("flying_profile_header_actions", comment: "")
            } else if section == 1 && self.buttonBlockedSection {
                view.text = ""
            } else {
                view.text = NSLocalizedString("flying_profile_header_details", comment: "")
            }
        default:
            if section == 0 {
                view.text = NSLocalizedString("flying_profile_header_actions", comment: "")
            } else if section == 1 {
                view.text = NSLocalizedString("flying_profile_header_details", comment: "")
            } else {
                view.text = ""
            }
        }
        
//        switch self.numberOfSection {
//        case 1:
//            view.text = NSLocalizedString("flying_profile_header_details", comment: "")
//        default:
//            if section == 0 {
//                view.text = NSLocalizedString("flying_profile_header_details", comment: "")
//            } else {
//                view.text = ""
//            }
//        }

        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    //
    // MARK: Private
    //
    
    fileprivate func getButtonsBottomSection(_ user: UserModel) -> Bool {
        guard let friendship = user.friendshipStatus else {
            return false
        }
        
        let lastAction = user.friendshipLastAction ?? -1
        
        switch friendship {
        case .blockedBeforeBeFriend where lastAction != self.sessionManager.user.userId, .blockedAfterBeFriend where lastAction != self.sessionManager.user.userId:
            return false
        default:
            return true
        }
    }
    
    fileprivate func getActionSection(_ user: UserModel) -> Bool {
        guard let friendship = user.friendshipStatus else {
                return false
        }
        
        switch friendship {
        case .blockedBeforeBeFriend, .blockedAfterBeFriend:
            return false
        default:
            return true
        }
    }
    
    fileprivate func getButtonsBottomRow(_ user: UserModel) -> Int {
        
        guard let reported = user.reported, !reported else {
            return 1
        }
        
        return 2
    }
    
    fileprivate func getFriendshipRow(_ user: UserModel) -> Int {
        if let friendshipStatus = user.friendshipStatus {
            let friendshipLastAction = user.friendshipLastAction ?? -1
            return ActionFlyingProfileCell.numberOfCells(friendshipStatus, userLastAction: friendshipLastAction)
        } else {
            return 0
        }
    }
    
    fileprivate func getDetailsUser(tableView: UITableView, user: UserModel, indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0 :
            let title = NSLocalizedString("generic_label_gender", comment: "")
            return FlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [title, user.getGenderType()])
        case 1 :
            let title = NSLocalizedString("generic_label_birthday", comment: "")
            if let brithday = user.birthday {
                return FlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [title, brithday])
            } else {
                return FlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [title, none])
            }
        default :
            let title = NSLocalizedString("generic_label_description", comment: "")
            if let details = user.details, details.length() > 0 {
                return FlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [title, details])
            } else {
                return FlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [title, none])
            }
        }
    }
    
    fileprivate func getButtonBottom(tableView: UITableView, user: UserModel, indexPath: IndexPath) -> UITableViewCell {
        guard let reported = user.reported, !reported else {
            return ActionFlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [user, indexPath.row, baseViewControllerSegue, true])
        }
        
        if indexPath.row == 0 {
            return ActionFlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [user, indexPath.row, baseViewControllerSegue, true])
        } else {
            return ActionFlyingProfileCell.initCell(tableView, indexPath: indexPath, objects: [true, baseViewControllerSegue])
        }
    }
    
}
