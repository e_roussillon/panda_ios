//
//  ProfileGenderDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 12/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class ProfileGenderDelegate: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {

    static let stringNone = NSLocalizedString("generic_label_none", comment: "")
    static let stringFemal = NSLocalizedString("generic_label_female", comment: "")
    static let stringMale = NSLocalizedString("generic_label_male", comment: "")
    var textFieldGender: BaseTextFieldView!
    var gender: GenderType!


    //
    //MARK: UIPickerViewDataSource
    //

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }


    //
    //MARK: UIPickerViewDelegate
    //

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let gender = GenderType(rawValue: row) ?? GenderType.none
        switch gender {
        case .male:
            return ProfileGenderDelegate.stringMale
        case .female:
            return ProfileGenderDelegate.stringFemal
        default:
            return ProfileGenderDelegate.stringNone
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        let gender = GenderType(rawValue: row) ?? GenderType.none
        self.gender = gender
        switch gender {
        case .male:
            textFieldGender.text = ProfileGenderDelegate.stringMale
        case .female:
            textFieldGender.text = ProfileGenderDelegate.stringFemal
        default:
            textFieldGender.text = ProfileGenderDelegate.stringNone
        }
    }


}
