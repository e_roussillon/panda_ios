//
//  ProfileViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation
import TTTAttributedLabel

class ProfileViewController: BasePageContentViewController, UITextFieldDelegate, UITextViewDelegate, TTTAttributedLabelDelegate {
    
    fileprivate let userManger: UserManager = UserManager.sharedInstance
    
    let picker = UIDatePicker()
    let pickerGender = UIPickerView()
    var user: UserModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var baseGalleryDelegate: BaseGalleryDelegate!
    @IBOutlet weak var profileGenderDelegate: ProfileGenderDelegate!
    @IBOutlet weak var viewImage: BaseImageProfil!
    @IBOutlet weak var textFieldPseudo: BaseTextFieldView!
    @IBOutlet weak var textFieldEmail: BaseTextFieldView!
    @IBOutlet weak var textFieldInfo: BaseLabelView!
    @IBOutlet weak var textFieldGender: BaseTextFieldView!
    @IBOutlet weak var textFieldBirthday: BaseTextFieldView!
    @IBOutlet weak var textViewDescription: BaseTextViewWithPlaceholder!
    @IBOutlet weak var buttonLogout: BaseButtonDark!
    @IBOutlet weak var labelTerms: TTTAttributedLabel!
    
    @IBOutlet weak var constraintHeightPseudo: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightEmail: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightGender: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightBirthday: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightDescription: NSLayoutConstraint!
    
    var currentTextField:UITextField?
    var currentTextView:UITextView?
    var defaultHeightDescription:CGFloat?
    var currentDate:Date?
    
    override var screenName: ScreenName? {
        return ScreenName.Profile
    }
    
    override var paramsAnalytics: [String : Any]? {
        return [KeyName.UserId.rawValue: self.user.userId ?? -1,
                KeyName.UserPseudo.rawValue: self.user.pseudo ?? ""]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLocalizedString()
        self.initLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addObservers()
        self.initLayoutUser()
        
        
        self.bag.add(self.userManger.onUpdateUserSucceed.add(self, handler: ProfileViewController.onUpdateUserSucceed))
        self.bag.add(self.userManger.onUpdateUserFailed.add(self, handler: ProfileViewController.onUpdateUserFailed))
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.layoutIfNeeded()
        self.defaultHeightDescription = self.constraintHeightDescription.constant
        self.adjustFrameComment()
        self.textViewDescription.baseTextField.scrollRangeToVisible(NSRange(location:0, length:0))
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.removedLoader()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLocalizedString() {
        self.title = NSLocalizedString("profile_screen_title", comment: "")
        
        self.textFieldPseudo.setPlaceholder(NSLocalizedString("generic_label_pseudo", comment: ""))
        self.textFieldEmail.setPlaceholder(NSLocalizedString("generic_label_email", comment: ""))
        self.textFieldBirthday.setPlaceholder(NSLocalizedString("generic_label_birthday", comment: ""))
        self.textFieldGender.setPlaceholder(NSLocalizedString("generic_label_gender", comment: ""))
        self.textViewDescription.setPlaceholder(NSLocalizedString("generic_label_description", comment: ""))
        self.textFieldInfo.text = NSLocalizedString("profile_label_info", comment: "")
        self.buttonLogout.setTitle(NSLocalizedString("profile_label_logout", comment: ""), for: UIControlState())
        TermsUtil.initTerms(labelTerms: labelTerms, delegate: self, isLight: true)
    }
    
    fileprivate func initLayout() {
        self.showCancel(false)
        
        self.textFieldPseudo.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: false, withNext: true)
        self.textFieldPseudo.removeConstraint(constraintHeightPseudo)
        self.textFieldPseudo.setKeyboard(UIKeyboardType.default)
        self.textFieldPseudo.delegate = self
        self.textFieldPseudo.autocorrectionType(UITextAutocorrectionType.no)
        self.textFieldEmail.baseTextField.returnKeyType = .next
        
        self.textFieldEmail.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: true)
        self.textFieldEmail.removeConstraint(constraintHeightEmail)
        self.textFieldEmail.setKeyboard(UIKeyboardType.emailAddress)
        self.textFieldEmail.delegate = self
        self.textFieldEmail.autocorrectionType(UITextAutocorrectionType.no)
        self.textFieldEmail.baseTextField.returnKeyType = .next
        
        self.textFieldGender.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: true)
        self.textFieldGender.removeConstraint(constraintHeightGender)
        self.profileGenderDelegate.textFieldGender = self.textFieldGender
        self.pickerGender.delegate = self.profileGenderDelegate
        self.pickerGender.dataSource = self.profileGenderDelegate
        self.pickerGender.accessibilityIdentifier = "GENDER_PICKER"
        self.textFieldGender.baseTextField.inputView = pickerGender
        
        self.textFieldBirthday.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: true)
        self.textFieldBirthday.removeConstraint(constraintHeightBirthday)
        self.picker.setDate(Date(), animated: false)
        self.picker.maximumDate = Date()
        self.picker.minimumDate = "01/01/1900".toDate() as Date
        self.picker.datePickerMode = UIDatePickerMode.date
        self.picker.addTarget(self, action: #selector(ProfileViewController.picketUpdate(_:)), for: UIControlEvents.valueChanged)
        self.picker.accessibilityIdentifier = "DATE_PICKER"
        self.textFieldBirthday.baseTextField.inputView = picker
        
        self.textViewDescription.baseTextField.inputAccessoryView = self.initToolBar(false, withPres: true, withNext: false)
        self.textViewDescription.setKeyboard(UIKeyboardType.default)
        self.textViewDescription.delegate = self
        self.textViewDescription.autocorrectionType(UITextAutocorrectionType.no)
        self.textViewDescription.baseTextField.returnKeyType = .done
    }
    
    fileprivate func initLayoutUser() {
        self.user = UserModel.getCurrentUser()
        self.viewImage.baseGalleryDelegate = self.baseGalleryDelegate
        
        if !self.viewImage.isNewImage {
            self.viewImage.setImage(self.user.urlThumb)
        }
        
        self.textFieldPseudo.text = self.user.pseudo ?? ""
        self.textFieldPseudo.enable(false)
        
        self.textFieldEmail.text = self.user.email ?? ""
        self.textFieldEmail.enable(false)
        
        self.textFieldGender.text = self.user.getGenderType()
        self.textFieldGender.enable(false)
        self.profileGenderDelegate.gender = self.user.gender!
        
        if let birth = self.user.birthday {
            self.textFieldBirthday.text = birth.getMediumStyleDate()
            self.picker.setDate(birth as Date, animated: false)
        } else {
            self.picker.setDate(Date(), animated: false)
            self.textFieldBirthday.text = NSLocalizedString("generic_label_none", comment: "")
        }
        self.textFieldBirthday.enable(false)
        self.currentDate = self.picker.date
        
        if let desc = self.user.details, desc.length() > 0 {
            self.textViewDescription.text = desc
        } else {
            self.textViewDescription.text = NSLocalizedString("generic_label_none", comment: "")
        }
        
        self.textViewDescription.enable(false)
    }
    
    fileprivate func initLayoutModeEdit(_ editable:Bool)  {
//        self.textFieldPseudo.enable(editable)
        self.textFieldEmail.enable(editable)
        self.textFieldGender.enable(editable)
        self.textFieldBirthday.enable(editable)
        self.textViewDescription.enable(editable)
        self.buttonLogout.isEnabled = !editable
        self.viewImage.editImage(editable)
    }
    
    fileprivate func initDescriptionModeEdit(_ editable:Bool) {
        if (editable == true && self.textViewDescription.text == NSLocalizedString("generic_label_none", comment: "")) {
            self.textViewDescription.text = ""
        }
    }
    
    //
    //MARK: UITextFieldDelegate
    //
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.currentTextField = textField
        self.currentTextView = nil
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.currentTextField = nil
        self.currentTextView = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.textFieldPseudo.baseTextField {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        } else if textField == self.textFieldEmail.baseTextField {
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        
        return true
    }
    
    //
    //MARK: UITextViewDelegate
    //
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.currentTextView = textView
        self.currentTextField = nil
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.currentTextView = nil
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let mutableString:String = (self.textViewDescription.text as NSString).replacingCharacters(in: range, with: text)
        
        if (mutableString.characters.count > 1000) {
            return false
        }
        
        if (self.textViewDescription.baseTextField == textView) {
            self.adjustFrameComment()
        }

        return true
        
    }
    
    func adjustFrameComment()
    {
        let newHeigth = self.textViewDescription.baseTextField.contentSize.height;
        
        if (newHeigth > self.defaultHeightDescription!) {
            constraintHeightDescription.constant = newHeigth
        } else {
            constraintHeightDescription.constant = self.defaultHeightDescription!
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }
    
    //
    // MARK: Keyboard
    //
    
    override func keyboardWillShow(_ notif:Notification) {
        super.keyboardWillShow(notif)
        
        let keyboardFrame = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, keyboardFrame!.size.height, 0)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
        }) 
    }
    
    override func keyboardWillHide(_ notif:Notification) {
        super.keyboardWillHide(notif)
        
        let contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0, 0, 0)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.scrollView.contentInset = contentInsets
            self.scrollView.scrollIndicatorInsets = contentInsets
            self.textViewDescription.baseTextField.scrollRangeToVisible(NSRange(location:0, length:0))
        }) 
    }
    
    
    
    //
    //MARK: API
    //
    
    fileprivate func saveUser() {
        if (isNotMissingInfo() == false) {
            let image: UIImage? = self.viewImage.isNewImage ? self.viewImage.imageView.image : nil
            self.showLoader()
            
            self.userManger.updateUser(self.textFieldPseudo.text, email: self.textFieldEmail.text, gender: self.profileGenderDelegate.gender, birthday: self.currentDate!, description: self.textViewDescription.text, image: image)
        }
    }
    
    //
    //MARK: Handle Error
    //
    
    fileprivate func isNotMissingInfo() -> Bool {
        var canNotLogin = false
        
        if (textFieldPseudo.text.characters.count == 0) {
            canNotLogin = true
            textFieldPseudo.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else if (textFieldPseudo.text.characters.count < Constants.FIELD.kPseudoMin || textFieldPseudo.text.characters.count > Constants.FIELD.kPseudoMax) {
            canNotLogin = true
            textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_empty_pseudo_max_min", comment: ""))
        } else {
            textFieldPseudo.setMessage(nil)
        }
        
        if (textFieldEmail.text.characters.count == 0) {
            canNotLogin = true
            textFieldEmail.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
        } else if !textFieldEmail.text.isValidEmail() {
            canNotLogin = true
            self.textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email_invalid", comment: ""))
        } else {
            textFieldEmail.setMessage(nil)
        }
        
        return canNotLogin
    }
        
    fileprivate func handlerError(_ response:ErrorResponse?) -> Bool {
        var result: Bool = false
        
        guard let type = response?.type, let list = response?.fields, type == ErrorType.changeset && list.count > 0 else {
            return result
        }
        
        
        for item in list {
            
            if item.field == "pseudo" {
                result = true
                self.textFieldPseudo.setMessage(NSLocalizedString("register_screen_error_empty_pseudo_max_min", comment: ""))
            } else if item.field == "email" {
                result = true
                
                if item.message?.contains("has already been taken") == true {
                    self.textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email", comment: ""))
                } else {
                    self.textFieldEmail.setMessage(NSLocalizedString("register_screen_error_email_invalid", comment: ""))
                }
                
            } else if item.field == "birthday" {
                result = true
                self.textFieldBirthday.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
            } else if item.field == "gender" {
                result = true
                self.textFieldGender.setMessage(NSLocalizedString("generic_error_empty_field", comment: ""))
            }
            
            
        }
        
        return result
    }
    
    //
    //MARK: IconBar
    //
    
    fileprivate func showCancel(_ show:Bool) {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.initLayoutModeEdit(show)
        self.initDescriptionModeEdit(show)
    
        if (!show) {
            self.navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(named: "dissmis_arrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(ProfileViewController.clickDismiss)), animated: true)
            self.navigationItem.setRightBarButton(UIBarButtonItem(image: UIImage(named: "edit_profile"), style: UIBarButtonItemStyle.done, target: self, action: #selector(ProfileViewController.clickEditAtive)), animated: true)
        } else {
            self.navigationItem.setRightBarButton(UIBarButtonItem(image: UIImage(named: "edit_ok"), style: UIBarButtonItemStyle.done, target: self, action: #selector(ProfileViewController.clickEditSave)), animated: true)
            self.navigationItem.setLeftBarButton(UIBarButtonItem(image: UIImage(named: "edit_canel"), style: UIBarButtonItemStyle.done, target: self, action: #selector(ProfileViewController.clickEditCancel)), animated: true)
            self.navigationItem.setHidesBackButton(false, animated: false)
        }
    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickLogout(_ sender: AnyObject) {
        self.dismiss(animated: true) { 
            BaseViewController.logoutPanda(true)
        }
        
        self.sendAnalytics(ActionName.ClickLogout)
    }
    
    @objc internal func clickDismiss() {
        self.dismiss(animated: true, completion: nil)
        self.sendAnalytics(ActionName.ClickDismissProfile)
    }
    
    @objc internal func clickEditAtive() {
        self.showCancel(true)
        self.sendAnalytics(ActionName.ClickEditProfile)
    }
    
    @objc internal func clickEditSave() {
        self.saveUser()
        self.sendAnalytics(ActionName.ClickSaveEditProfile)
    }
    
    @objc internal func clickEditCancel() {
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_profile_message_cancel", comment: ""), handlerOK: { (alert) -> Void in
            self.viewImage.isNewImage = false
            self.sendAnalytics(ActionName.ClickCancelEditProfile)
            self.showCancel(false)
            self.initLayoutUser()
        }) { (alert) -> Void in
            
        }
    }
    
    //
    //MARK: ToolBar
    //
    
    override func clickPres(_ sender: UIBarButtonItem) {
        
        if self.currentTextView == self.textViewDescription.baseTextField {
            self.currentTextView = nil
            self.currentTextField = self.textFieldBirthday.baseTextField
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
        } else if self.currentTextField == self.textFieldBirthday.baseTextField {
            self.currentTextView = nil
            self.currentTextField = self.textFieldGender.baseTextField
            self.textFieldGender.baseTextField.becomeFirstResponder()
        } else if self.currentTextField == self.textFieldGender.baseTextField {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        } else {
            self.textFieldPseudo.baseTextField.becomeFirstResponder()
        }
        
    }
    
    override func clickNext(_ sender: UIBarButtonItem) {
        
        if self.currentTextField == self.textFieldPseudo.baseTextField {
            self.textFieldEmail.baseTextField.becomeFirstResponder()
        } else if self.currentTextField == self.textFieldEmail.baseTextField {
            self.textFieldGender.baseTextField.becomeFirstResponder()
            self.currentTextField = self.textFieldGender.baseTextField
        } else if self.currentTextField == self.textFieldGender.baseTextField {
            self.textFieldBirthday.baseTextField.becomeFirstResponder()
            self.currentTextField = self.textFieldBirthday.baseTextField
        } else  {
            self.currentTextField = nil
            self.currentTextView = self.textViewDescription.baseTextField
            self.textViewDescription.baseTextField.becomeFirstResponder()
        }
        
    }
    
    //
    //MARK: UIDatePicker
    //
    
    @objc internal func picketUpdate(_ sender: UIDatePicker) {
        self.textFieldBirthday.text = sender.date.getMediumStyleDate()
        self.currentDate = sender.date
    }
    
    //
    // MARK: TTTAttributedLabelDelegate
    //
    
    func attributedLabel(_ label: TTTAttributedLabel, didSelectLinkWith url: URL) {
        TermsUtil.openTerms(navigationController: self.navigationController)
    }
    
    //
    //MARK: Handler
    //
    
    func onUpdateUserSucceed(_ userModel: UserModel) {
        self.user = userModel
        self.viewImage.isNewImage = false
        self.removedLoader()
        self.showCancel(false)
        self.initLayoutUser()
    }
    
    func onUpdateUserFailed(_ response: ErrorResponse?) {
        self.removedLoader()
        let _ = self.handlerError(response)
    }
    
    //
    // MARK: Analytics
    //
    
    func sendAnalytics(_ action: ActionName) {
        let param: [String : Any] = [KeyName.UserId.rawValue: "\(self.sessionManager.user.userId)",
                                     KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo]
        
        self.analyticsManager.trackAction(action, params: param)
    }
}
