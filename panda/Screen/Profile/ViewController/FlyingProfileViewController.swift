//
//  FlyingProfileViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class FlyingProfileViewController: BaseViewController {
    
    var user: UserModel! {
        get {
            return self.flyingProfileDelegate.user
        }
        set {
            self.flyingProfileDelegate.user = newValue
        }
    }
    
    fileprivate let friendshipManager = FriendshipManager.sharedInstance
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let userManager = UserManager.sharedInstance
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var flyingProfileDelegate: FlyingProfileDelegate!
    
    override var screenName: ScreenName? {
        return ScreenName.FlyingProfile
    }
    
    override var paramsAnalytics: [String : Any]? {
        return [KeyName.UserId.rawValue: self.flyingProfileDelegate.user?.userId ?? -1,
                KeyName.UserPseudo.rawValue: self.flyingProfileDelegate.user?.pseudo ?? ""]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadProfile()
        self.initTableview()
        self.initDelegate()
        self.initTitle()
        self.initMessage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bag.add(self.userManager.onGetUserSucceed.add(self, handler: FlyingProfileViewController.onGetUserOrUpdateFriendshipSucceed))
        self.bag.add(self.userManager.onGetUserFailed.add(self, handler: FlyingProfileViewController.onGetUserFailed))
        
        self.bag.add(self.userManager.onReportUserSucceed.add(self, handler: FlyingProfileViewController.onReportUserSucceed))
        self.bag.add(self.userManager.onReportUserFailed.add(self, handler: FlyingProfileViewController.onReportUserFailed))
        
        self.bag.add(self.friendshipManager.onUpdateFriendshipSucceed.add(self, handler: FlyingProfileViewController.onGetUserOrUpdateFriendshipSucceed))
        self.bag.add(self.friendshipManager.onUpdateFriendshipFailed.add(self, handler: FlyingProfileViewController.onUpdateFriendshipFailed))
        self.bag.add(self.friendshipManager.onUpdateFriendshipFailedUserIdNotFound.add(self, handler: FlyingProfileViewController.onUpdateFriendshipFailedUserIdNotFound))
        
        self.bag.add(self.socketNotifierManager.onUpdateFriendship.add(self, handler: FlyingProfileViewController.onUpdateFriendship))
        
    }
        
    //
    //MARK: Init
    //
    
    func initUser(_ user: UserModel) {
        self.user = user
    }
    
    fileprivate func initTitle() {
        self.initNavTitleWithLogo(self.user.urlThumb, title: self.user.pseudo ?? "")
    }
    
    fileprivate func initTableview() {
        FilterCell.registerNibIntoTableView(self.tableView)
        FlyingProfileCell.registerNibIntoTableView(self.tableView)
        ActionFlyingProfileCell.registerNibIntoTableView(self.tableView)
    }
    
    fileprivate func initDelegate() {
        self.flyingProfileDelegate.baseViewControllerSegue = self
    }
    
    fileprivate func initMessage() {
        var list: [UIBarButtonItem] = []
        if let status = user.friendshipStatus {
            switch status {
            case FriendshipType.blockedBeforeBeFriend, FriendshipType.blockedAfterBeFriend:
                self.navigationItem.rightBarButtonItem = nil
            default:
                list.append(UIBarButtonItem(image: UIImage(named: "chat"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FlyingProfileViewController.clickOpenConversation)))
            }
        }
        
        self.navigationItem.rightBarButtonItems = list
    }
    
    //
    //MARK: API
    //
    
    fileprivate func loadProfile() {
        
        if let email = self.user.email, email.length() > 0 {
            self.flyingProfileDelegate.user = user
            self.tableView.reloadData()
        } else {
            self.showLoader()
            self.userManager.getUserBy(id: "\(self.user.userId!)")
        }
    }
    
    fileprivate func updateFriendship(_ friendshipType: FriendshipType) {
        self.showLoader()
        friendshipManager.updateFriendship(self.user.userId, status: friendshipType)
    }
    
    fileprivate func reportUser() {
        if let id = self.user.userId {
            self.showLoader()
            self.userManager.reportUser(id)
        }
    }
    
    //
    //MARK: Handle Error
    //
    
    
    //
    //MARK: Click
    //
    
    @IBAction func clickDismiss(_ sender: AnyObject) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc internal func clickOpenConversation() {
        
        if let user = self.user, let status = user.friendshipStatus {
            UIViewController.launchChatting(user, friendshipStatus: status)
        }
    
    }
    
    //
    //MARK: Segue
    //
    
    override func launchSegue(_ name: String, values: [Any]?) {
        let param: [String: Any] = [KeyName.UserId.rawValue: self.sessionManager.user.userId,
                                          KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo as AnyObject? ?? "",
                                          KeyName.ToUserId.rawValue: "\(self.user.userId ?? -1)",
                                          KeyName.ToUserPseudo.rawValue: self.user.pseudo ?? ""]
        
        if let tag = values?.first as? Int,
            let friendshipType = FriendshipType(rawValue: tag), name == Constants.SEGUE.ACTION_FRIENDSHIP {
            
            switch friendshipType {
            case FriendshipType.blockedAfterBeFriend, FriendshipType.blockedBeforeBeFriend:
                
                AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_block_user_title", comment: ""), message: NSLocalizedString("dialog_block_user_message", comment: ""), handlerOK: { (alert) in
                    self.updateFriendship(friendshipType)
                    self.analyticsManager.trackAction(ActionName.ClickBlockUser, params: param)
                    }, handlerCancel: { (alert) in
                })
                break
                
            default:
                if friendshipType == FriendshipType.unBlocked {
                    self.analyticsManager.trackAction(ActionName.ClickUnblockUser, params: param)
                }
                updateFriendship(friendshipType)
            }
            
            
        } else if name == Constants.SEGUE.ACTION_FRIENDSHIP {
            AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_report_user_title", comment: ""), message: NSLocalizedString("dialog_report_user_message", comment: ""), handlerOK: { (alert) in
                    self.reportUser()
                    self.analyticsManager.trackAction(ActionName.ClickReportUser, params: param)
                }, handlerCancel: { (alert) in
            })
        }
    }
    
    //
    // MARK: Handler Friendship
    //
    
    func onGetUserOrUpdateFriendshipSucceed(_ userModel: UserModel) {
        self.user = userModel
        self.initTitle()
        self.removedLoader()
        self.initMessage()
        self.tableView.reloadData()
    }
    
    func onUpdateFriendshipFailed(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    func onUpdateFriendshipFailedUserIdNotFound(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    func onReportUserSucceed(_ userModel: UserModel) {
        self.user = userModel
        self.removedLoader()
        self.initMessage()
        self.tableView.reloadData()
    }
    
    func  onReportUserFailed(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    //
    // MARK: Handler User
    //
    
    func onGetUserFailed(_ error: ErrorResponse?) {
        self.removedLoader()
    }
    
    //
    // MARK: Handler Friendship send from other user
    //
    
    func onUpdateFriendship(_ user: UserModel) {
        self.user = user
        self.initMessage()
        self.tableView.reloadData()
    }
    
}
