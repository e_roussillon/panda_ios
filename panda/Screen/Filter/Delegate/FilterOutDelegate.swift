//
//  FilterOutDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class FilterOutDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var list: [FilterModel]! = []
    var baseViewControllerSegue: BaseViewControllerSegue!
    var isModeRemove: Bool = false
    
    //
    //MARK: UITableViewDataSource
    //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isModeRemove {
            return list.count
        } else {
            return list.count + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isModeRemove {
            return RemoveFilterCell.initCell(tableView, indexPath: indexPath, objects: [list[indexPath.row]])
        } else {
            if indexPath.row == 0 {
                return FilterCell.initCell(tableView, indexPath: indexPath, objects: [NSLocalizedString("filter_add_new_filter_title", comment: "")])
            } else {
                return FilterCell.initCell(tableView, indexPath: indexPath, objects: [list[indexPath.row - 1], indexPath.row == list.count])
            }
        }
    }
    
    //
    //MARK: UITableViewDelegate
    //
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return BaseLabelView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BaseLabelView()
        view.text = NSLocalizedString("filter_header_not_want", comment: "")
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isModeRemove {
            baseViewControllerSegue.launchSegue(TabBarFilterViewController.DELETE_FILTER, values: [list[indexPath.row]])
        } else {
            if indexPath.row == 0 {
                baseViewControllerSegue.launchSegue(TabBarFilterViewController.ADD_FILTER, values: nil)
            } else {
                baseViewControllerSegue.launchSegue(TabBarFilterViewController.UPDATE_FILTER, values: [list[indexPath.row - 1]])
            }
        }
    }
}
