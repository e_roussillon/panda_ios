//
//  FilterCell.swift
//  panda
//
//  Created by Edouard Roussillon on 1/30/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class RemoveFilterCell: UITableViewCell, BaseTableViewCellDelegate {
    static let filterHeight: CGFloat = 44.0
    
    let fontLight = UIFont.initHelveticaLight(15)
    let fontBold = UIFont.initHelvetica(16)
    
    @IBOutlet weak var labelTitle: UILabel!
    var filterModel: FilterModel?
    
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat {
        return filterHeight
    }
    
    override func configureCell(_ objects: [Any]?) {
        
        if let title = objects?.first as? String {
            self.filterModel = nil
            self.labelTitle.font = fontLight
            self.labelTitle.text = title
        } else if let filterModel = objects?.first as? FilterModel {
            self.labelTitle.font = fontBold
            self.filterModel = filterModel
            self.labelTitle.text = filterModel.name
        } else {
            self.filterModel = nil
            self.labelTitle.text = ""
        }
        
    }
}
