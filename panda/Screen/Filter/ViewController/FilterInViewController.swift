//
//  FilterInViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation

class FilterInViewController: BaseViewController {
    
    fileprivate let filterManager: FilterManager = FilterManager.sharedInstance

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var filterInDelegate: FilterInDelegate!
    @IBOutlet weak var barButtonRemove: UIBarButtonItem!
    
    override var screenName: ScreenName? {
        if self.sessionManager.user.isLogin {
            return ScreenName.FilterLike
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLocalizedString()
        self.initTableview()
        self.initDelegate()
        self.loadFilter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.bag.add(self.filterManager.onAddFilterSucceed.add(self, handler: FilterInViewController.onAddFilterSucceed))
        self.bag.add(self.filterManager.onAddFilterFailed.add(self, handler: FilterInViewController.onAddFilterFailed))
        
        self.bag.add(self.filterManager.onUpdateFilterSucceed.add(self, handler: FilterInViewController.onUpdateFilterSucceed))
        self.bag.add(self.filterManager.onUpdateFilterFailed.add(self, handler: FilterInViewController.onUpdateFilterFailed))
        self.bag.add(self.filterManager.onUpdateFilterFailedMissingParam.add(self, handler: FilterInViewController.onUpdateFilterFailedMissingParam))
        
        self.bag.add(self.filterManager.onRemoveFilterSucceed.add(self, handler: FilterInViewController.onRemoveFilterSucceed))
        self.bag.add(self.filterManager.onRemoveFilterFailed.add(self, handler: FilterInViewController.onRemoveFilterFailed))
    }
        
    //
    //MARK: Init
    //
    
    fileprivate func initLocalizedString() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = NSLocalizedString("filter_screen_title", comment: "")
        if let tabItem = self.tabBarController?.tabBarItem {
            tabItem.title = NSLocalizedString("filter_tabbar_like_title", comment: "")
        }
    }
    
    fileprivate func initTableview() {
        RemoveFilterCell.registerNibIntoTableView(self.tableView)
        FilterCell.registerNibIntoTableView(self.tableView)
    }
    
    fileprivate func initDelegate() {
        self.filterInDelegate.baseViewControllerSegue = self
    }
    
    //
    //MARK: API
    //
    
    fileprivate func loadFilter() {
        self.filterInDelegate.list = FilterModel.getFilters(isHighlight: true)
        self.tableView.reloadData()
        
        if self.filterInDelegate.list.count == 0 {
            self.showEmptyState(UIImage(named: "ic_like_big")!, message: NSLocalizedString("filter_empty_state_like", comment: ""))
        } else {
            self.removeEmptyState()
        }
    }
    
    fileprivate func addFilter(_ name: String) {
        self.showLoader()
        self.filterManager.addFilter(name, isHighlight: true)
    }
    
    fileprivate func updateFilter(_ filterModel: FilterModel, name: String) {
        self.showLoader()
        self.filterManager.updateFilter(filterModel, name: name)
    }
    
    fileprivate func removeFilter(_ filterModel: FilterModel) {
        self.showLoader()
        self.filterManager.removeFilter(filterModel)
    }
    
    //
    //MARK: Handle Add Filter
    //
    
    func onAddFilterSucceed(_ filterModel: FilterModel) {
        self.removedLoader()
        self.loadFilter()
    }
    
    func onAddFilterFailed(_ errorResponse: ErrorResponse?) {
        self.removedLoader()
        let _ = ErrorAPI.checkGenericFilterError(errorResponse)
    }
    
    //
    //MARK: Handle Update Filter
    //
    
    func onUpdateFilterSucceed(_ filterModel: FilterModel) {
        self.removedLoader()
        self.loadFilter()
    }
    
    func onUpdateFilterFailed(_ errorResponse: ErrorResponse?) {
        self.removedLoader()
        let _ = ErrorAPI.checkGenericFilterError(errorResponse)
    }
    
    func onUpdateFilterFailedMissingParam(_ errorResponse: ErrorResponse?) {
        self.removedLoader()
    }
    
    //
    //MARK: Handle Remove Filter
    //
    
    func onRemoveFilterSucceed() {
        self.removedLoader()
        self.loadFilter()
    }
    
    func onRemoveFilterFailed(_ errorResponse: ErrorResponse?) {
        self.removedLoader()
        let _ = ErrorAPI.checkGenericFilterError(errorResponse)
    }
    
    //
    //MARK: BaseViewControllerSegue
    //
    
    override func launchSegue(_ name: String, values: [Any]?) {
        if TabBarFilterViewController.ADD_FILTER == name {
            self.showAlertAddFilter()
        } else if let filterModel = values?.first as? FilterModel, TabBarFilterViewController.UPDATE_FILTER == name {
            self.showAlertUpdateFilter(filterModel)
        } else if let filterModel = values?.first as? FilterModel, TabBarFilterViewController.DELETE_FILTER == name {
            self.showAlertDeleteFilter(filterModel)
        }
    }
    
    //
    //MARK: Alert
    //
    
    internal func showAlertAddFilter() {
        TabBarFilterViewController.showAlertAddFilter(true, handlerOK: { (filterModel, alertController, alertAction) -> Void in
            
            guard let text = alertController.textFields?.first?.text, text.length() > 0 else {
                return
            }
            
            self.addFilter(text)
            self.sendAnalytics(ActionName.ClickNewFilterLike)
        }) { (filterModel, alertController, alertAction) -> Void in
            //TODO - ADD ALERTE
        }
    }
    
    internal func showAlertUpdateFilter(_ filterModel: FilterModel) {
        TabBarFilterViewController.showAlertUpdateFilter(filterModel, handlerOK: { (filterModel, alertController, alertAction) -> Void in
            
            guard let text = alertController.textFields?.first?.text, let mFilterModel = filterModel, let oldTitle = mFilterModel.name, text.length() > 0 && text != oldTitle else {
                return
            }
            
            self.updateFilter(mFilterModel, name: text)
            self.sendAnalytics(ActionName.ClickEditFilterLike)
        }) { (filterModel, alertController, alertAction) -> Void in
            //TODO - ADD ALERTE
        }
    }
    
    internal func showAlertDeleteFilter(_ filterModel: FilterModel) {
        AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_remove_filter_title", comment: ""), message: NSLocalizedString("dialog_filter_message_remove", comment: ""), handlerOK: { (alert) -> Void in
            self.removeFilter(filterModel)
            self.sendAnalytics(ActionName.ClickDeleteFilterLike)
            }, handlerCancel: { (alert) -> Void in
                print("")
        })

    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickBarButton(_ sender: AnyObject) {
        filterInDelegate.isModeRemove = !filterInDelegate.isModeRemove
        if filterInDelegate.isModeRemove {
            barButtonRemove.image = UIImage(named: "edit_ok")
        } else {
            barButtonRemove.image = UIImage(named: "remove")
        }
        tableView.reloadData()
    }
    
    //
    //MARK: Add fitler
    //
    
    override func showEmptyState(_ image: UIImage, message: String) {
        super.showEmptyState(image, message: message)
        
        self.view.bringSubview(toFront: self.tableView)
    }
    
    
    //
    // MARK: Analytics
    //
    
    fileprivate func sendAnalytics(_ actionName: ActionName) {
        let param: [String: Any] = [KeyName.UserId.rawValue: self.sessionManager.user.userId,
                     KeyName.UserPseudo.rawValue: self.sessionManager.user.userPseudo]
        
        self.analyticsManager.trackAction(actionName, params: param)
    }
    
}
