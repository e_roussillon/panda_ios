//
//  TabBarFilterViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/28/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

typealias HandlerAlert = (_ filterModel: FilterModel?, _ alertController: UIAlertController, _ alertAction: UIAlertAction) -> Void;

class TabBarFilterViewController: BaseTabBarViewController {
    
    static let ADD_FILTER = "ADD_FILTER"
    static let UPDATE_FILTER = "UPDATE_FILTER"
    static let DELETE_FILTER = "DELETE_FILTER"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initStyle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
  
    //
    //MARK: Private
    //
    
    fileprivate func initStyle() {
        self.tabBar.shadowImage = UIColor.UIColorToImage(Constants.COLOR.kTabbarShadow, size: CGSize(width: self.tabBar.frame.size.width, height: 1.0))
        let image = UIImageView(image: UIColor.UIColorToImage(Constants.COLOR.kTabbarTint, size: self.tabBar.frame.size))
        self.tabBar.insertSubview(image, at: 0)
    }
    
    //
    //MARK: Alert Filter
    //
    
    static func showAlertAddFilter(_ isHighlight: Bool, handlerOK: @escaping HandlerAlert, handlerCancel:@escaping HandlerAlert) {
        let msg = isHighlight ? NSLocalizedString("dialog_filter_message_want", comment: "") : NSLocalizedString("dialog_filter_message_not_want", comment: "")
        
        let alertController = AlertUtil.initAlertController(NSLocalizedString("dialog_add_filter_title", comment: ""), message: msg)

        alertController.addAction(UIAlertAction(title: AlertUtil.buttonOk, style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in
            handlerOK(nil, alertController, alertAction)
        }))
        
        alertController.addAction(UIAlertAction(title: AlertUtil.buttonCancel, style: UIAlertActionStyle.cancel, handler: { (alertAction) -> Void in
            handlerCancel(nil, alertController, alertAction)
        }))
        
        alertController.addTextField { (textField) -> Void in
            textField.placeholder =  NSLocalizedString("dialog_add_filter_title", comment: "")
        }
        
        AlertUtil.launchAlert(alertController)
    }
    
    static func showAlertUpdateFilter(_ filterModel: FilterModel, handlerOK: @escaping HandlerAlert, handlerCancel:@escaping HandlerAlert) {
     
        let msg = filterModel.isHighlight! ? NSLocalizedString("dialog_filter_message_want", comment: "") : NSLocalizedString("dialog_filter_message_not_want", comment: "")
        
        let alertController = AlertUtil.initAlertController(NSLocalizedString("dialog_update_filter_title", comment: ""), message: msg)
        
        alertController.addAction(UIAlertAction(title: AlertUtil.buttonOk, style: UIAlertActionStyle.default, handler: { (alertAction) -> Void in
            handlerOK(filterModel, alertController, alertAction)
        }))
        
        alertController.addAction(UIAlertAction(title: AlertUtil.buttonCancel, style: UIAlertActionStyle.cancel, handler: { (alertAction) -> Void in
            handlerCancel(filterModel, alertController, alertAction)
        }))
        
        alertController.addTextField { (textField) -> Void in
            textField.placeholder =  NSLocalizedString("dialog_update_filter_title", comment: "")
            textField.text = filterModel.name ?? ""
        }
        
        AlertUtil.launchAlert(alertController)
    }
}
