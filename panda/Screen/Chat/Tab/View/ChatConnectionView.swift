//
//  ChatConnectionView.swift
//  panda
//
//  Created by Edouard Roussillon on 3/28/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import Cartography

class ChatConnectionView: BaseSimpleEmptyStateView {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //
    // MARK: Init
    //
    
    override func initLocalizedString() {
        self.labelTitle.text = NSLocalizedString("chat_connection_title", comment: "")
        self.button.setTitle(NSLocalizedString("generic_label_retry_button", comment: ""), for: UIControlState())
        self.labelSubtitle.alpha = 0
        self.labelSubtitle.text = NSLocalizedString("chat_connection_retry_timer", comment: "").replacingOccurrences(of: "%1", with: "")
    }
    
    override func click() {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork() {
            self.button.isEnabled = false
            let _ = SocketManager.sharedInstance.startSocket()
        } else {
            AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_error_server", comment: ""))
        }
        
    }
    
}
