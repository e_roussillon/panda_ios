//
//  TabBarChatViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit
import CoreLocation

class TabBarChatViewController: BaseTabBarViewController {
    
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let chatManager = ChatManager.sharedInstance
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let gspManager = GPSManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initStyle()
        self.initListener()
        self.getMessagesCount()
        self.getLocalisation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.sessionManager.user.isLogin && self.gspManager.getGPSState() != GPSState.gotIt {
            let _ = self.gspManager.getCurrentLocalisation()
        }
    }
    
    //
    //MARK: Int
    //
    
    fileprivate func initStyle() {
        self.tabBar.shadowImage = UIColor.UIColorToImage(Constants.COLOR.kTabbarShadow, size: CGSize(width: self.tabBar.frame.size.width, height: 1.0))
        let image = UIImageView(image: UIColor.UIColorToImage(Constants.COLOR.kTabbarTint, size: self.tabBar.frame.size))
        self.tabBar.insertSubview(image, at: 0)
    }
    
    fileprivate func initListener() {
        self.bag.add(self.socketNotifierManager.onNewPrivateMessageNotification.add(self, handler: TabBarChatViewController.onNewPrivateMessageNotification))
        self.bag.add(self.socketNotifierManager.onGetBackupMessagesNotification.add(self, handler: TabBarChatViewController.onGetBackupMessagesNotification))
    }
    
    func getLocalisation() {
        self.tabBar.items?[0].title = NSLocalizedString("general_screen_title", comment: "")
        self.tabBar.items?[1].title = NSLocalizedString("sale_screen_title", comment: "")
        self.tabBar.items?[2].title = NSLocalizedString("private_chat_screen_title", comment: "")
    }
    
    //
    // MARK: Bool
    //
    
    func getMessagesCount() {
        let count = PrivateChatModel.getMessagesCount()
        if count > 0 {
            self.tabBar.items![2].badgeValue = "\(count)"
        } else {
            self.tabBar.items![2].badgeValue = nil
        }
    }
    
    //
    //MARK: ListChatsViewController
    //
    
    func onNewPrivateMessageNotification(_ val: (isNewItem: Bool, message: PrivateMessageModel)) {
        self.getMessagesCount()
    }
    
    func onGetBackupMessagesNotification() {
        self.getMessagesCount()
    }
}
