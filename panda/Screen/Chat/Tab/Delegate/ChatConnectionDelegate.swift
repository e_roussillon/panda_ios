//
//  ChatConnectionDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 9/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class ChatConnectionDelegate: NSObject {
    
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let socketManager = SocketManager.sharedInstance
    fileprivate let bag = DisposableBag()
    
    @IBOutlet weak var baseChatViewController: BaseChatViewController!
    @IBOutlet weak var chatConnectionView: ChatConnectionView!
    
    //
    // MARK: Init
    //
    
    func initDelegate() {
        self.initNofif()
    }
    
    func deinitDelegate() {
        self.removeNotif()
        self.showView(false)
    }
    
    //
    // MARK: Notif
    //
    
    fileprivate func initConnected() {
        self.showView(!self.socketManager.isSocketConnected())
    }
    
    fileprivate func initNofif() {
        self.bag.add(self.socketNotifierManager.onUpdateTimerNotification.add(self, handler: ChatConnectionDelegate.onUpdateTimerNotification))
        self.bag.add(self.socketNotifierManager.onStopTimerNotification.add(self, handler: ChatConnectionDelegate.onStopTimerNotification))
    }
    
    fileprivate func removeNotif() {
        self.bag.dispose()
    }
    
    //
    // MARK: Handler
    //
    
    func onUpdateTimerNotification(_ time: Int) {
        self.sessionManager.keychainUtil.resetLocalisation()
        self.chatConnectionView.button.isEnabled = true
        
        if time >= 1800 {
            socketManager.forceStopSocket({
                UIView.animate(withDuration: 0.3, animations: {
                    self.chatConnectionView.labelSubtitle.alpha = 0
                    self.chatConnectionView.labelSubtitle.text = NSLocalizedString("chat_connection_retry_timer", comment: "").replacingOccurrences(of: "%1", with: "")
                })
            })
        } else {
            if time == 0 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.chatConnectionView.labelSubtitle.alpha = 0
                }) 
            } else {
                if self.chatConnectionView.labelSubtitle.alpha == 0 {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.chatConnectionView.labelSubtitle.alpha = 1
                    }) 
                } else if self.chatConnectionView.alpha == 0 {
                    self.showView(true)
                }

                self.chatConnectionView.labelSubtitle.text = NSLocalizedString("chat_connection_retry_timer", comment: "").replacingOccurrences(of: "%1", with: time.secondsToString())
            }
        }
        
    }
    
    func onStopTimerNotification() {
        self.chatConnectionView.button.isEnabled = true
        
        self.chatConnectionView.labelSubtitle.text = NSLocalizedString("chat_connection_retry_timer", comment: "").replacingOccurrences(of: "%1", with: "")
        self.showView(false)
        SocketManager.sharedInstance.joinRooms()
    }
    
    //
    // MARK: ChatGSPView
    //
    
    fileprivate func showView(_ show: Bool) {
        self.baseChatViewController.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            self.chatConnectionView.labelSubtitle.alpha = 0
            self.chatConnectionView.alpha = show ? 1 : 0
        }) 
    }
    
}
