//
//  ChatFindRoomDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 9/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ChatFindRoomDelegate: NSObject {
    
    fileprivate let chatManager = ChatManager.sharedInstance
    fileprivate let socketManager = SocketManager.sharedInstance
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let bag = DisposableBag()
    
    @IBOutlet weak var baseChatViewController: BaseChatViewController!
    @IBOutlet weak var chatFindRoomView: ChatFindRoomView!
    @IBOutlet weak var chatGPSView: ChatGPSView!
    @IBOutlet weak var publicListDelegate: PublicListDelegate!
    
    //
    // MARK: Init
    //
    
    func initRoom() {
        if self.sessionManager.user.isLogin && self.chatManager.currentRoomId > 0 {
            self.joinRooms()
        }
    }
    
    func initDelegate() {
        self.initNofif()
    }
    
    func deinitDelegate() {
        self.removeNotif()
    }
    
    
    //
    // MARK: Notif
    //
    
    fileprivate func initNofif() {
        self.bag.add(self.chatManager.onFindPublicRoomSucceed.add(self, handler: ChatFindRoomDelegate.onFindPublicRoomSucceed))
        self.bag.add(self.chatManager.onFindPublicRoomFailed.add(self, handler: ChatFindRoomDelegate.onFindPublicRoomFailed))
        
        self.bag.add(self.socketNotifierManager.onJoinPublicRoomSucceed.add(self, handler: ChatFindRoomDelegate.onJoinPublicRoomSucceed))
        self.bag.add(self.socketNotifierManager.onJoinPublicRoomFailed.add(self, handler: ChatFindRoomDelegate.onJoinPublicRoomFailed))
        
        self.bag.add(self.socketNotifierManager.onJoinedChannelGeneral.add(self, handler: ChatFindRoomDelegate.onJoinedChannelGeneral))
        self.bag.add(self.socketNotifierManager.onJoinedChannelSale.add(self, handler: ChatFindRoomDelegate.onJoinedChannelSale))
        
        self.bag.add(self.socketNotifierManager.onUpdateTimerNotification.add(self, handler: ChatFindRoomDelegate.onUpdateTimerNotification))
    }
    
    fileprivate func removeNotif() {
        self.bag.dispose()
    }
    
    //
    // MARK: Handler ChatManager
    //
    
    func onFindPublicRoomSucceed(_ findRooms: FindRoomsModel) {
        guard findRooms.rooms?.count > 0 else {
            self.showView(true)
            return
        }
        
        self.showViewWithTitle(NSLocalizedString("generic_room_connection", comment: ""))
    }
    
    func onFindPublicRoomFailed() {
        self.setTitleDefault()
        self.showView(true)
    }

    //
    // MARK: Handler SocketManager
    //
    
    func onJoinPublicRoomSucceed() {
        self.showView(false)
    }
    
    func onJoinPublicRoomFailed() {
        self.showViewWithTitle(NSLocalizedString("generic_gps_loogin_for", comment: ""))
    }
    
    //
    // MARK: Handler Join SocketManager
    //
    
    func onJoinedChannelGeneral() {
        if let name = self.chatManager.currentRoom?.originalName {
            self.baseChatViewController.navigationItem.title = name.capitalized
        } else {
            self.setTitleDefault()
        }
    }
    
    func onJoinedChannelSale() {
        if let name = self.chatManager.currentRoom?.originalName {
            self.baseChatViewController.navigationItem.title = name.capitalized
        } else {
            self.setTitleDefault()
        }
    }
    
    //
    // MARK: socketNotifierManager
    //
    
    func onUpdateTimerNotification(_ time: Int) {
        self.chatFindRoomView.alpha = 0
        self.chatGPSView.alpha = 0
    }
    
    //
    // MARK: ChatGSPView
    //
    
    fileprivate func showView(_ show: Bool) {
        self.baseChatViewController.view.endEditing(true)
        if show {
            self.setTitleDefault()
        }
        
        self.chatFindRoomView.labelTitle.text = NSLocalizedString("chat_not_mapped_title", comment: "")
        self.chatFindRoomView.button.isHidden = false
        self.chatFindRoomView.button.isEnabled = true
        UIView.animate(withDuration: 0.5, animations: {
            self.chatFindRoomView.alpha = show ? 1 : 0
            self.chatGPSView.alpha = 0
        }) 
    }
    
    fileprivate func showViewWithTitle(_ title: String) {
        if self.baseChatViewController == nil {
            return
        }
        
        self.baseChatViewController.view.endEditing(true)
        self.setTitleDefault()
        
        self.chatGPSView.labelTitle.text = title
        self.chatGPSView.button.isHidden = true
        UIView.animate(withDuration: 0.5, animations: {
            self.chatFindRoomView.alpha = 0
            self.chatGPSView.alpha = 1
        }) 
    }
    
    fileprivate func setTitleDefault() {
        if self.baseChatViewController is GeneralChatViewController {
            self.baseChatViewController.navigationItem.title = NSLocalizedString("general_screen_title", comment: "")
        } else {
            self.baseChatViewController.navigationItem.title = NSLocalizedString("sale_screen_title", comment: "")
        }
    }
    
    //
    // MARK: JOIN
    //
    
    func joinRooms() {
        self.socketManager.joinRooms()
    }
    
}
