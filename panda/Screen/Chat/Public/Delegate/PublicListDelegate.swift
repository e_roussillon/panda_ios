//
//  PublicListDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 3/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PublicListDelegate: BaseViewModel {

    var binder: BaseTableBinder?

    private weak var tableView: UITableView?
    private weak var baseViewControllerSegue: BaseViewControllerSegue?
    private var isGeneral: Bool = false

    func initBinder(tableView: UITableView, baseViewControllerSegue: BaseViewControllerSegue, isGeneral: Bool = false) {
        if self.binder == nil {
            self.tableView = tableView
            self.baseViewControllerSegue = baseViewControllerSegue
            self.isGeneral = isGeneral
            self.binder = BaseTableBinder(tableView: tableView)
            self.registerNibIntoTable()
            self.clickCellObserver()
        }

        self.updateList()
    }

    private func registerNibIntoTable() {
        guard let tableView = self.tableView else {
            return
        }

        PublicAdViewCell.registerNibIntoTableView(tableView)
        PublicViewCell.registerNibIntoTableView(tableView)
        WelcomePublicViewCell.registerNibIntoTableView(tableView)
    }


    func updateList() {
        Observable<[PublicMessageModel]>
                .just(isGeneral ? PublicMessageModel.getAllPublicMessageGeneral() : PublicMessageModel.getAllPublicMessageSale())
                .reduce([BaseTableListBinder]()) { (_, publicMessageModels) -> [BaseTableListBinder] in
                    var newList = [BaseTableListBinder]()
                    var items: [PublicMessageCellViewModel] = []

                    for (index, publicMessageModel) in publicMessageModels.enumerated() {
                        let parentPublicMessageModel: PublicMessageModel? = (index > 0) ? publicMessageModels[index-1] : nil
                        items = items + [PublicMessageCellViewModel(publicMessageModel: publicMessageModel, parentPublicMessageModel: parentPublicMessageModel)]
                    }

                    newList.append(BaseTableListBinder(header: BaseTableHeaderViewModel(), items: items))
                    return newList
                }
                .asSingle()
                .subscribe(onSuccess: { (baseTableListBinders) in
                    self.binder?.updateList(newList: baseTableListBinders, withAnimation: false)
                }, onError: { (error) in
                    print(error)
                })
                .disposed(by: disposeBag)
    }

    func updateListWithItem(publicMessageModel: PublicMessageModel) {
        let newItemNoParent = PublicMessageCellViewModel(publicMessageModel: publicMessageModel, parentPublicMessageModel: nil)
        let isItemFoundIndex = self.binder?.findItem(itemToFind: newItemNoParent) ?? -1

        self.binder?.list
                .take(1)
                .map {(list) -> PublicMessageModel in
                    if list.count <= 0 || (list.count > 0 && list[0].items.count <= 0) {
                        self.binder?.add(item: newItemNoParent)
                    } else if let oldItem = list[0].items[list[0].items.count - 1] as? PublicMessageCellViewModel, isItemFoundIndex <= -1 {
                        self.binder?.add(item: PublicMessageCellViewModel(publicMessageModel: publicMessageModel, parentPublicMessageModel: oldItem.publicMessageModel))
                    } else if let oldItem = list[0].items[isItemFoundIndex] as? PublicMessageCellViewModel {
                        oldItem.updateModel(publicMessageModel: publicMessageModel)
                        self.binder?.replace(item: oldItem, atPosition: isItemFoundIndex)
                    }
                    return publicMessageModel
                }
                .bind(onNext: { (item) in
                    let userId = item.fromUserId ?? -1
                    if self.sessionManager.user.userId == userId || self.tableView?.isAtBottom ?? false {
                        self.tableView?.scrollToBottom()
                    }
                })
    }


    private func clickCellObserver() {
//        self.binder?.didSelectRow
//                .map { (header, row) -> BaseTableCellViewModel in
//                    row
//                }
//                .bind { [weak self] row in
//                    guard let `self` = self else { return }
//
//                    if let item = row as? ListChatCellViewModel {
//                        self.baseViewControllerSegue?.launchSegue(Constants.SEGUE.OPEN_CONVERSATION, values: [item.privateChatModel])
//                    }
//                }
//                .disposed(by: disposeBag)
    }


//    
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        baseViewControllerSegue.launchSegue(Constants.SEGUE.HIDE_KEYBOARD, values: nil)
////        guard let message: PublicMessageModel = list[indexPath.row],
////            let type = message.status,
////            let _: Int = self.chatManager.currentRoomId else {
////            return
////        }
////        
////        switch type {
////        case .Error:
////            message.status = .Sending
//////            SocketUtil.sendMessage(channelType, roomId: "\(roomId)", publicMessage: message, callback: { (mesage, error) -> Void in
//////                if let publicMessage: PublicMessageModel = message {
//////                    self.list[indexPath.row] = publicMessage
//////                    tableView.beginUpdates()
//////                    tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
//////                    tableView.endUpdates()
//////                }
//////            })
////        case .Sending:
////            break
////        case .Sent:
////            break
////        default:
////            break
////        }
//        
//    }
}
