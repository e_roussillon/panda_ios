//
//  ChatGPSDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 9/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation

class ChatGPSDelegate: NSObject, ChatGPSViewDelegate {
    
    fileprivate let gpsManager = GPSManager.sharedInstance
    fileprivate let chatManager = ChatManager.sharedInstance
    fileprivate let bag = DisposableBag()
    
    @IBOutlet weak var baseChatViewController: BaseChatViewController!
    @IBOutlet weak var chatGPSView: ChatGPSView!
    
    //
    // MARK: Init
    //
    
    func initDelegate() {
        self.initNofif()
        self.setupView(self.gpsManager.getGPSState())
    }
    
    func deinitDelegate() {
        self.removeNotif()
    }
    
    //
    // MARK: Notif
    //
    
    fileprivate func initNofif() {
        self.bag.add(self.gpsManager.onGetLocalisation.add(self, handler: ChatGPSDelegate.onGetLocalisation))
        self.bag.add(self.gpsManager.onGetLocalisationDenied.add(self, handler: ChatGPSDelegate.onGetLocalisationDenied))
        self.bag.add(self.gpsManager.onGetLocalisationNotDetermined.add(self, handler: ChatGPSDelegate.onGetLocalisationNotDetermined))
        self.bag.add(self.gpsManager.onGetLocalisationRestricted.add(self, handler: ChatGPSDelegate.onGetLocalisationRestricted))
        self.bag.add(self.gpsManager.onGetLocalisationFail.add(self, handler: ChatGPSDelegate.onGetLocalisationFail))
    }
    
    fileprivate func removeNotif() {
        self.bag.dispose()
    }
    
    //
    // MARK: ChatGPSViewDelegate
    //
    
    func clickButton() {
        self.gpsManager.getLastLocalisation()
        self.showView(false)
    }
    
    //
    // MARK: Handler
    //
    
    func onGetLocalisation(_ loc: CLLocationCoordinate2D) { self.setupView(.gotIt) }
    
    func onGetLocalisationDenied() { self.setupView(.denied) }

    func onGetLocalisationNotDetermined() { self.setupView(.notDetermined) }

    func onGetLocalisationRestricted() { self.setupView(.restricted) }

    func onGetLocalisationFail(_ error: Error) { self.setupView(.failed) }
    
    fileprivate func setupView(_ gpsStatus: GPSState) {
        switch gpsStatus {
        case .gotIt:
            if let name = self.chatManager.currentRoom?.originalName {
                self.baseChatViewController.navigationItem.title = name.capitalized
            } else {
                self.setTitleDefault()
            }
            self.showView(false)
            break
        case .denied:
            self.setTitleDefault()
            self.showView(true)
            break
        case .notDetermined, .lookingFor:
            self.showView(true, title: NSLocalizedString("generic_gps_loogin_for", comment: ""), hiddenButton: true)
            break
        case .restricted:
            self.setTitleDefault()
            self.showView(true)
            break
        case .failed:
            self.setTitleDefault()
            self.showView(true)
            break
        }
    }
    
    fileprivate func setTitleDefault() {
        if self.baseChatViewController is GeneralChatViewController {
            self.baseChatViewController.navigationItem.title = NSLocalizedString("general_screen_title", comment: "")
        } else {
            self.baseChatViewController.navigationItem.title = NSLocalizedString("sale_screen_title", comment: "")
        }
    }
    
    
    //
    // MARK: ChatGSPView
    //
    
    fileprivate func showView(_ show: Bool, title: String = NSLocalizedString("chat_gps_title", comment: ""), hiddenButton: Bool = false) {
        if self.chatGPSView.labelTitle.text != title || self.chatGPSView.button.isHidden != hiddenButton || self.chatGPSView.alpha != (show ? 1 : 0) {
            self.baseChatViewController.view.endEditing(true)
            self.chatGPSView.labelTitle.text = title
            self.chatGPSView.button.isHidden = hiddenButton
            
            UIView.animate(withDuration: 0.5, animations: {
                self.chatGPSView.alpha = show ? 1 : 0
            })
        }
    }
    
    
}
