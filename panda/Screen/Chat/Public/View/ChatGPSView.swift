//
//  ChatGPSView.swift
//  panda
//
//  Created by Edouard Roussillon on 9/24/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation

protocol ChatGPSViewDelegate {
    func clickButton()
}

class ChatGPSView: BaseSimpleEmptyStateView {
    
    var chatGPSViewDelegate: ChatGPSViewDelegate?
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //
    // MARK: To Override
    //
    
    override func initLocalizedString() {
        super.initLocalizedString()
        
        self.labelTitle.text = NSLocalizedString("chat_gps_title", comment: "")
        self.button.setTitle(NSLocalizedString("generic_label_retry_button", comment: ""), for: UIControlState())
    }
    
    override func click() {
        if ReachabilityManager.sharedInstance.isConnectedToNetwork() {
            super.click()
            if let deg = self.chatGPSViewDelegate {
                deg.clickButton()
            }
        } else {
            AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_error_server", comment: ""))
        }
    }
}
