//
// Created by Edouard Roussillion on 11/19/17.
// Copyright (c) 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class PublicMessageCellViewModel : BaseTableCellViewModel {

    static let minHeight: CGFloat = 25.0
    static let minHeightSmall: CGFloat = 19.0

    static let fontHighlight: UIFont = UIFont.initHelveticaBold(16)
    static let fontNormal:UIFont = UIFont.initHelvetica(17)
    static let fontDate:UIFont = UIFont.initHelveticaLight(14)
    static let fontUser:UIFont = UIFont.initHelveticaMedium(16)

    static let colorHighlight:UIColor = Constants.COLOR.kTextColorHighlight
    static let colorNormal:UIColor = Constants.COLOR.kTextColorNormal
    static let colorDate:UIColor = Constants.COLOR.kTextColorDate

    static let colorBorderSending = Constants.COLOR.kBorderColorSending
    static let colorBorderError = Constants.COLOR.kBorderColorError


    private(set) var publicMessageModel: PublicMessageModel
    let parentPublicMessageModel: PublicMessageModel?

    init(publicMessageModel: PublicMessageModel, parentPublicMessageModel: PublicMessageModel?) {
        self.publicMessageModel = publicMessageModel
        self.parentPublicMessageModel = parentPublicMessageModel
        super.init()
        self.identity = self.publicMessageModel.index ?? ""
    }

    func updateModel(publicMessageModel: PublicMessageModel) {
        self.publicMessageModel = publicMessageModel
    }

    override func equalTo(rhs: BaseTableCellViewModel) -> Bool {
        return (rhs as? PublicMessageCellViewModel).map { b in
            return self.identity == b.identity && self.publicMessageModel == b.publicMessageModel
        } ?? false
    }

    override public var identity: String {
        set { super.identity = newValue }
        get { return super.identity }
    }

    override func initCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let type = publicMessageModel.type ?? DataType.none
        let isWelcomeMessage = publicMessageModel.isWelcomeMessage ?? false

        switch type {
        case .url:
            let cell = PublicAdViewCell.initCell(tableView, indexPath: indexPath, objects: [self])
            cell.accessoryType = UITableViewCellAccessoryType.detailButton
            return cell
        default:
            if isWelcomeMessage {
                return WelcomePublicViewCell.initCell(tableView, indexPath: indexPath, objects: [self])
            } else {
                return PublicViewCell.initCell(tableView, indexPath: indexPath, objects: [self])
            }
        }
    }

    override func initCell(tableView: UITableView) -> UITableViewCell? {
        let type = publicMessageModel.type ?? DataType.none
        let isWelcomeMessage = publicMessageModel.isWelcomeMessage ?? false

        switch type {
        case .url:
            return PublicAdViewCell.initCell(tableView, objects: [self])
        default:
            if isWelcomeMessage {
                return WelcomePublicViewCell.initCell(tableView, objects: [self])
            } else {
                return PublicViewCell.initCell(tableView, objects: [self])
            }
        }
    }

    func message() -> String {
        return self.publicMessageModel.msg ?? ""
    }

    func status() -> StatusType {
        return publicMessageModel.status ?? StatusType.error
    }

    func isFullMessage() -> Bool {
        return isWelcomeMessage() || userId() != userIdParent()
    }

    func isParentMessageInit() -> Bool {
        return self.parentPublicMessageModel != nil
    }

    func isGeneral() -> Bool {
        return self.publicMessageModel.isGeneral ?? false
    }

    func index() -> String {
      return publicMessageModel.index ?? ""
    }

    func userId() -> Int {
      return publicMessageModel.fromUserId ?? -1
    }

    func date() -> String {
      return (publicMessageModel.insertedAt ?? Date()).getDateMessage()
    }

    func userName() -> String {
      return publicMessageModel.fromUserName ?? ""
    }

    func text() -> String {
        if isFullMessage() {
            return "[ \(date()) - \(userName()) ] \(msg())"
        } else {
            return msg()
        }
    }

    func msg() -> String {
        return publicMessageModel.msg ?? ""
    }

    func isHighlight() -> Bool {
      return publicMessageModel.isHighlight ?? false
    }

    func isWelcomeMessage() -> Bool {
      return parentPublicMessageModel?.isWelcomeMessage ?? false
    }

    func indexParent() -> String {
      return parentPublicMessageModel?.index ?? ""
    }

    func userIdParent() -> Int {
      return parentPublicMessageModel?.fromUserId ?? -1
    }

    func range() -> NSRange {
        return NSMakeRange(0, text().length())
    }

    func color() -> UIColor {
        return isHighlight() ? PublicMessageCellViewModel.colorHighlight : PublicMessageCellViewModel.colorNormal
    }

    func font() -> UIFont {
        return isHighlight() ? PublicMessageCellViewModel.fontHighlight : PublicMessageCellViewModel.fontNormal
    }

    func rangeDate() -> NSRange {
        return NSMakeRange(0, date().length() + 5)
    }

    func rangeUser() -> NSRange {
        return NSMakeRange(rangeDate().length, userName().length())
    }

    func rangeUserTail() -> NSRange {
        return NSMakeRange(rangeDate().length + rangeUser().length, 3)
    }

    func rangeMsg() -> NSRange {
        return NSMakeRange(rangeDate().length + rangeUser().length + rangeUserTail().length, msg().length())
    }


}
