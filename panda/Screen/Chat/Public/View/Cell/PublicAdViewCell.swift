//
//  PublicAdViewCell.swift
//  panda
//
//  Created by Edouard Roussillon on 11/11/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Foundation

class PublicAdViewCell: UITableViewCell {
    
//    fileprivate static let minHeight: CGFloat = 50.0
    
    @IBOutlet weak var labelMessage: UILabel!
    
//    static func sizeWithObjects(_ objects: [AnyObject]?) -> CGSize {
//        guard let publicMessage = objects?.first as? PublicMessageModel else {
//            return CGSize(width: ScreenSize.SCREEN_WIDTH, height: minHeight)
//        }
//
//        let dinamicMessage: UILabel! = UILabel(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH - 30, height: CGFloat.greatestFiniteMagnitude))
//        dinamicMessage.numberOfLines = 0
//        dinamicMessage.lineBreakMode = NSLineBreakMode.byWordWrapping
//
//        dinamicMessage.text = publicMessage.msg ?? ""
//        dinamicMessage.sizeToFit()
//
//        return CGSize(width: ScreenSize.SCREEN_WIDTH, height: max(minHeight, dinamicMessage.frame.height + 10))
//    }
    
    override func configureCell(_ objects: [Any]?) {
        guard let publicMessageCellViewModel = objects?.first as? PublicMessageCellViewModel else {
            return
        }
        
        self.labelMessage.text = publicMessageCellViewModel.message()
    }
    
}
