//
//  WelcomePublicViewCell.swift
//  panda
//
//  Created by Edouard Roussillon on 3/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class WelcomePublicViewCell: UITableViewCell {
    
    fileprivate static let minHeight: CGFloat = 25.0
    @IBOutlet weak var labelMessage: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        self.contentView.layoutIfNeeded()
        self.labelMessage.preferredMaxLayoutWidth = self.labelMessage.frame.size.width
    }

    override func configureCell(_ objects: [Any]?) {
        guard let publicMessageCellViewModel = objects?.first as? PublicMessageCellViewModel else {
            return
        }

        self.labelMessage.text = publicMessageCellViewModel.message()
    }
}
