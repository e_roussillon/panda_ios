//
//  PublicViewCell.swift
//  panda
//
//  Created by Edouard Roussillon on 3/25/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import TTTAttributedLabel

class PublicViewCell: UITableViewCell, TTTAttributedLabelDelegate {

    fileprivate var publicMessageCellViewModel: PublicMessageCellViewModel!

    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var labelMessage: TTTAttributedLabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        self.contentView.layoutIfNeeded()
        self.labelMessage.preferredMaxLayoutWidth = self.labelMessage.frame.size.width
    }

    override func configureCell(_ objects: [Any]?) {
        guard let publicMessageCellViewModel = objects?.first as? PublicMessageCellViewModel else {
            return
        }

        self.publicMessageCellViewModel = publicMessageCellViewModel

        self.labelMessage.numberOfLines = 0
        self.labelMessage.lineBreakMode = NSLineBreakMode.byWordWrapping


        if self.publicMessageCellViewModel.isFullMessage() {
            viewSeparator.isHidden = self.publicMessageCellViewModel.isWelcomeMessage()
            self.initFullText()
        } else {
            viewSeparator.isHidden = true
            self.initSmallText()
        }


        switch (self.publicMessageCellViewModel.status()) {
        case .error:
            self.initShadowError()
            break
        case .sending:
            self.initShadowSending()
            break
        default:
            self.initShadowSent()
            break
        }
    }

    fileprivate func initFullText() {

        let notifyingStr = NSMutableAttributedString(string: self.publicMessageCellViewModel.text())
        notifyingStr.beginEditing()
        notifyingStr.addAttribute(NSAttributedStringKey.foregroundColor, value: PublicMessageCellViewModel.colorDate, range: self.publicMessageCellViewModel.rangeDate())
        notifyingStr.addAttribute(NSAttributedStringKey.foregroundColor, value: PublicMessageCellViewModel.colorDate, range: self.publicMessageCellViewModel.rangeUserTail())

        notifyingStr.addAttribute(NSAttributedStringKey.font, value: PublicMessageCellViewModel.fontDate, range: self.publicMessageCellViewModel.rangeDate())
        notifyingStr.addAttribute(NSAttributedStringKey.font, value: PublicMessageCellViewModel.fontDate, range: self.publicMessageCellViewModel.rangeUserTail())

        notifyingStr.addAttribute(NSAttributedStringKey.foregroundColor, value: self.publicMessageCellViewModel.color(), range: self.publicMessageCellViewModel.rangeMsg())
        notifyingStr.addAttribute(NSAttributedStringKey.font, value: self.publicMessageCellViewModel.font(), range: self.publicMessageCellViewModel.rangeMsg())
        notifyingStr.endEditing()

        labelMessage.setText(notifyingStr)
        labelMessage.linkAttributes = [NSAttributedStringKey.foregroundColor: PublicMessageCellViewModel.colorDate, NSAttributedStringKey.font: PublicMessageCellViewModel.fontUser]
        labelMessage.activeLinkAttributes = [NSAttributedStringKey.foregroundColor: PublicMessageCellViewModel.colorNormal, NSAttributedStringKey.font: PublicMessageCellViewModel.fontUser]
        labelMessage.addLink(to: URL(string: "http://userId"), with: self.publicMessageCellViewModel.rangeUser())
        labelMessage.delegate = self
    }

    fileprivate func initSmallText() {
        let notifyingStr = NSMutableAttributedString(string: self.publicMessageCellViewModel.text())
        notifyingStr.beginEditing()
        notifyingStr.addAttribute(NSAttributedStringKey.foregroundColor, value: self.publicMessageCellViewModel.color(), range: self.publicMessageCellViewModel.range())
        notifyingStr.addAttribute(NSAttributedStringKey.font, value: self.publicMessageCellViewModel.font(), range: self.publicMessageCellViewModel.range())
        notifyingStr.endEditing()

        labelMessage.setText(notifyingStr)
    }

    fileprivate func initShadowSending() {
        self.warningView.isHidden = true
        self.rootView.layer.borderColor = UIColor.clear.cgColor
        self.rootView.layer.borderWidth = 0
        self.rootView.layer.cornerRadius = 0
    }

    fileprivate func initShadowSent() {
        self.warningView.isHidden = true
        self.rootView.layer.borderColor = UIColor.clear.cgColor
        self.rootView.layer.borderWidth = 0
        self.rootView.layer.cornerRadius = 0
    }

    fileprivate func initShadowError() {
        self.warningView.isHidden = false
        self.rootView.layer.borderColor = PublicMessageCellViewModel.colorBorderSending.cgColor
        self.rootView.layer.borderWidth = 1
        self.rootView.layer.cornerRadius = 3
    }

    //
    // MARK: TTTAttributedLabelDelegate
    //

    func attributedLabel(_ label: TTTAttributedLabel, didSelectLinkWith url: URL) {
        let action = self.publicMessageCellViewModel.isGeneral() ? ActionName.ClickUserFromGeneralChat : ActionName.ClickUserFromTradeChat

        let userId = self.publicMessageCellViewModel.userId()
        let userName = self.publicMessageCellViewModel.userName()

        let sessionManager = SessionManager.sharedInstance
        let param: [String: Any] = [KeyName.UserId.rawValue: "\(sessionManager.user.userId)",
                                    KeyName.UserPseudo.rawValue: sessionManager.user.userPseudo,
                                    KeyName.ToUserId.rawValue: "\(userId)",
                                    KeyName.ToUserPseudo.rawValue: userName]
        AnalyticsManager.sharedInstance.trackAction(action, params: param)
        UIViewController.launchProfileUser(userId, userName: userName)
    }

    //
    // MARK: Click
    //

    @IBAction func clickWarning(_ sender: AnyObject) {
//        SocketManager.sharedInstance.resendPublicMessage(self.publicMessage)
    }
}
