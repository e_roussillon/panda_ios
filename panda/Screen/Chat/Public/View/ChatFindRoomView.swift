//
//  ChatNotMappedView.swift
//  panda
//
//  Created by Edouard Roussillon on 3/29/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

protocol ChatFindRoomViewDelegate {
    func clickButton()
}

class ChatFindRoomView: BaseSimpleEmptyStateView {
    
    fileprivate let socketManager = SocketManager.sharedInstance
    fileprivate let sessionManager = SessionManager.sharedInstance
    fileprivate let chatManager = ChatManager.sharedInstance
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //
    // MARK: To Override
    //

    override func initLocalizedString() {
        super.initLocalizedString()
        
        self.labelTitle.text = NSLocalizedString("chat_not_mapped_title", comment: "")
        self.button.setTitle(NSLocalizedString("generic_label_retry_button", comment: ""), for: UIControlState())
    }
    
    override func click() {
        
        if ReachabilityManager.sharedInstance.isConnectedToNetwork() {
            super.click()
            self.button.isEnabled = false
            self.sessionManager.keychainUtil.resetLocalisation()
            self.socketManager.joinRooms()
        } else {
            AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_error_server", comment: ""))
        }
    }
    
}
