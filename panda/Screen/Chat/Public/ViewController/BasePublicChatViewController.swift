//
//  BasePublicChatViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 9/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class BasePublicChatViewController: BaseChatViewController {
    
    @IBOutlet weak var chatFindRoomDelegate: ChatFindRoomDelegate!
    @IBOutlet weak var chatGPSDelegate: ChatGPSDelegate!
    @IBOutlet weak var chatConnectionDelegate: ChatConnectionDelegate!
    @IBOutlet weak var publicListDelegate: PublicListDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initNotif()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        self.initCell()
//        self.publicListDelegate.baseViewControllerSegue = self
        
    }
    
    fileprivate func initNotif() {
        self.bag.add(self.socketNotifierManager.onNewGeneralMessageNotification.add(self, handler: BasePublicChatViewController.onNewGeneralMessageNotification))
        self.bag.add(self.socketNotifierManager.onNewSaleMessageNotification.add(self, handler: BasePublicChatViewController.onNewSaleMessageNotification))
        self.bag.add(self.socketNotifierManager.onNewBroadcastMessageNotification.add(self, handler: BasePublicChatViewController.onNewBroadcastMessageNotification))
        
        self.bag.add(self.socketNotifierManager.onFailGeneralMessageNotification.add(self, handler: BasePublicChatViewController.onFailGeneralMessageNotification))
        self.bag.add(self.socketNotifierManager.onFailSaleMessageNotification.add(self, handler: BasePublicChatViewController.onFailSaleMessageNotification))
        self.bag.add(self.socketNotifierManager.onFailBroadcastMessageNotification.add(self, handler: BasePublicChatViewController.onFailBroadcastMessageNotification))
    }
    
    fileprivate func initCell() {
        PublicViewCell.registerNibIntoTableView(self.tableView)
        PublicAdViewCell.registerNibIntoTableView(self.tableView)
        WelcomePublicViewCell.registerNibIntoTableView(self.tableView)
    }
    
    //
    // MARK: Observables
    //
    
    override func initObservables() {
        super.initObservables()
        
        self.baseTextChatView
            .sendDriver
            .drive(onNext: { [weak self] message in
                guard let `self` = self else { return }
                self.clickSend(message)
            })
            .disposed(by: disposeBag)
    }
    
    func clickSend(_ msg: String) {}
    
    //
    // MARK: New Message
    //
    
    func onNewGeneralMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    func onNewSaleMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    func onNewBroadcastMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    
    //
    // MARK: Fail Message
    //
    
    func onFailGeneralMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    func onFailSaleMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    func onFailBroadcastMessageNotification(_ publicMessageModel: PublicMessageModel) {}
    
    
    //
    // MARK: Keyboard
    //
    
    override func removeObservers() {
        super.removeObservers()
        
//        if self.constraintBottomTextChatView != nil {
//            self.constraintBottomTextChatView.constant = 0
//        }
    }
    
    //
    // MARK: Long press gesture
    //
    
//    override func handleCopyMessage(_ path: IndexPath) {
//        super.handleCopyMessage(path)
//        let cell: PublicMessageModel = publicListDelegate.list[path.row]
//        if cell.type == DataType.none {
//            let isWelcomeMessage = cell.isWelcomeMessage ?? false
//
//            if !isWelcomeMessage {
//                UIPasteboard.general.string = cell.msg
//                AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_copied", comment: ""))
//            }
//        }
//    }
    
    //
    // MARK: Analytics
    //
    
    func sendAnalytics(_ action: ActionName) {
        let param: [String: Any] = [KeyName.RoomId.rawValue: "\(self.chatManager.currentRoomId)",
                                          KeyName.RoomName.rawValue: self.sessionManager.keychainUtil.currentRoom?.originalName ?? "",
                                          KeyName.UserId.rawValue : "\(self.sessionManager.user.userId)",
                                          KeyName.UserPseudo.rawValue : self.sessionManager.user.userPseudo]
        self.analyticsManager.trackAction(action, params: param)
    }
}

