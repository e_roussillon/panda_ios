//
//  GeneralChatViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation
import CoreLocation

class GeneralChatViewController: BasePublicChatViewController {
    
    override var screenName: ScreenName? {
        if self.sessionManager.user.isLogin {
            return ScreenName.GeneralChat
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initLayout()
        self.chatFindRoomDelegate.initRoom()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.chatConnectionDelegate.initDelegate()
        self.chatFindRoomDelegate.initDelegate()
        self.chatGPSDelegate.initDelegate()
        self.loadCells()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.chatConnectionDelegate.deinitDelegate()
        self.chatFindRoomDelegate.deinitDelegate()
        self.chatGPSDelegate.deinitDelegate()
    }
    
    //
    // MARK: Init
    //
    
    fileprivate func initLayout() {
        self.channelType = ChannelType.Generic
        self.initLocalizedString()
    }
    
    fileprivate func initLocalizedString() {
        self.navigationItem.title = NSLocalizedString("general_screen_title", comment: "")
    }
    
    //
    // MARK: Send Button
    //
    
    override func clickSend(_ msg: String) {
        super.clickSend(msg)
        
        if self.chatManager.currentRoomId > -1 {
            self.sendAnalytics(ActionName.SendGeneralMessage)
            self.socketManager.sendPublicMessage(true, message: msg)
        } 
    }
    
    //
    //MARK: Query DB
    //
    
    fileprivate func loadCells() {
        self.publicListDelegate.initBinder(tableView: self.tableView, baseViewControllerSegue: self, isGeneral: true)
    }

    //
    // MARK: Add/Refresh item into list
    //

    override func onNewGeneralMessageNotification(_ publicMessageModel: PublicMessageModel) {
        self.publicListDelegate.updateListWithItem(publicMessageModel: publicMessageModel)
    }

    override func onNewBroadcastMessageNotification(_ publicMessageModel: PublicMessageModel) {
        self.publicListDelegate.updateListWithItem(publicMessageModel: publicMessageModel)
    }

    override func onFailGeneralMessageNotification(_ publicMessageModel: PublicMessageModel) {
        self.publicListDelegate.updateListWithItem(publicMessageModel: publicMessageModel)
    }
}
