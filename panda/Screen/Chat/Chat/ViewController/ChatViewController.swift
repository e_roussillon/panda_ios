//
//  ChatViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class ChatViewController: BaseChatViewController, GalleryDelegate {

    @IBOutlet weak var chatPrivateConnectionDelegate: ChatPrivateConnectionDelegate!
    @IBOutlet weak var baseGalleryDelegate: BaseGalleryDelegate!

    private let chatViewModel = ChatViewModel()

    var user: UserModel!
    var friendshipStatus: FriendshipType!
    var friendshipLastAction: Int!
    var removedSearchAfterSelecteChat: Bool = false
    
    var viewHasAppeared = false

    override var screenName: ScreenName? {
        return ScreenName.PrivateChat
    }

    override var paramsAnalytics: [String: Any] {
        return [KeyName.UserId.rawValue: self.user.userId ?? -1,
                KeyName.UserPseudo.rawValue: self.user.pseudo ?? ""]
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initNavigationBar()
        self.chatViewModel.initBinder(tableView: tableView, user: user, friendshipStatus: friendshipStatus, friendshipLastAction: friendshipLastAction)
        self.initGallery()
        self.updateBackOrCancelButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.bag.add(self.socketNotifierManager.onUpdateFriendship.add(self, handler: ChatViewController.onUpdateFriendship))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewHasAppeared = true
        self.baseTextChatView.isHiddenPicture = false

        if let index = self.navigationController?.viewControllers.index(where: { (viewController) -> Bool in
            return viewController is SearchViewController
        }), removedSearchAfterSelecteChat {
            self.navigationController?.viewControllers.remove(at: index)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.chatPrivateConnectionDelegate.deinitDelegate()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if !viewHasAppeared, tableView.dataSource?.numberOfSections?(in: tableView) ?? 0 > 0 {
            self.tableView.scrollToBottom(false)
        }
    }

    //
    // MARK: Init
    //

    fileprivate func initNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = self.user.pseudo ?? ""
    }

    fileprivate func initGallery() {
        self.baseGalleryDelegate.galleryDelegate = self
    }

    fileprivate func updateBackOrCancelButton() {
        self.chatViewModel.isLongPressMode.debounce(0.1).drive(onNext: { [weak self] isLongPress in
            guard let `self` = self else {
                return
            }

            if isLongPress {
                self.navigationItem.hidesBackButton = true
                let newBackButton = UIBarButtonItem(image: UIImage(named: "ic_close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatViewController.cancelEditionChat(sender:)))
                self.navigationItem.leftBarButtonItem = newBackButton
                self.hiddenToolBar(isHidden: false)
                self.hiddenTextChatToolbar(isHidden: true)
            } else {
                self.navigationItem.hidesBackButton = false
                self.navigationItem.leftBarButtonItem = nil
                self.hiddenToolBar(isHidden: true)
                self.hiddenTextChatToolbar(isHidden: false)
            }

        }).disposed(by: self.disposeBag)

        self.chatViewModel.cellSelected.debounce(0.1).drive(onNext: { [weak self] cellSelected in
            guard let `self` = self else {
                return
            }

            self.disableButtonToolBar(isHidden: cellSelected.count > 1)

        }).disposed(by: self.disposeBag)
    }

    //
    // MARK: Handler
    //

    func onUpdateFriendship(_ user: UserModel) {
        if let status = user.friendshipStatus, status == FriendshipType.blockedBeforeBeFriend || status == FriendshipType.blockedAfterBeFriend {
            AlertUtil.showTitleWithMessage(NSLocalizedString("dialog_title", comment: ""), message: NSLocalizedString("dialog_message_logout_close_chat", comment: ""), handlerOK: { (alert) in
                let _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }

    @objc func cancelEditionChat(sender: UIBarButtonItem? = nil) {
        self.chatViewModel.cleanSelectedRow()
    }

    //
    // MARK: Handler Image
    //

//    func onNewImageUploadingNotification(bytesWritten: Int64?, totalBytesWritten: Int64?, totalBytesExpected: Int64?, message: PrivateMessageModel) {
//        message.uploading = CGFloat(totalBytesWritten!) / CGFloat(totalBytesExpected!)
//        message.isFailed = false
//        let _ = self.chatDelegate.getItem(message, replace: true)
//    }
//
//    func onNewImageUploadingNotification(_ privateMessageModel: PrivateMessageModel) {
//        privateMessageModel.uploading = 0
//        privateMessageModel.isFailed = true
//        let _ = self.chatDelegate.getItem(privateMessageModel, replace: true)
//    }

    //
    // MARK: Observables
    //

    override func initObservables() {
        super.initObservables()

        self.baseTextChatView
                .sendDriver
                .drive(onNext: { [weak self] message in
                    guard let `self` = self else {
                        return
                    }
                    if self.sessionManager.user.isLogin {
                        self.socketManager.sendPrivateMessage(self.chatViewModel.roomId,
                                message: message,
                                image: nil,
                                friendshipStatus: self.friendshipStatus,
                                friendshipLastAction: self.friendshipLastAction,
                                toUser: self.user)
                    }
                })
                .disposed(by: disposeBag)

        self.baseTextChatView
                .pictureDriver
                .drive(onNext: { [weak self] in
                    guard let `self` = self else {
                        return
                    }

                    self.baseGalleryDelegate.showActionSheet()
                })
                .disposed(by: disposeBag)
    }

    //
    // MARK: Toobar
    //

    private func hiddenTextChatToolbar(isHidden: Bool) {
        self.baseTextChatView.isHiddenToolbar = isHidden
        self.baseTextChatView.isHiddenPicture = isHidden
    }

    private func hiddenToolBar(isHidden: Bool) {
        self.baseToolBarView.isHiddenToolbar = isHidden
        self.baseToolBarView.isHiddenCopy = isHidden
        self.baseToolBarView.isHiddenForward = isHidden
        self.baseToolBarView.isHiddenReply = isHidden
    }

    private func disableButtonToolBar(isHidden: Bool) {
        self.baseToolBarView.isHiddenCopy = isHidden
        self.baseToolBarView.isHiddenReply = isHidden
    }

    //
    // MARK: GalleryDelegate
    //

    func sendPicture(_ image: UIImage) {
        if self.sessionManager.user.isLogin {
            self.socketManager.sendPrivateMessage(self.chatViewModel.roomId,
                    message: "",
                    image: image,
                    friendshipStatus: self.friendshipStatus,
                    friendshipLastAction: self.friendshipLastAction,
                    toUser: self.user)
        }
    }

    func pictureToBig() {
        AlertUtil.showTitleWithMessage(message: NSLocalizedString("dialog_message_image_too_big", comment: ""))
    }

    func cancelSelectPicture() {

    }

    //
    // MARK: Long Press
    //

    override func handleLongPress(_ path: IndexPath) {
        self.chatViewModel.clickAsLongPress()
    }

    //
    // MARK: ButtonClickDelegate
    //
    override func buttonDeleteClicked() {
        super.buttonDeleteClicked()

        self.chatViewModel
                .cellSelected
                .drive(onNext: { [weak self] (cellSelected) in
                    guard let `self` = self else { return }

                    var listMessageIds: [Int] = []

                    for cell in cellSelected {
                        cell.deletingMessage()
                        listMessageIds.append(cell.messageId)

                        let section = ChatSectionViewBinder(date: cell.messageModel.insertedAt ?? Date())
                        self.chatViewModel.binder?.removeCellOrSection(item: cell, atSection: section)
                    }

                    self.socketManager.deletePrivateMessages(roomId: self.chatViewModel.roomId, listMessageIds: listMessageIds)
                    self.chatViewModel.cleanSelectedRow()
                })
                .dispose()
    }

    override func buttonCopyClicked() {
        super.buttonCopyClicked()
        
        self.chatViewModel
            .cellSelected
            .drive(onNext: { [weak self] (cellSelected) in
                guard let `self` = self, cellSelected.count > 0 else { return }
                UIPasteboard.general.string = cellSelected[0].message
                self.cancelEditionChat()
            })
            .dispose()
    }

    override func buttonReplyClicked() {
        super.buttonReplyClicked()
        
        self.chatViewModel
            .cellSelected
            .drive(onNext: { [weak self] (cellSelected) in
                guard let `self` = self else { return }
                print("")
            })
            .dispose()
    }

    override func buttonForwardClicked() {
        super.buttonForwardClicked()
        
        self.chatViewModel
            .cellSelected
            .drive(onNext: { [weak self] (cellSelected) in
                guard let `self` = self else { return }
                print("")
            })
            .dispose()
    }
}
