//
//  ChatSectionViewBinder.swift
//  panda
//
//  Created by Edouard Roussillion on 10/6/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

open class ChatSectionViewBinder : BaseTableHeaderViewModel {

    private let date: Date
    private let viewHeader = BaseLabelView()

    override public var identity: String {
        set { super.identity = newValue }
        get { return super.identity }
    }

    public init(date: Date) {
        self.date = date
        super.init()
        self.identity = self.date.getLongStyleDate()
    }
    
    public override func initView() -> UIView {
        viewHeader.text = self.date.getDateMessageSection()
        return viewHeader
    }
    
    public override func initHeight() -> CGFloat {
        return BaseLabelView.height
    }

}
