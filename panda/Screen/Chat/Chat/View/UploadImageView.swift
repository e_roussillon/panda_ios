//
//  UploadImageView.swift
//  panda
//
//  Created by Edouard Roussillon on 10/8/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import QuartzCore

class UploadImageView: UIView {
    
    var message: PrivateMessageModel!
    var size: CGFloat = 0.0
    var failUpload: Bool = false
    fileprivate let bag = DisposableBag()
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonDownload: UIView!
    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var progrationBar: UIView!
    
    
    @IBOutlet weak var constraintRightLabel: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightProgress: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("UploadImageView", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    deinit {
        self.bag.dispose()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
        
        self.initHandler()
        self.buttonDownload.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(UploadImageView.retryUpload)))
        self.buttonDownload.layer.cornerRadius = 8.0
        self.buttonDownload.layer.masksToBounds = true
        self.layer.cornerRadius = 8.0
        self.layer.masksToBounds = true
    }
    
    fileprivate func initHandler() {
        self.bag.add(self.socketNotifierManager.onNewImageUploadingNotification.add(self, handler: UploadImageView.onNewImageUploadingNotification))
        self.bag.add(self.socketNotifierManager.onNewPrivateMessageFailNotification.add(self, handler: UploadImageView.onNewImageUploadingNotification))
    }
    
    func loadImage(_ message: PrivateMessageModel) {
        self.message = message
        
        if let dataId = self.message.dataId, dataId > 0 {
            self.message.uploading = 1
            self.message.isFailed = false
        }
        
        self.size = self.message.uploading * 34
        self.failUpload = self.message.isFailed
        
        if self.failUpload == false && self.size == 0 {
            self.indicatorView.isHidden = false
            self.indicatorView.startAnimating()
            self.labelSize.text = ""
            self.constraintRightLabel.constant = 0
        } else if self.failUpload {
            self.indicatorView.isHidden = true
            self.indicatorView.stopAnimating()
            self.labelSize.text = NSLocalizedString("generic_label_upload_image", comment: "")
            self.constraintRightLabel.constant = 5
        } else {
            self.indicatorView.isHidden = true
            self.indicatorView.stopAnimating()
            self.labelSize.text = ""
            self.constraintRightLabel.constant = 0
        }

        if let dataLocal = message.dataLocal, dataLocal.length() > 0 {
            imageView.image = UIImage(localName: dataLocal)
        } else {
            imageView.image = UIImage(named: "logo_panda")
        }
        
        self.constraintHeightProgress.constant = self.size
        print("size -> \(self.size)")
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        })
    }
    
    @objc func retryUpload() {
        if self.failUpload {
            self.message.uploading = 0
            self.message.isFailed = false
            SocketManager.sharedInstance.resendPrivateMessages(self.message)
            self.labelSize.text = ""
            self.constraintRightLabel.constant = 0
            self.failUpload = false
            self.size = 0
            self.constraintHeightProgress.constant = self.size
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            })
        }
    }
    
    //
    // MARK: Hanlder
    //
    
    func onNewImageUploadingNotification(_ val: (bytesWritten: Int64?, totalBytesWritten: Int64?, totalBytesExpected: Int64?, message: PrivateMessageModel)) {
        if let dataLocal = message.dataLocal, let dataLocal2 = self.message.dataLocal, dataLocal == dataLocal2 {
            self.message.uploading = CGFloat(val.totalBytesWritten!) / CGFloat(val.totalBytesExpected!)
            self.message.isFailed = false
            self.indicatorView.isHidden = true
            self.indicatorView.stopAnimating()
            self.labelSize.text = ""
            self.constraintRightLabel.constant = 0
            self.failUpload = false
            self.size = self.message.uploading * self.buttonDownload.frame.height
            self.constraintHeightProgress.constant = self.size
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            })
        }
    }
    
    func onNewImageUploadingNotification(_ privateMessageModel: PrivateMessageModel) {
        self.message.uploading = 0.0
        self.message.isFailed = true
        self.labelSize.text = NSLocalizedString("generic_label_upload_image", comment: "")
        self.constraintRightLabel.constant = 5
        self.failUpload = true
        self.size = 0
        self.constraintHeightProgress.constant = self.size
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        })
    }
}
