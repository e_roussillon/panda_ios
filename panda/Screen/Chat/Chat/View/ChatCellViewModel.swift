//
//  ChatCellViewModel.swift
//  panda
//
//  Created by Edouard Roussillion on 10/6/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

public class ChatCellViewModel: BaseTableCellViewModel {
    private let privateMessageModel: PrivateMessageModel
    var scrollToBottom: Bool = false
    var isSelected: Bool = false {
        didSet {
            selectedSubject.onNext(self.isSelected ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none)
        }
    }

    override func equalTo(rhs: BaseTableCellViewModel) -> Bool {
        return (rhs as? ChatCellViewModel).map { b in
            return self.identity == b.identity && self.privateMessageModel === b.privateMessageModel
            } ?? false
    }

    init(privateMessageModel: PrivateMessageModel) {
        self.privateMessageModel = privateMessageModel
        super.init()
        self.identity = self.privateMessageModel.index ?? ""
    }

    override public var identity: String {
        set { super.identity = newValue }
        get { return super.identity }
    }

    override public func initCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let userId = sessionManager.user.userId

        var cell: UITableViewCell

        if self.privateMessageModel.fromUserId == userId {
            guard let _ = self.privateMessageModel.messageId, let status = self.privateMessageModel.status else {
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatSendingCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatSendingWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
                return cell
            }

            switch status {
            case .sending:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatSendingCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatSendingWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
            case .sent:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatSentCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatSentWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
            case .received:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatSendReceivedCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatSendReceivedWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
            case .read, .confirmed:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatReadCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatReadWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
            case .deleting, .deleted:
                cell = ChatReceivedCell.initCell(tableView, indexPath: indexPath, objects: [self])
            case .error:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    cell = ChatSendingCell.initCell(tableView, indexPath: indexPath, objects: [self])
                } else {
                    cell = ChatSendingWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
                }
            }
        } else {
            if let type = self.privateMessageModel.type, type == DataType.none {
                cell = ChatReceivedCell.initCell(tableView, indexPath: indexPath, objects: [self])
            } else {
                cell = ChatReceivedWithPictureCell.initCell(tableView, indexPath: indexPath, objects: [self])
            }
        }
        
        return cell
    }

    override public func initCell(tableView: UITableView) -> UITableViewCell? {
        let userId = sessionManager.user.userId

        if self.privateMessageModel.fromUserId == userId {
            guard let _ = self.privateMessageModel.messageId, let status = self.privateMessageModel.status else {
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatSendingCell.initCell(tableView, objects: [self])
                } else {
                    return ChatSendingWithPictureCell.initCell(tableView, objects: [self])
                }
            }

            switch status {
            case .sending:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatSendingCell.initCell(tableView, objects: [self])
                } else {
                    return ChatSendingWithPictureCell.initCell(tableView, objects: [self])
                }
            case .sent:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatSentCell.initCell(tableView, objects: [self])
                } else {
                    return ChatSentWithPictureCell.initCell(tableView, objects: [self])
                }
            case .received:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatSendReceivedCell.initCell(tableView, objects: [self])
                } else {
                    return ChatSendReceivedWithPictureCell.initCell(tableView, objects: [self])
                }
            case .read, .confirmed:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatReadCell.initCell(tableView, objects: [self])
                } else {
                    return ChatReadWithPictureCell.initCell(tableView, objects: [self])
                }
            case .deleting, .deleted:
                return ChatReceivedCell.initCell(tableView, objects: [self])
            case .error:
                if let type = self.privateMessageModel.type, type == DataType.none {
                    return ChatSendingCell.initCell(tableView, objects: [self])
                } else {
                    return ChatSendingWithPictureCell.initCell(tableView, objects: [self])
                }
            }
        } else {
            if let type = self.privateMessageModel.type, type == DataType.none {
                return ChatReceivedCell.initCell(tableView, objects: [self])
            } else {
                return ChatReceivedWithPictureCell.initCell(tableView, objects: [self])
            }
        }
    }

    //
    // MARK: Provider
    //

    lazy var message: String = { [unowned self] in
        self.privateMessageModel.msg ?? ""
    }()

    lazy var timestamp: String = { [unowned self] in
        self.privateMessageModel.insertedAt?.getShortTimeString() ?? ""
    }()

    lazy var messageModel: PrivateMessageModel = { [unowned self] in
        self.privateMessageModel
    }()

    lazy var isFromMe: Bool = { [unowned self] in
        self.privateMessageModel.fromUserId == SessionManager.sharedInstance.user.userId
    }()

    lazy var messageId: Int = { [unowned self] in
        self.privateMessageModel.messageId ?? -1
    }()
    
    lazy var status: StatusType = { [unowned self] in
        self.privateMessageModel.status ?? StatusType.error
        }()

    func deletingMessage() {
        self.privateMessageModel.status = StatusType.deleting
        self.privateMessageModel.updateStatus()
    }
    
    private let selectedSubject: PublishSubject<UITableViewCellAccessoryType> = PublishSubject()
    lazy var selected: Driver<UITableViewCellAccessoryType> = { [unowned self] in
        self.selectedSubject.asDriver(onErrorJustReturn: self.isSelected ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none)
    }()
}
