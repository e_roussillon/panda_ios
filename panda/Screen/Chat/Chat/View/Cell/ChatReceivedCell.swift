//
//  ChatReceivedCell.swift
//  panda
//
//  Created by Edouard Roussillon on 5/7/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class ChatReceivedCell: BaseChatCell {

    override func configureCell(_ objects: [Any]?) {
        super.configureCell(objects)

        if chatCellViewModel.status == StatusType.deleting ||  chatCellViewModel.status == StatusType.deleted {
            messageLabel.text = "This message was deleted"
            messageLabel.font = UIFont.initHelveticaLightItalic(17)
        } else {
            messageLabel.font = UIFont.initHelveticaLight(17)
        }

    }
}
