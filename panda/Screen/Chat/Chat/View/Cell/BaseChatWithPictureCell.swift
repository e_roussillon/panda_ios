//
//  BaseChatWithPictureCell.swift
//  panda
//
//  Created by Edouard Roussillon on 10/5/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import AlamofireImage

class BaseChatWithPictureCell: BaseChatCell {
    
    @IBOutlet weak var imageSend: ImageLoaderView!
    @IBOutlet weak var heightImageConstraint: NSLayoutConstraint!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.heightImageConstraint.constant = self.imageSend.frame.width
    }
    
    override func configureCell(_ objects: [Any]?) {
        super.configureCell(objects)
        
        guard let chatCellViewModel = objects?.first as? ChatCellViewModel else {
            return
        }
        
        self.imageSend.loadImage(chatCellViewModel.messageModel)
        self.messageLabel.text = chatCellViewModel.message
        self.timeLabel.text = chatCellViewModel.timestamp
    }
}
