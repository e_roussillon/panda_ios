//
//  BaseChatCell.swift
//  panda
//
//  Created by Edouard Roussillion on 10/14/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class BaseChatCell: UITableViewCell {
 
    private(set) var disposeBag = DisposeBag()
    private(set) var chatCellViewModel: ChatCellViewModel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        self.contentView.layoutIfNeeded()
        self.messageLabel.preferredMaxLayoutWidth = self.contentView.frame.width - 90
    }

    override func configureCell(_ objects: [Any]?) {
        guard let chatCell = objects?.first as? ChatCellViewModel else {
            self.messageLabel.text = ""
            return
        }

        self.contentView.backgroundColor = UIColor.red

        chatCellViewModel = chatCell
        
        self.messageLabel.text = chatCellViewModel.message
        self.timeLabel.text = chatCellViewModel.timestamp
//        chatCellViewModel
//            .selected
//            .drive(onNext: { [weak self](type) in
//                self?.accessoryType = type
//            })
//            .disposed(by: self.disposeBag)
    }
}
