//
//  ImageLoaderView.swift
//  panda
//
//  Created by Edouard Roussillon on 10/8/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import Cartography
import AlamofireImage
import QuartzCore

class ImageLoaderView: UIView {
    
    let downloader = ImageDownloader()
    var message: PrivateMessageModel!
    var request: AlamofireImage.RequestReceipt?
    var size: CGFloat = 0.0
    
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet var rootView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonDownload: UIView!
    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var progrationBar: UIView!
    @IBOutlet weak var constraintHeightProgress: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xib is link with rootView, we have to add it to super View
        Bundle.main.loadNibNamed("ImageLoaderView", owner: self, options: nil)
        
        self.backgroundColor = UIColor.clear
        self.addSubview(self.rootView)
        
        self.initLayout()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initLayout() {
        constrain(self.rootView) { view in
            view.left == view.superview!.left
            view.right == view.superview!.right
            view.top == view.superview!.top
            view.bottom == view.superview!.bottom
        }
        
        self.buttonDownload.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ImageLoaderView.downloadImage)))
        
        self.buttonDownload.layer.cornerRadius = 8.0
        self.buttonDownload.layer.masksToBounds = true
        self.layer.cornerRadius = 8.0
        self.layer.masksToBounds = true
    }
    
    func loadImage(_ message: PrivateMessageModel) {
        self.message = message
        
        self.initSizeImage()
        
        
        self.buttonDownload.isHidden = true
        self.indicatorView.isHidden = true
        self.indicatorView.stopAnimating()
        
        
        
        if let dataLocal = message.dataLocal, dataLocal.length() > 0 {
            imageView.image = UIImage(localName: dataLocal)
        } else if let urlOriginal = message.urlOriginal, let image = urlOriginal.isCached() {
            imageView.image = image
        } else if let urlBlur = message.urlBlur, let image = urlBlur.isCached() {
            imageView.image = image
            self.buttonDownload.isHidden = false
        } else if let urlBlur = message.urlBlur {
            imageView.af_setImage(withURL: urlBlur, placeholderImage: UIImage(named: "logo_panda"))
            self.buttonDownload.isHidden = false
        } else {
            imageView.image = UIImage(named: "logo_panda")
        }
        
        
        
    }
    
    fileprivate func initSizeImage() {
        if let size = self.message.size {
            self.labelSize.text = "\(String(format: "%.2f", size)) kB"
        } else {
            self.labelSize.text = ""
        }
    }
    
    @objc func downloadImage() {
        //DO not thing it was use  with swift 2.3
//        if let resq = self.request {
//            self.indicatorView.hidden = true
//            self.indicatorView.stopAnimating()
//            downloader.cancelRequestForRequestReceipt(resq)
//            self.request = nil
//            self.constraintHeightProgress.constant = 0
//            UIView.animateWithDuration(0.3, animations: {
//                self.layoutIfNeeded()
//                self.initSizeImage()
//            })
//            return
//        }
        
        if let urlOriginal = self.message.urlOriginal {
            self.indicatorView.isHidden = false
            self.indicatorView.startAnimating()
            self.labelSize.text = ""
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            })
            
            self.imageView.af_setImage(withURL: urlOriginal, placeholderImage: self.imageView.image, progress: { (progress) in
                
                self.indicatorView.isHidden = true
                self.indicatorView.stopAnimating()
                
                self.size = CGFloat(progress.completedUnitCount) / CGFloat(progress.totalUnitCount) * self.buttonDownload.frame.height
                self.constraintHeightProgress.constant = self.size
                UIView.animate(withDuration: 0.3, animations: {
                    self.layoutIfNeeded()
                })
                
            }, completion: { (response) in
                if let _ = response.result.value {
                    self.buttonDownload.isHidden = true
                    self.request = nil
                    self.size = 0.0
                }
            })
        }
        
    }
}
