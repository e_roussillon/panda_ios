//
//  ChatViewModel.swift
//  panda
//
//  Created by Edouard Roussillion on 10/6/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class ChatViewModel: BaseViewModel {

    var binder: BaseTableBinder?

    private let socketManager: SocketManager = SocketManager.sharedInstance
    private weak var tableView: UITableView?
    var roomId: Int!
    private var user: UserModel!
    private var friendshipStatus: FriendshipType!
    private var friendshipLastAction: Int!

    private let isLongPressModeSubject: Variable<Bool> = Variable(false)
    var isLongPressMode: Driver<Bool>!

    private let cellSelectedSubject: Variable<[ChatCellViewModel]> = Variable([])
    var cellSelected: Driver<[ChatCellViewModel]>!

    //
    //MARK Init
    //

    func initBinder(tableView: UITableView, user: UserModel!, friendshipStatus: FriendshipType!, friendshipLastAction: Int) {
        self.tableView = tableView
        self.user = user
        self.friendshipStatus = friendshipStatus
        self.friendshipLastAction = friendshipLastAction

        self.isLongPressMode = self.isLongPressModeSubject.asDriver()
        self.cellSelected = self.cellSelectedSubject.asDriver()

        self.binder = BaseTableBinder(tableView: tableView)
        self.registerNibIntoTable()
        self.initRoom()

        self.binder?.list.subscribe({ (listEvent) in
            guard let list = listEvent.element, list.count > 0 else {
                return
            }

            let newList: BaseTableListBinder = list[list.count - 1]
            let item: BaseTableCellViewModel = newList.items[newList.items.count - 1]

            if item is ChatCellViewModel {
                if (item as! ChatCellViewModel).scrollToBottom {
                    (item as! ChatCellViewModel).scrollToBottom = false
                    self.tableView?.scrollToBottom(true)
                }
            }
        }).disposed(by: disposeBag)
        self.clickOnRow()

        self.bag.add(self.socketNotifierManager.onNewPrivateMessageNotification.add(self, handler: ChatViewModel.onNewPrivateMessageNotification))
        self.bag.add(self.socketNotifierManager.onGetBackupMessagesNotification.add(self, handler: ChatViewModel.onGetBackupMessagesNotification))
        self.bag.add(self.socketNotifierManager.onNewImageUploadingNotification.add(self, handler: ChatViewModel.onNewImageUploadingNotification))
        self.bag.add(self.socketNotifierManager.onDeletePrivateMessageNotification.add(self, handler: ChatViewModel.onDeletePrivateMessageNotification))
        self.bag.add(self.socketNotifierManager.onHiddenPrivateMessageNotification.add(self, handler: ChatViewModel.onHiddenPrivateMessageNotification))
    }

    private func registerNibIntoTable() {
        guard let tableView = self.tableView else {
            return
        }

        ChatReadCell.registerNibIntoTableView(tableView)
        ChatReadWithPictureCell.registerNibIntoTableView(tableView)

        ChatSendingCell.registerNibIntoTableView(tableView)
        ChatSendingWithPictureCell.registerNibIntoTableView(tableView)

        ChatSentCell.registerNibIntoTableView(tableView)
        ChatSentWithPictureCell.registerNibIntoTableView(tableView)

        ChatReceivedCell.registerNibIntoTableView(tableView)
        ChatReceivedWithPictureCell.registerNibIntoTableView(tableView)

        ChatSendReceivedCell.registerNibIntoTableView(tableView)
        ChatSendReceivedWithPictureCell.registerNibIntoTableView(tableView)
    }

    private func initRoom() {
        self.roomId = PrivateChatModel.getRoom(user.userId ?? -1)

        if self.roomId > -1 {
            self.initMessages()
        }
    }

    //
    // MARK: Notif
    //
    func onNewPrivateMessageNotification(_ val: (isNewItem: Bool, message: PrivateMessageModel)) {
        if let messageRoomId = val.message.roomId, (messageRoomId == self.roomId && messageRoomId == -1 && val.message.status == .sending) {
            return
        }

        let newCell = ChatCellViewModel(privateMessageModel: val.message)

        if (val.isNewItem && val.message.fromUserId == self.sessionManager.user.userId) {
            newCell.scrollToBottom = true
        } else if val.isNewItem && val.message.fromUserId != self.sessionManager.user.userId && tableView?.isAtBottom ?? false {
            newCell.scrollToBottom = true
        }

        let newSection = ChatSectionViewBinder(date: val.message.insertedAt ?? Date())
        let newList = BaseTableListBinder(header: newSection, items: [newCell])

        if val.isNewItem {
            self.binder?.add(item: newList)
        } else {
            self.binder?.reload(item: newList, withAnimation: true)
        }
    }

    func onDeletePrivateMessageNotification(_ message: PrivateMessageModel) {
        if let messageRoomId = message.roomId, (messageRoomId == self.roomId && messageRoomId == -1 && message.status == .sending) {
            return
        }

        let deleteCell = ChatCellViewModel(privateMessageModel: message)
        let section = ChatSectionViewBinder(date: message.insertedAt ?? Date())
        self.binder?.removeCellOrSection(item: deleteCell, atSection: section)
    }

    func onHiddenPrivateMessageNotification(_ message: PrivateMessageModel) {
        if let messageRoomId = message.roomId, (messageRoomId == self.roomId && messageRoomId == -1 && message.status == .sending) {
            return
        }

        let hiddenCell = ChatCellViewModel(privateMessageModel: message)
        let section = ChatSectionViewBinder(date: message.insertedAt ?? Date())
        self.binder?.replace(item: hiddenCell, atSection: section)
    }

    //
    // MARK: Handler
    //

    func onGetBackupMessagesNotification() {
        self.initMessages();
    }

    //
    // MARK: Handler Image
    //

    func onNewImageUploadingNotification(val: (bytesWritten: Int64?, totalBytesWritten: Int64?, totalBytesExpected: Int64?, message: PrivateMessageModel)) {
        if let messageRoomId = val.message.roomId, (messageRoomId == self.roomId && messageRoomId == -1 && val.message.status == .sending) {
            return
        }
        val.message.uploading = CGFloat(val.totalBytesWritten!) / CGFloat(val.totalBytesExpected!)
        val.message.isFailed = false

        let newCell = ChatCellViewModel(privateMessageModel: val.message)
        let newSection = ChatSectionViewBinder(date: val.message.insertedAt ?? Date())
        let newList = BaseTableListBinder(header: newSection, items: [newCell])

        self.binder?.reload(item: newList, withAnimation: true)
    }

    //
    // MARK: List
    //

    private func initMessages() {
        let list = PrivateMessageModel.getMessagesByRoom(self.roomId)
        let userId = self.sessionManager.user.userId

        var listToSave: [PrivateMessageModel] = []
        for message in list {
            if message.fromUserId == userId && message.status == StatusType.read {
                message.status = StatusType.confirmed
                listToSave.append(message)
            } else if message.fromUserId != userId && (message.status == StatusType.received || message.status == StatusType.sent) {
                message.status = StatusType.read
                listToSave.append(message)
            }

        }

        self.addItems(list)
        self.socketManager.sendPrivateMessages(listToSave)
    }

    private func addItems(_ privateMessages: [PrivateMessageModel]) {
        Observable<[PrivateMessageModel]>
                .just(privateMessages)
                .map({ (privateMessages) -> [PrivateMessageModel] in
                    return privateMessages.sorted(by: { (privateMessage1, privateMessage2) -> Bool in
                        return privateMessage1.insertedAt!.isLessThanDate(privateMessage2.insertedAt!)
                    })
                })
                .reduce([BaseTableListBinder]()) { (_, privateMessages) -> [BaseTableListBinder] in
                    var newList = [BaseTableListBinder]()
                    var newItem: ChatCellViewModel
                    var newSection: ChatSectionViewBinder

                    for privateMessage in privateMessages {
                        newItem = ChatCellViewModel(privateMessageModel: privateMessage)
                        newSection = ChatSectionViewBinder(date: privateMessage.insertedAt ?? Date())

                        if let index = newList.index(where: { (baseTableListBinder) -> Bool in
                            baseTableListBinder.header == newSection
                        }) {
                            newList[index].items = newList[index].items + [newItem]
                        } else {
                            newList.append(BaseTableListBinder(header: newSection, items: [newItem]))
                        }
                    }

                    return newList
                }
                .asSingle()
                .subscribe(onSuccess: { (baseTableListBinders) in
                    self.binder?.updateList(newList: baseTableListBinders, withAnimation: false)
                }, onError: { (error) in
                    print(error)
                })
                .disposed(by: disposeBag)
    }

    func clickOnRow() {
        guard let binder = self.binder else {
            return
        }

        binder.didSelectRow
                .drive(onNext: { [weak self] (baseTableHeaderViewModel, baseTableCellViewModel) in
                    guard let `self` = self, let chatCellViewModel = baseTableCellViewModel as? ChatCellViewModel else {
                        return
                    }

                    if self.isLongPressModeSubject.value {
                        chatCellViewModel.isSelected = !chatCellViewModel.isSelected
                    }

                    var list = self.cellSelectedSubject.value
                    
                    if (chatCellViewModel.isSelected) {
                        list.append(chatCellViewModel)
                    } else {
                        list.removeObject(chatCellViewModel)
                    }
                    
                    self.cellSelectedSubject.value = list

                    if list.count == 0 {
                        self.isLongPressModeSubject.value = false
                    }
                })
                .disposed(by: self.disposeBag)
    }

    func clickAsLongPress() {
        self.isLongPressModeSubject.value = true
    }

    func cleanSelectedRow() {
        Observable
                .just(1)
                .asDriver(onErrorJustReturn: 1)
                .drive(onNext: { [weak self] (item) in
                    guard let `self` = self else {
                        return
                    }
                    
                    var list = self.cellSelectedSubject.value

                    for chatCellViewModel in list {
                        chatCellViewModel.isSelected = false
                    }

                    list.removeAll()
                    self.cellSelectedSubject.value = list
                    self.isLongPressModeSubject.value = false
                }).disposed(by: self.disposeBag)
    }
}
