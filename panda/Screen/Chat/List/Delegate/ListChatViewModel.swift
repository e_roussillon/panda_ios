//
//  ChatViewModel.swift
//  panda
//
//  Created by Edouard Roussillion on 11/3/17.
//  Copyright © 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources

class ListChatViewModel: BaseViewModel {

    var binder: BaseTableBinder?

    private weak var tableView: UITableView?
    private weak var baseViewControllerSegue: BaseViewControllerSegue?

    func initBinder(tableView: UITableView, baseViewControllerSegue: BaseViewControllerSegue) {
        if self.binder == nil {
            self.tableView = tableView
            self.baseViewControllerSegue = baseViewControllerSegue

            self.binder = BaseTableBinder(tableView: tableView)

            self.registerNibIntoTable()
            self.clickCellObserver()
        }

        self.updateList()
    }

    private func registerNibIntoTable() {
        guard let tableView = self.tableView else {
            return
        }

        ListChatsCell.registerNibIntoTableView(tableView)
    }


    func updateList() {
        Observable<[PrivateChatModel]>
                .just(PrivateChatModel.getRooms())
                .reduce([BaseTableListBinder]()) { (_, privateChatModels) -> [BaseTableListBinder] in
                    var newList = [BaseTableListBinder]()
                    var items: [ListChatCellViewModel] = []

                    for privateChatModel in privateChatModels {
                        items = items + [ListChatCellViewModel(privateChatModel: privateChatModel, baseViewControllerSegue: self.baseViewControllerSegue)]
                    }

                    newList.append(BaseTableListBinder(header: BaseTableHeaderViewModel(), items: items))
                    return newList
                }
                .asSingle()
                .subscribe(onSuccess: { (baseTableListBinders) in
                    self.binder?.updateList(newList: baseTableListBinders, withAnimation: false)
                }, onError: { (error) in
                    print(error)
                })
                .disposed(by: disposeBag)
    }

    private func clickCellObserver() {
        self.binder?.didSelectRow
                .map { (header, row) -> BaseTableCellViewModel in
                    row
                }
                .drive(onNext: { [weak self] row in
                    guard let `self` = self else {
                        return
                    }

                    if let item = row as? ListChatCellViewModel {
                        self.baseViewControllerSegue?.launchSegue(Constants.SEGUE.OPEN_CONVERSATION, values: [item.privateChatModel])
                    }
                })
                .disposed(by: disposeBag)
    }
}
