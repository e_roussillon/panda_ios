//
//  ChatPrivateConnectionDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 9/26/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class ChatPrivateConnectionDelegate: NSObject {
    
    fileprivate let bag = DisposableBag()
    
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let socketManager = SocketManager.sharedInstance
    
    @IBOutlet weak var baseViewController: BaseViewController!
    
    //
    // MARK: Init
    //
    
    func initDelegate() {
        self.initNotifier()
        
        if socketManager.isSocketConnected() {
            self.setTitle()
        } else {
            self.setTitle(NSLocalizedString("generic_room_connection", comment: ""))
        }
        
    }
    
    func deinitDelegate() {
        self.removeNotifier()
    }
    
    //
    // MARK: Notif
    //
    
    fileprivate func initNotifier() {
        self.bag.add(self.socketNotifierManager.onJoinedChannelPrivate.add(self, handler: ChatPrivateConnectionDelegate.onJoinedChannelPrivate))
        self.bag.add(self.socketNotifierManager.onUpdateTimerNotification.add(self, handler: ChatPrivateConnectionDelegate.onUpdateTimerNotification))
        self.bag.add(self.socketNotifierManager.onStopTimerNotification.add(self, handler: ChatPrivateConnectionDelegate.onStopTimerNotification))
    }
    
    fileprivate func removeNotifier() {
        self.bag.dispose()
    }
    
    //
    // MARK: Handler
    //
    
    func onJoinedChannelPrivate() {
        self.setTitle()
    }
    
    func onUpdateTimerNotification(_ time: Int) {
        
        if time >= 1800 {
            self.setTitle(NSLocalizedString("generic_impossible_connection", comment: ""))
        } else {
            if time == 0 {
                self.setTitle(NSLocalizedString("generic_room_connection", comment: ""))
            } else {
                self.setTitle(NSLocalizedString("chat_connection_retry_timer", comment: "").replacingOccurrences(of: "%1", with: time.secondsToString()))
            }
        }
    }
    
    func onStopTimerNotification() {
        self.setTitle()
    }

    //
    // MARK: Set Title
    //
    
    fileprivate func setTitle(_ title: String = "") {
        if title.isEmpty {
            if let chat = self.baseViewController as? ChatViewController {
                self.baseViewController.navigationItem.title = chat.user.pseudo ?? ""
            } else if let _ = self.baseViewController as? ListChatsViewController {
                self.baseViewController.navigationItem.title = NSLocalizedString("private_chat_screen_title", comment: "")
            } else {
                self.baseViewController.navigationItem.title = ""
            }
        } else {
            self.baseViewController.navigationItem.title = title
        }
    }
}