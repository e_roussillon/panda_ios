//
// Created by Edouard Roussillion on 11/3/17.
// Copyright (c) 2017 Edouard Roussillon. All rights reserved.
//

import Foundation
import AlamofireImage
import RxCocoa
import RxSwift

class ListChatCellViewModel: BaseTableCellViewModel {

    let privateChatModel: PrivateChatModel
    private let baseViewControllerSegue: BaseViewControllerSegue?

    init(privateChatModel: PrivateChatModel, baseViewControllerSegue: BaseViewControllerSegue?) {
        self.privateChatModel = privateChatModel
        self.baseViewControllerSegue = baseViewControllerSegue
        super.init()
        self.identity = String(self.privateChatModel.rowId ?? -1)
    }

    override func equalTo(rhs: BaseTableCellViewModel) -> Bool {
        return (rhs as? ListChatCellViewModel).map { b in
            return self.identity == b.identity && self.privateChatModel == b.privateChatModel
        } ?? false
    }

    override public var identity: String {
        set { super.identity = newValue }
        get { return super.identity }
    }

    override func initCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        return ListChatsCell.initCell(tableView, indexPath: indexPath, objects: [self])
    }

    override func initCell(tableView: UITableView) -> UITableViewCell? {
        return ListChatsCell.initCell(tableView, objects: [self])
    }

    func name() -> String {
        return self.privateChatModel.nameRoom ?? ""
    }

    func timer() -> String {
        return self.privateChatModel.msgSent?.getTimeAgo ?? ""
    }

    func message() -> String {
        if (self.privateChatModel.type == DataType.none.rawValue) {
            return self.privateChatModel.msg
        } else {
            return NSLocalizedString("private_chat_image", comment: "")
        }

    }

    func timerUser() -> String {
        if let userStatus = self.privateChatModel.userStatus,
           let userLastSeen = self.privateChatModel.userLastSeen, userStatus == FriendshipType.friend {
            if let userIsOnline = self.privateChatModel.userIsOnline, userIsOnline {
                return NSLocalizedString("friend_time_online", comment: "")
            } else {
                return userLastSeen.getDateMessage()
            }
        } else {
            return ""
        }
    }

    func badgeClipsToBounds() -> Bool {
        return true
    }

    func badgeCornerRadius(widthImage: CGFloat) -> CGFloat {
        return widthImage / 2
    }

    func badgeIsHidden() -> Bool {
        if let count = self.privateChatModel.numOfMsgNotRead, count > 0 {
            return false
        } else {
            return true
        }
    }

    func badgeText() -> String {
        if let count = self.privateChatModel.numOfMsgNotRead, count > 0 {
            return "\(count)"
        } else {
            return ""
        }
    }

    func imageCornerRadius(widthImage: CGFloat) -> CGFloat {
        return widthImage / 2
    }

    func imageMasksToBounds() -> Bool {
        return true
    }

    func setupImageUser(buttonUser: UIButton) {
        if let imageUrl = privateChatModel.urlThumb {
            buttonUser.af_setImage(for: UIControlState.normal, url: imageUrl) //, placeHolderImage: plaseHolder
        } else {
            buttonUser.setImage(UIImage(named: "logo_panda"), for: UIControlState())
        }
    }

    func setupClick(buttonUser: UIButton) {
        buttonUser.rx.tap
                .debounce(1.0, scheduler: MainScheduler.instance)
                .subscribe(){ [weak self] event in
                    guard let `self` = self else { return }

                    self.baseViewControllerSegue?.launchSegue(Constants.SEGUE.SHOW_PROFILE, values: [self.privateChatModel])
                }
                .disposed(by: disposeBag)
    }
}
