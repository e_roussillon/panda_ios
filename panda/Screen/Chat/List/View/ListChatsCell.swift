//
//  ListChatsCell.swift
//  panda
//
//  Created by Edouard Roussillon on 2/5/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import QuartzCore

class ListChatsCell: UITableViewCell {

    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var buttonUser: UIButton!
    @IBOutlet weak var labelTimerUser: UILabel!
    
    @IBOutlet weak var constraintWidthImage: NSLayoutConstraint!

    override func configureCell(_ objects: [Any]?) {
        let placeHolder = UIImage(named: "logo_panda")
        
        if let chat = objects?.first as? ListChatCellViewModel {
            self.labelName.text = chat.name()
            self.labelTimer.text = chat.timer()
            self.labelMessage.text = chat.message()
            self.labelTimerUser.text = chat.timerUser()

            self.badgeLabel.clipsToBounds = chat.badgeClipsToBounds()
            self.badgeLabel.layer.cornerRadius = chat.badgeCornerRadius(widthImage: self.badgeLabel.frame.height)
            self.badgeLabel.isHidden = chat.badgeIsHidden()
            self.badgeLabel.text = chat.badgeText()

            self.buttonUser.layer.cornerRadius = chat.imageCornerRadius(widthImage: self.constraintWidthImage.constant)
            self.buttonUser.layer.masksToBounds = chat.imageMasksToBounds()

            self.buttonUser.isHidden = false
            chat.setupImageUser(buttonUser: self.buttonUser)
            chat.setupClick(buttonUser: self.buttonUser)
        } else {

            self.labelName.text = ""
            self.labelTimer.text = ""
            self.labelMessage.text = ""
            self.badgeLabel.isHidden = true
            self.buttonUser.isHidden = true
        }
    }
}
