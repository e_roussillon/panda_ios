//
//  ListChatsViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 11/20/15.
//  Copyright © 2015 Edouard Roussillon. All rights reserved.
//

import UIKit

class ListChatsViewController: BaseViewController {
    fileprivate let socketNotifierManager = SocketNotifierManager.sharedInstance
    
    @IBOutlet weak var chatPrivateConnectionDelegate: ChatPrivateConnectionDelegate!
    @IBOutlet weak var tableView: UITableView!

    private let listChatViewModel = ListChatViewModel()
    
    override var screenName: ScreenName? {
        if self.sessionManager.user.isLogin {
            return ScreenName.PrivateListChat
        } else {
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initLocalizedString()
        self.initDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.chatPrivateConnectionDelegate.initDelegate()
        self.initListener()
        self.listChatViewModel.updateList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.chatPrivateConnectionDelegate.deinitDelegate()
    }
    
    //
    //MARK: Init
    //
    
    fileprivate func initListener() {
        self.bag.add(self.socketNotifierManager.onNewPrivateMessageNotification.add(self, handler: ListChatsViewController.onNewPrivateMessageNotification))
        
        self.bag.add(self.socketNotifierManager.onUpdateFriendship.add(self, handler: ListChatsViewController.onUpdateFriendship))
        
        self.bag.add(self.socketNotifierManager.onNewConnection.add(self, handler: ListChatsViewController.onNewConnection))
        self.bag.add(self.socketNotifierManager.onNewDisconnection.add(self, handler: ListChatsViewController.onNewDisconnection))
        
        self.bag.add(self.socketNotifierManager.onGetBackupMessagesNotification.add(self, handler: ListChatsViewController.onGetBackupMessagesNotification))
    }
    
    fileprivate func initLocalizedString() {
        self.navigationItem.title = NSLocalizedString("private_chat_screen_title", comment: "")
    }

    fileprivate func initDelegate() {
        listChatViewModel.initBinder(tableView: self.tableView, baseViewControllerSegue: self)

    }

    //
    //MARK: BaseViewControllerSegue
    //
    
    override func launchSegue(_ name: String, values:[Any]?) {
        if name == Constants.SEGUE.SHOW_PROFILE {
            if let chat = values?.first as? PrivateChatModel, let userId = chat.userId, let pseudo = chat.nameRoom {
               UIViewController.launchProfileUser(userId, userName: pseudo)
            }
        } else if name == Constants.SEGUE.OPEN_CONVERSATION {
            if let chat = values?.first as? PrivateChatModel {
                let user = UserModel.getUser(chat.userId)
                UIViewController.launchChatting(user, friendshipStatus: chat.userStatus, friendshipLastAction: chat.userStatusLastAction)
            }
        }
    }
    
    //
    //MARK: Click
    //
    
    @IBAction func clickNewChat(_ sender: AnyObject) {
        UIViewController.launchSearch(true)
    }
    
    @IBAction func clickSetting(_ sender: AnyObject) {
        UIViewController.launchProfileUser(sessionManager.user.userId, userName: "")
    }
    
    //
    //MARK: ListChatsViewController
    //
    
    func onNewPrivateMessageNotification(_ val: (isNewItem: Bool, message: PrivateMessageModel)) {
        self.listChatViewModel.updateList()
    }
    
    func onUpdateFriendship(_ user: UserModel) {
        self.listChatViewModel.updateList()
    }
    
    func onNewConnection(_ users: [UserModel]) {
        self.listChatViewModel.updateList()
    }
    
    func onNewDisconnection(_ user: UserModel) {
        self.listChatViewModel.updateList()
    }
    
    func onGetBackupMessagesNotification() {
        self.listChatViewModel.updateList()
    }
    
}
