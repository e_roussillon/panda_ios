//
//  SearchViewController.swift
//  panda
//
//  Created by Edouard Roussillon on 7/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

class SearchViewController: BaseViewController, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchDelegate: SearchDelegate!
    
    var removedSearchAfterSelecteChat: Bool! = false
    
    fileprivate var txtSearchField: UITextField!
    fileprivate let socketNotifierManager: SocketNotifierManager = SocketNotifierManager.sharedInstance
    fileprivate let searchManaget: SearchManager = SearchManager.sharedInstance
    fileprivate var searchController = UISearchController(searchResultsController: nil)
    
    override var screenName: ScreenName? {
        return ScreenName.Search
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initTableView()
        self.initSearchBar()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.searchDelegate.reloadList()
        self.addObservers()
        self.bag.add(searchManaget.onSearchSucceed.add(searchDelegate, handler: SearchDelegate.onSearchSucceed))
        self.bag.add(searchManaget.onSearchFailed.add(searchDelegate, handler: SearchDelegate.onSearchFailed))
        self.bag.add(socketNotifierManager.onUpdateFriendship.add(searchDelegate, handler: SearchDelegate.onUpdateFriendship))
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.removeObservers()
    }
    
    //
    // MARK: Init
    //
    
    fileprivate func initSearchBar() {
        self.extendedLayoutIncludesOpaqueBars = true
        self.searchController.searchBar.showsCancelButton = true
        self.searchController.searchBar.delegate = self
        self.searchController.searchResultsUpdater = self.searchDelegate
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = true
        self.navigationItem.titleView = self.searchController.searchBar
        self.navigationItem.hidesBackButton = true
        
        self.txtSearchField = self.searchController.searchBar.value(forKey: "_searchField") as! UITextField
        self.txtSearchField.borderStyle = UITextBorderStyle.roundedRect
        self.txtSearchField.layer.cornerRadius = 10;
        self.txtSearchField.layer.borderWidth = 1.0;
        self.txtSearchField.layer.borderColor = Constants.COLOR.kNavigationButton.cgColor
        self.txtSearchField.inputAccessoryView = self.initToolBar(true, withPres: false, withNext: false)
    }
    
    fileprivate func initTableView() {
        self.searchDelegate.baseViewControllerSegue = self
        SearchCell.registerNibIntoTableView(self.tableView)
    }
    
    //
    // MARK: BaseViewControllerSegue
    //
    
    override func launchSegue(_ name: String, values:[Any]?) {
        if name == Constants.SEGUE.SHOW_PROFILE {
            if let user = values?.first as? UserModel {
                UIViewController.launchProfileUser(user)
            }
        } else if name == Constants.SEGUE.OPEN_CONVERSATION {
            if let user = values?.first as? UserModel {
                let status = user.friendshipStatus ?? FriendshipType.notFriend
                let lastAction = user.friendshipLastAction ?? -1
                    
                UIViewController.launchChatting(user, friendshipStatus: status, friendshipLastAction: lastAction, removedSearchAfterSelecteChat: self.removedSearchAfterSelecteChat)
            }
        }
    }
    
    //
    //MARK: Keyboard
    //
    
    override func keyboardWillShow(_ notif: Notification) {
        super.keyboardWillShow(notif)
        
        let keyboardFrame = (notif.userInfo![UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, keyboardFrame!.size.height, 0)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tableView.contentInset = contentInsets;
            self.tableView.scrollIndicatorInsets = contentInsets;
            
        }) 
    }
    
    override func keyboardWillHide(_ notif: Notification) {
        super.keyboardWillHide(notif)
        
        let contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, 0, 0)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.tableView.contentInset = contentInsets;
            self.tableView.scrollIndicatorInsets = contentInsets;
            
        }) 
    }
    
    //
    // MARK:
    //
    
    override internal func resignKeyboard(_ sender: UIBarButtonItem) {
        self.txtSearchField.resignFirstResponder()
    }
    
    //
    // MARK: UISearchBarDelegate
    //
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    //
    // MARK: Handler
    //
    
    
    
}
