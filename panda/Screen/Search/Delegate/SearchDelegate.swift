//
//  SearchDelegate.swift
//  panda
//
//  Created by Edouard Roussillon on 7/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class SearchDelegate: NSObject, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    
    var baseViewControllerSegue: BaseViewControllerSegue!
    fileprivate var isSearching: Bool = false
    
    fileprivate var listFriend:[UserModel] = UserModel.getUsersByFriendshipTypeFriend()
    fileprivate var listPending:[UserModel] = UserModel.getUsersByFriendshipTypePending()
    
    fileprivate let baseInfinitRefreshView = BaseInfinitRefreshView()
    fileprivate let searchManaget: SearchManager = SearchManager.sharedInstance
    fileprivate var listFriendSearch:[UserModel] = []
    fileprivate var listPendingSearch:[UserModel] = []
    fileprivate var listOtherSearch:[UserModel] = []
    fileprivate var page = 0
    fileprivate var searchText = ""
    fileprivate var paginator: Paginated<UserModel>!
    fileprivate var countSection: Int = 1
    
    //
    // MARK: UITableViewDataSource
    //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.isSearching {
            let selectionFriendSearch = listFriendSearch.count > 0 ? 1 : 0
            let selectionPendingSearch = listPendingSearch.count > 0 ? 1 : 0
            let selectionOtherSearch = listOtherSearch.count > 0 ? 1 : 0
            
            countSection = selectionFriendSearch + selectionPendingSearch + selectionOtherSearch
        } else {
            let selectionFriend = listFriend.count > 0 ? 1 : 0
            let selectionPending = listPending.count > 0 ? 1 : 0
            
            countSection = selectionFriend + selectionPending
        }
        
        return countSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            let selectionFriendSearch = listFriendSearch.count > 0 ? true : false
            let selectionPendingSearch = listPendingSearch.count > 0 ? true : false
            let selectionOtherSearch = listOtherSearch.count > 0 ? true : false
            
            switch countSection {
            case 0:
                return 0
            case 1:
                if selectionPendingSearch {
                    return listPendingSearch.count
                } else if selectionFriendSearch {
                    return listFriendSearch.count
                } else {
                    return listOtherSearch.count
                }
            case 2:
                if section == 0 {
                    if selectionPendingSearch {
                        return listPendingSearch.count
                    } else {
                        return listFriendSearch.count
                    }
                } else {
                    if selectionOtherSearch {
                        return listOtherSearch.count
                    } else if selectionPendingSearch {
                        return listPendingSearch.count
                    } else {
                        return listFriendSearch.count
                    }
                }
            default:
                if section == 0 {
                    if selectionPendingSearch {
                        return listPendingSearch.count
                    } else {
                        return listFriendSearch.count
                    }
                } else if section == 1 {
                    if selectionFriendSearch {
                        return listFriendSearch.count
                    } else {
                        return listPendingSearch.count
                    }
                } else {
                    return listOtherSearch.count
                }
            }
        } else {
            let selectionPending = listPending.count > 0 ? true : false
            
            if section == 0 {
                if selectionPending {
                    return listPending.count
                } else {
                    return listFriend.count
                }
            } else {
                return listFriend.count
            }
        }
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        return SearchCell.initCell(tableView, indexPath: indexPath, objects: [self.getUserModel(indexPath), self.baseViewControllerSegue])
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return BaseLabelView.height
    }
    
    //
    //MARK: UITableViewDelegate
    //
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BaseLabelView()
        
        if self.isSearching {
            let selectionFriendSearch = listFriendSearch.count > 0 ? true : false
            let selectionPendingSearch = listPendingSearch.count > 0 ? true : false
            let selectionOtherSearch = listOtherSearch.count > 0 ? true : false
            
            switch countSection {
            case 1:
                if selectionPendingSearch {
                    view.text = NSLocalizedString("search_pending_title", comment: "")
                } else if selectionFriendSearch {
                    view.text = NSLocalizedString("search_accepted_title", comment: "")
                } else {
                    view.text = NSLocalizedString("search_other_title", comment: "")
                }
            case 2:
                if section == 0 {
                    if selectionPendingSearch {
                        view.text = NSLocalizedString("search_pending_title", comment: "")
                    } else {
                        view.text = NSLocalizedString("search_accepted_title", comment: "")
                    }
                } else {
                    if selectionOtherSearch {
                        view.text = NSLocalizedString("search_other_title", comment: "")
                    } else if selectionPendingSearch {
                        view.text = NSLocalizedString("search_pending_title", comment: "")
                    } else {
                        view.text = NSLocalizedString("search_accepted_title", comment: "")
                    }
                }
            default:
                if section == 0 {
                    if selectionPendingSearch {
                        view.text = NSLocalizedString("search_pending_title", comment: "")
                    } else {
                        view.text = NSLocalizedString("search_accepted_title", comment: "")
                    }
                } else if section == 1 {
                    if selectionFriendSearch {
                        view.text = NSLocalizedString("search_accepted_title", comment: "")
                    } else {
                        view.text = NSLocalizedString("search_pending_title", comment: "")
                    }
                } else {
                    view.text = NSLocalizedString("search_other_title", comment: "")
                }
            }
        } else {
            let selectionPending = listPending.count > 0 ? true : false
            
            if section == 0 {
                if selectionPending {
                    view.text = NSLocalizedString("search_pending_title", comment: "")
                } else {
                    view.text = NSLocalizedString("search_accepted_title", comment: "")
                }
            } else {
                view.text = NSLocalizedString("search_accepted_title", comment: "")
            }
        }

        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.baseViewControllerSegue.launchSegue(Constants.SEGUE.SHOW_PROFILE, values: [self.getUserModel(indexPath)])
    }
    
    //
    // MARK: UISearchResultsUpdating
    //
    
    func updateSearchResults(for searchController: UISearchController) {
        print("\(searchController.searchBar.text)")
        
        let search = searchController.searchBar.text ?? ""
            
        if search.length() == 0 {
            self.isSearching = false
        } else if search.length() >= 3 {
            self.isSearching = true
            self.page = 1
            self.searchText = search
            self.searchAPI()
            self.tableView.fty.infiniteScroll.add(animator: baseInfinitRefreshView, action: {
                self.page = self.page + 1
                self.searchAPI()
            })
        } else {
            self.isSearching = true
        }
        
        if self.isSearching {
            listFriendSearch = self.searchIntoList(self.listFriend, text: search)
            listPendingSearch = self.searchIntoList(self.listPending, text: search)
        } else {
            listFriendSearch = []
            listPendingSearch = []
            listOtherSearch = []
            self.tableView.fty.infiniteScroll.end()
            self.tableView.fty.infiniteScroll.remove()
        }
        
        self.tableView.reloadData()
    }
    
    fileprivate func searchIntoList(_ list: [UserModel], text: String) -> [UserModel] {
        return list.filter { (user) -> Bool in
            if let pseudo = user.pseudo {
                return pseudo.lowercased().contains(text.lowercased())
            } else {
                return false
            }
        }
    }
    
    //
    // MARK: Generic
    //
    
    func reloadList() {
        self.listFriend = UserModel.getUsersByFriendshipTypeFriend()
        self.listPending = UserModel.getUsersByFriendshipTypePending()
        
        if self.searchText.length() > 0 {
            self.listFriendSearch = self.searchIntoList(self.listFriend, text: self.searchText)
            self.listPendingSearch = self.searchIntoList(self.listPending, text: self.searchText)
            
            self.listOtherSearch = self.listOtherSearch.filter({ (user) -> Bool in
                if self.listPending.contains(where: { (userPending) -> Bool  in
                    return userPending.pseudo! == user.pseudo!
                }) || self.listFriendSearch.contains(where: { (userFriend) -> Bool  in
                    return userFriend.pseudo! == user.pseudo!
                }) {
                    return false
                } else {
                    return true
                }
            })
            self.listOtherSearch.sort(by: { (user1, user2) -> Bool in
                return user1 < user2
            })
        }
        
        self.tableView.reloadData()
    }
    
    fileprivate func getUserModel(_ indexPath: IndexPath) -> UserModel {
        var user: UserModel!
        
        if self.isSearching {
            let selectionFriendSearch = listFriendSearch.count > 0 ? true : false
            let selectionPendingSearch = listPendingSearch.count > 0 ? true : false
            let selectionOtherSearch = listOtherSearch.count > 0 ? true : false
            
            switch countSection {
            case 1:
                if selectionPendingSearch {
                    user = self.listPendingSearch[indexPath.row]
                } else if selectionFriendSearch {
                    user = self.listFriendSearch[indexPath.row]
                } else {
                    user = self.listOtherSearch[indexPath.row]
                }
            case 2:
                if indexPath.section == 0 {
                    if selectionPendingSearch {
                        user = self.listPendingSearch[indexPath.row]
                    } else {
                        user = self.listFriendSearch[indexPath.row]
                    }
                } else {
                    if selectionOtherSearch {
                        user = self.listOtherSearch[indexPath.row]
                    } else if selectionPendingSearch {
                        user = self.listPendingSearch[indexPath.row]
                    } else {
                        user = self.listFriendSearch[indexPath.row]
                    }
                }
            default:
                if indexPath.section == 0 {
                    if selectionPendingSearch {
                        user = self.listPendingSearch[indexPath.row]
                    } else {
                        user = self.listFriendSearch[indexPath.row]
                    }
                } else if indexPath.section == 1 {
                    if selectionFriendSearch {
                        user = self.listFriendSearch[indexPath.row]
                    } else {
                        user = self.listPendingSearch[indexPath.row]
                    }
                } else {
                    user = self.listOtherSearch[indexPath.row]
                }
            }
        } else {
            let selectionPending = listPending.count > 0 ? true : false
            
            if indexPath.section == 0 {
                if selectionPending {
                    user = self.listPending[indexPath.row]
                } else {
                    user = self.listFriend[indexPath.row]
                }
            } else {
                user = self.listFriend[indexPath.row]
            }
        }
        
        return user
    }
    
    
    //
    // MARK: SearchManager
    //
    
    fileprivate func searchAPI() {
        searchManaget.searchSpeudo(self.searchText, page: self.page)
    }
    
    func onSearchSucceed(_ users: Paginated<UserModel>) {
        self.paginator = users
        
        self.tableView.fty.infiniteScroll.end()
        
        if self.paginator.totalPages <= self.page {
            self.tableView.fty.infiniteScroll.remove()
        }
        
        if self.paginator.pageNumber == 1 {
            self.listOtherSearch = users.data ?? []
        } else {
            self.listOtherSearch = self.listOtherSearch + (users.data ?? [])
        }
        
        self.tableView.reloadData()
    }
    
    func onSearchFailed(_ error: ErrorResponse?) {
        
    }
    
    func onUpdateFriendship(_ user: UserModel) {
        if self.isSearching {
            let status = (user.friendshipStatus ?? FriendshipType.notFriend)
            switch status {
            case .notFriend, .cancelRequest, .refusedBeforeBeFriend, .refusedAfterBeFriend:
                if let index = self.listOtherSearch.index(of: user) {
                    self.listOtherSearch[index] = user
                } else {
                    self.listOtherSearch.append(user)
                }
            default:
               self.listOtherSearch.removeObject(user)
            }
            
        }
        self.reloadList()
    }
}
