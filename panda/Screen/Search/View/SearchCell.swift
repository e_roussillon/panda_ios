//
//  SearchCell.swift
//  panda
//
//  Created by Edouard Roussillon on 7/22/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchCell: UITableViewCell {
    
    fileprivate let sessionManager = SessionManager.sharedInstance
    static let chatHeight: CGFloat = 50.0
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonUser: UIButton!
    @IBOutlet weak var buttonMessage: UIButton!
    
    var baseViewControllerSegue: BaseViewControllerSegue!
    var userModel: UserModel?
    
    static func heightWithObjects(_ objects: [Any]?) -> CGFloat {
        return chatHeight
    }
    
    override func configureCell(_ objects: [Any]?) {
        let plaseHolder = UIImage(named: "logo_panda")
        
        self.buttonUser.layer.cornerRadius = 0
        self.buttonUser.layer.masksToBounds = false
        
        if let user = objects?.first as? UserModel  {
            self.userModel = user
            self.labelName.text = user.pseudo ?? ""
            self.buttonUser.setImage(plaseHolder, for: UIControlState())
            if let url = user.urlThumb {
                self.buttonUser.af_setImage(for: UIControlState.normal, url: url) //, plaseHolder: plaseHolder
                self.buttonUser.layer.cornerRadius = self.buttonUser.frame.width / 2
                self.buttonUser.layer.masksToBounds = true
            }
            
        } else {
            self.labelName.text = ""
            self.buttonMessage.isHidden = true
            self.buttonUser.setImage(plaseHolder, for: UIControlState())
        }
        
        if let del = objects?[1] as? BaseViewControllerSegue {
            self.baseViewControllerSegue = del
        }
        
    }
    
    //
    //Click
    //
    
    @IBAction func clickMessage(_ sender: AnyObject) {
        if let user = userModel {
            self.baseViewControllerSegue.launchSegue(Constants.SEGUE.OPEN_CONVERSATION, values: [user])
        }
    }
        
}
