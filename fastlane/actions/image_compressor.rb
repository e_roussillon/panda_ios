module Fastlane
  module Actions
    module SharedValues
      IMAGE_COMPRESSOR_CUSTOM_VALUE = :IMAGE_COMPRESSOR_CUSTOM_VALUE
    end

    # To share this integration with the other fastlane users:
    # - Fork https://github.com/fastlane/fastlane/tree/master/fastlane
    # - Clone the forked repository
    # - Move this integration into lib/fastlane/actions
    # - Commit, push and submit the pull request

    class ImageCompressorAction < Action

      @images = nil
      @images_not_found = []

      @new_images = nil
      @old_images = nil

      def self.run(params)
        # fastlane will take care of reading in the parameter and fetching the environment variable:
        base = File.dirname(__FILE__).gsub('fastlane/actions', "#{params[:path_images_asset]}")
        UI.message "Parameter Path: #{base}"

        @images = Dir.glob("#{base}/**/*.png")

        @images.each do |image|
          if !image.include?(" ")
            sh "pngquant -f #{image}"
            old_size = (File.size(image).to_f / 2**20)
            new_size = (File.size(image.gsub('.png', '-fs8.png')).to_f / 2**20)
            UI.message "----> Old size  : #{old_size} - New Size #{new_size}"
          else
            @images_not_found.push(image)
          end
        end

        @new_images = Dir.glob("#{params[:path_images_asset]}/**/*-fs8.png")
        @old_images = @images - @images_not_found

        UI.message ""
        UI.message "REMOVE OLD IMAGES"
        UI.message ""
        @old_images.each do |image|
          sh "rm #{image.gsub(' ', '\\ ')}"
        end

        UI.message ""
        UI.message "RENAME NEW IMAGES"
        UI.message ""
        @new_images.each do |image|
          image = image.gsub(' ', '\\ ')
          new_image = image.gsub('-fs8.', '.')
          UI.message "#{image.split('\\').last} -> #{new_image.split('\\').last}"
          File.rename(image, new_image)
        end

        if @images_not_found.size() > 0
          UI.message "Can not read this images: "
          @images_not_found.each do |image|
            UI.message "    #{image}"
          end
        end

        # sh "shellcommand ./path"

        # Actions.lane_context[SharedValues::IMAGE_COMPRESSOR_CUSTOM_VALUE] = "my_val"
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "A short description with <= 80 characters of what this action does"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
        # Define all options your action supports.

        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :path_images_asset,
                                       env_name: "PATH_IMAGES_ASSET",
                                       description: "Path of the asset of the project",
                                       is_string: true, # true: verifies the input is a string, false: every kind of value
                                       default_value: false) # the default value if the user didn't provide one
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['PATH_IMAGE_ASSET', 'Path of the asset of the project']
        ]
      end

      def self.return_value
        # If you method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["Your GitHub/Twitter Name"]
      end

      def self.is_supported?(platform)
        # you can do things like
        #
        #  true
        #
        #  platform == :ios
        #
        #  [:ios, :mac].include?(platform)
        #

        platform == :ios
      end
    end
  end
end
