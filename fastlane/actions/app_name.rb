module Fastlane
  module Actions
    module SharedValues
      APP_NAME_CUSTOM_VALUE = :APP_NAME_CUSTOM_VALUE
    end

    # To share this integration with the other fastlane users:
    # - Fork https://github.com/fastlane/fastlane/tree/master/fastlane
    # - Clone the forked repository
    # - Move this integration into lib/fastlane/actions
    # - Commit, push and submit the pull request

    class AppNameAction < Action

      @apps = {
        'fr.com.panda-dev': {name: 'panda-dev', scheme: 'panda-dev'},
        'fr.com.panda-cal': {name: 'panda-cal', scheme: 'panda-cal'},
        'fr.com.panda-beta': {name: 'panda-beta', scheme: 'panda-beta'},
        'fr.com.panda': {name: 'panda', scheme: 'panda'}
      }

      def self.run(params)
        # fastlane will take care of reading in the parameter and fetching the environment variable:
        bundle = params[:bundle]

        if params[:all_bundles] == true
          @apps.keys
        else
          @apps[bundle.to_sym]
        end

      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "A short description with <= 80 characters of what this action does"
      end

      def self.details
        # Optional:
        # this is your chance to provide a more detailed description of this action
        "You can use this action to do cool things..."
      end

      def self.available_options
        # Define all options your action supports.

        # Below a few examples
        [
          FastlaneCore::ConfigItem.new(key: :bundle,
                                       env_name: "APP_NAME_BUNDLE", # The name of the environment variable
                                       description: "Bundle for AppNameAction", # a short description of this parameter
                                       default_value: "br.com.carrefour.qa",
                                       optional: true,
                                       verify_block: proc do |value|
                                          UI.user_error!("No Bunlde for AppNameAction given, pass using `bundle: 'bundle_id'`") unless (value and not value.empty?)
                                          # UI.user_error!("Couldn't find file at path '#{value}'") unless File.exist?(value)
                                       end),
           FastlaneCore::ConfigItem.new(key: :all_bundles,
                                        env_name: "APP_NAME_ALL_BUNDLES", # The name of the environment variable
                                        description: "All bundles for AppNameAction", # a short description of this parameter
                                        default_value: false,
                                        optional: true,
                                        is_string: false)
        ]
      end

      def self.output
        # Define the shared values you are going to provide
        # Example
        [
          ['APP_NAME_CUSTOM_VALUE', 'A description of what this value contains']
        ]
      end

      def self.return_value
        # If you method provides a return value, you can describe here what it does
      end

      def self.authors
        # So no one will ever forget your contribution to fastlane :) You are awesome btw!
        ["Your GitHub/Twitter Name"]
      end

      def self.is_supported?(platform)
        # you can do things like
        #
        #  true
        #
        #  platform == :ios
        #
        #  [:ios, :mac].include?(platform)
        #

        platform == :ios
      end
    end
  end
end
