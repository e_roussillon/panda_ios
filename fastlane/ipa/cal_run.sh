PATH_PROJECT=${PWD}/..
SCHEME=carrefour-cal
WORKSPACE=${PATH_PROJECT}/carrefour.xcworkspace
DESTINATION="platform=iOS Simulator,name=iPhone 6s"
CONFIGURATION=Debug
PATH_EXPORT=${PATH_PROJECT}/fastlane/ipa/app/

xcodebuild -workspace ${WORKSPACE} \
  -scheme ${SCHEME} -destination "${DESTINATION}" \
  -configuration ${CONFIGURATION} clean build \
  CONFIGURATION_BUILD_DIR="${PATH_EXPORT}" | xcpretty
