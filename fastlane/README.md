fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

## Choose your installation method:

<table width="100%" >
<tr>
<th width="33%"><a href="http://brew.sh">Homebrew</a></td>
<th width="33%">Installer Script</td>
<th width="33%">Rubygems</td>
</tr>
<tr>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS</td>
<td width="33%" align="center">macOS or Linux with Ruby 2.0.0 or above</td>
</tr>
<tr>
<td width="33%"><code>brew cask install fastlane</code></td>
<td width="33%"><a href="https://download.fastlane.tools">Download the zip file</a>. Then double click on the <code>install</code> script (or run it in a terminal window).</td>
<td width="33%"><code>sudo gem install fastlane -NV</code></td>
</tr>
</table>

# Available Actions
## iOS
### ios compress_image
```
fastlane ios compress_image
```

### ios build_qa
```
fastlane ios build_qa
```

### ios init_cert
```
fastlane ios init_cert
```

### ios force_init_cert
```
fastlane ios force_init_cert
```

### ios add_device
```
fastlane ios add_device
```

### ios test
```
fastlane ios test
```

### ios release_qa
```
fastlane ios release_qa
```

### ios release_dev
```
fastlane ios release_dev
```

### ios release_beta
```
fastlane ios release_beta
```

### ios release_prod
```
fastlane ios release_prod
```

### ios full_qa
```
fastlane ios full_qa
```

### ios full_dev
```
fastlane ios full_dev
```

### ios full_beta
```
fastlane ios full_beta
```

### ios full_prod
```
fastlane ios full_prod
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
