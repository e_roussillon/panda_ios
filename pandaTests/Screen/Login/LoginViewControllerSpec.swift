//
//  LoginViewControllerSpec.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import UIKit
import Nimble
import Quick
import KIF

@testable import panda

class LoginViewControllerSpec: BaseTestSpec {
    
    var loginViewController:LoginViewController!
    private var isValidUser:Bool = false
    
    override func spec() {
        super.spec()
        
        describe("when user is on screen login") {
            
            it("should setup the screen") {
                self.loginViewController = StoryboardUtil.loginViewController()
                UIApplication.sharedApplication().keyWindow!.rootViewController = self.loginViewController
                let _ = self.loginViewController!.view
                UIApplication.sharedApplication().keyWindow!.makeKeyAndVisible()
            }
            
            context("try to login with use not register") {
                it("should errors showing at fields' bottom") {
                    //Assemble
                    self.cleanLoginTextFields()
                    let textEmail = self.loginViewController.textFieldEmail.labelError
                    let textPassword = self.loginViewController.textFieldPassword.labelError
                    
                    // Action
                    self.insertInvalidEmail()
                    self.tester.tapViewWithAccessibilityLabel("Next")
                    self.wait()
                    
                    self.insertInvalidPassword()
                    self.tester.tapViewWithAccessibilityLabel("Done")
                    self.wait()
                    
                    self.tester.tapViewWithAccessibilityLabel("login_button")
                    self.wait()
                    
                    //Assertion
                    expect(textEmail.text).to(equal("Field is empty or invalid"))
                    expect(textPassword.text).to(equal("Field is empty or invalid"))
                }
            }
            
        }
        
    }
    
    private func insertInvalidEmail() {
        let user: String = "123@gmail.com"
        self.tester.enterText(user, intoViewWithAccessibilityLabel: "login_email_label")
    }
    
    private func insertInvalidPassword() {
        let password: String = "123"
        self.tester.enterText(password, intoViewWithAccessibilityLabel: "login_password_label")
    }
    
    private func cleanLoginTextFields() {
        self.loginViewController.textFieldEmail.text = ""
        self.loginViewController.textFieldPassword.text = ""
    }
    
    //
    // MARK: API
    //
    
    override func response(requestInfo: RequestInfo) -> RequestResponse {
        return ResponseUtil().response(422, jsonName: "FailLoginWithoutPasswordOrAndEmail", error: nil)
    }
    
    
}