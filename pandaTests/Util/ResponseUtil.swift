//
//  ResponseUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation

@testable import panda

class ResponseUtil: NSObject {
    
    func response(status: Int, jsonName: String, error: NSError?) -> RequestResponse {
        let data = LocalFileUtil.loadfile(self.dynamicType, fileName: jsonName)
        return RequestResponse.init(status: status, data: data, error: error)
    }
    
    func response(status: Int, error: NSError?) -> RequestResponse {
        return RequestResponse.init(status: status, data: nil, error: error)
    }
    
    func response(error: NSError?) -> RequestResponse {
        return RequestResponse.init(status: nil, data: nil, error: error)
    }
    
}