//
//  LocalFileUtil.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Foundation
import ObjectMapper

class LocalFileUtil: NSObject {
    
    static func convertDataToDictionary(data: NSData) -> [String:AnyObject] {
        do {
            if let disc = try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String:AnyObject] {
                return disc
            } else {
                return [:]
            }
        } catch let error as NSError {
            print(error)
        }
        return [:]
    }
    
    
    static func loadfile(forClass: AnyClass, fileName: String) -> NSData? {
//        forClass = self.dynamicType
        let bundle = NSBundle(forClass: forClass)
        let urlpath = bundle.pathForResource(fileName, ofType: "json")
        
        return NSFileManager.defaultManager().contentsAtPath(urlpath!)
    }
    
}