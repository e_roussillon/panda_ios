//
//  BaseTestSpec.swift
//  panda
//
//  Created by Edouard Roussillon on 4/23/16.
//  Copyright © 2016 Edouard Roussillon. All rights reserved.
//

import Quick
import Nimble
import UIKit
import ObjectMapper

@testable import panda

class BaseTestSpec: QuickSpec, BaseAPIManagerMockDelegate {
    var user:UserModel!
    
    override func spec() {
        super.spec()
        
        BaseAPI.connectorManager = BaseAPIManagerMock(delegate: self)
    }
    
    //
    // MARK: Actions
    //
    
    func wait( seconds seconds:NSTimeInterval = 1.0 ) {
        tester.waitForTimeInterval(seconds)
    }
    
    func hasAlertOnScreen() -> Bool {
        var hasAlert: Bool = false
        
        for windows in UIApplication.sharedApplication().windows {
            let subviews = windows.subviews
            if( subviews.count > 0 ) {
                for view in subviews {
                    if view is UIAlertController {
                        hasAlert = true
                        break
                    }
                }
            }
        }
        
        return hasAlert
    }
    
    //
    // MARK: BaseAPIManagerMockDelegate
    //
    
    func response(requestInfo: RequestInfo) -> RequestResponse {
        assertionFailure("you forgot to override this method")
        return RequestResponse.init(status: nil, data: nil, error: nil)
    }
    
}